
package wsdl.nowocancelacion.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InPolizaId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inPolizaId"
})
@XmlRootElement(name = "ASERTANowoCancelacionBSGeneraCancelacion_Input")
public class ASERTANowoCancelacionBSGeneraCancelacionInput {

    @XmlElement(name = "InPolizaId", required = true)
    protected String inPolizaId;

    /**
     * Obtiene el valor de la propiedad inPolizaId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInPolizaId() {
        return inPolizaId;
    }

    /**
     * Define el valor de la propiedad inPolizaId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInPolizaId(String value) {
        this.inPolizaId = value;
    }

}
