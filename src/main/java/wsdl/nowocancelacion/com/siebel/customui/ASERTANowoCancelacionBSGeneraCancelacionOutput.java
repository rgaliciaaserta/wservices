
package wsdl.nowocancelacion.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OutExito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIdNotaCredito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIvaDevolucion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutMensaje" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutPrimaDevolucion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutTotalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "outExito",
    "outIdNotaCredito",
    "outIvaDevolucion",
    "outMensaje",
    "outPrimaDevolucion",
    "outTotalDevolucion"
})
@XmlRootElement(name = "ASERTANowoCancelacionBSGeneraCancelacion_Output")
public class ASERTANowoCancelacionBSGeneraCancelacionOutput {

    @XmlElement(name = "OutExito", required = true)
    protected String outExito;
    @XmlElement(name = "OutIdNotaCredito", required = true)
    protected String outIdNotaCredito;
    @XmlElement(name = "OutIvaDevolucion", required = true)
    protected String outIvaDevolucion;
    @XmlElement(name = "OutMensaje", required = true)
    protected String outMensaje;
    @XmlElement(name = "OutPrimaDevolucion", required = true)
    protected String outPrimaDevolucion;
    @XmlElement(name = "OutTotalDevolucion", required = true)
    protected String outTotalDevolucion;

    /**
     * Obtiene el valor de la propiedad outExito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutExito() {
        return outExito;
    }

    /**
     * Define el valor de la propiedad outExito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutExito(String value) {
        this.outExito = value;
    }

    /**
     * Obtiene el valor de la propiedad outIdNotaCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIdNotaCredito() {
        return outIdNotaCredito;
    }

    /**
     * Define el valor de la propiedad outIdNotaCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIdNotaCredito(String value) {
        this.outIdNotaCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad outIvaDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIvaDevolucion() {
        return outIvaDevolucion;
    }

    /**
     * Define el valor de la propiedad outIvaDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIvaDevolucion(String value) {
        this.outIvaDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad outMensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutMensaje() {
        return outMensaje;
    }

    /**
     * Define el valor de la propiedad outMensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutMensaje(String value) {
        this.outMensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad outPrimaDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutPrimaDevolucion() {
        return outPrimaDevolucion;
    }

    /**
     * Define el valor de la propiedad outPrimaDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutPrimaDevolucion(String value) {
        this.outPrimaDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad outTotalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutTotalDevolucion() {
        return outTotalDevolucion;
    }

    /**
     * Define el valor de la propiedad outTotalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutTotalDevolucion(String value) {
        this.outTotalDevolucion = value;
    }

}
