
package wsdl.serviceconsultas.org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPdf }
     * 
     */
    public GetPdf createGetPdf() {
        return new GetPdf();
    }

    /**
     * Create an instance of {@link GetPdfResponse }
     * 
     */
    public GetPdfResponse createGetPdfResponse() {
        return new GetPdfResponse();
    }

    /**
     * Create an instance of {@link GetXml }
     * 
     */
    public GetXml createGetXml() {
        return new GetXml();
    }

    /**
     * Create an instance of {@link GetXmlResponse }
     * 
     */
    public GetXmlResponse createGetXmlResponse() {
        return new GetXmlResponse();
    }

    /**
     * Create an instance of {@link GetXmlAcuse }
     * 
     */
    public GetXmlAcuse createGetXmlAcuse() {
        return new GetXmlAcuse();
    }

    /**
     * Create an instance of {@link GetXmlAcuseResponse }
     * 
     */
    public GetXmlAcuseResponse createGetXmlAcuseResponse() {
        return new GetXmlAcuseResponse();
    }

    /**
     * Create an instance of {@link GetPdfAcuse }
     * 
     */
    public GetPdfAcuse createGetPdfAcuse() {
        return new GetPdfAcuse();
    }

    /**
     * Create an instance of {@link GetPdfAcuseResponse }
     * 
     */
    public GetPdfAcuseResponse createGetPdfAcuseResponse() {
        return new GetPdfAcuseResponse();
    }

}
