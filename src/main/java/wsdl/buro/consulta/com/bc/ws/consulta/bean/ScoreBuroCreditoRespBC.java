
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ScoreBuroCreditoRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ScoreBuroCreditoRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nombreScore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodigoScore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ValorScore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodigoRazon" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *         &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScoreBuroCreditoRespBC", propOrder = {
    "nombreScore",
    "codigoScore",
    "valorScore",
    "codigoRazon",
    "codigoError"
})
public class ScoreBuroCreditoRespBC {

    protected String nombreScore;
    @XmlElement(name = "CodigoScore")
    protected String codigoScore;
    @XmlElement(name = "ValorScore")
    protected String valorScore;
    @XmlElement(name = "CodigoRazon", required = true)
    protected List<String> codigoRazon;
    @XmlElement(name = "CodigoError")
    protected String codigoError;

    /**
     * Obtiene el valor de la propiedad nombreScore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreScore() {
        return nombreScore;
    }

    /**
     * Define el valor de la propiedad nombreScore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreScore(String value) {
        this.nombreScore = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoScore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoScore() {
        return codigoScore;
    }

    /**
     * Define el valor de la propiedad codigoScore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoScore(String value) {
        this.codigoScore = value;
    }

    /**
     * Obtiene el valor de la propiedad valorScore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorScore() {
        return valorScore;
    }

    /**
     * Define el valor de la propiedad valorScore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorScore(String value) {
        this.valorScore = value;
    }

    /**
     * Gets the value of the codigoRazon property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codigoRazon property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodigoRazon().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodigoRazon() {
        if (codigoRazon == null) {
            codigoRazon = new ArrayList<String>();
        }
        return this.codigoRazon;
    }

    /**
     * Obtiene el valor de la propiedad codigoError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoError() {
        return codigoError;
    }

    /**
     * Define el valor de la propiedad codigoError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoError(String value) {
        this.codigoError = value;
    }

}
