
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EncabezadoBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EncabezadoBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroReferenciaOperador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProductoRequerido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClavePais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdentificadorBuro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUnidadMonetaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImporteContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Idioma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EncabezadoBC", propOrder = {
    "version",
    "numeroReferenciaOperador",
    "productoRequerido",
    "clavePais",
    "identificadorBuro",
    "claveUsuario",
    "password",
    "tipoConsulta",
    "tipoContrato",
    "claveUnidadMonetaria",
    "importeContrato",
    "idioma",
    "tipoSalida"
})
@XmlSeeAlso({
    EncabezadoRespBC.class
})
public class EncabezadoBC {

    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "NumeroReferenciaOperador")
    protected String numeroReferenciaOperador;
    @XmlElement(name = "ProductoRequerido")
    protected String productoRequerido;
    @XmlElement(name = "ClavePais")
    protected String clavePais;
    @XmlElement(name = "IdentificadorBuro")
    protected String identificadorBuro;
    @XmlElement(name = "ClaveUsuario")
    protected String claveUsuario;
    @XmlElement(name = "Password")
    protected String password;
    @XmlElement(name = "TipoConsulta")
    protected String tipoConsulta;
    @XmlElement(name = "TipoContrato")
    protected String tipoContrato;
    @XmlElement(name = "ClaveUnidadMonetaria")
    protected String claveUnidadMonetaria;
    @XmlElement(name = "ImporteContrato")
    protected String importeContrato;
    @XmlElement(name = "Idioma")
    protected String idioma;
    @XmlElement(name = "TipoSalida")
    protected String tipoSalida;

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroReferenciaOperador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroReferenciaOperador() {
        return numeroReferenciaOperador;
    }

    /**
     * Define el valor de la propiedad numeroReferenciaOperador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroReferenciaOperador(String value) {
        this.numeroReferenciaOperador = value;
    }

    /**
     * Obtiene el valor de la propiedad productoRequerido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductoRequerido() {
        return productoRequerido;
    }

    /**
     * Define el valor de la propiedad productoRequerido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductoRequerido(String value) {
        this.productoRequerido = value;
    }

    /**
     * Obtiene el valor de la propiedad clavePais.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClavePais() {
        return clavePais;
    }

    /**
     * Define el valor de la propiedad clavePais.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClavePais(String value) {
        this.clavePais = value;
    }

    /**
     * Obtiene el valor de la propiedad identificadorBuro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorBuro() {
        return identificadorBuro;
    }

    /**
     * Define el valor de la propiedad identificadorBuro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorBuro(String value) {
        this.identificadorBuro = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUsuario() {
        return claveUsuario;
    }

    /**
     * Define el valor de la propiedad claveUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUsuario(String value) {
        this.claveUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad password.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Define el valor de la propiedad password.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoConsulta() {
        return tipoConsulta;
    }

    /**
     * Define el valor de la propiedad tipoConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoConsulta(String value) {
        this.tipoConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Define el valor de la propiedad tipoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContrato(String value) {
        this.tipoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUnidadMonetaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUnidadMonetaria() {
        return claveUnidadMonetaria;
    }

    /**
     * Define el valor de la propiedad claveUnidadMonetaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUnidadMonetaria(String value) {
        this.claveUnidadMonetaria = value;
    }

    /**
     * Obtiene el valor de la propiedad importeContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteContrato() {
        return importeContrato;
    }

    /**
     * Define el valor de la propiedad importeContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteContrato(String value) {
        this.importeContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad idioma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * Define el valor de la propiedad idioma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdioma(String value) {
        this.idioma = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSalida() {
        return tipoSalida;
    }

    /**
     * Define el valor de la propiedad tipoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSalida(String value) {
        this.tipoSalida = value;
    }

}
