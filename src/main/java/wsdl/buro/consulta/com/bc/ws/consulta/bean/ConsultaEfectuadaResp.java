
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaEfectuadaResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaEfectuadaResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NombreOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUnidadMonetaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImporteCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoResponsabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaEfectuadaResp", propOrder = {
    "fechaConsulta",
    "claveOtorgante",
    "nombreOtorgante",
    "tipoCredito",
    "claveUnidadMonetaria",
    "importeCredito",
    "tipoResponsabilidad"
})
public class ConsultaEfectuadaResp {

    @XmlElement(name = "FechaConsulta")
    protected String fechaConsulta;
    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "NombreOtorgante")
    protected String nombreOtorgante;
    @XmlElement(name = "TipoCredito")
    protected String tipoCredito;
    @XmlElement(name = "ClaveUnidadMonetaria")
    protected String claveUnidadMonetaria;
    @XmlElement(name = "ImporteCredito")
    protected String importeCredito;
    @XmlElement(name = "TipoResponsabilidad")
    protected String tipoResponsabilidad;

    /**
     * Obtiene el valor de la propiedad fechaConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaConsulta() {
        return fechaConsulta;
    }

    /**
     * Define el valor de la propiedad fechaConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaConsulta(String value) {
        this.fechaConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOtorgante() {
        return nombreOtorgante;
    }

    /**
     * Define el valor de la propiedad nombreOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOtorgante(String value) {
        this.nombreOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCredito() {
        return tipoCredito;
    }

    /**
     * Define el valor de la propiedad tipoCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCredito(String value) {
        this.tipoCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUnidadMonetaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUnidadMonetaria() {
        return claveUnidadMonetaria;
    }

    /**
     * Define el valor de la propiedad claveUnidadMonetaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUnidadMonetaria(String value) {
        this.claveUnidadMonetaria = value;
    }

    /**
     * Obtiene el valor de la propiedad importeCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteCredito() {
        return importeCredito;
    }

    /**
     * Define el valor de la propiedad importeCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteCredito(String value) {
        this.importeCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoResponsabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoResponsabilidad() {
        return tipoResponsabilidad;
    }

    /**
     * Define el valor de la propiedad tipoResponsabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoResponsabilidad(String value) {
        this.tipoResponsabilidad = value;
    }

}
