
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DetalleConsulta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DetalleConsulta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FolioConsultaOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProductoRequerido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUnidadMonetaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImporteContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroFirma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetalleConsulta", propOrder = {
    "folioConsultaOtorgante",
    "productoRequerido",
    "tipoCuenta",
    "claveUnidadMonetaria",
    "importeContrato",
    "numeroFirma"
})
public class DetalleConsulta {

    @XmlElement(name = "FolioConsultaOtorgante")
    protected String folioConsultaOtorgante;
    @XmlElement(name = "ProductoRequerido")
    protected String productoRequerido;
    @XmlElement(name = "TipoCuenta")
    protected String tipoCuenta;
    @XmlElement(name = "ClaveUnidadMonetaria")
    protected String claveUnidadMonetaria;
    @XmlElement(name = "ImporteContrato")
    protected String importeContrato;
    @XmlElement(name = "NumeroFirma")
    protected String numeroFirma;

    /**
     * Obtiene el valor de la propiedad folioConsultaOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioConsultaOtorgante() {
        return folioConsultaOtorgante;
    }

    /**
     * Define el valor de la propiedad folioConsultaOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioConsultaOtorgante(String value) {
        this.folioConsultaOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad productoRequerido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductoRequerido() {
        return productoRequerido;
    }

    /**
     * Define el valor de la propiedad productoRequerido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductoRequerido(String value) {
        this.productoRequerido = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Define el valor de la propiedad tipoCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCuenta(String value) {
        this.tipoCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUnidadMonetaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUnidadMonetaria() {
        return claveUnidadMonetaria;
    }

    /**
     * Define el valor de la propiedad claveUnidadMonetaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUnidadMonetaria(String value) {
        this.claveUnidadMonetaria = value;
    }

    /**
     * Obtiene el valor de la propiedad importeContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteContrato() {
        return importeContrato;
    }

    /**
     * Define el valor de la propiedad importeContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteContrato(String value) {
        this.importeContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroFirma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroFirma() {
        return numeroFirma;
    }

    /**
     * Define el valor de la propiedad numeroFirma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroFirma(String value) {
        this.numeroFirma = value;
    }

}
