
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PersonaResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Encabezado" type="{http://bean.consulta.ws.bc.com/}EncabezadoResp" minOccurs="0"/&gt;
 *         &lt;element name="Nombre" type="{http://bean.consulta.ws.bc.com/}NombreResp" minOccurs="0"/&gt;
 *         &lt;element name="Domicilios" type="{http://bean.consulta.ws.bc.com/}DomiciliosResp" minOccurs="0"/&gt;
 *         &lt;element name="Empleos" type="{http://bean.consulta.ws.bc.com/}EmpleosResp" minOccurs="0"/&gt;
 *         &lt;element name="Mensajes" type="{http://bean.consulta.ws.bc.com/}MensajesResp" minOccurs="0"/&gt;
 *         &lt;element name="Cuentas" type="{http://bean.consulta.ws.bc.com/}CuentasResp" minOccurs="0"/&gt;
 *         &lt;element name="ConsultasEfectuadas" type="{http://bean.consulta.ws.bc.com/}ConsultasEfectuadasResp" minOccurs="0"/&gt;
 *         &lt;element name="DeclaracionesConsumidor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Caracteristicas" type="{http://bean.consulta.ws.bc.com/}CaracteristicasRespBC" minOccurs="0"/&gt;
 *         &lt;element name="ReportePDF" type="{http://bean.consulta.ws.bc.com/}ReportePDF" minOccurs="0"/&gt;
 *         &lt;element name="FR" type="{http://bean.consulta.ws.bc.com/}FR" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaResp", propOrder = {
    "encabezado",
    "nombre",
    "domicilios",
    "empleos",
    "mensajes",
    "cuentas",
    "consultasEfectuadas",
    "declaracionesConsumidor",
    "caracteristicas",
    "reportePDF",
    "fr"
})
public class PersonaResp {

    @XmlElement(name = "Encabezado")
    protected EncabezadoResp encabezado;
    @XmlElement(name = "Nombre")
    protected NombreResp nombre;
    @XmlElement(name = "Domicilios")
    protected DomiciliosResp domicilios;
    @XmlElement(name = "Empleos")
    protected EmpleosResp empleos;
    @XmlElement(name = "Mensajes")
    protected MensajesResp mensajes;
    @XmlElement(name = "Cuentas")
    protected CuentasResp cuentas;
    @XmlElement(name = "ConsultasEfectuadas")
    protected ConsultasEfectuadasResp consultasEfectuadas;
    @XmlElement(name = "DeclaracionesConsumidor")
    protected String declaracionesConsumidor;
    @XmlElement(name = "Caracteristicas")
    protected CaracteristicasRespBC caracteristicas;
    @XmlElement(name = "ReportePDF")
    protected ReportePDF reportePDF;
    @XmlElement(name = "FR")
    protected FR fr;

    /**
     * Obtiene el valor de la propiedad encabezado.
     * 
     * @return
     *     possible object is
     *     {@link EncabezadoResp }
     *     
     */
    public EncabezadoResp getEncabezado() {
        return encabezado;
    }

    /**
     * Define el valor de la propiedad encabezado.
     * 
     * @param value
     *     allowed object is
     *     {@link EncabezadoResp }
     *     
     */
    public void setEncabezado(EncabezadoResp value) {
        this.encabezado = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link NombreResp }
     *     
     */
    public NombreResp getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link NombreResp }
     *     
     */
    public void setNombre(NombreResp value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad domicilios.
     * 
     * @return
     *     possible object is
     *     {@link DomiciliosResp }
     *     
     */
    public DomiciliosResp getDomicilios() {
        return domicilios;
    }

    /**
     * Define el valor de la propiedad domicilios.
     * 
     * @param value
     *     allowed object is
     *     {@link DomiciliosResp }
     *     
     */
    public void setDomicilios(DomiciliosResp value) {
        this.domicilios = value;
    }

    /**
     * Obtiene el valor de la propiedad empleos.
     * 
     * @return
     *     possible object is
     *     {@link EmpleosResp }
     *     
     */
    public EmpleosResp getEmpleos() {
        return empleos;
    }

    /**
     * Define el valor de la propiedad empleos.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpleosResp }
     *     
     */
    public void setEmpleos(EmpleosResp value) {
        this.empleos = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajes.
     * 
     * @return
     *     possible object is
     *     {@link MensajesResp }
     *     
     */
    public MensajesResp getMensajes() {
        return mensajes;
    }

    /**
     * Define el valor de la propiedad mensajes.
     * 
     * @param value
     *     allowed object is
     *     {@link MensajesResp }
     *     
     */
    public void setMensajes(MensajesResp value) {
        this.mensajes = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentas.
     * 
     * @return
     *     possible object is
     *     {@link CuentasResp }
     *     
     */
    public CuentasResp getCuentas() {
        return cuentas;
    }

    /**
     * Define el valor de la propiedad cuentas.
     * 
     * @param value
     *     allowed object is
     *     {@link CuentasResp }
     *     
     */
    public void setCuentas(CuentasResp value) {
        this.cuentas = value;
    }

    /**
     * Obtiene el valor de la propiedad consultasEfectuadas.
     * 
     * @return
     *     possible object is
     *     {@link ConsultasEfectuadasResp }
     *     
     */
    public ConsultasEfectuadasResp getConsultasEfectuadas() {
        return consultasEfectuadas;
    }

    /**
     * Define el valor de la propiedad consultasEfectuadas.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultasEfectuadasResp }
     *     
     */
    public void setConsultasEfectuadas(ConsultasEfectuadasResp value) {
        this.consultasEfectuadas = value;
    }

    /**
     * Obtiene el valor de la propiedad declaracionesConsumidor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclaracionesConsumidor() {
        return declaracionesConsumidor;
    }

    /**
     * Define el valor de la propiedad declaracionesConsumidor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclaracionesConsumidor(String value) {
        this.declaracionesConsumidor = value;
    }

    /**
     * Obtiene el valor de la propiedad caracteristicas.
     * 
     * @return
     *     possible object is
     *     {@link CaracteristicasRespBC }
     *     
     */
    public CaracteristicasRespBC getCaracteristicas() {
        return caracteristicas;
    }

    /**
     * Define el valor de la propiedad caracteristicas.
     * 
     * @param value
     *     allowed object is
     *     {@link CaracteristicasRespBC }
     *     
     */
    public void setCaracteristicas(CaracteristicasRespBC value) {
        this.caracteristicas = value;
    }

    /**
     * Obtiene el valor de la propiedad reportePDF.
     * 
     * @return
     *     possible object is
     *     {@link ReportePDF }
     *     
     */
    public ReportePDF getReportePDF() {
        return reportePDF;
    }

    /**
     * Define el valor de la propiedad reportePDF.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportePDF }
     *     
     */
    public void setReportePDF(ReportePDF value) {
        this.reportePDF = value;
    }

    /**
     * Obtiene el valor de la propiedad fr.
     * 
     * @return
     *     possible object is
     *     {@link FR }
     *     
     */
    public FR getFR() {
        return fr;
    }

    /**
     * Define el valor de la propiedad fr.
     * 
     * @param value
     *     allowed object is
     *     {@link FR }
     *     
     */
    public void setFR(FR value) {
        this.fr = value;
    }

}
