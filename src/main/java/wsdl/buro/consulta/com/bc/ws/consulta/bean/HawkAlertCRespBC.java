
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para HawkAlertCRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HawkAlertCRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HawkAlertC" type="{http://bean.consulta.ws.bc.com/}HawkAlertConsultaRespBC" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HawkAlertCRespBC", propOrder = {
    "hawkAlertC"
})
public class HawkAlertCRespBC {

    @XmlElement(name = "HawkAlertC", nillable = true)
    protected List<HawkAlertConsultaRespBC> hawkAlertC;

    /**
     * Gets the value of the hawkAlertC property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hawkAlertC property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHawkAlertC().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HawkAlertConsultaRespBC }
     * 
     * 
     */
    public List<HawkAlertConsultaRespBC> getHawkAlertC() {
        if (hawkAlertC == null) {
            hawkAlertC = new ArrayList<HawkAlertConsultaRespBC>();
        }
        return this.hawkAlertC;
    }

}
