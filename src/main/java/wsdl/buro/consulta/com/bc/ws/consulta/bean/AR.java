
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AR complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AR"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReferenciaOperador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SujetoNoAutenticado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOPasswordErroneo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorSistemaBC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EtiquetaSegmentoErronea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FaltaCampoRequerido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorReporteBloqueado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AR", propOrder = {
    "referenciaOperador",
    "sujetoNoAutenticado",
    "claveOPasswordErroneo",
    "errorSistemaBC",
    "etiquetaSegmentoErronea",
    "faltaCampoRequerido",
    "errorReporteBloqueado"
})
public class AR {

    @XmlElement(name = "ReferenciaOperador")
    protected String referenciaOperador;
    @XmlElement(name = "SujetoNoAutenticado")
    protected String sujetoNoAutenticado;
    @XmlElement(name = "ClaveOPasswordErroneo")
    protected String claveOPasswordErroneo;
    @XmlElement(name = "ErrorSistemaBC")
    protected String errorSistemaBC;
    @XmlElement(name = "EtiquetaSegmentoErronea")
    protected String etiquetaSegmentoErronea;
    @XmlElement(name = "FaltaCampoRequerido")
    protected String faltaCampoRequerido;
    @XmlElement(name = "ErrorReporteBloqueado")
    protected String errorReporteBloqueado;

    /**
     * Obtiene el valor de la propiedad referenciaOperador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaOperador() {
        return referenciaOperador;
    }

    /**
     * Define el valor de la propiedad referenciaOperador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaOperador(String value) {
        this.referenciaOperador = value;
    }

    /**
     * Obtiene el valor de la propiedad sujetoNoAutenticado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSujetoNoAutenticado() {
        return sujetoNoAutenticado;
    }

    /**
     * Define el valor de la propiedad sujetoNoAutenticado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSujetoNoAutenticado(String value) {
        this.sujetoNoAutenticado = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOPasswordErroneo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOPasswordErroneo() {
        return claveOPasswordErroneo;
    }

    /**
     * Define el valor de la propiedad claveOPasswordErroneo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOPasswordErroneo(String value) {
        this.claveOPasswordErroneo = value;
    }

    /**
     * Obtiene el valor de la propiedad errorSistemaBC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorSistemaBC() {
        return errorSistemaBC;
    }

    /**
     * Define el valor de la propiedad errorSistemaBC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorSistemaBC(String value) {
        this.errorSistemaBC = value;
    }

    /**
     * Obtiene el valor de la propiedad etiquetaSegmentoErronea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEtiquetaSegmentoErronea() {
        return etiquetaSegmentoErronea;
    }

    /**
     * Define el valor de la propiedad etiquetaSegmentoErronea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEtiquetaSegmentoErronea(String value) {
        this.etiquetaSegmentoErronea = value;
    }

    /**
     * Obtiene el valor de la propiedad faltaCampoRequerido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaltaCampoRequerido() {
        return faltaCampoRequerido;
    }

    /**
     * Define el valor de la propiedad faltaCampoRequerido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaltaCampoRequerido(String value) {
        this.faltaCampoRequerido = value;
    }

    /**
     * Obtiene el valor de la propiedad errorReporteBloqueado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorReporteBloqueado() {
        return errorReporteBloqueado;
    }

    /**
     * Define el valor de la propiedad errorReporteBloqueado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorReporteBloqueado(String value) {
        this.errorReporteBloqueado = value;
    }

}
