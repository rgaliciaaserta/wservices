
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para consulta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="consulta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Encabezado" type="{http://bean.consulta.ws.bc.com/}Encabezado"/&gt;
 *         &lt;element name="Personas" type="{http://bean.consulta.ws.bc.com/}Personas"/&gt;
 *         &lt;element name="Caracteristicas" type="{http://bean.consulta.ws.bc.com/}Caracteristicas" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consulta", propOrder = {
    "encabezado",
    "personas",
    "caracteristicas"
})
public class Consulta {

    @XmlElement(name = "Encabezado", required = true)
    protected Encabezado encabezado;
    @XmlElement(name = "Personas", required = true)
    protected Personas personas;
    @XmlElement(name = "Caracteristicas")
    protected Caracteristicas caracteristicas;

    /**
     * Obtiene el valor de la propiedad encabezado.
     * 
     * @return
     *     possible object is
     *     {@link Encabezado }
     *     
     */
    public Encabezado getEncabezado() {
        return encabezado;
    }

    /**
     * Define el valor de la propiedad encabezado.
     * 
     * @param value
     *     allowed object is
     *     {@link Encabezado }
     *     
     */
    public void setEncabezado(Encabezado value) {
        this.encabezado = value;
    }

    /**
     * Obtiene el valor de la propiedad personas.
     * 
     * @return
     *     possible object is
     *     {@link Personas }
     *     
     */
    public Personas getPersonas() {
        return personas;
    }

    /**
     * Define el valor de la propiedad personas.
     * 
     * @param value
     *     allowed object is
     *     {@link Personas }
     *     
     */
    public void setPersonas(Personas value) {
        this.personas = value;
    }

    /**
     * Obtiene el valor de la propiedad caracteristicas.
     * 
     * @return
     *     possible object is
     *     {@link Caracteristicas }
     *     
     */
    public Caracteristicas getCaracteristicas() {
        return caracteristicas;
    }

    /**
     * Define el valor de la propiedad caracteristicas.
     * 
     * @param value
     *     allowed object is
     *     {@link Caracteristicas }
     *     
     */
    public void setCaracteristicas(Caracteristicas value) {
        this.caracteristicas = value;
    }

}
