
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para UR complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="UR"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NumeroReferenciaOperador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SolicitudClienteErronea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VersionProporcionadaErronea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProductoSolicitadoErroneo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PasswordOClaveErronea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SegmentoRequeridoNoProporcionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UltimaInformacionValidaCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InformacionErroneaParaConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ValorErroneoCampoRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorSistemaBuroCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EtiquetaSegmentoErronea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OrdenErroneoSegmento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroErroneoSegmentos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FaltaCampoRequerido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorReporteBloqueado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UR", propOrder = {
    "numeroReferenciaOperador",
    "solicitudClienteErronea",
    "versionProporcionadaErronea",
    "productoSolicitadoErroneo",
    "passwordOClaveErronea",
    "segmentoRequeridoNoProporcionado",
    "ultimaInformacionValidaCliente",
    "informacionErroneaParaConsulta",
    "valorErroneoCampoRelacionado",
    "errorSistemaBuroCredito",
    "etiquetaSegmentoErronea",
    "ordenErroneoSegmento",
    "numeroErroneoSegmentos",
    "faltaCampoRequerido",
    "errorReporteBloqueado"
})
public class UR {

    @XmlElement(name = "NumeroReferenciaOperador")
    protected String numeroReferenciaOperador;
    @XmlElement(name = "SolicitudClienteErronea")
    protected String solicitudClienteErronea;
    @XmlElement(name = "VersionProporcionadaErronea")
    protected String versionProporcionadaErronea;
    @XmlElement(name = "ProductoSolicitadoErroneo")
    protected String productoSolicitadoErroneo;
    @XmlElement(name = "PasswordOClaveErronea")
    protected String passwordOClaveErronea;
    @XmlElement(name = "SegmentoRequeridoNoProporcionado")
    protected String segmentoRequeridoNoProporcionado;
    @XmlElement(name = "UltimaInformacionValidaCliente")
    protected String ultimaInformacionValidaCliente;
    @XmlElement(name = "InformacionErroneaParaConsulta")
    protected String informacionErroneaParaConsulta;
    @XmlElement(name = "ValorErroneoCampoRelacionado")
    protected String valorErroneoCampoRelacionado;
    @XmlElement(name = "ErrorSistemaBuroCredito")
    protected String errorSistemaBuroCredito;
    @XmlElement(name = "EtiquetaSegmentoErronea")
    protected String etiquetaSegmentoErronea;
    @XmlElement(name = "OrdenErroneoSegmento")
    protected String ordenErroneoSegmento;
    @XmlElement(name = "NumeroErroneoSegmentos")
    protected String numeroErroneoSegmentos;
    @XmlElement(name = "FaltaCampoRequerido")
    protected String faltaCampoRequerido;
    @XmlElement(name = "ErrorReporteBloqueado")
    protected String errorReporteBloqueado;

    /**
     * Obtiene el valor de la propiedad numeroReferenciaOperador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroReferenciaOperador() {
        return numeroReferenciaOperador;
    }

    /**
     * Define el valor de la propiedad numeroReferenciaOperador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroReferenciaOperador(String value) {
        this.numeroReferenciaOperador = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudClienteErronea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSolicitudClienteErronea() {
        return solicitudClienteErronea;
    }

    /**
     * Define el valor de la propiedad solicitudClienteErronea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSolicitudClienteErronea(String value) {
        this.solicitudClienteErronea = value;
    }

    /**
     * Obtiene el valor de la propiedad versionProporcionadaErronea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionProporcionadaErronea() {
        return versionProporcionadaErronea;
    }

    /**
     * Define el valor de la propiedad versionProporcionadaErronea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionProporcionadaErronea(String value) {
        this.versionProporcionadaErronea = value;
    }

    /**
     * Obtiene el valor de la propiedad productoSolicitadoErroneo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductoSolicitadoErroneo() {
        return productoSolicitadoErroneo;
    }

    /**
     * Define el valor de la propiedad productoSolicitadoErroneo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductoSolicitadoErroneo(String value) {
        this.productoSolicitadoErroneo = value;
    }

    /**
     * Obtiene el valor de la propiedad passwordOClaveErronea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordOClaveErronea() {
        return passwordOClaveErronea;
    }

    /**
     * Define el valor de la propiedad passwordOClaveErronea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordOClaveErronea(String value) {
        this.passwordOClaveErronea = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentoRequeridoNoProporcionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentoRequeridoNoProporcionado() {
        return segmentoRequeridoNoProporcionado;
    }

    /**
     * Define el valor de la propiedad segmentoRequeridoNoProporcionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentoRequeridoNoProporcionado(String value) {
        this.segmentoRequeridoNoProporcionado = value;
    }

    /**
     * Obtiene el valor de la propiedad ultimaInformacionValidaCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUltimaInformacionValidaCliente() {
        return ultimaInformacionValidaCliente;
    }

    /**
     * Define el valor de la propiedad ultimaInformacionValidaCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUltimaInformacionValidaCliente(String value) {
        this.ultimaInformacionValidaCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionErroneaParaConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformacionErroneaParaConsulta() {
        return informacionErroneaParaConsulta;
    }

    /**
     * Define el valor de la propiedad informacionErroneaParaConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformacionErroneaParaConsulta(String value) {
        this.informacionErroneaParaConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad valorErroneoCampoRelacionado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorErroneoCampoRelacionado() {
        return valorErroneoCampoRelacionado;
    }

    /**
     * Define el valor de la propiedad valorErroneoCampoRelacionado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorErroneoCampoRelacionado(String value) {
        this.valorErroneoCampoRelacionado = value;
    }

    /**
     * Obtiene el valor de la propiedad errorSistemaBuroCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorSistemaBuroCredito() {
        return errorSistemaBuroCredito;
    }

    /**
     * Define el valor de la propiedad errorSistemaBuroCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorSistemaBuroCredito(String value) {
        this.errorSistemaBuroCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad etiquetaSegmentoErronea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEtiquetaSegmentoErronea() {
        return etiquetaSegmentoErronea;
    }

    /**
     * Define el valor de la propiedad etiquetaSegmentoErronea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEtiquetaSegmentoErronea(String value) {
        this.etiquetaSegmentoErronea = value;
    }

    /**
     * Obtiene el valor de la propiedad ordenErroneoSegmento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdenErroneoSegmento() {
        return ordenErroneoSegmento;
    }

    /**
     * Define el valor de la propiedad ordenErroneoSegmento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdenErroneoSegmento(String value) {
        this.ordenErroneoSegmento = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroErroneoSegmentos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroErroneoSegmentos() {
        return numeroErroneoSegmentos;
    }

    /**
     * Define el valor de la propiedad numeroErroneoSegmentos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroErroneoSegmentos(String value) {
        this.numeroErroneoSegmentos = value;
    }

    /**
     * Obtiene el valor de la propiedad faltaCampoRequerido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaltaCampoRequerido() {
        return faltaCampoRequerido;
    }

    /**
     * Define el valor de la propiedad faltaCampoRequerido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaltaCampoRequerido(String value) {
        this.faltaCampoRequerido = value;
    }

    /**
     * Obtiene el valor de la propiedad errorReporteBloqueado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorReporteBloqueado() {
        return errorReporteBloqueado;
    }

    /**
     * Define el valor de la propiedad errorReporteBloqueado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorReporteBloqueado(String value) {
        this.errorReporteBloqueado = value;
    }

}
