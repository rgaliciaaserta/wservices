
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultasEfectuadasRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultasEfectuadasRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaEfectuada" type="{http://bean.consulta.ws.bc.com/}ConsultaEfectuadaRespBC" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultasEfectuadasRespBC", propOrder = {
    "consultaEfectuada"
})
public class ConsultasEfectuadasRespBC {

    @XmlElement(name = "ConsultaEfectuada", nillable = true)
    protected List<ConsultaEfectuadaRespBC> consultaEfectuada;

    /**
     * Gets the value of the consultaEfectuada property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consultaEfectuada property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsultaEfectuada().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsultaEfectuadaRespBC }
     * 
     * 
     */
    public List<ConsultaEfectuadaRespBC> getConsultaEfectuada() {
        if (consultaEfectuada == null) {
            consultaEfectuada = new ArrayList<ConsultaEfectuadaRespBC>();
        }
        return this.consultaEfectuada;
    }

}
