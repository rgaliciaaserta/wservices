
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para persona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="persona"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DetalleConsulta" type="{http://bean.consulta.ws.bc.com/}DetalleConsulta"/&gt;
 *         &lt;element name="Nombre" type="{http://bean.consulta.ws.bc.com/}Nombre"/&gt;
 *         &lt;element name="Domicilios" type="{http://bean.consulta.ws.bc.com/}Domicilios"/&gt;
 *         &lt;element name="Empleos" type="{http://bean.consulta.ws.bc.com/}Empleos"/&gt;
 *         &lt;element name="CuentasReferencia" type="{http://bean.consulta.ws.bc.com/}CuentasReferencia"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "persona", propOrder = {
    "detalleConsulta",
    "nombre",
    "domicilios",
    "empleos",
    "cuentasReferencia"
})
public class Persona {

    @XmlElement(name = "DetalleConsulta", required = true)
    protected DetalleConsulta detalleConsulta;
    @XmlElement(name = "Nombre", required = true)
    protected Nombre nombre;
    @XmlElement(name = "Domicilios", required = true)
    protected Domicilios domicilios;
    @XmlElement(name = "Empleos", required = true)
    protected Empleos empleos;
    @XmlElement(name = "CuentasReferencia", required = true)
    protected CuentasReferencia cuentasReferencia;

    /**
     * Obtiene el valor de la propiedad detalleConsulta.
     * 
     * @return
     *     possible object is
     *     {@link DetalleConsulta }
     *     
     */
    public DetalleConsulta getDetalleConsulta() {
        return detalleConsulta;
    }

    /**
     * Define el valor de la propiedad detalleConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link DetalleConsulta }
     *     
     */
    public void setDetalleConsulta(DetalleConsulta value) {
        this.detalleConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link Nombre }
     *     
     */
    public Nombre getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link Nombre }
     *     
     */
    public void setNombre(Nombre value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad domicilios.
     * 
     * @return
     *     possible object is
     *     {@link Domicilios }
     *     
     */
    public Domicilios getDomicilios() {
        return domicilios;
    }

    /**
     * Define el valor de la propiedad domicilios.
     * 
     * @param value
     *     allowed object is
     *     {@link Domicilios }
     *     
     */
    public void setDomicilios(Domicilios value) {
        this.domicilios = value;
    }

    /**
     * Obtiene el valor de la propiedad empleos.
     * 
     * @return
     *     possible object is
     *     {@link Empleos }
     *     
     */
    public Empleos getEmpleos() {
        return empleos;
    }

    /**
     * Define el valor de la propiedad empleos.
     * 
     * @param value
     *     allowed object is
     *     {@link Empleos }
     *     
     */
    public void setEmpleos(Empleos value) {
        this.empleos = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasReferencia.
     * 
     * @return
     *     possible object is
     *     {@link CuentasReferencia }
     *     
     */
    public CuentasReferencia getCuentasReferencia() {
        return cuentasReferencia;
    }

    /**
     * Define el valor de la propiedad cuentasReferencia.
     * 
     * @param value
     *     allowed object is
     *     {@link CuentasReferencia }
     *     
     */
    public void setCuentasReferencia(CuentasReferencia value) {
        this.cuentasReferencia = value;
    }

}
