
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ErrorResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ErrorResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FolioConsultaOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProductoRequerido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Errores" type="{http://bean.consulta.ws.bc.com/}ErroresResp" minOccurs="0"/&gt;
 *         &lt;element name="AR" type="{http://bean.consulta.ws.bc.com/}AR"/&gt;
 *         &lt;element name="UR" type="{http://bean.consulta.ws.bc.com/}UR"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorResp", propOrder = {
    "claveOtorgante",
    "folioConsultaOtorgante",
    "productoRequerido",
    "errores",
    "ar",
    "ur"
})
public class ErrorResp {

    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "FolioConsultaOtorgante")
    protected String folioConsultaOtorgante;
    @XmlElement(name = "ProductoRequerido")
    protected String productoRequerido;
    @XmlElement(name = "Errores")
    protected ErroresResp errores;
    @XmlElement(name = "AR", required = true, nillable = true)
    protected AR ar;
    @XmlElement(name = "UR", required = true, nillable = true)
    protected UR ur;

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad folioConsultaOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioConsultaOtorgante() {
        return folioConsultaOtorgante;
    }

    /**
     * Define el valor de la propiedad folioConsultaOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioConsultaOtorgante(String value) {
        this.folioConsultaOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad productoRequerido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductoRequerido() {
        return productoRequerido;
    }

    /**
     * Define el valor de la propiedad productoRequerido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductoRequerido(String value) {
        this.productoRequerido = value;
    }

    /**
     * Obtiene el valor de la propiedad errores.
     * 
     * @return
     *     possible object is
     *     {@link ErroresResp }
     *     
     */
    public ErroresResp getErrores() {
        return errores;
    }

    /**
     * Define el valor de la propiedad errores.
     * 
     * @param value
     *     allowed object is
     *     {@link ErroresResp }
     *     
     */
    public void setErrores(ErroresResp value) {
        this.errores = value;
    }

    /**
     * Obtiene el valor de la propiedad ar.
     * 
     * @return
     *     possible object is
     *     {@link AR }
     *     
     */
    public AR getAR() {
        return ar;
    }

    /**
     * Define el valor de la propiedad ar.
     * 
     * @param value
     *     allowed object is
     *     {@link AR }
     *     
     */
    public void setAR(AR value) {
        this.ar = value;
    }

    /**
     * Obtiene el valor de la propiedad ur.
     * 
     * @return
     *     possible object is
     *     {@link UR }
     *     
     */
    public UR getUR() {
        return ur;
    }

    /**
     * Define el valor de la propiedad ur.
     * 
     * @param value
     *     allowed object is
     *     {@link UR }
     *     
     */
    public void setUR(UR value) {
        this.ur = value;
    }

}
