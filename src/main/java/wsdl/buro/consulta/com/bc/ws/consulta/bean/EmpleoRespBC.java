
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EmpleoRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EmpleoRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://bean.consulta.ws.bc.com/}EmpleoBC"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaReportoEmpleo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaVerificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ModoVerificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmpleoRespBC", propOrder = {
    "fechaReportoEmpleo",
    "fechaVerificacion",
    "modoVerificacion"
})
public class EmpleoRespBC
    extends EmpleoBC
{

    @XmlElement(name = "FechaReportoEmpleo")
    protected String fechaReportoEmpleo;
    @XmlElement(name = "FechaVerificacion")
    protected String fechaVerificacion;
    @XmlElement(name = "ModoVerificacion")
    protected String modoVerificacion;

    /**
     * Obtiene el valor de la propiedad fechaReportoEmpleo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaReportoEmpleo() {
        return fechaReportoEmpleo;
    }

    /**
     * Define el valor de la propiedad fechaReportoEmpleo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaReportoEmpleo(String value) {
        this.fechaReportoEmpleo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVerificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVerificacion() {
        return fechaVerificacion;
    }

    /**
     * Define el valor de la propiedad fechaVerificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVerificacion(String value) {
        this.fechaVerificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad modoVerificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModoVerificacion() {
        return modoVerificacion;
    }

    /**
     * Define el valor de la propiedad modoVerificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModoVerificacion(String value) {
        this.modoVerificacion = value;
    }

}
