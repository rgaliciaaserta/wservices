
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaEfectuadaRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaEfectuadaRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdentificacionBuro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NombreOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TelefonoOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUnidadMonetaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImporteContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IndicadorTipoResponsabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ConsumidorNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ResultadoFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdentificadorOrigenConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaEfectuadaRespBC", propOrder = {
    "fechaConsulta",
    "identificacionBuro",
    "claveOtorgante",
    "nombreOtorgante",
    "telefonoOtorgante",
    "tipoContrato",
    "claveUnidadMonetaria",
    "importeContrato",
    "indicadorTipoResponsabilidad",
    "consumidorNuevo",
    "resultadoFinal",
    "identificadorOrigenConsulta"
})
public class ConsultaEfectuadaRespBC {

    @XmlElement(name = "FechaConsulta")
    protected String fechaConsulta;
    @XmlElement(name = "IdentificacionBuro")
    protected String identificacionBuro;
    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "NombreOtorgante")
    protected String nombreOtorgante;
    @XmlElement(name = "TelefonoOtorgante")
    protected String telefonoOtorgante;
    @XmlElement(name = "TipoContrato")
    protected String tipoContrato;
    @XmlElement(name = "ClaveUnidadMonetaria")
    protected String claveUnidadMonetaria;
    @XmlElement(name = "ImporteContrato")
    protected String importeContrato;
    @XmlElement(name = "IndicadorTipoResponsabilidad")
    protected String indicadorTipoResponsabilidad;
    @XmlElement(name = "ConsumidorNuevo")
    protected String consumidorNuevo;
    @XmlElement(name = "ResultadoFinal")
    protected String resultadoFinal;
    @XmlElement(name = "IdentificadorOrigenConsulta")
    protected String identificadorOrigenConsulta;

    /**
     * Obtiene el valor de la propiedad fechaConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaConsulta() {
        return fechaConsulta;
    }

    /**
     * Define el valor de la propiedad fechaConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaConsulta(String value) {
        this.fechaConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad identificacionBuro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificacionBuro() {
        return identificacionBuro;
    }

    /**
     * Define el valor de la propiedad identificacionBuro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificacionBuro(String value) {
        this.identificacionBuro = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOtorgante() {
        return nombreOtorgante;
    }

    /**
     * Define el valor de la propiedad nombreOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOtorgante(String value) {
        this.nombreOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad telefonoOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefonoOtorgante() {
        return telefonoOtorgante;
    }

    /**
     * Define el valor de la propiedad telefonoOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefonoOtorgante(String value) {
        this.telefonoOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Define el valor de la propiedad tipoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContrato(String value) {
        this.tipoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUnidadMonetaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUnidadMonetaria() {
        return claveUnidadMonetaria;
    }

    /**
     * Define el valor de la propiedad claveUnidadMonetaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUnidadMonetaria(String value) {
        this.claveUnidadMonetaria = value;
    }

    /**
     * Obtiene el valor de la propiedad importeContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteContrato() {
        return importeContrato;
    }

    /**
     * Define el valor de la propiedad importeContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteContrato(String value) {
        this.importeContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorTipoResponsabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorTipoResponsabilidad() {
        return indicadorTipoResponsabilidad;
    }

    /**
     * Define el valor de la propiedad indicadorTipoResponsabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorTipoResponsabilidad(String value) {
        this.indicadorTipoResponsabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad consumidorNuevo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumidorNuevo() {
        return consumidorNuevo;
    }

    /**
     * Define el valor de la propiedad consumidorNuevo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumidorNuevo(String value) {
        this.consumidorNuevo = value;
    }

    /**
     * Obtiene el valor de la propiedad resultadoFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultadoFinal() {
        return resultadoFinal;
    }

    /**
     * Define el valor de la propiedad resultadoFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultadoFinal(String value) {
        this.resultadoFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad identificadorOrigenConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorOrigenConsulta() {
        return identificadorOrigenConsulta;
    }

    /**
     * Define el valor de la propiedad identificadorOrigenConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorOrigenConsulta(String value) {
        this.identificadorOrigenConsulta = value;
    }

}
