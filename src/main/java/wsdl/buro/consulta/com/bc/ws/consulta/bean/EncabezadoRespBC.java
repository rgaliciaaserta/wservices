
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EncabezadoRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EncabezadoRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://bean.consulta.ws.bc.com/}EncabezadoBC"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveRetornoConsumidorPrincipal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveRetornoConsumidorSecundario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroControlConsulta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EncabezadoRespBC", propOrder = {
    "claveOtorgante",
    "claveRetornoConsumidorPrincipal",
    "claveRetornoConsumidorSecundario",
    "numeroControlConsulta"
})
public class EncabezadoRespBC
    extends EncabezadoBC
{

    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "ClaveRetornoConsumidorPrincipal")
    protected String claveRetornoConsumidorPrincipal;
    @XmlElement(name = "ClaveRetornoConsumidorSecundario")
    protected String claveRetornoConsumidorSecundario;
    @XmlElement(name = "NumeroControlConsulta", required = true)
    protected String numeroControlConsulta;

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad claveRetornoConsumidorPrincipal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveRetornoConsumidorPrincipal() {
        return claveRetornoConsumidorPrincipal;
    }

    /**
     * Define el valor de la propiedad claveRetornoConsumidorPrincipal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveRetornoConsumidorPrincipal(String value) {
        this.claveRetornoConsumidorPrincipal = value;
    }

    /**
     * Obtiene el valor de la propiedad claveRetornoConsumidorSecundario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveRetornoConsumidorSecundario() {
        return claveRetornoConsumidorSecundario;
    }

    /**
     * Define el valor de la propiedad claveRetornoConsumidorSecundario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveRetornoConsumidorSecundario(String value) {
        this.claveRetornoConsumidorSecundario = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroControlConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroControlConsulta() {
        return numeroControlConsulta;
    }

    /**
     * Define el valor de la propiedad numeroControlConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroControlConsulta(String value) {
        this.numeroControlConsulta = value;
    }

}
