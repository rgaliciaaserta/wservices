
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DireccionRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DireccionRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://bean.consulta.ws.bc.com/}Direccion"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaReporteDireccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DireccionRespBC", propOrder = {
    "fechaReporteDireccion"
})
public class DireccionRespBC
    extends Direccion
{

    @XmlElement(name = "FechaReporteDireccion")
    protected String fechaReporteDireccion;

    /**
     * Obtiene el valor de la propiedad fechaReporteDireccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaReporteDireccion() {
        return fechaReporteDireccion;
    }

    /**
     * Define el valor de la propiedad fechaReporteDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaReporteDireccion(String value) {
        this.fechaReporteDireccion = value;
    }

}
