
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ReportePDF complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ReportePDF"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Reporte" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="Error" type="{http://bean.consulta.ws.bc.com/}ErrorRespBC" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportePDF", propOrder = {
    "reporte",
    "error"
})
public class ReportePDF {

    @XmlElement(name = "Reporte")
    protected byte[] reporte;
    @XmlElement(name = "Error")
    protected ErrorRespBC error;

    /**
     * Obtiene el valor de la propiedad reporte.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getReporte() {
        return reporte;
    }

    /**
     * Define el valor de la propiedad reporte.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setReporte(byte[] value) {
        this.reporte = value;
    }

    /**
     * Obtiene el valor de la propiedad error.
     * 
     * @return
     *     possible object is
     *     {@link ErrorRespBC }
     *     
     */
    public ErrorRespBC getError() {
        return error;
    }

    /**
     * Define el valor de la propiedad error.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorRespBC }
     *     
     */
    public void setError(ErrorRespBC value) {
        this.error = value;
    }

}
