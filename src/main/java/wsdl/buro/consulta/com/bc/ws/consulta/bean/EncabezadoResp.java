
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EncabezadoResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EncabezadoResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FolioConsultaOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExpedienteEncontrado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FolioConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EncabezadoResp", propOrder = {
    "version",
    "folioConsultaOtorgante",
    "claveOtorgante",
    "expedienteEncontrado",
    "folioConsulta"
})
public class EncabezadoResp {

    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "FolioConsultaOtorgante")
    protected String folioConsultaOtorgante;
    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "ExpedienteEncontrado")
    protected String expedienteEncontrado;
    @XmlElement(name = "FolioConsulta")
    protected String folioConsulta;

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad folioConsultaOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioConsultaOtorgante() {
        return folioConsultaOtorgante;
    }

    /**
     * Define el valor de la propiedad folioConsultaOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioConsultaOtorgante(String value) {
        this.folioConsultaOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad expedienteEncontrado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpedienteEncontrado() {
        return expedienteEncontrado;
    }

    /**
     * Define el valor de la propiedad expedienteEncontrado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpedienteEncontrado(String value) {
        this.expedienteEncontrado = value;
    }

    /**
     * Obtiene el valor de la propiedad folioConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioConsulta() {
        return folioConsulta;
    }

    /**
     * Define el valor de la propiedad folioConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioConsulta(String value) {
        this.folioConsulta = value;
    }

}
