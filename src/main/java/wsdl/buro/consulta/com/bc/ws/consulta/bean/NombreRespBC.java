
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para NombreRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="NombreRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://bean.consulta.ws.bc.com/}NombreBC"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaRecepcionInformacionDependientes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaDefuncion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NombreRespBC", propOrder = {
    "fechaRecepcionInformacionDependientes",
    "fechaDefuncion"
})
public class NombreRespBC
    extends NombreBC
{

    @XmlElement(name = "FechaRecepcionInformacionDependientes")
    protected String fechaRecepcionInformacionDependientes;
    @XmlElement(name = "FechaDefuncion")
    protected String fechaDefuncion;

    /**
     * Obtiene el valor de la propiedad fechaRecepcionInformacionDependientes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRecepcionInformacionDependientes() {
        return fechaRecepcionInformacionDependientes;
    }

    /**
     * Define el valor de la propiedad fechaRecepcionInformacionDependientes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRecepcionInformacionDependientes(String value) {
        this.fechaRecepcionInformacionDependientes = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDefuncion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDefuncion() {
        return fechaDefuncion;
    }

    /**
     * Define el valor de la propiedad fechaDefuncion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDefuncion(String value) {
        this.fechaDefuncion = value;
    }

}
