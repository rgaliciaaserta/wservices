
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PersonaRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Encabezado" type="{http://bean.consulta.ws.bc.com/}EncabezadoRespBC" minOccurs="0"/&gt;
 *         &lt;element name="Nombre" type="{http://bean.consulta.ws.bc.com/}NombreRespBC" minOccurs="0"/&gt;
 *         &lt;element name="Domicilios" type="{http://bean.consulta.ws.bc.com/}DomiciliosRespBC" minOccurs="0"/&gt;
 *         &lt;element name="Empleos" type="{http://bean.consulta.ws.bc.com/}EmpleosRespBC" minOccurs="0"/&gt;
 *         &lt;element name="Cuentas" type="{http://bean.consulta.ws.bc.com/}CuentasRBC" minOccurs="0"/&gt;
 *         &lt;element name="ConsultasEfectuadas" type="{http://bean.consulta.ws.bc.com/}ConsultasEfectuadasRespBC" minOccurs="0"/&gt;
 *         &lt;element name="ResumenReporte" type="{http://bean.consulta.ws.bc.com/}ResumenReporteRBC" minOccurs="0"/&gt;
 *         &lt;element name="HawkAlertConsulta" type="{http://bean.consulta.ws.bc.com/}HawkAlertCRespBC" minOccurs="0"/&gt;
 *         &lt;element name="HawkAlertBD" type="{http://bean.consulta.ws.bc.com/}HawkAlertDBRespBC" minOccurs="0"/&gt;
 *         &lt;element name="DeclaracionesCliente" type="{http://bean.consulta.ws.bc.com/}DeclaracionesClienteRespBC" minOccurs="0"/&gt;
 *         &lt;element name="ScoreBuroCredito" type="{http://bean.consulta.ws.bc.com/}ScoreBCRespBC" minOccurs="0"/&gt;
 *         &lt;element name="Error" type="{http://bean.consulta.ws.bc.com/}ErrorRespBC" minOccurs="0"/&gt;
 *         &lt;element name="ReporteImpreso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Caracteristicas" type="{http://bean.consulta.ws.bc.com/}CaracteristicasRespBC" minOccurs="0"/&gt;
 *         &lt;element name="ReportePDF" type="{http://bean.consulta.ws.bc.com/}ReportePDF" minOccurs="0"/&gt;
 *         &lt;element name="FR" type="{http://bean.consulta.ws.bc.com/}FR" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaRespBC", propOrder = {
    "encabezado",
    "nombre",
    "domicilios",
    "empleos",
    "cuentas",
    "consultasEfectuadas",
    "resumenReporte",
    "hawkAlertConsulta",
    "hawkAlertBD",
    "declaracionesCliente",
    "scoreBuroCredito",
    "error",
    "reporteImpreso",
    "caracteristicas",
    "reportePDF",
    "fr"
})
public class PersonaRespBC {

    @XmlElement(name = "Encabezado")
    protected EncabezadoRespBC encabezado;
    @XmlElement(name = "Nombre")
    protected NombreRespBC nombre;
    @XmlElement(name = "Domicilios")
    protected DomiciliosRespBC domicilios;
    @XmlElement(name = "Empleos")
    protected EmpleosRespBC empleos;
    @XmlElement(name = "Cuentas")
    protected CuentasRBC cuentas;
    @XmlElement(name = "ConsultasEfectuadas")
    protected ConsultasEfectuadasRespBC consultasEfectuadas;
    @XmlElement(name = "ResumenReporte")
    protected ResumenReporteRBC resumenReporte;
    @XmlElement(name = "HawkAlertConsulta")
    protected HawkAlertCRespBC hawkAlertConsulta;
    @XmlElement(name = "HawkAlertBD")
    protected HawkAlertDBRespBC hawkAlertBD;
    @XmlElement(name = "DeclaracionesCliente")
    protected DeclaracionesClienteRespBC declaracionesCliente;
    @XmlElement(name = "ScoreBuroCredito")
    protected ScoreBCRespBC scoreBuroCredito;
    @XmlElement(name = "Error")
    protected ErrorRespBC error;
    @XmlElement(name = "ReporteImpreso")
    protected String reporteImpreso;
    @XmlElement(name = "Caracteristicas")
    protected CaracteristicasRespBC caracteristicas;
    @XmlElement(name = "ReportePDF")
    protected ReportePDF reportePDF;
    @XmlElement(name = "FR")
    protected FR fr;

    /**
     * Obtiene el valor de la propiedad encabezado.
     * 
     * @return
     *     possible object is
     *     {@link EncabezadoRespBC }
     *     
     */
    public EncabezadoRespBC getEncabezado() {
        return encabezado;
    }

    /**
     * Define el valor de la propiedad encabezado.
     * 
     * @param value
     *     allowed object is
     *     {@link EncabezadoRespBC }
     *     
     */
    public void setEncabezado(EncabezadoRespBC value) {
        this.encabezado = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link NombreRespBC }
     *     
     */
    public NombreRespBC getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link NombreRespBC }
     *     
     */
    public void setNombre(NombreRespBC value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad domicilios.
     * 
     * @return
     *     possible object is
     *     {@link DomiciliosRespBC }
     *     
     */
    public DomiciliosRespBC getDomicilios() {
        return domicilios;
    }

    /**
     * Define el valor de la propiedad domicilios.
     * 
     * @param value
     *     allowed object is
     *     {@link DomiciliosRespBC }
     *     
     */
    public void setDomicilios(DomiciliosRespBC value) {
        this.domicilios = value;
    }

    /**
     * Obtiene el valor de la propiedad empleos.
     * 
     * @return
     *     possible object is
     *     {@link EmpleosRespBC }
     *     
     */
    public EmpleosRespBC getEmpleos() {
        return empleos;
    }

    /**
     * Define el valor de la propiedad empleos.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpleosRespBC }
     *     
     */
    public void setEmpleos(EmpleosRespBC value) {
        this.empleos = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentas.
     * 
     * @return
     *     possible object is
     *     {@link CuentasRBC }
     *     
     */
    public CuentasRBC getCuentas() {
        return cuentas;
    }

    /**
     * Define el valor de la propiedad cuentas.
     * 
     * @param value
     *     allowed object is
     *     {@link CuentasRBC }
     *     
     */
    public void setCuentas(CuentasRBC value) {
        this.cuentas = value;
    }

    /**
     * Obtiene el valor de la propiedad consultasEfectuadas.
     * 
     * @return
     *     possible object is
     *     {@link ConsultasEfectuadasRespBC }
     *     
     */
    public ConsultasEfectuadasRespBC getConsultasEfectuadas() {
        return consultasEfectuadas;
    }

    /**
     * Define el valor de la propiedad consultasEfectuadas.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultasEfectuadasRespBC }
     *     
     */
    public void setConsultasEfectuadas(ConsultasEfectuadasRespBC value) {
        this.consultasEfectuadas = value;
    }

    /**
     * Obtiene el valor de la propiedad resumenReporte.
     * 
     * @return
     *     possible object is
     *     {@link ResumenReporteRBC }
     *     
     */
    public ResumenReporteRBC getResumenReporte() {
        return resumenReporte;
    }

    /**
     * Define el valor de la propiedad resumenReporte.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenReporteRBC }
     *     
     */
    public void setResumenReporte(ResumenReporteRBC value) {
        this.resumenReporte = value;
    }

    /**
     * Obtiene el valor de la propiedad hawkAlertConsulta.
     * 
     * @return
     *     possible object is
     *     {@link HawkAlertCRespBC }
     *     
     */
    public HawkAlertCRespBC getHawkAlertConsulta() {
        return hawkAlertConsulta;
    }

    /**
     * Define el valor de la propiedad hawkAlertConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link HawkAlertCRespBC }
     *     
     */
    public void setHawkAlertConsulta(HawkAlertCRespBC value) {
        this.hawkAlertConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad hawkAlertBD.
     * 
     * @return
     *     possible object is
     *     {@link HawkAlertDBRespBC }
     *     
     */
    public HawkAlertDBRespBC getHawkAlertBD() {
        return hawkAlertBD;
    }

    /**
     * Define el valor de la propiedad hawkAlertBD.
     * 
     * @param value
     *     allowed object is
     *     {@link HawkAlertDBRespBC }
     *     
     */
    public void setHawkAlertBD(HawkAlertDBRespBC value) {
        this.hawkAlertBD = value;
    }

    /**
     * Obtiene el valor de la propiedad declaracionesCliente.
     * 
     * @return
     *     possible object is
     *     {@link DeclaracionesClienteRespBC }
     *     
     */
    public DeclaracionesClienteRespBC getDeclaracionesCliente() {
        return declaracionesCliente;
    }

    /**
     * Define el valor de la propiedad declaracionesCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link DeclaracionesClienteRespBC }
     *     
     */
    public void setDeclaracionesCliente(DeclaracionesClienteRespBC value) {
        this.declaracionesCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad scoreBuroCredito.
     * 
     * @return
     *     possible object is
     *     {@link ScoreBCRespBC }
     *     
     */
    public ScoreBCRespBC getScoreBuroCredito() {
        return scoreBuroCredito;
    }

    /**
     * Define el valor de la propiedad scoreBuroCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link ScoreBCRespBC }
     *     
     */
    public void setScoreBuroCredito(ScoreBCRespBC value) {
        this.scoreBuroCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad error.
     * 
     * @return
     *     possible object is
     *     {@link ErrorRespBC }
     *     
     */
    public ErrorRespBC getError() {
        return error;
    }

    /**
     * Define el valor de la propiedad error.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorRespBC }
     *     
     */
    public void setError(ErrorRespBC value) {
        this.error = value;
    }

    /**
     * Obtiene el valor de la propiedad reporteImpreso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporteImpreso() {
        return reporteImpreso;
    }

    /**
     * Define el valor de la propiedad reporteImpreso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporteImpreso(String value) {
        this.reporteImpreso = value;
    }

    /**
     * Obtiene el valor de la propiedad caracteristicas.
     * 
     * @return
     *     possible object is
     *     {@link CaracteristicasRespBC }
     *     
     */
    public CaracteristicasRespBC getCaracteristicas() {
        return caracteristicas;
    }

    /**
     * Define el valor de la propiedad caracteristicas.
     * 
     * @param value
     *     allowed object is
     *     {@link CaracteristicasRespBC }
     *     
     */
    public void setCaracteristicas(CaracteristicasRespBC value) {
        this.caracteristicas = value;
    }

    /**
     * Obtiene el valor de la propiedad reportePDF.
     * 
     * @return
     *     possible object is
     *     {@link ReportePDF }
     *     
     */
    public ReportePDF getReportePDF() {
        return reportePDF;
    }

    /**
     * Define el valor de la propiedad reportePDF.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportePDF }
     *     
     */
    public void setReportePDF(ReportePDF value) {
        this.reportePDF = value;
    }

    /**
     * Obtiene el valor de la propiedad fr.
     * 
     * @return
     *     possible object is
     *     {@link FR }
     *     
     */
    public FR getFR() {
        return fr;
    }

    /**
     * Define el valor de la propiedad fr.
     * 
     * @param value
     *     allowed object is
     *     {@link FR }
     *     
     */
    public void setFR(FR value) {
        this.fr = value;
    }

}
