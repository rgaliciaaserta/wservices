
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Direccion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Direccion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Direccion1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Direccion2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ColoniaPoblacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DelegacionMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Ciudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaResidencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoDomicilio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IndicadorEspecialDomicilio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Direccion", propOrder = {
    "direccion1",
    "direccion2",
    "coloniaPoblacion",
    "delegacionMunicipio",
    "ciudad",
    "estado",
    "cp",
    "fechaResidencia",
    "numeroTelefono",
    "extension",
    "fax",
    "tipoDomicilio",
    "indicadorEspecialDomicilio",
    "codPais"
})
@XmlSeeAlso({
    DireccionRespBC.class
})
public class Direccion {

    @XmlElement(name = "Direccion1")
    protected String direccion1;
    @XmlElement(name = "Direccion2")
    protected String direccion2;
    @XmlElement(name = "ColoniaPoblacion")
    protected String coloniaPoblacion;
    @XmlElement(name = "DelegacionMunicipio")
    protected String delegacionMunicipio;
    @XmlElement(name = "Ciudad")
    protected String ciudad;
    @XmlElement(name = "Estado")
    protected String estado;
    @XmlElement(name = "CP")
    protected String cp;
    @XmlElement(name = "FechaResidencia")
    protected String fechaResidencia;
    @XmlElement(name = "NumeroTelefono")
    protected String numeroTelefono;
    @XmlElement(name = "Extension")
    protected String extension;
    @XmlElement(name = "Fax")
    protected String fax;
    @XmlElement(name = "TipoDomicilio")
    protected String tipoDomicilio;
    @XmlElement(name = "IndicadorEspecialDomicilio")
    protected String indicadorEspecialDomicilio;
    @XmlElement(name = "CodPais")
    protected String codPais;

    /**
     * Obtiene el valor de la propiedad direccion1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion1() {
        return direccion1;
    }

    /**
     * Define el valor de la propiedad direccion1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion1(String value) {
        this.direccion1 = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion2() {
        return direccion2;
    }

    /**
     * Define el valor de la propiedad direccion2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion2(String value) {
        this.direccion2 = value;
    }

    /**
     * Obtiene el valor de la propiedad coloniaPoblacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColoniaPoblacion() {
        return coloniaPoblacion;
    }

    /**
     * Define el valor de la propiedad coloniaPoblacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColoniaPoblacion(String value) {
        this.coloniaPoblacion = value;
    }

    /**
     * Obtiene el valor de la propiedad delegacionMunicipio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegacionMunicipio() {
        return delegacionMunicipio;
    }

    /**
     * Define el valor de la propiedad delegacionMunicipio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegacionMunicipio(String value) {
        this.delegacionMunicipio = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Define el valor de la propiedad ciudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad(String value) {
        this.ciudad = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad cp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCP() {
        return cp;
    }

    /**
     * Define el valor de la propiedad cp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCP(String value) {
        this.cp = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaResidencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaResidencia() {
        return fechaResidencia;
    }

    /**
     * Define el valor de la propiedad fechaResidencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaResidencia(String value) {
        this.fechaResidencia = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    /**
     * Define el valor de la propiedad numeroTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefono(String value) {
        this.numeroTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Obtiene el valor de la propiedad fax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Define el valor de la propiedad fax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDomicilio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDomicilio() {
        return tipoDomicilio;
    }

    /**
     * Define el valor de la propiedad tipoDomicilio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDomicilio(String value) {
        this.tipoDomicilio = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorEspecialDomicilio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorEspecialDomicilio() {
        return indicadorEspecialDomicilio;
    }

    /**
     * Define el valor de la propiedad indicadorEspecialDomicilio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorEspecialDomicilio(String value) {
        this.indicadorEspecialDomicilio = value;
    }

    /**
     * Obtiene el valor de la propiedad codPais.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPais() {
        return codPais;
    }

    /**
     * Define el valor de la propiedad codPais.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPais(String value) {
        this.codPais = value;
    }

}
