
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para NombreBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="NombreBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ApellidoPaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApellidoMaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApellidoAdicional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PrimerNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SegundoNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Prefijo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Sufijo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Nacionalidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Residencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroLicenciaConducir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroCedulaProfesional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroRegistroElectoral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveImpuestosOtroPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOtroPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroDependientes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EdadesDependientes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NombreBC", propOrder = {
    "apellidoPaterno",
    "apellidoMaterno",
    "apellidoAdicional",
    "primerNombre",
    "segundoNombre",
    "fechaNacimiento",
    "rfc",
    "prefijo",
    "sufijo",
    "nacionalidad",
    "residencia",
    "numeroLicenciaConducir",
    "estadoCivil",
    "sexo",
    "numeroCedulaProfesional",
    "numeroRegistroElectoral",
    "claveImpuestosOtroPais",
    "claveOtroPais",
    "numeroDependientes",
    "edadesDependientes"
})
@XmlSeeAlso({
    NombreRespBC.class
})
public class NombreBC {

    @XmlElement(name = "ApellidoPaterno")
    protected String apellidoPaterno;
    @XmlElement(name = "ApellidoMaterno")
    protected String apellidoMaterno;
    @XmlElement(name = "ApellidoAdicional")
    protected String apellidoAdicional;
    @XmlElement(name = "PrimerNombre")
    protected String primerNombre;
    @XmlElement(name = "SegundoNombre")
    protected String segundoNombre;
    @XmlElement(name = "FechaNacimiento")
    protected String fechaNacimiento;
    @XmlElement(name = "RFC")
    protected String rfc;
    @XmlElement(name = "Prefijo")
    protected String prefijo;
    @XmlElement(name = "Sufijo")
    protected String sufijo;
    @XmlElement(name = "Nacionalidad")
    protected String nacionalidad;
    @XmlElement(name = "Residencia")
    protected String residencia;
    @XmlElement(name = "NumeroLicenciaConducir")
    protected String numeroLicenciaConducir;
    @XmlElement(name = "EstadoCivil")
    protected String estadoCivil;
    @XmlElement(name = "Sexo")
    protected String sexo;
    @XmlElement(name = "NumeroCedulaProfesional")
    protected String numeroCedulaProfesional;
    @XmlElement(name = "NumeroRegistroElectoral")
    protected String numeroRegistroElectoral;
    @XmlElement(name = "ClaveImpuestosOtroPais")
    protected String claveImpuestosOtroPais;
    @XmlElement(name = "ClaveOtroPais")
    protected String claveOtroPais;
    @XmlElement(name = "NumeroDependientes")
    protected String numeroDependientes;
    @XmlElement(name = "EdadesDependientes")
    protected String edadesDependientes;

    /**
     * Obtiene el valor de la propiedad apellidoPaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * Define el valor de la propiedad apellidoPaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoPaterno(String value) {
        this.apellidoPaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoMaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * Define el valor de la propiedad apellidoMaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoMaterno(String value) {
        this.apellidoMaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoAdicional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoAdicional() {
        return apellidoAdicional;
    }

    /**
     * Define el valor de la propiedad apellidoAdicional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoAdicional(String value) {
        this.apellidoAdicional = value;
    }

    /**
     * Obtiene el valor de la propiedad primerNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * Define el valor de la propiedad primerNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimerNombre(String value) {
        this.primerNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad segundoNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * Define el valor de la propiedad segundoNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegundoNombre(String value) {
        this.segundoNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaNacimiento(String value) {
        this.fechaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad rfc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFC() {
        return rfc;
    }

    /**
     * Define el valor de la propiedad rfc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFC(String value) {
        this.rfc = value;
    }

    /**
     * Obtiene el valor de la propiedad prefijo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * Define el valor de la propiedad prefijo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefijo(String value) {
        this.prefijo = value;
    }

    /**
     * Obtiene el valor de la propiedad sufijo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSufijo() {
        return sufijo;
    }

    /**
     * Define el valor de la propiedad sufijo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSufijo(String value) {
        this.sufijo = value;
    }

    /**
     * Obtiene el valor de la propiedad nacionalidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * Define el valor de la propiedad nacionalidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNacionalidad(String value) {
        this.nacionalidad = value;
    }

    /**
     * Obtiene el valor de la propiedad residencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencia() {
        return residencia;
    }

    /**
     * Define el valor de la propiedad residencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencia(String value) {
        this.residencia = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroLicenciaConducir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLicenciaConducir() {
        return numeroLicenciaConducir;
    }

    /**
     * Define el valor de la propiedad numeroLicenciaConducir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLicenciaConducir(String value) {
        this.numeroLicenciaConducir = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Define el valor de la propiedad estadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoCivil(String value) {
        this.estadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad sexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Define el valor de la propiedad sexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSexo(String value) {
        this.sexo = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCedulaProfesional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCedulaProfesional() {
        return numeroCedulaProfesional;
    }

    /**
     * Define el valor de la propiedad numeroCedulaProfesional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCedulaProfesional(String value) {
        this.numeroCedulaProfesional = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroRegistroElectoral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroRegistroElectoral() {
        return numeroRegistroElectoral;
    }

    /**
     * Define el valor de la propiedad numeroRegistroElectoral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroRegistroElectoral(String value) {
        this.numeroRegistroElectoral = value;
    }

    /**
     * Obtiene el valor de la propiedad claveImpuestosOtroPais.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveImpuestosOtroPais() {
        return claveImpuestosOtroPais;
    }

    /**
     * Define el valor de la propiedad claveImpuestosOtroPais.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveImpuestosOtroPais(String value) {
        this.claveImpuestosOtroPais = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOtroPais.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtroPais() {
        return claveOtroPais;
    }

    /**
     * Define el valor de la propiedad claveOtroPais.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtroPais(String value) {
        this.claveOtroPais = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDependientes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDependientes() {
        return numeroDependientes;
    }

    /**
     * Define el valor de la propiedad numeroDependientes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDependientes(String value) {
        this.numeroDependientes = value;
    }

    /**
     * Obtiene el valor de la propiedad edadesDependientes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdadesDependientes() {
        return edadesDependientes;
    }

    /**
     * Define el valor de la propiedad edadesDependientes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdadesDependientes(String value) {
        this.edadesDependientes = value;
    }

}
