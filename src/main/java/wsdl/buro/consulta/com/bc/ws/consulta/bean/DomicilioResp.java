
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DomicilioResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DomicilioResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://bean.consulta.ws.bc.com/}Domicilio"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaRegistroDomicilio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DomicilioResp", propOrder = {
    "fechaRegistroDomicilio"
})
public class DomicilioResp
    extends Domicilio
{

    @XmlElement(name = "FechaRegistroDomicilio")
    protected String fechaRegistroDomicilio;

    /**
     * Obtiene el valor de la propiedad fechaRegistroDomicilio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRegistroDomicilio() {
        return fechaRegistroDomicilio;
    }

    /**
     * Define el valor de la propiedad fechaRegistroDomicilio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRegistroDomicilio(String value) {
        this.fechaRegistroDomicilio = value;
    }

}
