
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CuentaResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CuentaResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaActualizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegistroImpugnado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NombreOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentaActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoResponsabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUnidadMonetaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ValorActivoValuacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FrecuenciaPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MontoPagar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAperturaCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaUltimoPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaUltimaCompra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaCierreCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaReporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UltimaFechaSaldoCero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Garantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreditoMaximo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SaldoActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LimiteCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SaldoVencido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroPagosVencidos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PagoActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HistoricoPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaRecienteHistoricoPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAntiguaHistoricoPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClavePrevencion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosReportados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PeorAtraso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaPeorAtraso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SaldoVencidoPeorAtraso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MontoUltimoPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CuentaResp", propOrder = {
    "fechaActualizacion",
    "registroImpugnado",
    "claveOtorgante",
    "nombreOtorgante",
    "cuentaActual",
    "tipoResponsabilidad",
    "tipoCuenta",
    "tipoCredito",
    "claveUnidadMonetaria",
    "valorActivoValuacion",
    "numeroPagos",
    "frecuenciaPagos",
    "montoPagar",
    "fechaAperturaCuenta",
    "fechaUltimoPago",
    "fechaUltimaCompra",
    "fechaCierreCuenta",
    "fechaReporte",
    "ultimaFechaSaldoCero",
    "garantia",
    "creditoMaximo",
    "saldoActual",
    "limiteCredito",
    "saldoVencido",
    "numeroPagosVencidos",
    "pagoActual",
    "historicoPagos",
    "fechaRecienteHistoricoPagos",
    "fechaAntiguaHistoricoPagos",
    "clavePrevencion",
    "totalPagosReportados",
    "peorAtraso",
    "fechaPeorAtraso",
    "saldoVencidoPeorAtraso",
    "montoUltimoPago"
})
public class CuentaResp {

    @XmlElement(name = "FechaActualizacion")
    protected String fechaActualizacion;
    @XmlElement(name = "RegistroImpugnado")
    protected String registroImpugnado;
    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "NombreOtorgante")
    protected String nombreOtorgante;
    @XmlElement(name = "CuentaActual")
    protected String cuentaActual;
    @XmlElement(name = "TipoResponsabilidad")
    protected String tipoResponsabilidad;
    @XmlElement(name = "TipoCuenta")
    protected String tipoCuenta;
    @XmlElement(name = "TipoCredito")
    protected String tipoCredito;
    @XmlElement(name = "ClaveUnidadMonetaria")
    protected String claveUnidadMonetaria;
    @XmlElement(name = "ValorActivoValuacion")
    protected String valorActivoValuacion;
    @XmlElement(name = "NumeroPagos")
    protected String numeroPagos;
    @XmlElement(name = "FrecuenciaPagos")
    protected String frecuenciaPagos;
    @XmlElement(name = "MontoPagar")
    protected String montoPagar;
    @XmlElement(name = "FechaAperturaCuenta")
    protected String fechaAperturaCuenta;
    @XmlElement(name = "FechaUltimoPago")
    protected String fechaUltimoPago;
    @XmlElement(name = "FechaUltimaCompra")
    protected String fechaUltimaCompra;
    @XmlElement(name = "FechaCierreCuenta")
    protected String fechaCierreCuenta;
    @XmlElement(name = "FechaReporte")
    protected String fechaReporte;
    @XmlElement(name = "UltimaFechaSaldoCero")
    protected String ultimaFechaSaldoCero;
    @XmlElement(name = "Garantia")
    protected String garantia;
    @XmlElement(name = "CreditoMaximo")
    protected String creditoMaximo;
    @XmlElement(name = "SaldoActual")
    protected String saldoActual;
    @XmlElement(name = "LimiteCredito")
    protected String limiteCredito;
    @XmlElement(name = "SaldoVencido")
    protected String saldoVencido;
    @XmlElement(name = "NumeroPagosVencidos")
    protected String numeroPagosVencidos;
    @XmlElement(name = "PagoActual")
    protected String pagoActual;
    @XmlElement(name = "HistoricoPagos")
    protected String historicoPagos;
    @XmlElement(name = "FechaRecienteHistoricoPagos")
    protected String fechaRecienteHistoricoPagos;
    @XmlElement(name = "FechaAntiguaHistoricoPagos")
    protected String fechaAntiguaHistoricoPagos;
    @XmlElement(name = "ClavePrevencion")
    protected String clavePrevencion;
    @XmlElement(name = "TotalPagosReportados")
    protected String totalPagosReportados;
    @XmlElement(name = "PeorAtraso")
    protected String peorAtraso;
    @XmlElement(name = "FechaPeorAtraso")
    protected String fechaPeorAtraso;
    @XmlElement(name = "SaldoVencidoPeorAtraso")
    protected String saldoVencidoPeorAtraso;
    @XmlElement(name = "MontoUltimoPago")
    protected String montoUltimoPago;

    /**
     * Obtiene el valor de la propiedad fechaActualizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    /**
     * Define el valor de la propiedad fechaActualizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaActualizacion(String value) {
        this.fechaActualizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad registroImpugnado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroImpugnado() {
        return registroImpugnado;
    }

    /**
     * Define el valor de la propiedad registroImpugnado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroImpugnado(String value) {
        this.registroImpugnado = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOtorgante() {
        return nombreOtorgante;
    }

    /**
     * Define el valor de la propiedad nombreOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOtorgante(String value) {
        this.nombreOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaActual() {
        return cuentaActual;
    }

    /**
     * Define el valor de la propiedad cuentaActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaActual(String value) {
        this.cuentaActual = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoResponsabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoResponsabilidad() {
        return tipoResponsabilidad;
    }

    /**
     * Define el valor de la propiedad tipoResponsabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoResponsabilidad(String value) {
        this.tipoResponsabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Define el valor de la propiedad tipoCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCuenta(String value) {
        this.tipoCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCredito() {
        return tipoCredito;
    }

    /**
     * Define el valor de la propiedad tipoCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCredito(String value) {
        this.tipoCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUnidadMonetaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUnidadMonetaria() {
        return claveUnidadMonetaria;
    }

    /**
     * Define el valor de la propiedad claveUnidadMonetaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUnidadMonetaria(String value) {
        this.claveUnidadMonetaria = value;
    }

    /**
     * Obtiene el valor de la propiedad valorActivoValuacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorActivoValuacion() {
        return valorActivoValuacion;
    }

    /**
     * Define el valor de la propiedad valorActivoValuacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorActivoValuacion(String value) {
        this.valorActivoValuacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPagos() {
        return numeroPagos;
    }

    /**
     * Define el valor de la propiedad numeroPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPagos(String value) {
        this.numeroPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad frecuenciaPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrecuenciaPagos() {
        return frecuenciaPagos;
    }

    /**
     * Define el valor de la propiedad frecuenciaPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrecuenciaPagos(String value) {
        this.frecuenciaPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad montoPagar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoPagar() {
        return montoPagar;
    }

    /**
     * Define el valor de la propiedad montoPagar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoPagar(String value) {
        this.montoPagar = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAperturaCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAperturaCuenta() {
        return fechaAperturaCuenta;
    }

    /**
     * Define el valor de la propiedad fechaAperturaCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAperturaCuenta(String value) {
        this.fechaAperturaCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaUltimoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    /**
     * Define el valor de la propiedad fechaUltimoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaUltimoPago(String value) {
        this.fechaUltimoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaUltimaCompra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaUltimaCompra() {
        return fechaUltimaCompra;
    }

    /**
     * Define el valor de la propiedad fechaUltimaCompra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaUltimaCompra(String value) {
        this.fechaUltimaCompra = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCierreCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCierreCuenta() {
        return fechaCierreCuenta;
    }

    /**
     * Define el valor de la propiedad fechaCierreCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCierreCuenta(String value) {
        this.fechaCierreCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaReporte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaReporte() {
        return fechaReporte;
    }

    /**
     * Define el valor de la propiedad fechaReporte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaReporte(String value) {
        this.fechaReporte = value;
    }

    /**
     * Obtiene el valor de la propiedad ultimaFechaSaldoCero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUltimaFechaSaldoCero() {
        return ultimaFechaSaldoCero;
    }

    /**
     * Define el valor de la propiedad ultimaFechaSaldoCero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUltimaFechaSaldoCero(String value) {
        this.ultimaFechaSaldoCero = value;
    }

    /**
     * Obtiene el valor de la propiedad garantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * Define el valor de la propiedad garantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGarantia(String value) {
        this.garantia = value;
    }

    /**
     * Obtiene el valor de la propiedad creditoMaximo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditoMaximo() {
        return creditoMaximo;
    }

    /**
     * Define el valor de la propiedad creditoMaximo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditoMaximo(String value) {
        this.creditoMaximo = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoActual() {
        return saldoActual;
    }

    /**
     * Define el valor de la propiedad saldoActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoActual(String value) {
        this.saldoActual = value;
    }

    /**
     * Obtiene el valor de la propiedad limiteCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimiteCredito() {
        return limiteCredito;
    }

    /**
     * Define el valor de la propiedad limiteCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimiteCredito(String value) {
        this.limiteCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoVencido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoVencido() {
        return saldoVencido;
    }

    /**
     * Define el valor de la propiedad saldoVencido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoVencido(String value) {
        this.saldoVencido = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroPagosVencidos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPagosVencidos() {
        return numeroPagosVencidos;
    }

    /**
     * Define el valor de la propiedad numeroPagosVencidos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPagosVencidos(String value) {
        this.numeroPagosVencidos = value;
    }

    /**
     * Obtiene el valor de la propiedad pagoActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPagoActual() {
        return pagoActual;
    }

    /**
     * Define el valor de la propiedad pagoActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPagoActual(String value) {
        this.pagoActual = value;
    }

    /**
     * Obtiene el valor de la propiedad historicoPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoricoPagos() {
        return historicoPagos;
    }

    /**
     * Define el valor de la propiedad historicoPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoricoPagos(String value) {
        this.historicoPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRecienteHistoricoPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRecienteHistoricoPagos() {
        return fechaRecienteHistoricoPagos;
    }

    /**
     * Define el valor de la propiedad fechaRecienteHistoricoPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRecienteHistoricoPagos(String value) {
        this.fechaRecienteHistoricoPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAntiguaHistoricoPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAntiguaHistoricoPagos() {
        return fechaAntiguaHistoricoPagos;
    }

    /**
     * Define el valor de la propiedad fechaAntiguaHistoricoPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAntiguaHistoricoPagos(String value) {
        this.fechaAntiguaHistoricoPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad clavePrevencion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClavePrevencion() {
        return clavePrevencion;
    }

    /**
     * Define el valor de la propiedad clavePrevencion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClavePrevencion(String value) {
        this.clavePrevencion = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosReportados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosReportados() {
        return totalPagosReportados;
    }

    /**
     * Define el valor de la propiedad totalPagosReportados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosReportados(String value) {
        this.totalPagosReportados = value;
    }

    /**
     * Obtiene el valor de la propiedad peorAtraso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeorAtraso() {
        return peorAtraso;
    }

    /**
     * Define el valor de la propiedad peorAtraso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeorAtraso(String value) {
        this.peorAtraso = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaPeorAtraso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaPeorAtraso() {
        return fechaPeorAtraso;
    }

    /**
     * Define el valor de la propiedad fechaPeorAtraso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaPeorAtraso(String value) {
        this.fechaPeorAtraso = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoVencidoPeorAtraso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoVencidoPeorAtraso() {
        return saldoVencidoPeorAtraso;
    }

    /**
     * Define el valor de la propiedad saldoVencidoPeorAtraso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoVencidoPeorAtraso(String value) {
        this.saldoVencidoPeorAtraso = value;
    }

    /**
     * Obtiene el valor de la propiedad montoUltimoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoUltimoPago() {
        return montoUltimoPago;
    }

    /**
     * Define el valor de la propiedad montoUltimoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoUltimoPago(String value) {
        this.montoUltimoPago = value;
    }

}
