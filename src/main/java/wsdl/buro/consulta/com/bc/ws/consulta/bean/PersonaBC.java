
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PersonaBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Encabezado" type="{http://bean.consulta.ws.bc.com/}EncabezadoBC"/&gt;
 *         &lt;element name="Nombre" type="{http://bean.consulta.ws.bc.com/}NombreBC" minOccurs="0"/&gt;
 *         &lt;element name="Domicilios" type="{http://bean.consulta.ws.bc.com/}DomiciliosBC"/&gt;
 *         &lt;element name="Empleos" type="{http://bean.consulta.ws.bc.com/}EmpleosBC" minOccurs="0"/&gt;
 *         &lt;element name="CuentaC" type="{http://bean.consulta.ws.bc.com/}CuentaClienteBC" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Autentica" type="{http://bean.consulta.ws.bc.com/}AutenticacionBC" minOccurs="0"/&gt;
 *         &lt;element name="Caracteristicas" type="{http://bean.consulta.ws.bc.com/}Caracteristicas" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaBC", propOrder = {
    "encabezado",
    "nombre",
    "domicilios",
    "empleos",
    "cuentaC",
    "autentica",
    "caracteristicas"
})
public class PersonaBC {

    @XmlElement(name = "Encabezado", required = true)
    protected EncabezadoBC encabezado;
    @XmlElement(name = "Nombre")
    protected NombreBC nombre;
    @XmlElement(name = "Domicilios", required = true)
    protected DomiciliosBC domicilios;
    @XmlElement(name = "Empleos")
    protected EmpleosBC empleos;
    @XmlElement(name = "CuentaC", nillable = true)
    protected List<CuentaClienteBC> cuentaC;
    @XmlElement(name = "Autentica")
    protected AutenticacionBC autentica;
    @XmlElement(name = "Caracteristicas")
    protected Caracteristicas caracteristicas;

    /**
     * Obtiene el valor de la propiedad encabezado.
     * 
     * @return
     *     possible object is
     *     {@link EncabezadoBC }
     *     
     */
    public EncabezadoBC getEncabezado() {
        return encabezado;
    }

    /**
     * Define el valor de la propiedad encabezado.
     * 
     * @param value
     *     allowed object is
     *     {@link EncabezadoBC }
     *     
     */
    public void setEncabezado(EncabezadoBC value) {
        this.encabezado = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link NombreBC }
     *     
     */
    public NombreBC getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link NombreBC }
     *     
     */
    public void setNombre(NombreBC value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad domicilios.
     * 
     * @return
     *     possible object is
     *     {@link DomiciliosBC }
     *     
     */
    public DomiciliosBC getDomicilios() {
        return domicilios;
    }

    /**
     * Define el valor de la propiedad domicilios.
     * 
     * @param value
     *     allowed object is
     *     {@link DomiciliosBC }
     *     
     */
    public void setDomicilios(DomiciliosBC value) {
        this.domicilios = value;
    }

    /**
     * Obtiene el valor de la propiedad empleos.
     * 
     * @return
     *     possible object is
     *     {@link EmpleosBC }
     *     
     */
    public EmpleosBC getEmpleos() {
        return empleos;
    }

    /**
     * Define el valor de la propiedad empleos.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpleosBC }
     *     
     */
    public void setEmpleos(EmpleosBC value) {
        this.empleos = value;
    }

    /**
     * Gets the value of the cuentaC property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cuentaC property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCuentaC().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CuentaClienteBC }
     * 
     * 
     */
    public List<CuentaClienteBC> getCuentaC() {
        if (cuentaC == null) {
            cuentaC = new ArrayList<CuentaClienteBC>();
        }
        return this.cuentaC;
    }

    /**
     * Obtiene el valor de la propiedad autentica.
     * 
     * @return
     *     possible object is
     *     {@link AutenticacionBC }
     *     
     */
    public AutenticacionBC getAutentica() {
        return autentica;
    }

    /**
     * Define el valor de la propiedad autentica.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticacionBC }
     *     
     */
    public void setAutentica(AutenticacionBC value) {
        this.autentica = value;
    }

    /**
     * Obtiene el valor de la propiedad caracteristicas.
     * 
     * @return
     *     possible object is
     *     {@link Caracteristicas }
     *     
     */
    public Caracteristicas getCaracteristicas() {
        return caracteristicas;
    }

    /**
     * Define el valor de la propiedad caracteristicas.
     * 
     * @param value
     *     allowed object is
     *     {@link Caracteristicas }
     *     
     */
    public void setCaracteristicas(Caracteristicas value) {
        this.caracteristicas = value;
    }

}
