
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EmpleoResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EmpleoResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://bean.consulta.ws.bc.com/}Empleo"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaVerificacionEmpleo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmpleoResp", propOrder = {
    "fechaVerificacionEmpleo"
})
public class EmpleoResp
    extends Empleo
{

    @XmlElement(name = "FechaVerificacionEmpleo")
    protected String fechaVerificacionEmpleo;

    /**
     * Obtiene el valor de la propiedad fechaVerificacionEmpleo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVerificacionEmpleo() {
        return fechaVerificacionEmpleo;
    }

    /**
     * Define el valor de la propiedad fechaVerificacionEmpleo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVerificacionEmpleo(String value) {
        this.fechaVerificacionEmpleo = value;
    }

}
