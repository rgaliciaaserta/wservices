
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CuentasRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CuentasRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaActualizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegistroImpugnado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NombreOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroTelefonoOtorgante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdentificadorSociedadInformacionCrediticia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroCuentaActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IndicadorTipoResponsabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveUnidadMonetaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ValorActivoValuacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FrecuenciaPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MontoPagar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAperturaCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaUltimoPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaUltimaCompra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaCierreCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaReporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ModoReportar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UltimaFechaSaldoCero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Garantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreditoMaximo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SaldoActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LimiteCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SaldoVencido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroPagosVencidos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FormaPagoActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HistoricoPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaMasRecienteHistoricoPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaMasAntiguaHistoricoPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosReportados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosCalificadosMOP2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosCalificadosMOP3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosCalificadosMOP4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosCalificadosMOP5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImporteSaldoMorosidadHistMasGrave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaHistoricaMorosidadMasGrave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MopHistoricoMorosidadMasGrave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaInicioReestructura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MontoUltimoPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CuentasRespBC", propOrder = {
    "fechaActualizacion",
    "registroImpugnado",
    "claveOtorgante",
    "nombreOtorgante",
    "numeroTelefonoOtorgante",
    "identificadorSociedadInformacionCrediticia",
    "numeroCuentaActual",
    "indicadorTipoResponsabilidad",
    "tipoCuenta",
    "tipoContrato",
    "claveUnidadMonetaria",
    "valorActivoValuacion",
    "numeroPagos",
    "frecuenciaPagos",
    "montoPagar",
    "fechaAperturaCuenta",
    "fechaUltimoPago",
    "fechaUltimaCompra",
    "fechaCierreCuenta",
    "fechaReporte",
    "modoReportar",
    "ultimaFechaSaldoCero",
    "garantia",
    "creditoMaximo",
    "saldoActual",
    "limiteCredito",
    "saldoVencido",
    "numeroPagosVencidos",
    "formaPagoActual",
    "historicoPagos",
    "fechaMasRecienteHistoricoPagos",
    "fechaMasAntiguaHistoricoPagos",
    "claveObservacion",
    "totalPagosReportados",
    "totalPagosCalificadosMOP2",
    "totalPagosCalificadosMOP3",
    "totalPagosCalificadosMOP4",
    "totalPagosCalificadosMOP5",
    "importeSaldoMorosidadHistMasGrave",
    "fechaHistoricaMorosidadMasGrave",
    "mopHistoricoMorosidadMasGrave",
    "fechaInicioReestructura",
    "montoUltimoPago"
})
public class CuentasRespBC {

    @XmlElement(name = "FechaActualizacion")
    protected String fechaActualizacion;
    @XmlElement(name = "RegistroImpugnado")
    protected String registroImpugnado;
    @XmlElement(name = "ClaveOtorgante")
    protected String claveOtorgante;
    @XmlElement(name = "NombreOtorgante")
    protected String nombreOtorgante;
    @XmlElement(name = "NumeroTelefonoOtorgante")
    protected String numeroTelefonoOtorgante;
    @XmlElement(name = "IdentificadorSociedadInformacionCrediticia")
    protected String identificadorSociedadInformacionCrediticia;
    @XmlElement(name = "NumeroCuentaActual")
    protected String numeroCuentaActual;
    @XmlElement(name = "IndicadorTipoResponsabilidad")
    protected String indicadorTipoResponsabilidad;
    @XmlElement(name = "TipoCuenta")
    protected String tipoCuenta;
    @XmlElement(name = "TipoContrato")
    protected String tipoContrato;
    @XmlElement(name = "ClaveUnidadMonetaria")
    protected String claveUnidadMonetaria;
    @XmlElement(name = "ValorActivoValuacion")
    protected String valorActivoValuacion;
    @XmlElement(name = "NumeroPagos")
    protected String numeroPagos;
    @XmlElement(name = "FrecuenciaPagos")
    protected String frecuenciaPagos;
    @XmlElement(name = "MontoPagar")
    protected String montoPagar;
    @XmlElement(name = "FechaAperturaCuenta")
    protected String fechaAperturaCuenta;
    @XmlElement(name = "FechaUltimoPago")
    protected String fechaUltimoPago;
    @XmlElement(name = "FechaUltimaCompra")
    protected String fechaUltimaCompra;
    @XmlElement(name = "FechaCierreCuenta")
    protected String fechaCierreCuenta;
    @XmlElement(name = "FechaReporte")
    protected String fechaReporte;
    @XmlElement(name = "ModoReportar")
    protected String modoReportar;
    @XmlElement(name = "UltimaFechaSaldoCero")
    protected String ultimaFechaSaldoCero;
    @XmlElement(name = "Garantia")
    protected String garantia;
    @XmlElement(name = "CreditoMaximo")
    protected String creditoMaximo;
    @XmlElement(name = "SaldoActual")
    protected String saldoActual;
    @XmlElement(name = "LimiteCredito")
    protected String limiteCredito;
    @XmlElement(name = "SaldoVencido")
    protected String saldoVencido;
    @XmlElement(name = "NumeroPagosVencidos")
    protected String numeroPagosVencidos;
    @XmlElement(name = "FormaPagoActual")
    protected String formaPagoActual;
    @XmlElement(name = "HistoricoPagos")
    protected String historicoPagos;
    @XmlElement(name = "FechaMasRecienteHistoricoPagos")
    protected String fechaMasRecienteHistoricoPagos;
    @XmlElement(name = "FechaMasAntiguaHistoricoPagos")
    protected String fechaMasAntiguaHistoricoPagos;
    @XmlElement(name = "ClaveObservacion")
    protected String claveObservacion;
    @XmlElement(name = "TotalPagosReportados")
    protected String totalPagosReportados;
    @XmlElement(name = "TotalPagosCalificadosMOP2")
    protected String totalPagosCalificadosMOP2;
    @XmlElement(name = "TotalPagosCalificadosMOP3")
    protected String totalPagosCalificadosMOP3;
    @XmlElement(name = "TotalPagosCalificadosMOP4")
    protected String totalPagosCalificadosMOP4;
    @XmlElement(name = "TotalPagosCalificadosMOP5")
    protected String totalPagosCalificadosMOP5;
    @XmlElement(name = "ImporteSaldoMorosidadHistMasGrave")
    protected String importeSaldoMorosidadHistMasGrave;
    @XmlElement(name = "FechaHistoricaMorosidadMasGrave")
    protected String fechaHistoricaMorosidadMasGrave;
    @XmlElement(name = "MopHistoricoMorosidadMasGrave")
    protected String mopHistoricoMorosidadMasGrave;
    @XmlElement(name = "FechaInicioReestructura")
    protected String fechaInicioReestructura;
    @XmlElement(name = "MontoUltimoPago")
    protected String montoUltimoPago;

    /**
     * Obtiene el valor de la propiedad fechaActualizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    /**
     * Define el valor de la propiedad fechaActualizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaActualizacion(String value) {
        this.fechaActualizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad registroImpugnado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroImpugnado() {
        return registroImpugnado;
    }

    /**
     * Define el valor de la propiedad registroImpugnado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroImpugnado(String value) {
        this.registroImpugnado = value;
    }

    /**
     * Obtiene el valor de la propiedad claveOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveOtorgante() {
        return claveOtorgante;
    }

    /**
     * Define el valor de la propiedad claveOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveOtorgante(String value) {
        this.claveOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOtorgante() {
        return nombreOtorgante;
    }

    /**
     * Define el valor de la propiedad nombreOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOtorgante(String value) {
        this.nombreOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroTelefonoOtorgante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefonoOtorgante() {
        return numeroTelefonoOtorgante;
    }

    /**
     * Define el valor de la propiedad numeroTelefonoOtorgante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefonoOtorgante(String value) {
        this.numeroTelefonoOtorgante = value;
    }

    /**
     * Obtiene el valor de la propiedad identificadorSociedadInformacionCrediticia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorSociedadInformacionCrediticia() {
        return identificadorSociedadInformacionCrediticia;
    }

    /**
     * Define el valor de la propiedad identificadorSociedadInformacionCrediticia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorSociedadInformacionCrediticia(String value) {
        this.identificadorSociedadInformacionCrediticia = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCuentaActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCuentaActual() {
        return numeroCuentaActual;
    }

    /**
     * Define el valor de la propiedad numeroCuentaActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCuentaActual(String value) {
        this.numeroCuentaActual = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorTipoResponsabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorTipoResponsabilidad() {
        return indicadorTipoResponsabilidad;
    }

    /**
     * Define el valor de la propiedad indicadorTipoResponsabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorTipoResponsabilidad(String value) {
        this.indicadorTipoResponsabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Define el valor de la propiedad tipoCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCuenta(String value) {
        this.tipoCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Define el valor de la propiedad tipoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContrato(String value) {
        this.tipoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad claveUnidadMonetaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveUnidadMonetaria() {
        return claveUnidadMonetaria;
    }

    /**
     * Define el valor de la propiedad claveUnidadMonetaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveUnidadMonetaria(String value) {
        this.claveUnidadMonetaria = value;
    }

    /**
     * Obtiene el valor de la propiedad valorActivoValuacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorActivoValuacion() {
        return valorActivoValuacion;
    }

    /**
     * Define el valor de la propiedad valorActivoValuacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorActivoValuacion(String value) {
        this.valorActivoValuacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPagos() {
        return numeroPagos;
    }

    /**
     * Define el valor de la propiedad numeroPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPagos(String value) {
        this.numeroPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad frecuenciaPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrecuenciaPagos() {
        return frecuenciaPagos;
    }

    /**
     * Define el valor de la propiedad frecuenciaPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrecuenciaPagos(String value) {
        this.frecuenciaPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad montoPagar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoPagar() {
        return montoPagar;
    }

    /**
     * Define el valor de la propiedad montoPagar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoPagar(String value) {
        this.montoPagar = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAperturaCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAperturaCuenta() {
        return fechaAperturaCuenta;
    }

    /**
     * Define el valor de la propiedad fechaAperturaCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAperturaCuenta(String value) {
        this.fechaAperturaCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaUltimoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    /**
     * Define el valor de la propiedad fechaUltimoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaUltimoPago(String value) {
        this.fechaUltimoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaUltimaCompra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaUltimaCompra() {
        return fechaUltimaCompra;
    }

    /**
     * Define el valor de la propiedad fechaUltimaCompra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaUltimaCompra(String value) {
        this.fechaUltimaCompra = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCierreCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCierreCuenta() {
        return fechaCierreCuenta;
    }

    /**
     * Define el valor de la propiedad fechaCierreCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCierreCuenta(String value) {
        this.fechaCierreCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaReporte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaReporte() {
        return fechaReporte;
    }

    /**
     * Define el valor de la propiedad fechaReporte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaReporte(String value) {
        this.fechaReporte = value;
    }

    /**
     * Obtiene el valor de la propiedad modoReportar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModoReportar() {
        return modoReportar;
    }

    /**
     * Define el valor de la propiedad modoReportar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModoReportar(String value) {
        this.modoReportar = value;
    }

    /**
     * Obtiene el valor de la propiedad ultimaFechaSaldoCero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUltimaFechaSaldoCero() {
        return ultimaFechaSaldoCero;
    }

    /**
     * Define el valor de la propiedad ultimaFechaSaldoCero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUltimaFechaSaldoCero(String value) {
        this.ultimaFechaSaldoCero = value;
    }

    /**
     * Obtiene el valor de la propiedad garantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * Define el valor de la propiedad garantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGarantia(String value) {
        this.garantia = value;
    }

    /**
     * Obtiene el valor de la propiedad creditoMaximo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditoMaximo() {
        return creditoMaximo;
    }

    /**
     * Define el valor de la propiedad creditoMaximo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditoMaximo(String value) {
        this.creditoMaximo = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoActual() {
        return saldoActual;
    }

    /**
     * Define el valor de la propiedad saldoActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoActual(String value) {
        this.saldoActual = value;
    }

    /**
     * Obtiene el valor de la propiedad limiteCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimiteCredito() {
        return limiteCredito;
    }

    /**
     * Define el valor de la propiedad limiteCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimiteCredito(String value) {
        this.limiteCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoVencido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoVencido() {
        return saldoVencido;
    }

    /**
     * Define el valor de la propiedad saldoVencido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoVencido(String value) {
        this.saldoVencido = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroPagosVencidos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPagosVencidos() {
        return numeroPagosVencidos;
    }

    /**
     * Define el valor de la propiedad numeroPagosVencidos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPagosVencidos(String value) {
        this.numeroPagosVencidos = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPagoActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPagoActual() {
        return formaPagoActual;
    }

    /**
     * Define el valor de la propiedad formaPagoActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPagoActual(String value) {
        this.formaPagoActual = value;
    }

    /**
     * Obtiene el valor de la propiedad historicoPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoricoPagos() {
        return historicoPagos;
    }

    /**
     * Define el valor de la propiedad historicoPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoricoPagos(String value) {
        this.historicoPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaMasRecienteHistoricoPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaMasRecienteHistoricoPagos() {
        return fechaMasRecienteHistoricoPagos;
    }

    /**
     * Define el valor de la propiedad fechaMasRecienteHistoricoPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaMasRecienteHistoricoPagos(String value) {
        this.fechaMasRecienteHistoricoPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaMasAntiguaHistoricoPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaMasAntiguaHistoricoPagos() {
        return fechaMasAntiguaHistoricoPagos;
    }

    /**
     * Define el valor de la propiedad fechaMasAntiguaHistoricoPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaMasAntiguaHistoricoPagos(String value) {
        this.fechaMasAntiguaHistoricoPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad claveObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveObservacion() {
        return claveObservacion;
    }

    /**
     * Define el valor de la propiedad claveObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveObservacion(String value) {
        this.claveObservacion = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosReportados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosReportados() {
        return totalPagosReportados;
    }

    /**
     * Define el valor de la propiedad totalPagosReportados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosReportados(String value) {
        this.totalPagosReportados = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosCalificadosMOP2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosCalificadosMOP2() {
        return totalPagosCalificadosMOP2;
    }

    /**
     * Define el valor de la propiedad totalPagosCalificadosMOP2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosCalificadosMOP2(String value) {
        this.totalPagosCalificadosMOP2 = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosCalificadosMOP3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosCalificadosMOP3() {
        return totalPagosCalificadosMOP3;
    }

    /**
     * Define el valor de la propiedad totalPagosCalificadosMOP3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosCalificadosMOP3(String value) {
        this.totalPagosCalificadosMOP3 = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosCalificadosMOP4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosCalificadosMOP4() {
        return totalPagosCalificadosMOP4;
    }

    /**
     * Define el valor de la propiedad totalPagosCalificadosMOP4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosCalificadosMOP4(String value) {
        this.totalPagosCalificadosMOP4 = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosCalificadosMOP5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosCalificadosMOP5() {
        return totalPagosCalificadosMOP5;
    }

    /**
     * Define el valor de la propiedad totalPagosCalificadosMOP5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosCalificadosMOP5(String value) {
        this.totalPagosCalificadosMOP5 = value;
    }

    /**
     * Obtiene el valor de la propiedad importeSaldoMorosidadHistMasGrave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteSaldoMorosidadHistMasGrave() {
        return importeSaldoMorosidadHistMasGrave;
    }

    /**
     * Define el valor de la propiedad importeSaldoMorosidadHistMasGrave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteSaldoMorosidadHistMasGrave(String value) {
        this.importeSaldoMorosidadHistMasGrave = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaHistoricaMorosidadMasGrave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaHistoricaMorosidadMasGrave() {
        return fechaHistoricaMorosidadMasGrave;
    }

    /**
     * Define el valor de la propiedad fechaHistoricaMorosidadMasGrave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaHistoricaMorosidadMasGrave(String value) {
        this.fechaHistoricaMorosidadMasGrave = value;
    }

    /**
     * Obtiene el valor de la propiedad mopHistoricoMorosidadMasGrave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMopHistoricoMorosidadMasGrave() {
        return mopHistoricoMorosidadMasGrave;
    }

    /**
     * Define el valor de la propiedad mopHistoricoMorosidadMasGrave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMopHistoricoMorosidadMasGrave(String value) {
        this.mopHistoricoMorosidadMasGrave = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioReestructura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicioReestructura() {
        return fechaInicioReestructura;
    }

    /**
     * Define el valor de la propiedad fechaInicioReestructura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicioReestructura(String value) {
        this.fechaInicioReestructura = value;
    }

    /**
     * Obtiene el valor de la propiedad montoUltimoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoUltimoPago() {
        return montoUltimoPago;
    }

    /**
     * Define el valor de la propiedad montoUltimoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoUltimoPago(String value) {
        this.montoUltimoPago = value;
    }

}
