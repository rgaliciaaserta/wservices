
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResumenReporteRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResumenReporteRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaIngresoBD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP0" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOPUR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroCuentas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentasPagosFijosHipotecas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentasRevolventesAbiertas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentasCerradas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentasNegativasActuales" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentasClavesHistoriaNegativa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentasDisputa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroSolicitudesUltimos6Meses" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NuevaDireccionReportadaUltimos60Dias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MensajesAlerta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExistenciaDeclaracionesConsumidor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoMoneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalCreditosMaximosRevolventes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalLimitesCreditoRevolventes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalSaldosActualesRevolventes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalSaldosVencidosRevolventes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosRevolventes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PctLimiteCreditoUtilizadoRevolventes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalCreditosMaximosPagosFijos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalSaldosActualesPagosFijos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalSaldosVencidosPagosFijos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPagosPagosFijos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP96" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP97" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroMOP99" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAperturaCuentaMasAntigua" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAperturaCuentaMasReciente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalSolicitudesReporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaSolicitudReporteMasReciente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroTotalCuentasDespachoCobranza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAperturaCuentaMasRecienteDespachoCobranza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroTotalSolicitudesDespachosCobranza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaSolicitudMasRecienteDespachoCobranza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResumenReporteRespBC", propOrder = {
    "fechaIngresoBD",
    "numeroMOP7",
    "numeroMOP6",
    "numeroMOP5",
    "numeroMOP4",
    "numeroMOP3",
    "numeroMOP2",
    "numeroMOP1",
    "numeroMOP0",
    "numeroMOPUR",
    "numeroCuentas",
    "cuentasPagosFijosHipotecas",
    "cuentasRevolventesAbiertas",
    "cuentasCerradas",
    "cuentasNegativasActuales",
    "cuentasClavesHistoriaNegativa",
    "cuentasDisputa",
    "numeroSolicitudesUltimos6Meses",
    "nuevaDireccionReportadaUltimos60Dias",
    "mensajesAlerta",
    "existenciaDeclaracionesConsumidor",
    "tipoMoneda",
    "totalCreditosMaximosRevolventes",
    "totalLimitesCreditoRevolventes",
    "totalSaldosActualesRevolventes",
    "totalSaldosVencidosRevolventes",
    "totalPagosRevolventes",
    "pctLimiteCreditoUtilizadoRevolventes",
    "totalCreditosMaximosPagosFijos",
    "totalSaldosActualesPagosFijos",
    "totalSaldosVencidosPagosFijos",
    "totalPagosPagosFijos",
    "numeroMOP96",
    "numeroMOP97",
    "numeroMOP99",
    "fechaAperturaCuentaMasAntigua",
    "fechaAperturaCuentaMasReciente",
    "totalSolicitudesReporte",
    "fechaSolicitudReporteMasReciente",
    "numeroTotalCuentasDespachoCobranza",
    "fechaAperturaCuentaMasRecienteDespachoCobranza",
    "numeroTotalSolicitudesDespachosCobranza",
    "fechaSolicitudMasRecienteDespachoCobranza"
})
public class ResumenReporteRespBC {

    @XmlElement(name = "FechaIngresoBD")
    protected String fechaIngresoBD;
    @XmlElement(name = "NumeroMOP7")
    protected String numeroMOP7;
    @XmlElement(name = "NumeroMOP6")
    protected String numeroMOP6;
    @XmlElement(name = "NumeroMOP5")
    protected String numeroMOP5;
    @XmlElement(name = "NumeroMOP4")
    protected String numeroMOP4;
    @XmlElement(name = "NumeroMOP3")
    protected String numeroMOP3;
    @XmlElement(name = "NumeroMOP2")
    protected String numeroMOP2;
    @XmlElement(name = "NumeroMOP1")
    protected String numeroMOP1;
    @XmlElement(name = "NumeroMOP0")
    protected String numeroMOP0;
    @XmlElement(name = "NumeroMOPUR")
    protected String numeroMOPUR;
    @XmlElement(name = "NumeroCuentas")
    protected String numeroCuentas;
    @XmlElement(name = "CuentasPagosFijosHipotecas")
    protected String cuentasPagosFijosHipotecas;
    @XmlElement(name = "CuentasRevolventesAbiertas")
    protected String cuentasRevolventesAbiertas;
    @XmlElement(name = "CuentasCerradas")
    protected String cuentasCerradas;
    @XmlElement(name = "CuentasNegativasActuales")
    protected String cuentasNegativasActuales;
    @XmlElement(name = "CuentasClavesHistoriaNegativa")
    protected String cuentasClavesHistoriaNegativa;
    @XmlElement(name = "CuentasDisputa")
    protected String cuentasDisputa;
    @XmlElement(name = "NumeroSolicitudesUltimos6Meses")
    protected String numeroSolicitudesUltimos6Meses;
    @XmlElement(name = "NuevaDireccionReportadaUltimos60Dias")
    protected String nuevaDireccionReportadaUltimos60Dias;
    @XmlElement(name = "MensajesAlerta")
    protected String mensajesAlerta;
    @XmlElement(name = "ExistenciaDeclaracionesConsumidor")
    protected String existenciaDeclaracionesConsumidor;
    @XmlElement(name = "TipoMoneda")
    protected String tipoMoneda;
    @XmlElement(name = "TotalCreditosMaximosRevolventes")
    protected String totalCreditosMaximosRevolventes;
    @XmlElement(name = "TotalLimitesCreditoRevolventes")
    protected String totalLimitesCreditoRevolventes;
    @XmlElement(name = "TotalSaldosActualesRevolventes")
    protected String totalSaldosActualesRevolventes;
    @XmlElement(name = "TotalSaldosVencidosRevolventes")
    protected String totalSaldosVencidosRevolventes;
    @XmlElement(name = "TotalPagosRevolventes")
    protected String totalPagosRevolventes;
    @XmlElement(name = "PctLimiteCreditoUtilizadoRevolventes")
    protected String pctLimiteCreditoUtilizadoRevolventes;
    @XmlElement(name = "TotalCreditosMaximosPagosFijos")
    protected String totalCreditosMaximosPagosFijos;
    @XmlElement(name = "TotalSaldosActualesPagosFijos")
    protected String totalSaldosActualesPagosFijos;
    @XmlElement(name = "TotalSaldosVencidosPagosFijos")
    protected String totalSaldosVencidosPagosFijos;
    @XmlElement(name = "TotalPagosPagosFijos")
    protected String totalPagosPagosFijos;
    @XmlElement(name = "NumeroMOP96")
    protected String numeroMOP96;
    @XmlElement(name = "NumeroMOP97")
    protected String numeroMOP97;
    @XmlElement(name = "NumeroMOP99")
    protected String numeroMOP99;
    @XmlElement(name = "FechaAperturaCuentaMasAntigua")
    protected String fechaAperturaCuentaMasAntigua;
    @XmlElement(name = "FechaAperturaCuentaMasReciente")
    protected String fechaAperturaCuentaMasReciente;
    @XmlElement(name = "TotalSolicitudesReporte")
    protected String totalSolicitudesReporte;
    @XmlElement(name = "FechaSolicitudReporteMasReciente")
    protected String fechaSolicitudReporteMasReciente;
    @XmlElement(name = "NumeroTotalCuentasDespachoCobranza")
    protected String numeroTotalCuentasDespachoCobranza;
    @XmlElement(name = "FechaAperturaCuentaMasRecienteDespachoCobranza")
    protected String fechaAperturaCuentaMasRecienteDespachoCobranza;
    @XmlElement(name = "NumeroTotalSolicitudesDespachosCobranza")
    protected String numeroTotalSolicitudesDespachosCobranza;
    @XmlElement(name = "FechaSolicitudMasRecienteDespachoCobranza")
    protected String fechaSolicitudMasRecienteDespachoCobranza;

    /**
     * Obtiene el valor de la propiedad fechaIngresoBD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaIngresoBD() {
        return fechaIngresoBD;
    }

    /**
     * Define el valor de la propiedad fechaIngresoBD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaIngresoBD(String value) {
        this.fechaIngresoBD = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP7() {
        return numeroMOP7;
    }

    /**
     * Define el valor de la propiedad numeroMOP7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP7(String value) {
        this.numeroMOP7 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP6() {
        return numeroMOP6;
    }

    /**
     * Define el valor de la propiedad numeroMOP6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP6(String value) {
        this.numeroMOP6 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP5() {
        return numeroMOP5;
    }

    /**
     * Define el valor de la propiedad numeroMOP5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP5(String value) {
        this.numeroMOP5 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP4() {
        return numeroMOP4;
    }

    /**
     * Define el valor de la propiedad numeroMOP4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP4(String value) {
        this.numeroMOP4 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP3() {
        return numeroMOP3;
    }

    /**
     * Define el valor de la propiedad numeroMOP3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP3(String value) {
        this.numeroMOP3 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP2() {
        return numeroMOP2;
    }

    /**
     * Define el valor de la propiedad numeroMOP2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP2(String value) {
        this.numeroMOP2 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP1() {
        return numeroMOP1;
    }

    /**
     * Define el valor de la propiedad numeroMOP1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP1(String value) {
        this.numeroMOP1 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP0.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP0() {
        return numeroMOP0;
    }

    /**
     * Define el valor de la propiedad numeroMOP0.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP0(String value) {
        this.numeroMOP0 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOPUR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOPUR() {
        return numeroMOPUR;
    }

    /**
     * Define el valor de la propiedad numeroMOPUR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOPUR(String value) {
        this.numeroMOPUR = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCuentas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCuentas() {
        return numeroCuentas;
    }

    /**
     * Define el valor de la propiedad numeroCuentas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCuentas(String value) {
        this.numeroCuentas = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasPagosFijosHipotecas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentasPagosFijosHipotecas() {
        return cuentasPagosFijosHipotecas;
    }

    /**
     * Define el valor de la propiedad cuentasPagosFijosHipotecas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentasPagosFijosHipotecas(String value) {
        this.cuentasPagosFijosHipotecas = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasRevolventesAbiertas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentasRevolventesAbiertas() {
        return cuentasRevolventesAbiertas;
    }

    /**
     * Define el valor de la propiedad cuentasRevolventesAbiertas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentasRevolventesAbiertas(String value) {
        this.cuentasRevolventesAbiertas = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasCerradas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentasCerradas() {
        return cuentasCerradas;
    }

    /**
     * Define el valor de la propiedad cuentasCerradas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentasCerradas(String value) {
        this.cuentasCerradas = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasNegativasActuales.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentasNegativasActuales() {
        return cuentasNegativasActuales;
    }

    /**
     * Define el valor de la propiedad cuentasNegativasActuales.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentasNegativasActuales(String value) {
        this.cuentasNegativasActuales = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasClavesHistoriaNegativa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentasClavesHistoriaNegativa() {
        return cuentasClavesHistoriaNegativa;
    }

    /**
     * Define el valor de la propiedad cuentasClavesHistoriaNegativa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentasClavesHistoriaNegativa(String value) {
        this.cuentasClavesHistoriaNegativa = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentasDisputa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentasDisputa() {
        return cuentasDisputa;
    }

    /**
     * Define el valor de la propiedad cuentasDisputa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentasDisputa(String value) {
        this.cuentasDisputa = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSolicitudesUltimos6Meses.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSolicitudesUltimos6Meses() {
        return numeroSolicitudesUltimos6Meses;
    }

    /**
     * Define el valor de la propiedad numeroSolicitudesUltimos6Meses.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSolicitudesUltimos6Meses(String value) {
        this.numeroSolicitudesUltimos6Meses = value;
    }

    /**
     * Obtiene el valor de la propiedad nuevaDireccionReportadaUltimos60Dias.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNuevaDireccionReportadaUltimos60Dias() {
        return nuevaDireccionReportadaUltimos60Dias;
    }

    /**
     * Define el valor de la propiedad nuevaDireccionReportadaUltimos60Dias.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNuevaDireccionReportadaUltimos60Dias(String value) {
        this.nuevaDireccionReportadaUltimos60Dias = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajesAlerta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajesAlerta() {
        return mensajesAlerta;
    }

    /**
     * Define el valor de la propiedad mensajesAlerta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajesAlerta(String value) {
        this.mensajesAlerta = value;
    }

    /**
     * Obtiene el valor de la propiedad existenciaDeclaracionesConsumidor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExistenciaDeclaracionesConsumidor() {
        return existenciaDeclaracionesConsumidor;
    }

    /**
     * Define el valor de la propiedad existenciaDeclaracionesConsumidor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExistenciaDeclaracionesConsumidor(String value) {
        this.existenciaDeclaracionesConsumidor = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMoneda() {
        return tipoMoneda;
    }

    /**
     * Define el valor de la propiedad tipoMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMoneda(String value) {
        this.tipoMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCreditosMaximosRevolventes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalCreditosMaximosRevolventes() {
        return totalCreditosMaximosRevolventes;
    }

    /**
     * Define el valor de la propiedad totalCreditosMaximosRevolventes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalCreditosMaximosRevolventes(String value) {
        this.totalCreditosMaximosRevolventes = value;
    }

    /**
     * Obtiene el valor de la propiedad totalLimitesCreditoRevolventes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalLimitesCreditoRevolventes() {
        return totalLimitesCreditoRevolventes;
    }

    /**
     * Define el valor de la propiedad totalLimitesCreditoRevolventes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalLimitesCreditoRevolventes(String value) {
        this.totalLimitesCreditoRevolventes = value;
    }

    /**
     * Obtiene el valor de la propiedad totalSaldosActualesRevolventes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalSaldosActualesRevolventes() {
        return totalSaldosActualesRevolventes;
    }

    /**
     * Define el valor de la propiedad totalSaldosActualesRevolventes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalSaldosActualesRevolventes(String value) {
        this.totalSaldosActualesRevolventes = value;
    }

    /**
     * Obtiene el valor de la propiedad totalSaldosVencidosRevolventes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalSaldosVencidosRevolventes() {
        return totalSaldosVencidosRevolventes;
    }

    /**
     * Define el valor de la propiedad totalSaldosVencidosRevolventes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalSaldosVencidosRevolventes(String value) {
        this.totalSaldosVencidosRevolventes = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosRevolventes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosRevolventes() {
        return totalPagosRevolventes;
    }

    /**
     * Define el valor de la propiedad totalPagosRevolventes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosRevolventes(String value) {
        this.totalPagosRevolventes = value;
    }

    /**
     * Obtiene el valor de la propiedad pctLimiteCreditoUtilizadoRevolventes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPctLimiteCreditoUtilizadoRevolventes() {
        return pctLimiteCreditoUtilizadoRevolventes;
    }

    /**
     * Define el valor de la propiedad pctLimiteCreditoUtilizadoRevolventes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPctLimiteCreditoUtilizadoRevolventes(String value) {
        this.pctLimiteCreditoUtilizadoRevolventes = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCreditosMaximosPagosFijos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalCreditosMaximosPagosFijos() {
        return totalCreditosMaximosPagosFijos;
    }

    /**
     * Define el valor de la propiedad totalCreditosMaximosPagosFijos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalCreditosMaximosPagosFijos(String value) {
        this.totalCreditosMaximosPagosFijos = value;
    }

    /**
     * Obtiene el valor de la propiedad totalSaldosActualesPagosFijos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalSaldosActualesPagosFijos() {
        return totalSaldosActualesPagosFijos;
    }

    /**
     * Define el valor de la propiedad totalSaldosActualesPagosFijos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalSaldosActualesPagosFijos(String value) {
        this.totalSaldosActualesPagosFijos = value;
    }

    /**
     * Obtiene el valor de la propiedad totalSaldosVencidosPagosFijos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalSaldosVencidosPagosFijos() {
        return totalSaldosVencidosPagosFijos;
    }

    /**
     * Define el valor de la propiedad totalSaldosVencidosPagosFijos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalSaldosVencidosPagosFijos(String value) {
        this.totalSaldosVencidosPagosFijos = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPagosPagosFijos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagosPagosFijos() {
        return totalPagosPagosFijos;
    }

    /**
     * Define el valor de la propiedad totalPagosPagosFijos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagosPagosFijos(String value) {
        this.totalPagosPagosFijos = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP96.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP96() {
        return numeroMOP96;
    }

    /**
     * Define el valor de la propiedad numeroMOP96.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP96(String value) {
        this.numeroMOP96 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP97.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP97() {
        return numeroMOP97;
    }

    /**
     * Define el valor de la propiedad numeroMOP97.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP97(String value) {
        this.numeroMOP97 = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMOP99.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroMOP99() {
        return numeroMOP99;
    }

    /**
     * Define el valor de la propiedad numeroMOP99.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroMOP99(String value) {
        this.numeroMOP99 = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAperturaCuentaMasAntigua.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAperturaCuentaMasAntigua() {
        return fechaAperturaCuentaMasAntigua;
    }

    /**
     * Define el valor de la propiedad fechaAperturaCuentaMasAntigua.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAperturaCuentaMasAntigua(String value) {
        this.fechaAperturaCuentaMasAntigua = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAperturaCuentaMasReciente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAperturaCuentaMasReciente() {
        return fechaAperturaCuentaMasReciente;
    }

    /**
     * Define el valor de la propiedad fechaAperturaCuentaMasReciente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAperturaCuentaMasReciente(String value) {
        this.fechaAperturaCuentaMasReciente = value;
    }

    /**
     * Obtiene el valor de la propiedad totalSolicitudesReporte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalSolicitudesReporte() {
        return totalSolicitudesReporte;
    }

    /**
     * Define el valor de la propiedad totalSolicitudesReporte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalSolicitudesReporte(String value) {
        this.totalSolicitudesReporte = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSolicitudReporteMasReciente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSolicitudReporteMasReciente() {
        return fechaSolicitudReporteMasReciente;
    }

    /**
     * Define el valor de la propiedad fechaSolicitudReporteMasReciente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSolicitudReporteMasReciente(String value) {
        this.fechaSolicitudReporteMasReciente = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroTotalCuentasDespachoCobranza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTotalCuentasDespachoCobranza() {
        return numeroTotalCuentasDespachoCobranza;
    }

    /**
     * Define el valor de la propiedad numeroTotalCuentasDespachoCobranza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTotalCuentasDespachoCobranza(String value) {
        this.numeroTotalCuentasDespachoCobranza = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAperturaCuentaMasRecienteDespachoCobranza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAperturaCuentaMasRecienteDespachoCobranza() {
        return fechaAperturaCuentaMasRecienteDespachoCobranza;
    }

    /**
     * Define el valor de la propiedad fechaAperturaCuentaMasRecienteDespachoCobranza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAperturaCuentaMasRecienteDespachoCobranza(String value) {
        this.fechaAperturaCuentaMasRecienteDespachoCobranza = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroTotalSolicitudesDespachosCobranza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTotalSolicitudesDespachosCobranza() {
        return numeroTotalSolicitudesDespachosCobranza;
    }

    /**
     * Define el valor de la propiedad numeroTotalSolicitudesDespachosCobranza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTotalSolicitudesDespachosCobranza(String value) {
        this.numeroTotalSolicitudesDespachosCobranza = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSolicitudMasRecienteDespachoCobranza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSolicitudMasRecienteDespachoCobranza() {
        return fechaSolicitudMasRecienteDespachoCobranza;
    }

    /**
     * Define el valor de la propiedad fechaSolicitudMasRecienteDespachoCobranza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSolicitudMasRecienteDespachoCobranza(String value) {
        this.fechaSolicitudMasRecienteDespachoCobranza = value;
    }

}
