
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CaracteristicasRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CaracteristicasRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Caracteristica" type="{http://bean.consulta.ws.bc.com/}CaracteristicasBC" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaracteristicasRespBC", propOrder = {
    "caracteristica"
})
public class CaracteristicasRespBC {

    @XmlElement(name = "Caracteristica", required = true, nillable = true)
    protected List<CaracteristicasBC> caracteristica;

    /**
     * Gets the value of the caracteristica property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caracteristica property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaracteristica().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaracteristicasBC }
     * 
     * 
     */
    public List<CaracteristicasBC> getCaracteristica() {
        if (caracteristica == null) {
            caracteristica = new ArrayList<CaracteristicasBC>();
        }
        return this.caracteristica;
    }

}
