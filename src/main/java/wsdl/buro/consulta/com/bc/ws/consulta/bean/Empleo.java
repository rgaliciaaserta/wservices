
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Empleo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Empleo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NombreEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ColoniaPoblacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DelegacionMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Ciudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Puesto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaContratacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaveMoneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SalarioMensual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaUltimoDiaEmpleo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Empleo", propOrder = {
    "nombreEmpresa",
    "direccion",
    "coloniaPoblacion",
    "delegacionMunicipio",
    "ciudad",
    "estado",
    "cp",
    "numeroTelefono",
    "extension",
    "fax",
    "puesto",
    "fechaContratacion",
    "claveMoneda",
    "salarioMensual",
    "fechaUltimoDiaEmpleo",
    "codPais"
})
@XmlSeeAlso({
    EmpleoResp.class
})
public class Empleo {

    @XmlElement(name = "NombreEmpresa")
    protected String nombreEmpresa;
    @XmlElement(name = "Direccion")
    protected String direccion;
    @XmlElement(name = "ColoniaPoblacion")
    protected String coloniaPoblacion;
    @XmlElement(name = "DelegacionMunicipio")
    protected String delegacionMunicipio;
    @XmlElement(name = "Ciudad")
    protected String ciudad;
    @XmlElement(name = "Estado")
    protected String estado;
    @XmlElement(name = "CP")
    protected String cp;
    @XmlElement(name = "NumeroTelefono")
    protected String numeroTelefono;
    @XmlElement(name = "Extension")
    protected String extension;
    @XmlElement(name = "Fax")
    protected String fax;
    @XmlElement(name = "Puesto")
    protected String puesto;
    @XmlElement(name = "FechaContratacion")
    protected String fechaContratacion;
    @XmlElement(name = "ClaveMoneda")
    protected String claveMoneda;
    @XmlElement(name = "SalarioMensual")
    protected String salarioMensual;
    @XmlElement(name = "FechaUltimoDiaEmpleo")
    protected String fechaUltimoDiaEmpleo;
    @XmlElement(name = "CodPais")
    protected String codPais;

    /**
     * Obtiene el valor de la propiedad nombreEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * Define el valor de la propiedad nombreEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreEmpresa(String value) {
        this.nombreEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad coloniaPoblacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColoniaPoblacion() {
        return coloniaPoblacion;
    }

    /**
     * Define el valor de la propiedad coloniaPoblacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColoniaPoblacion(String value) {
        this.coloniaPoblacion = value;
    }

    /**
     * Obtiene el valor de la propiedad delegacionMunicipio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegacionMunicipio() {
        return delegacionMunicipio;
    }

    /**
     * Define el valor de la propiedad delegacionMunicipio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegacionMunicipio(String value) {
        this.delegacionMunicipio = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Define el valor de la propiedad ciudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad(String value) {
        this.ciudad = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad cp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCP() {
        return cp;
    }

    /**
     * Define el valor de la propiedad cp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCP(String value) {
        this.cp = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    /**
     * Define el valor de la propiedad numeroTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefono(String value) {
        this.numeroTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Obtiene el valor de la propiedad fax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Define el valor de la propiedad fax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Obtiene el valor de la propiedad puesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuesto() {
        return puesto;
    }

    /**
     * Define el valor de la propiedad puesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuesto(String value) {
        this.puesto = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaContratacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaContratacion() {
        return fechaContratacion;
    }

    /**
     * Define el valor de la propiedad fechaContratacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaContratacion(String value) {
        this.fechaContratacion = value;
    }

    /**
     * Obtiene el valor de la propiedad claveMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveMoneda() {
        return claveMoneda;
    }

    /**
     * Define el valor de la propiedad claveMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveMoneda(String value) {
        this.claveMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad salarioMensual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalarioMensual() {
        return salarioMensual;
    }

    /**
     * Define el valor de la propiedad salarioMensual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalarioMensual(String value) {
        this.salarioMensual = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaUltimoDiaEmpleo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaUltimoDiaEmpleo() {
        return fechaUltimoDiaEmpleo;
    }

    /**
     * Define el valor de la propiedad fechaUltimoDiaEmpleo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaUltimoDiaEmpleo(String value) {
        this.fechaUltimoDiaEmpleo = value;
    }

    /**
     * Obtiene el valor de la propiedad codPais.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPais() {
        return codPais;
    }

    /**
     * Define el valor de la propiedad codPais.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPais(String value) {
        this.codPais = value;
    }

}
