
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsdl.buro.consulta.com.bc.ws.consulta.bean package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultaCC_QNAME = new QName("http://bean.consulta.ws.bc.com/", "consultaCC");
    private final static QName _ConsultaCCResponse_QNAME = new QName("http://bean.consulta.ws.bc.com/", "consultaCCResponse");
    private final static QName _ConsultaPDF_QNAME = new QName("http://bean.consulta.ws.bc.com/", "consultaPDF");
    private final static QName _ConsultaPDFResponse_QNAME = new QName("http://bean.consulta.ws.bc.com/", "consultaPDFResponse");
    private final static QName _ConsultaXML_QNAME = new QName("http://bean.consulta.ws.bc.com/", "consultaXML");
    private final static QName _ConsultaXMLResponse_QNAME = new QName("http://bean.consulta.ws.bc.com/", "consultaXMLResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsdl.buro.consulta.com.bc.ws.consulta.bean
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultaCC }
     * 
     */
    public ConsultaCC createConsultaCC() {
        return new ConsultaCC();
    }

    /**
     * Create an instance of {@link ConsultaCCResponse }
     * 
     */
    public ConsultaCCResponse createConsultaCCResponse() {
        return new ConsultaCCResponse();
    }

    /**
     * Create an instance of {@link ConsultaPDF }
     * 
     */
    public ConsultaPDF createConsultaPDF() {
        return new ConsultaPDF();
    }

    /**
     * Create an instance of {@link ConsultaPDFResponse }
     * 
     */
    public ConsultaPDFResponse createConsultaPDFResponse() {
        return new ConsultaPDFResponse();
    }

    /**
     * Create an instance of {@link ConsultaXML }
     * 
     */
    public ConsultaXML createConsultaXML() {
        return new ConsultaXML();
    }

    /**
     * Create an instance of {@link ConsultaXMLResponse }
     * 
     */
    public ConsultaXMLResponse createConsultaXMLResponse() {
        return new ConsultaXMLResponse();
    }

    /**
     * Create an instance of {@link ConsultaBC }
     * 
     */
    public ConsultaBC createConsultaBC() {
        return new ConsultaBC();
    }

    /**
     * Create an instance of {@link PersonasBC }
     * 
     */
    public PersonasBC createPersonasBC() {
        return new PersonasBC();
    }

    /**
     * Create an instance of {@link PersonaBC }
     * 
     */
    public PersonaBC createPersonaBC() {
        return new PersonaBC();
    }

    /**
     * Create an instance of {@link EncabezadoBC }
     * 
     */
    public EncabezadoBC createEncabezadoBC() {
        return new EncabezadoBC();
    }

    /**
     * Create an instance of {@link EncabezadoRespBC }
     * 
     */
    public EncabezadoRespBC createEncabezadoRespBC() {
        return new EncabezadoRespBC();
    }

    /**
     * Create an instance of {@link NombreBC }
     * 
     */
    public NombreBC createNombreBC() {
        return new NombreBC();
    }

    /**
     * Create an instance of {@link NombreRespBC }
     * 
     */
    public NombreRespBC createNombreRespBC() {
        return new NombreRespBC();
    }

    /**
     * Create an instance of {@link DomiciliosBC }
     * 
     */
    public DomiciliosBC createDomiciliosBC() {
        return new DomiciliosBC();
    }

    /**
     * Create an instance of {@link Direccion }
     * 
     */
    public Direccion createDireccion() {
        return new Direccion();
    }

    /**
     * Create an instance of {@link DireccionRespBC }
     * 
     */
    public DireccionRespBC createDireccionRespBC() {
        return new DireccionRespBC();
    }

    /**
     * Create an instance of {@link EmpleosBC }
     * 
     */
    public EmpleosBC createEmpleosBC() {
        return new EmpleosBC();
    }

    /**
     * Create an instance of {@link EmpleoBC }
     * 
     */
    public EmpleoBC createEmpleoBC() {
        return new EmpleoBC();
    }

    /**
     * Create an instance of {@link EmpleoRespBC }
     * 
     */
    public EmpleoRespBC createEmpleoRespBC() {
        return new EmpleoRespBC();
    }

    /**
     * Create an instance of {@link CuentaClienteBC }
     * 
     */
    public CuentaClienteBC createCuentaClienteBC() {
        return new CuentaClienteBC();
    }

    /**
     * Create an instance of {@link AutenticacionBC }
     * 
     */
    public AutenticacionBC createAutenticacionBC() {
        return new AutenticacionBC();
    }

    /**
     * Create an instance of {@link Caracteristicas }
     * 
     */
    public Caracteristicas createCaracteristicas() {
        return new Caracteristicas();
    }

    /**
     * Create an instance of {@link ReportePDF }
     * 
     */
    public ReportePDF createReportePDF() {
        return new ReportePDF();
    }

    /**
     * Create an instance of {@link ErrorRespBC }
     * 
     */
    public ErrorRespBC createErrorRespBC() {
        return new ErrorRespBC();
    }

    /**
     * Create an instance of {@link AR }
     * 
     */
    public AR createAR() {
        return new AR();
    }

    /**
     * Create an instance of {@link UR }
     * 
     */
    public UR createUR() {
        return new UR();
    }

    /**
     * Create an instance of {@link Consulta }
     * 
     */
    public Consulta createConsulta() {
        return new Consulta();
    }

    /**
     * Create an instance of {@link Encabezado }
     * 
     */
    public Encabezado createEncabezado() {
        return new Encabezado();
    }

    /**
     * Create an instance of {@link Personas }
     * 
     */
    public Personas createPersonas() {
        return new Personas();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link DetalleConsulta }
     * 
     */
    public DetalleConsulta createDetalleConsulta() {
        return new DetalleConsulta();
    }

    /**
     * Create an instance of {@link Nombre }
     * 
     */
    public Nombre createNombre() {
        return new Nombre();
    }

    /**
     * Create an instance of {@link NombreResp }
     * 
     */
    public NombreResp createNombreResp() {
        return new NombreResp();
    }

    /**
     * Create an instance of {@link Domicilios }
     * 
     */
    public Domicilios createDomicilios() {
        return new Domicilios();
    }

    /**
     * Create an instance of {@link Domicilio }
     * 
     */
    public Domicilio createDomicilio() {
        return new Domicilio();
    }

    /**
     * Create an instance of {@link DomicilioResp }
     * 
     */
    public DomicilioResp createDomicilioResp() {
        return new DomicilioResp();
    }

    /**
     * Create an instance of {@link Empleos }
     * 
     */
    public Empleos createEmpleos() {
        return new Empleos();
    }

    /**
     * Create an instance of {@link Empleo }
     * 
     */
    public Empleo createEmpleo() {
        return new Empleo();
    }

    /**
     * Create an instance of {@link EmpleoResp }
     * 
     */
    public EmpleoResp createEmpleoResp() {
        return new EmpleoResp();
    }

    /**
     * Create an instance of {@link CuentasReferencia }
     * 
     */
    public CuentasReferencia createCuentasReferencia() {
        return new CuentasReferencia();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link ErrorResp }
     * 
     */
    public ErrorResp createErrorResp() {
        return new ErrorResp();
    }

    /**
     * Create an instance of {@link ErroresResp }
     * 
     */
    public ErroresResp createErroresResp() {
        return new ErroresResp();
    }

    /**
     * Create an instance of {@link PersonasResp }
     * 
     */
    public PersonasResp createPersonasResp() {
        return new PersonasResp();
    }

    /**
     * Create an instance of {@link PersonaResp }
     * 
     */
    public PersonaResp createPersonaResp() {
        return new PersonaResp();
    }

    /**
     * Create an instance of {@link EncabezadoResp }
     * 
     */
    public EncabezadoResp createEncabezadoResp() {
        return new EncabezadoResp();
    }

    /**
     * Create an instance of {@link DomiciliosResp }
     * 
     */
    public DomiciliosResp createDomiciliosResp() {
        return new DomiciliosResp();
    }

    /**
     * Create an instance of {@link EmpleosResp }
     * 
     */
    public EmpleosResp createEmpleosResp() {
        return new EmpleosResp();
    }

    /**
     * Create an instance of {@link MensajesResp }
     * 
     */
    public MensajesResp createMensajesResp() {
        return new MensajesResp();
    }

    /**
     * Create an instance of {@link MensajeResp }
     * 
     */
    public MensajeResp createMensajeResp() {
        return new MensajeResp();
    }

    /**
     * Create an instance of {@link CuentasResp }
     * 
     */
    public CuentasResp createCuentasResp() {
        return new CuentasResp();
    }

    /**
     * Create an instance of {@link CuentaResp }
     * 
     */
    public CuentaResp createCuentaResp() {
        return new CuentaResp();
    }

    /**
     * Create an instance of {@link ConsultasEfectuadasResp }
     * 
     */
    public ConsultasEfectuadasResp createConsultasEfectuadasResp() {
        return new ConsultasEfectuadasResp();
    }

    /**
     * Create an instance of {@link ConsultaEfectuadaResp }
     * 
     */
    public ConsultaEfectuadaResp createConsultaEfectuadaResp() {
        return new ConsultaEfectuadaResp();
    }

    /**
     * Create an instance of {@link CaracteristicasRespBC }
     * 
     */
    public CaracteristicasRespBC createCaracteristicasRespBC() {
        return new CaracteristicasRespBC();
    }

    /**
     * Create an instance of {@link CaracteristicasBC }
     * 
     */
    public CaracteristicasBC createCaracteristicasBC() {
        return new CaracteristicasBC();
    }

    /**
     * Create an instance of {@link FR }
     * 
     */
    public FR createFR() {
        return new FR();
    }

    /**
     * Create an instance of {@link RespuestaBC }
     * 
     */
    public RespuestaBC createRespuestaBC() {
        return new RespuestaBC();
    }

    /**
     * Create an instance of {@link PersonasRespBC }
     * 
     */
    public PersonasRespBC createPersonasRespBC() {
        return new PersonasRespBC();
    }

    /**
     * Create an instance of {@link PersonaRespBC }
     * 
     */
    public PersonaRespBC createPersonaRespBC() {
        return new PersonaRespBC();
    }

    /**
     * Create an instance of {@link DomiciliosRespBC }
     * 
     */
    public DomiciliosRespBC createDomiciliosRespBC() {
        return new DomiciliosRespBC();
    }

    /**
     * Create an instance of {@link EmpleosRespBC }
     * 
     */
    public EmpleosRespBC createEmpleosRespBC() {
        return new EmpleosRespBC();
    }

    /**
     * Create an instance of {@link CuentasRBC }
     * 
     */
    public CuentasRBC createCuentasRBC() {
        return new CuentasRBC();
    }

    /**
     * Create an instance of {@link CuentasRespBC }
     * 
     */
    public CuentasRespBC createCuentasRespBC() {
        return new CuentasRespBC();
    }

    /**
     * Create an instance of {@link ConsultasEfectuadasRespBC }
     * 
     */
    public ConsultasEfectuadasRespBC createConsultasEfectuadasRespBC() {
        return new ConsultasEfectuadasRespBC();
    }

    /**
     * Create an instance of {@link ConsultaEfectuadaRespBC }
     * 
     */
    public ConsultaEfectuadaRespBC createConsultaEfectuadaRespBC() {
        return new ConsultaEfectuadaRespBC();
    }

    /**
     * Create an instance of {@link ResumenReporteRBC }
     * 
     */
    public ResumenReporteRBC createResumenReporteRBC() {
        return new ResumenReporteRBC();
    }

    /**
     * Create an instance of {@link ResumenReporteRespBC }
     * 
     */
    public ResumenReporteRespBC createResumenReporteRespBC() {
        return new ResumenReporteRespBC();
    }

    /**
     * Create an instance of {@link HawkAlertCRespBC }
     * 
     */
    public HawkAlertCRespBC createHawkAlertCRespBC() {
        return new HawkAlertCRespBC();
    }

    /**
     * Create an instance of {@link HawkAlertConsultaRespBC }
     * 
     */
    public HawkAlertConsultaRespBC createHawkAlertConsultaRespBC() {
        return new HawkAlertConsultaRespBC();
    }

    /**
     * Create an instance of {@link HawkAlertDBRespBC }
     * 
     */
    public HawkAlertDBRespBC createHawkAlertDBRespBC() {
        return new HawkAlertDBRespBC();
    }

    /**
     * Create an instance of {@link HawkAlertBDRespBC }
     * 
     */
    public HawkAlertBDRespBC createHawkAlertBDRespBC() {
        return new HawkAlertBDRespBC();
    }

    /**
     * Create an instance of {@link DeclaracionesClienteRespBC }
     * 
     */
    public DeclaracionesClienteRespBC createDeclaracionesClienteRespBC() {
        return new DeclaracionesClienteRespBC();
    }

    /**
     * Create an instance of {@link ScoreBCRespBC }
     * 
     */
    public ScoreBCRespBC createScoreBCRespBC() {
        return new ScoreBCRespBC();
    }

    /**
     * Create an instance of {@link ScoreBuroCreditoRespBC }
     * 
     */
    public ScoreBuroCreditoRespBC createScoreBuroCreditoRespBC() {
        return new ScoreBuroCreditoRespBC();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaCC }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaCC }{@code >}
     */
    @XmlElementDecl(namespace = "http://bean.consulta.ws.bc.com/", name = "consultaCC")
    public JAXBElement<ConsultaCC> createConsultaCC(ConsultaCC value) {
        return new JAXBElement<ConsultaCC>(_ConsultaCC_QNAME, ConsultaCC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaCCResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaCCResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://bean.consulta.ws.bc.com/", name = "consultaCCResponse")
    public JAXBElement<ConsultaCCResponse> createConsultaCCResponse(ConsultaCCResponse value) {
        return new JAXBElement<ConsultaCCResponse>(_ConsultaCCResponse_QNAME, ConsultaCCResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaPDF }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaPDF }{@code >}
     */
    @XmlElementDecl(namespace = "http://bean.consulta.ws.bc.com/", name = "consultaPDF")
    public JAXBElement<ConsultaPDF> createConsultaPDF(ConsultaPDF value) {
        return new JAXBElement<ConsultaPDF>(_ConsultaPDF_QNAME, ConsultaPDF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaPDFResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaPDFResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://bean.consulta.ws.bc.com/", name = "consultaPDFResponse")
    public JAXBElement<ConsultaPDFResponse> createConsultaPDFResponse(ConsultaPDFResponse value) {
        return new JAXBElement<ConsultaPDFResponse>(_ConsultaPDFResponse_QNAME, ConsultaPDFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://bean.consulta.ws.bc.com/", name = "consultaXML")
    public JAXBElement<ConsultaXML> createConsultaXML(ConsultaXML value) {
        return new JAXBElement<ConsultaXML>(_ConsultaXML_QNAME, ConsultaXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://bean.consulta.ws.bc.com/", name = "consultaXMLResponse")
    public JAXBElement<ConsultaXMLResponse> createConsultaXMLResponse(ConsultaXMLResponse value) {
        return new JAXBElement<ConsultaXMLResponse>(_ConsultaXMLResponse_QNAME, ConsultaXMLResponse.class, null, value);
    }

}
