
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CaracteristicasBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CaracteristicasBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Plantilla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdCaracteristica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroCaracteristica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaracteristicasBC", propOrder = {
    "plantilla",
    "idCaracteristica",
    "numeroCaracteristica",
    "valor",
    "codigoError"
})
public class CaracteristicasBC {

    @XmlElement(name = "Plantilla")
    protected String plantilla;
    @XmlElement(name = "IdCaracteristica")
    protected String idCaracteristica;
    @XmlElement(name = "NumeroCaracteristica")
    protected String numeroCaracteristica;
    @XmlElement(name = "Valor")
    protected String valor;
    @XmlElement(name = "CodigoError")
    protected String codigoError;

    /**
     * Obtiene el valor de la propiedad plantilla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlantilla() {
        return plantilla;
    }

    /**
     * Define el valor de la propiedad plantilla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlantilla(String value) {
        this.plantilla = value;
    }

    /**
     * Obtiene el valor de la propiedad idCaracteristica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCaracteristica() {
        return idCaracteristica;
    }

    /**
     * Define el valor de la propiedad idCaracteristica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCaracteristica(String value) {
        this.idCaracteristica = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCaracteristica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCaracteristica() {
        return numeroCaracteristica;
    }

    /**
     * Define el valor de la propiedad numeroCaracteristica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCaracteristica(String value) {
        this.numeroCaracteristica = value;
    }

    /**
     * Obtiene el valor de la propiedad valor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValor() {
        return valor;
    }

    /**
     * Define el valor de la propiedad valor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValor(String value) {
        this.valor = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoError() {
        return codigoError;
    }

    /**
     * Define el valor de la propiedad codigoError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoError(String value) {
        this.codigoError = value;
    }

}
