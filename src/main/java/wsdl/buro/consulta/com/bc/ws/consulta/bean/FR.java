
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FR complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FR"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LeyendaReporteBloqueado" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FR", propOrder = {
    "leyendaReporteBloqueado"
})
public class FR {

    @XmlElement(name = "LeyendaReporteBloqueado", required = true)
    protected String leyendaReporteBloqueado;

    /**
     * Obtiene el valor de la propiedad leyendaReporteBloqueado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeyendaReporteBloqueado() {
        return leyendaReporteBloqueado;
    }

    /**
     * Define el valor de la propiedad leyendaReporteBloqueado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeyendaReporteBloqueado(String value) {
        this.leyendaReporteBloqueado = value;
    }

}
