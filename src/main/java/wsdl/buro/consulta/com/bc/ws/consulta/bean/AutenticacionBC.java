
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AutenticacionBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AutenticacionBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TipoReporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoSalidaAU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReferenciaOperador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TarjetaCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UltimosCuatroDigitos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EjercidoCreditoHipotecario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EjercidoCreditoAutomotriz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutenticacionBC", propOrder = {
    "tipoReporte",
    "tipoSalidaAU",
    "referenciaOperador",
    "tarjetaCredito",
    "ultimosCuatroDigitos",
    "ejercidoCreditoHipotecario",
    "ejercidoCreditoAutomotriz"
})
public class AutenticacionBC {

    @XmlElement(name = "TipoReporte")
    protected String tipoReporte;
    @XmlElement(name = "TipoSalidaAU")
    protected String tipoSalidaAU;
    @XmlElement(name = "ReferenciaOperador")
    protected String referenciaOperador;
    @XmlElement(name = "TarjetaCredito")
    protected String tarjetaCredito;
    @XmlElement(name = "UltimosCuatroDigitos")
    protected String ultimosCuatroDigitos;
    @XmlElement(name = "EjercidoCreditoHipotecario")
    protected String ejercidoCreditoHipotecario;
    @XmlElement(name = "EjercidoCreditoAutomotriz")
    protected String ejercidoCreditoAutomotriz;

    /**
     * Obtiene el valor de la propiedad tipoReporte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoReporte() {
        return tipoReporte;
    }

    /**
     * Define el valor de la propiedad tipoReporte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoReporte(String value) {
        this.tipoReporte = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoSalidaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSalidaAU() {
        return tipoSalidaAU;
    }

    /**
     * Define el valor de la propiedad tipoSalidaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSalidaAU(String value) {
        this.tipoSalidaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad referenciaOperador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaOperador() {
        return referenciaOperador;
    }

    /**
     * Define el valor de la propiedad referenciaOperador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaOperador(String value) {
        this.referenciaOperador = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjetaCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarjetaCredito() {
        return tarjetaCredito;
    }

    /**
     * Define el valor de la propiedad tarjetaCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarjetaCredito(String value) {
        this.tarjetaCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad ultimosCuatroDigitos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUltimosCuatroDigitos() {
        return ultimosCuatroDigitos;
    }

    /**
     * Define el valor de la propiedad ultimosCuatroDigitos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUltimosCuatroDigitos(String value) {
        this.ultimosCuatroDigitos = value;
    }

    /**
     * Obtiene el valor de la propiedad ejercidoCreditoHipotecario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEjercidoCreditoHipotecario() {
        return ejercidoCreditoHipotecario;
    }

    /**
     * Define el valor de la propiedad ejercidoCreditoHipotecario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEjercidoCreditoHipotecario(String value) {
        this.ejercidoCreditoHipotecario = value;
    }

    /**
     * Obtiene el valor de la propiedad ejercidoCreditoAutomotriz.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEjercidoCreditoAutomotriz() {
        return ejercidoCreditoAutomotriz;
    }

    /**
     * Define el valor de la propiedad ejercidoCreditoAutomotriz.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEjercidoCreditoAutomotriz(String value) {
        this.ejercidoCreditoAutomotriz = value;
    }

}
