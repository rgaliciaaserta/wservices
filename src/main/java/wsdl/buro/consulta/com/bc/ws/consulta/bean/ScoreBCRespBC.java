
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ScoreBCRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ScoreBCRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ScoreBC" type="{http://bean.consulta.ws.bc.com/}ScoreBuroCreditoRespBC" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScoreBCRespBC", propOrder = {
    "scoreBC"
})
public class ScoreBCRespBC {

    @XmlElement(name = "ScoreBC", nillable = true)
    protected List<ScoreBuroCreditoRespBC> scoreBC;

    /**
     * Gets the value of the scoreBC property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scoreBC property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScoreBC().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScoreBuroCreditoRespBC }
     * 
     * 
     */
    public List<ScoreBuroCreditoRespBC> getScoreBC() {
        if (scoreBC == null) {
            scoreBC = new ArrayList<ScoreBuroCreditoRespBC>();
        }
        return this.scoreBC;
    }

}
