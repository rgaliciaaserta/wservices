
package wsdl.buro.consulta.com.bc.ws.consulta.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ErrorRespBC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ErrorRespBC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AR" type="{http://bean.consulta.ws.bc.com/}AR"/&gt;
 *         &lt;element name="UR" type="{http://bean.consulta.ws.bc.com/}UR"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorRespBC", propOrder = {
    "ar",
    "ur"
})
public class ErrorRespBC {

    @XmlElement(name = "AR", required = true)
    protected AR ar;
    @XmlElement(name = "UR", required = true)
    protected UR ur;

    /**
     * Obtiene el valor de la propiedad ar.
     * 
     * @return
     *     possible object is
     *     {@link AR }
     *     
     */
    public AR getAR() {
        return ar;
    }

    /**
     * Define el valor de la propiedad ar.
     * 
     * @param value
     *     allowed object is
     *     {@link AR }
     *     
     */
    public void setAR(AR value) {
        this.ar = value;
    }

    /**
     * Obtiene el valor de la propiedad ur.
     * 
     * @return
     *     possible object is
     *     {@link UR }
     *     
     */
    public UR getUR() {
        return ur;
    }

    /**
     * Define el valor de la propiedad ur.
     * 
     * @param value
     *     allowed object is
     *     {@link UR }
     *     
     */
    public void setUR(UR value) {
        this.ur = value;
    }

}
