
package wsdl.altacliente.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OutExito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIDCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutMsgError" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "outExito",
    "outIDCliente",
    "outMsgError"
})
@XmlRootElement(name = "ASERTANowo-AltaClienteNowoAltaClienteNowo_Output")
public class ASERTANowoAltaClienteNowoAltaClienteNowoOutput {

    @XmlElement(name = "OutExito", required = true)
    protected String outExito;
    @XmlElement(name = "OutIDCliente", required = true)
    protected String outIDCliente;
    @XmlElement(name = "OutMsgError", required = true)
    protected String outMsgError;

    /**
     * Obtiene el valor de la propiedad outExito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutExito() {
        return outExito;
    }

    /**
     * Define el valor de la propiedad outExito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutExito(String value) {
        this.outExito = value;
    }

    /**
     * Obtiene el valor de la propiedad outIDCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIDCliente() {
        return outIDCliente;
    }

    /**
     * Define el valor de la propiedad outIDCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIDCliente(String value) {
        this.outIDCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad outMsgError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutMsgError() {
        return outMsgError;
    }

    /**
     * Define el valor de la propiedad outMsgError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutMsgError(String value) {
        this.outMsgError = value;
    }

}
