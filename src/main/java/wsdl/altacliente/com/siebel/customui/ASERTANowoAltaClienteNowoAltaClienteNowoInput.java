
package wsdl.altacliente.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Ocupacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Pais_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CLABE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Calle_spcDomiclio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Ciudad_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InPerfil" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Numero_spcExterior_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Numero_spcInterior_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScoreHocelot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Correo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Estado_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Calle_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Fecha_spcNacimiento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Numero_spcInterior_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InCodigoAgenteNowo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Inicio_spcVigencia_spcGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Numero_spcIdentificacion_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Tarjeta_spcCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InCodigoPropiedad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Colonia_spcDomicilio_spcCliente_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Monto_spcRenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaVencimientoTarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Colonia_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Fin_spcVigencia_spcGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InTipoPersona" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Inicio_spcVigencia_spcContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Pais_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Segundo_spcApellido" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Delegacion_slhMunicipio_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="M2_spcDomicilio_spcRentar_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoResidencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Lugar_spcNacimiento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Nacionalidad" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Codigo_spcPostal_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Delegacion_slhMunicipio_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Primer_spcApellido" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Tipo_spcIdentificacion_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Codigo_spcPostal_spcDomicilio_spca_spcrentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Dia_spcLimite_spcPago_spcRenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Fin_spcVigencia_spcContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Banco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Curp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Nombre" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Numero_spcExterior_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Ciudad_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Estado_spcDomicilio_spcCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ocupacion",
    "paisSpcDomicilioSpcaSpcrentar",
    "clabe",
    "calleSpcDomiclioSpcCliente",
    "ciudadSpcDomicilioSpcaSpcrentar",
    "inPerfil",
    "numeroSpcExteriorSpcDomicilioSpcaSpcrentar",
    "numeroSpcInteriorSpcDomicilioSpcaSpcrentar",
    "scoreHocelot",
    "correo",
    "estadoSpcDomicilioSpcaSpcrentar",
    "calleSpcDomicilioSpcaSpcrentar",
    "fechaSpcNacimiento",
    "numeroSpcInteriorSpcDomicilioSpcCliente",
    "nue",
    "inCodigoAgenteNowo",
    "rfc",
    "inicioSpcVigenciaSpcGarantia",
    "numeroSpcIdentificacionSpcCliente",
    "tarjetaSpcCredito",
    "inCodigoPropiedad",
    "coloniaSpcDomicilioSpcClienteSpcaSpcrentar",
    "montoSpcRenta",
    "fechaVencimientoTarjeta",
    "coloniaSpcDomicilioSpcCliente",
    "finSpcVigenciaSpcGarantia",
    "inTipoPersona",
    "inicioSpcVigenciaSpcContrato",
    "paisSpcDomicilioSpcCliente",
    "segundoSpcApellido",
    "telefono",
    "delegacionSlhMunicipioSpcDomicilioSpcaSpcrentar",
    "m2SpcDomicilioSpcRentarSpcCliente",
    "tipoResidencia",
    "lugarSpcNacimiento",
    "nacionalidad",
    "codigoSpcPostalSpcDomicilioSpcCliente",
    "cuenta",
    "delegacionSlhMunicipioSpcDomicilioSpcCliente",
    "primerSpcApellido",
    "tipoSpcIdentificacionSpcCliente",
    "codigoSpcPostalSpcDomicilioSpcaSpcrentar",
    "diaSpcLimiteSpcPagoSpcRenta",
    "finSpcVigenciaSpcContrato",
    "banco",
    "curp",
    "nombre",
    "numeroSpcExteriorSpcDomicilioSpcCliente",
    "ciudadSpcDomicilioSpcCliente",
    "estadoSpcDomicilioSpcCliente"
})
@XmlRootElement(name = "ASERTANowo-AltaClienteNowoAltaClienteNowo_Input")
public class ASERTANowoAltaClienteNowoAltaClienteNowoInput {

    @XmlElement(name = "Ocupacion")
    protected String ocupacion;
    @XmlElement(name = "Pais_spcDomicilio_spca_spcrentar")
    protected String paisSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "CLABE")
    protected String clabe;
    @XmlElement(name = "Calle_spcDomiclio_spcCliente", required = true)
    protected String calleSpcDomiclioSpcCliente;
    @XmlElement(name = "Ciudad_spcDomicilio_spca_spcrentar")
    protected String ciudadSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "InPerfil", required = true)
    protected String inPerfil;
    @XmlElement(name = "Numero_spcExterior_spcDomicilio_spca_spcrentar")
    protected String numeroSpcExteriorSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "Numero_spcInterior_spcDomicilio_spca_spcrentar")
    protected String numeroSpcInteriorSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "ScoreHocelot")
    protected String scoreHocelot;
    @XmlElement(name = "Correo", required = true)
    protected String correo;
    @XmlElement(name = "Estado_spcDomicilio_spca_spcrentar")
    protected String estadoSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "Calle_spcDomicilio_spca_spcrentar")
    protected String calleSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "Fecha_spcNacimiento", required = true)
    protected String fechaSpcNacimiento;
    @XmlElement(name = "Numero_spcInterior_spcDomicilio_spcCliente", required = true)
    protected String numeroSpcInteriorSpcDomicilioSpcCliente;
    @XmlElement(name = "NUE")
    protected String nue;
    @XmlElement(name = "InCodigoAgenteNowo")
    protected String inCodigoAgenteNowo;
    @XmlElement(name = "RFC", required = true)
    protected String rfc;
    @XmlElement(name = "Inicio_spcVigencia_spcGarantia")
    protected String inicioSpcVigenciaSpcGarantia;
    @XmlElement(name = "Numero_spcIdentificacion_spcCliente", required = true)
    protected String numeroSpcIdentificacionSpcCliente;
    @XmlElement(name = "Tarjeta_spcCredito")
    protected String tarjetaSpcCredito;
    @XmlElement(name = "InCodigoPropiedad")
    protected String inCodigoPropiedad;
    @XmlElement(name = "Colonia_spcDomicilio_spcCliente_spca_spcrentar")
    protected String coloniaSpcDomicilioSpcClienteSpcaSpcrentar;
    @XmlElement(name = "Monto_spcRenta")
    protected String montoSpcRenta;
    @XmlElement(name = "FechaVencimientoTarjeta")
    protected String fechaVencimientoTarjeta;
    @XmlElement(name = "Colonia_spcDomicilio_spcCliente", required = true)
    protected String coloniaSpcDomicilioSpcCliente;
    @XmlElement(name = "Fin_spcVigencia_spcGarantia")
    protected String finSpcVigenciaSpcGarantia;
    @XmlElement(name = "InTipoPersona", required = true)
    protected String inTipoPersona;
    @XmlElement(name = "Inicio_spcVigencia_spcContrato")
    protected String inicioSpcVigenciaSpcContrato;
    @XmlElement(name = "Pais_spcDomicilio_spcCliente", required = true)
    protected String paisSpcDomicilioSpcCliente;
    @XmlElement(name = "Segundo_spcApellido", required = true)
    protected String segundoSpcApellido;
    @XmlElement(name = "Telefono", required = true)
    protected String telefono;
    @XmlElement(name = "Delegacion_slhMunicipio_spcDomicilio_spca_spcrentar")
    protected String delegacionSlhMunicipioSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "M2_spcDomicilio_spcRentar_spcCliente")
    protected String m2SpcDomicilioSpcRentarSpcCliente;
    @XmlElement(name = "TipoResidencia")
    protected String tipoResidencia;
    @XmlElement(name = "Lugar_spcNacimiento", required = true)
    protected String lugarSpcNacimiento;
    @XmlElement(name = "Nacionalidad", required = true)
    protected String nacionalidad;
    @XmlElement(name = "Codigo_spcPostal_spcDomicilio_spcCliente", required = true)
    protected String codigoSpcPostalSpcDomicilioSpcCliente;
    @XmlElement(name = "Cuenta")
    protected String cuenta;
    @XmlElement(name = "Delegacion_slhMunicipio_spcDomicilio_spcCliente", required = true)
    protected String delegacionSlhMunicipioSpcDomicilioSpcCliente;
    @XmlElement(name = "Primer_spcApellido", required = true)
    protected String primerSpcApellido;
    @XmlElement(name = "Tipo_spcIdentificacion_spcCliente", required = true)
    protected String tipoSpcIdentificacionSpcCliente;
    @XmlElement(name = "Codigo_spcPostal_spcDomicilio_spca_spcrentar")
    protected String codigoSpcPostalSpcDomicilioSpcaSpcrentar;
    @XmlElement(name = "Dia_spcLimite_spcPago_spcRenta")
    protected String diaSpcLimiteSpcPagoSpcRenta;
    @XmlElement(name = "Fin_spcVigencia_spcContrato")
    protected String finSpcVigenciaSpcContrato;
    @XmlElement(name = "Banco")
    protected String banco;
    @XmlElement(name = "Curp", required = true)
    protected String curp;
    @XmlElement(name = "Nombre", required = true)
    protected String nombre;
    @XmlElement(name = "Numero_spcExterior_spcDomicilio_spcCliente", required = true)
    protected String numeroSpcExteriorSpcDomicilioSpcCliente;
    @XmlElement(name = "Ciudad_spcDomicilio_spcCliente", required = true)
    protected String ciudadSpcDomicilioSpcCliente;
    @XmlElement(name = "Estado_spcDomicilio_spcCliente", required = true)
    protected String estadoSpcDomicilioSpcCliente;

    /**
     * Obtiene el valor de la propiedad ocupacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOcupacion() {
        return ocupacion;
    }

    /**
     * Define el valor de la propiedad ocupacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOcupacion(String value) {
        this.ocupacion = value;
    }

    /**
     * Obtiene el valor de la propiedad paisSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaisSpcDomicilioSpcaSpcrentar() {
        return paisSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad paisSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaisSpcDomicilioSpcaSpcrentar(String value) {
        this.paisSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad clabe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLABE() {
        return clabe;
    }

    /**
     * Define el valor de la propiedad clabe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLABE(String value) {
        this.clabe = value;
    }

    /**
     * Obtiene el valor de la propiedad calleSpcDomiclioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalleSpcDomiclioSpcCliente() {
        return calleSpcDomiclioSpcCliente;
    }

    /**
     * Define el valor de la propiedad calleSpcDomiclioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalleSpcDomiclioSpcCliente(String value) {
        this.calleSpcDomiclioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadSpcDomicilioSpcaSpcrentar() {
        return ciudadSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad ciudadSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadSpcDomicilioSpcaSpcrentar(String value) {
        this.ciudadSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad inPerfil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInPerfil() {
        return inPerfil;
    }

    /**
     * Define el valor de la propiedad inPerfil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInPerfil(String value) {
        this.inPerfil = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSpcExteriorSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSpcExteriorSpcDomicilioSpcaSpcrentar() {
        return numeroSpcExteriorSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad numeroSpcExteriorSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSpcExteriorSpcDomicilioSpcaSpcrentar(String value) {
        this.numeroSpcExteriorSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSpcInteriorSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSpcInteriorSpcDomicilioSpcaSpcrentar() {
        return numeroSpcInteriorSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad numeroSpcInteriorSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSpcInteriorSpcDomicilioSpcaSpcrentar(String value) {
        this.numeroSpcInteriorSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad scoreHocelot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScoreHocelot() {
        return scoreHocelot;
    }

    /**
     * Define el valor de la propiedad scoreHocelot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScoreHocelot(String value) {
        this.scoreHocelot = value;
    }

    /**
     * Obtiene el valor de la propiedad correo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Define el valor de la propiedad correo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreo(String value) {
        this.correo = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoSpcDomicilioSpcaSpcrentar() {
        return estadoSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad estadoSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoSpcDomicilioSpcaSpcrentar(String value) {
        this.estadoSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad calleSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalleSpcDomicilioSpcaSpcrentar() {
        return calleSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad calleSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalleSpcDomicilioSpcaSpcrentar(String value) {
        this.calleSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSpcNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSpcNacimiento() {
        return fechaSpcNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaSpcNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSpcNacimiento(String value) {
        this.fechaSpcNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSpcInteriorSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSpcInteriorSpcDomicilioSpcCliente() {
        return numeroSpcInteriorSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad numeroSpcInteriorSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSpcInteriorSpcDomicilioSpcCliente(String value) {
        this.numeroSpcInteriorSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad nue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUE() {
        return nue;
    }

    /**
     * Define el valor de la propiedad nue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUE(String value) {
        this.nue = value;
    }

    /**
     * Obtiene el valor de la propiedad inCodigoAgenteNowo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInCodigoAgenteNowo() {
        return inCodigoAgenteNowo;
    }

    /**
     * Define el valor de la propiedad inCodigoAgenteNowo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInCodigoAgenteNowo(String value) {
        this.inCodigoAgenteNowo = value;
    }

    /**
     * Obtiene el valor de la propiedad rfc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFC() {
        return rfc;
    }

    /**
     * Define el valor de la propiedad rfc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFC(String value) {
        this.rfc = value;
    }

    /**
     * Obtiene el valor de la propiedad inicioSpcVigenciaSpcGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioSpcVigenciaSpcGarantia() {
        return inicioSpcVigenciaSpcGarantia;
    }

    /**
     * Define el valor de la propiedad inicioSpcVigenciaSpcGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioSpcVigenciaSpcGarantia(String value) {
        this.inicioSpcVigenciaSpcGarantia = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSpcIdentificacionSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSpcIdentificacionSpcCliente() {
        return numeroSpcIdentificacionSpcCliente;
    }

    /**
     * Define el valor de la propiedad numeroSpcIdentificacionSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSpcIdentificacionSpcCliente(String value) {
        this.numeroSpcIdentificacionSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjetaSpcCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarjetaSpcCredito() {
        return tarjetaSpcCredito;
    }

    /**
     * Define el valor de la propiedad tarjetaSpcCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarjetaSpcCredito(String value) {
        this.tarjetaSpcCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad inCodigoPropiedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInCodigoPropiedad() {
        return inCodigoPropiedad;
    }

    /**
     * Define el valor de la propiedad inCodigoPropiedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInCodigoPropiedad(String value) {
        this.inCodigoPropiedad = value;
    }

    /**
     * Obtiene el valor de la propiedad coloniaSpcDomicilioSpcClienteSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColoniaSpcDomicilioSpcClienteSpcaSpcrentar() {
        return coloniaSpcDomicilioSpcClienteSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad coloniaSpcDomicilioSpcClienteSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColoniaSpcDomicilioSpcClienteSpcaSpcrentar(String value) {
        this.coloniaSpcDomicilioSpcClienteSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad montoSpcRenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoSpcRenta() {
        return montoSpcRenta;
    }

    /**
     * Define el valor de la propiedad montoSpcRenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoSpcRenta(String value) {
        this.montoSpcRenta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimientoTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVencimientoTarjeta() {
        return fechaVencimientoTarjeta;
    }

    /**
     * Define el valor de la propiedad fechaVencimientoTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVencimientoTarjeta(String value) {
        this.fechaVencimientoTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad coloniaSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColoniaSpcDomicilioSpcCliente() {
        return coloniaSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad coloniaSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColoniaSpcDomicilioSpcCliente(String value) {
        this.coloniaSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad finSpcVigenciaSpcGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinSpcVigenciaSpcGarantia() {
        return finSpcVigenciaSpcGarantia;
    }

    /**
     * Define el valor de la propiedad finSpcVigenciaSpcGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinSpcVigenciaSpcGarantia(String value) {
        this.finSpcVigenciaSpcGarantia = value;
    }

    /**
     * Obtiene el valor de la propiedad inTipoPersona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInTipoPersona() {
        return inTipoPersona;
    }

    /**
     * Define el valor de la propiedad inTipoPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInTipoPersona(String value) {
        this.inTipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad inicioSpcVigenciaSpcContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioSpcVigenciaSpcContrato() {
        return inicioSpcVigenciaSpcContrato;
    }

    /**
     * Define el valor de la propiedad inicioSpcVigenciaSpcContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioSpcVigenciaSpcContrato(String value) {
        this.inicioSpcVigenciaSpcContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad paisSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaisSpcDomicilioSpcCliente() {
        return paisSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad paisSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaisSpcDomicilioSpcCliente(String value) {
        this.paisSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad segundoSpcApellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegundoSpcApellido() {
        return segundoSpcApellido;
    }

    /**
     * Define el valor de la propiedad segundoSpcApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegundoSpcApellido(String value) {
        this.segundoSpcApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Define el valor de la propiedad telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Obtiene el valor de la propiedad delegacionSlhMunicipioSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegacionSlhMunicipioSpcDomicilioSpcaSpcrentar() {
        return delegacionSlhMunicipioSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad delegacionSlhMunicipioSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegacionSlhMunicipioSpcDomicilioSpcaSpcrentar(String value) {
        this.delegacionSlhMunicipioSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad m2SpcDomicilioSpcRentarSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getM2SpcDomicilioSpcRentarSpcCliente() {
        return m2SpcDomicilioSpcRentarSpcCliente;
    }

    /**
     * Define el valor de la propiedad m2SpcDomicilioSpcRentarSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setM2SpcDomicilioSpcRentarSpcCliente(String value) {
        this.m2SpcDomicilioSpcRentarSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoResidencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoResidencia() {
        return tipoResidencia;
    }

    /**
     * Define el valor de la propiedad tipoResidencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoResidencia(String value) {
        this.tipoResidencia = value;
    }

    /**
     * Obtiene el valor de la propiedad lugarSpcNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLugarSpcNacimiento() {
        return lugarSpcNacimiento;
    }

    /**
     * Define el valor de la propiedad lugarSpcNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLugarSpcNacimiento(String value) {
        this.lugarSpcNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad nacionalidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * Define el valor de la propiedad nacionalidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNacionalidad(String value) {
        this.nacionalidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoSpcPostalSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSpcPostalSpcDomicilioSpcCliente() {
        return codigoSpcPostalSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad codigoSpcPostalSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSpcPostalSpcDomicilioSpcCliente(String value) {
        this.codigoSpcPostalSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad delegacionSlhMunicipioSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegacionSlhMunicipioSpcDomicilioSpcCliente() {
        return delegacionSlhMunicipioSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad delegacionSlhMunicipioSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegacionSlhMunicipioSpcDomicilioSpcCliente(String value) {
        this.delegacionSlhMunicipioSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad primerSpcApellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimerSpcApellido() {
        return primerSpcApellido;
    }

    /**
     * Define el valor de la propiedad primerSpcApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimerSpcApellido(String value) {
        this.primerSpcApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoSpcIdentificacionSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSpcIdentificacionSpcCliente() {
        return tipoSpcIdentificacionSpcCliente;
    }

    /**
     * Define el valor de la propiedad tipoSpcIdentificacionSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSpcIdentificacionSpcCliente(String value) {
        this.tipoSpcIdentificacionSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoSpcPostalSpcDomicilioSpcaSpcrentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoSpcPostalSpcDomicilioSpcaSpcrentar() {
        return codigoSpcPostalSpcDomicilioSpcaSpcrentar;
    }

    /**
     * Define el valor de la propiedad codigoSpcPostalSpcDomicilioSpcaSpcrentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoSpcPostalSpcDomicilioSpcaSpcrentar(String value) {
        this.codigoSpcPostalSpcDomicilioSpcaSpcrentar = value;
    }

    /**
     * Obtiene el valor de la propiedad diaSpcLimiteSpcPagoSpcRenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiaSpcLimiteSpcPagoSpcRenta() {
        return diaSpcLimiteSpcPagoSpcRenta;
    }

    /**
     * Define el valor de la propiedad diaSpcLimiteSpcPagoSpcRenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiaSpcLimiteSpcPagoSpcRenta(String value) {
        this.diaSpcLimiteSpcPagoSpcRenta = value;
    }

    /**
     * Obtiene el valor de la propiedad finSpcVigenciaSpcContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinSpcVigenciaSpcContrato() {
        return finSpcVigenciaSpcContrato;
    }

    /**
     * Define el valor de la propiedad finSpcVigenciaSpcContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinSpcVigenciaSpcContrato(String value) {
        this.finSpcVigenciaSpcContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad banco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Define el valor de la propiedad banco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanco(String value) {
        this.banco = value;
    }

    /**
     * Obtiene el valor de la propiedad curp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurp() {
        return curp;
    }

    /**
     * Define el valor de la propiedad curp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurp(String value) {
        this.curp = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSpcExteriorSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSpcExteriorSpcDomicilioSpcCliente() {
        return numeroSpcExteriorSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad numeroSpcExteriorSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSpcExteriorSpcDomicilioSpcCliente(String value) {
        this.numeroSpcExteriorSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadSpcDomicilioSpcCliente() {
        return ciudadSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad ciudadSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadSpcDomicilioSpcCliente(String value) {
        this.ciudadSpcDomicilioSpcCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoSpcDomicilioSpcCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoSpcDomicilioSpcCliente() {
        return estadoSpcDomicilioSpcCliente;
    }

    /**
     * Define el valor de la propiedad estadoSpcDomicilioSpcCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoSpcDomicilioSpcCliente(String value) {
        this.estadoSpcDomicilioSpcCliente = value;
    }

}
