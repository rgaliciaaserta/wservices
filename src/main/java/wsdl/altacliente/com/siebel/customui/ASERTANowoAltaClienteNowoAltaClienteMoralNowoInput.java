
package wsdl.altacliente.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DelegacionMunicipioDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InicioVigenciaGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InicioVigenciaContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NumeroExteriorDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ColoniaDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CLABE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Correo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MetrosCuadradosDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MontoRenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DiaLimitePagoRenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CodigoPostalDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FinVigenciaContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaConstitucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EstadoDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InCodigoPropiedad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CiudadDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodigoPostalDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaisDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroExteriorDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EstadoDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CiudadDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DelegacionMunicipioDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroInteriorDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Cuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Perfil" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CalleDomiclioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CalleDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ColoniaDomicilioClienteaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FinVigenciaGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Banco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaisDomicilioaRentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroInteriorDomicilioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "delegacionMunicipioDomicilioCliente",
    "inicioVigenciaGarantia",
    "inicioVigenciaContrato",
    "razonSocial",
    "numeroExteriorDomicilioCliente",
    "coloniaDomicilioCliente",
    "clabe",
    "correo",
    "metrosCuadradosDomicilioaRentar",
    "montoRenta",
    "diaLimitePagoRenta",
    "rfc",
    "codigoPostalDomicilioCliente",
    "finVigenciaContrato",
    "fechaConstitucion",
    "estadoDomicilioCliente",
    "inCodigoPropiedad",
    "ciudadDomicilioaRentar",
    "codigoPostalDomicilioaRentar",
    "paisDomicilioCliente",
    "numeroExteriorDomicilioaRentar",
    "estadoDomicilioaRentar",
    "ciudadDomicilioCliente",
    "delegacionMunicipioDomicilioaRentar",
    "numeroInteriorDomicilioaRentar",
    "cuenta",
    "perfil",
    "calleDomiclioCliente",
    "calleDomicilioaRentar",
    "coloniaDomicilioClienteaRentar",
    "finVigenciaGarantia",
    "banco",
    "paisDomicilioaRentar",
    "numeroInteriorDomicilioCliente"
})
@XmlRootElement(name = "ASERTANowo-AltaClienteNowoAltaClienteMoralNowo_Input")
public class ASERTANowoAltaClienteNowoAltaClienteMoralNowoInput {

    @XmlElement(name = "DelegacionMunicipioDomicilioCliente")
    protected String delegacionMunicipioDomicilioCliente;
    @XmlElement(name = "InicioVigenciaGarantia")
    protected String inicioVigenciaGarantia;
    @XmlElement(name = "InicioVigenciaContrato")
    protected String inicioVigenciaContrato;
    @XmlElement(name = "RazonSocial", required = true)
    protected String razonSocial;
    @XmlElement(name = "NumeroExteriorDomicilioCliente")
    protected String numeroExteriorDomicilioCliente;
    @XmlElement(name = "ColoniaDomicilioCliente")
    protected String coloniaDomicilioCliente;
    @XmlElement(name = "CLABE")
    protected String clabe;
    @XmlElement(name = "Correo")
    protected String correo;
    @XmlElement(name = "MetrosCuadradosDomicilioaRentar")
    protected String metrosCuadradosDomicilioaRentar;
    @XmlElement(name = "MontoRenta")
    protected String montoRenta;
    @XmlElement(name = "DiaLimitePagoRenta")
    protected String diaLimitePagoRenta;
    @XmlElement(name = "RFC", required = true)
    protected String rfc;
    @XmlElement(name = "CodigoPostalDomicilioCliente")
    protected String codigoPostalDomicilioCliente;
    @XmlElement(name = "FinVigenciaContrato")
    protected String finVigenciaContrato;
    @XmlElement(name = "FechaConstitucion")
    protected String fechaConstitucion;
    @XmlElement(name = "EstadoDomicilioCliente")
    protected String estadoDomicilioCliente;
    @XmlElement(name = "InCodigoPropiedad")
    protected String inCodigoPropiedad;
    @XmlElement(name = "CiudadDomicilioaRentar")
    protected String ciudadDomicilioaRentar;
    @XmlElement(name = "CodigoPostalDomicilioaRentar")
    protected String codigoPostalDomicilioaRentar;
    @XmlElement(name = "PaisDomicilioCliente")
    protected String paisDomicilioCliente;
    @XmlElement(name = "NumeroExteriorDomicilioaRentar")
    protected String numeroExteriorDomicilioaRentar;
    @XmlElement(name = "EstadoDomicilioaRentar")
    protected String estadoDomicilioaRentar;
    @XmlElement(name = "CiudadDomicilioCliente")
    protected String ciudadDomicilioCliente;
    @XmlElement(name = "DelegacionMunicipioDomicilioaRentar")
    protected String delegacionMunicipioDomicilioaRentar;
    @XmlElement(name = "NumeroInteriorDomicilioaRentar")
    protected String numeroInteriorDomicilioaRentar;
    @XmlElement(name = "Cuenta")
    protected String cuenta;
    @XmlElement(name = "Perfil", required = true)
    protected String perfil;
    @XmlElement(name = "CalleDomiclioCliente")
    protected String calleDomiclioCliente;
    @XmlElement(name = "CalleDomicilioaRentar")
    protected String calleDomicilioaRentar;
    @XmlElement(name = "ColoniaDomicilioClienteaRentar")
    protected String coloniaDomicilioClienteaRentar;
    @XmlElement(name = "FinVigenciaGarantia")
    protected String finVigenciaGarantia;
    @XmlElement(name = "Banco")
    protected String banco;
    @XmlElement(name = "PaisDomicilioaRentar")
    protected String paisDomicilioaRentar;
    @XmlElement(name = "NumeroInteriorDomicilioCliente")
    protected String numeroInteriorDomicilioCliente;

    /**
     * Obtiene el valor de la propiedad delegacionMunicipioDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegacionMunicipioDomicilioCliente() {
        return delegacionMunicipioDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad delegacionMunicipioDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegacionMunicipioDomicilioCliente(String value) {
        this.delegacionMunicipioDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad inicioVigenciaGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioVigenciaGarantia() {
        return inicioVigenciaGarantia;
    }

    /**
     * Define el valor de la propiedad inicioVigenciaGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioVigenciaGarantia(String value) {
        this.inicioVigenciaGarantia = value;
    }

    /**
     * Obtiene el valor de la propiedad inicioVigenciaContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioVigenciaContrato() {
        return inicioVigenciaContrato;
    }

    /**
     * Define el valor de la propiedad inicioVigenciaContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioVigenciaContrato(String value) {
        this.inicioVigenciaContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad razonSocial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * Define el valor de la propiedad razonSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroExteriorDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroExteriorDomicilioCliente() {
        return numeroExteriorDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad numeroExteriorDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroExteriorDomicilioCliente(String value) {
        this.numeroExteriorDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad coloniaDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColoniaDomicilioCliente() {
        return coloniaDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad coloniaDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColoniaDomicilioCliente(String value) {
        this.coloniaDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad clabe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLABE() {
        return clabe;
    }

    /**
     * Define el valor de la propiedad clabe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLABE(String value) {
        this.clabe = value;
    }

    /**
     * Obtiene el valor de la propiedad correo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Define el valor de la propiedad correo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreo(String value) {
        this.correo = value;
    }

    /**
     * Obtiene el valor de la propiedad metrosCuadradosDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetrosCuadradosDomicilioaRentar() {
        return metrosCuadradosDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad metrosCuadradosDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetrosCuadradosDomicilioaRentar(String value) {
        this.metrosCuadradosDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad montoRenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoRenta() {
        return montoRenta;
    }

    /**
     * Define el valor de la propiedad montoRenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoRenta(String value) {
        this.montoRenta = value;
    }

    /**
     * Obtiene el valor de la propiedad diaLimitePagoRenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiaLimitePagoRenta() {
        return diaLimitePagoRenta;
    }

    /**
     * Define el valor de la propiedad diaLimitePagoRenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiaLimitePagoRenta(String value) {
        this.diaLimitePagoRenta = value;
    }

    /**
     * Obtiene el valor de la propiedad rfc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFC() {
        return rfc;
    }

    /**
     * Define el valor de la propiedad rfc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFC(String value) {
        this.rfc = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoPostalDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPostalDomicilioCliente() {
        return codigoPostalDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad codigoPostalDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPostalDomicilioCliente(String value) {
        this.codigoPostalDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad finVigenciaContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinVigenciaContrato() {
        return finVigenciaContrato;
    }

    /**
     * Define el valor de la propiedad finVigenciaContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinVigenciaContrato(String value) {
        this.finVigenciaContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaConstitucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaConstitucion() {
        return fechaConstitucion;
    }

    /**
     * Define el valor de la propiedad fechaConstitucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaConstitucion(String value) {
        this.fechaConstitucion = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoDomicilioCliente() {
        return estadoDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad estadoDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoDomicilioCliente(String value) {
        this.estadoDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad inCodigoPropiedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInCodigoPropiedad() {
        return inCodigoPropiedad;
    }

    /**
     * Define el valor de la propiedad inCodigoPropiedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInCodigoPropiedad(String value) {
        this.inCodigoPropiedad = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDomicilioaRentar() {
        return ciudadDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad ciudadDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDomicilioaRentar(String value) {
        this.ciudadDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoPostalDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPostalDomicilioaRentar() {
        return codigoPostalDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad codigoPostalDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPostalDomicilioaRentar(String value) {
        this.codigoPostalDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad paisDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaisDomicilioCliente() {
        return paisDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad paisDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaisDomicilioCliente(String value) {
        this.paisDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroExteriorDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroExteriorDomicilioaRentar() {
        return numeroExteriorDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad numeroExteriorDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroExteriorDomicilioaRentar(String value) {
        this.numeroExteriorDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoDomicilioaRentar() {
        return estadoDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad estadoDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoDomicilioaRentar(String value) {
        this.estadoDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDomicilioCliente() {
        return ciudadDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad ciudadDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDomicilioCliente(String value) {
        this.ciudadDomicilioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad delegacionMunicipioDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelegacionMunicipioDomicilioaRentar() {
        return delegacionMunicipioDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad delegacionMunicipioDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelegacionMunicipioDomicilioaRentar(String value) {
        this.delegacionMunicipioDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroInteriorDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroInteriorDomicilioaRentar() {
        return numeroInteriorDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad numeroInteriorDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroInteriorDomicilioaRentar(String value) {
        this.numeroInteriorDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad perfil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     * Define el valor de la propiedad perfil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerfil(String value) {
        this.perfil = value;
    }

    /**
     * Obtiene el valor de la propiedad calleDomiclioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalleDomiclioCliente() {
        return calleDomiclioCliente;
    }

    /**
     * Define el valor de la propiedad calleDomiclioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalleDomiclioCliente(String value) {
        this.calleDomiclioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad calleDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalleDomicilioaRentar() {
        return calleDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad calleDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalleDomicilioaRentar(String value) {
        this.calleDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad coloniaDomicilioClienteaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColoniaDomicilioClienteaRentar() {
        return coloniaDomicilioClienteaRentar;
    }

    /**
     * Define el valor de la propiedad coloniaDomicilioClienteaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColoniaDomicilioClienteaRentar(String value) {
        this.coloniaDomicilioClienteaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad finVigenciaGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinVigenciaGarantia() {
        return finVigenciaGarantia;
    }

    /**
     * Define el valor de la propiedad finVigenciaGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinVigenciaGarantia(String value) {
        this.finVigenciaGarantia = value;
    }

    /**
     * Obtiene el valor de la propiedad banco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Define el valor de la propiedad banco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanco(String value) {
        this.banco = value;
    }

    /**
     * Obtiene el valor de la propiedad paisDomicilioaRentar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaisDomicilioaRentar() {
        return paisDomicilioaRentar;
    }

    /**
     * Define el valor de la propiedad paisDomicilioaRentar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaisDomicilioaRentar(String value) {
        this.paisDomicilioaRentar = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroInteriorDomicilioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroInteriorDomicilioCliente() {
        return numeroInteriorDomicilioCliente;
    }

    /**
     * Define el valor de la propiedad numeroInteriorDomicilioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroInteriorDomicilioCliente(String value) {
        this.numeroInteriorDomicilioCliente = value;
    }

}
