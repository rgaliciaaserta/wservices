
package wsdl.creabase.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InIdObligado" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InEsquema" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RefInmNowo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InMoneda" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InCodigoPropiedad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HocOperationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InIdAsegurado" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InIdContratante" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InBucket" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InMontoRenta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InFechaInicio" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InMeses" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="InAgenteReferenciador" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inIdObligado",
    "inEsquema",
    "refInmNowo",
    "inMoneda",
    "inCodigoPropiedad",
    "hocOperationId",
    "inIdAsegurado",
    "inIdContratante",
    "inBucket",
    "inMontoRenta",
    "inFechaInicio",
    "inMeses",
    "inAgenteReferenciador"
})
@XmlRootElement(name = "ASERTANowo-CreaBaseEmisionCertificadoNowo_1_Input")
public class ASERTANowoCreaBaseEmisionCertificadoNowo1Input {

    @XmlElement(name = "InIdObligado", required = true)
    protected String inIdObligado;
    @XmlElement(name = "InEsquema", required = true)
    protected String inEsquema;
    @XmlElement(name = "RefInmNowo")
    protected String refInmNowo;
    @XmlElement(name = "InMoneda", required = true)
    protected String inMoneda;
    @XmlElement(name = "InCodigoPropiedad")
    protected String inCodigoPropiedad;
    @XmlElement(name = "HocOperationId")
    protected String hocOperationId;
    @XmlElement(name = "InIdAsegurado", required = true)
    protected String inIdAsegurado;
    @XmlElement(name = "InIdContratante", required = true)
    protected String inIdContratante;
    @XmlElement(name = "InBucket", required = true)
    protected String inBucket;
    @XmlElement(name = "InMontoRenta", required = true)
    protected String inMontoRenta;
    @XmlElement(name = "InFechaInicio", required = true)
    protected String inFechaInicio;
    @XmlElement(name = "InMeses", required = true)
    protected String inMeses;
    @XmlElement(name = "InAgenteReferenciador", required = true)
    protected String inAgenteReferenciador;

    /**
     * Obtiene el valor de la propiedad inIdObligado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInIdObligado() {
        return inIdObligado;
    }

    /**
     * Define el valor de la propiedad inIdObligado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInIdObligado(String value) {
        this.inIdObligado = value;
    }

    /**
     * Obtiene el valor de la propiedad inEsquema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInEsquema() {
        return inEsquema;
    }

    /**
     * Define el valor de la propiedad inEsquema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInEsquema(String value) {
        this.inEsquema = value;
    }

    /**
     * Obtiene el valor de la propiedad refInmNowo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefInmNowo() {
        return refInmNowo;
    }

    /**
     * Define el valor de la propiedad refInmNowo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefInmNowo(String value) {
        this.refInmNowo = value;
    }

    /**
     * Obtiene el valor de la propiedad inMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInMoneda() {
        return inMoneda;
    }

    /**
     * Define el valor de la propiedad inMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInMoneda(String value) {
        this.inMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad inCodigoPropiedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInCodigoPropiedad() {
        return inCodigoPropiedad;
    }

    /**
     * Define el valor de la propiedad inCodigoPropiedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInCodigoPropiedad(String value) {
        this.inCodigoPropiedad = value;
    }

    /**
     * Obtiene el valor de la propiedad hocOperationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHocOperationId() {
        return hocOperationId;
    }

    /**
     * Define el valor de la propiedad hocOperationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHocOperationId(String value) {
        this.hocOperationId = value;
    }

    /**
     * Obtiene el valor de la propiedad inIdAsegurado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInIdAsegurado() {
        return inIdAsegurado;
    }

    /**
     * Define el valor de la propiedad inIdAsegurado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInIdAsegurado(String value) {
        this.inIdAsegurado = value;
    }

    /**
     * Obtiene el valor de la propiedad inIdContratante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInIdContratante() {
        return inIdContratante;
    }

    /**
     * Define el valor de la propiedad inIdContratante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInIdContratante(String value) {
        this.inIdContratante = value;
    }

    /**
     * Obtiene el valor de la propiedad inBucket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInBucket() {
        return inBucket;
    }

    /**
     * Define el valor de la propiedad inBucket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInBucket(String value) {
        this.inBucket = value;
    }

    /**
     * Obtiene el valor de la propiedad inMontoRenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInMontoRenta() {
        return inMontoRenta;
    }

    /**
     * Define el valor de la propiedad inMontoRenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInMontoRenta(String value) {
        this.inMontoRenta = value;
    }

    /**
     * Obtiene el valor de la propiedad inFechaInicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInFechaInicio() {
        return inFechaInicio;
    }

    /**
     * Define el valor de la propiedad inFechaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInFechaInicio(String value) {
        this.inFechaInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad inMeses.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInMeses() {
        return inMeses;
    }

    /**
     * Define el valor de la propiedad inMeses.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInMeses(String value) {
        this.inMeses = value;
    }

    /**
     * Obtiene el valor de la propiedad inAgenteReferenciador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInAgenteReferenciador() {
        return inAgenteReferenciador;
    }

    /**
     * Define el valor de la propiedad inAgenteReferenciador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInAgenteReferenciador(String value) {
        this.inAgenteReferenciador = value;
    }

}
