
package wsdl.creabase.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OutDerechosPoliza" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutExito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutGastosInv" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIVARecibo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIdBase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutMensaje" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutPrima" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutPrimaTarifa" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutSubTotalRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutTotalCertificado" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutTotalRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "outDerechosPoliza",
    "outExito",
    "outGastosInv",
    "outIVARecibo",
    "outIdBase",
    "outMensaje",
    "outPrima",
    "outPrimaTarifa",
    "outSubTotalRecibo",
    "outTotalCertificado",
    "outTotalRecibo"
})
@XmlRootElement(name = "ASERTANowo-CreaBaseEmisionCertificadoNowo_1_Output")
public class ASERTANowoCreaBaseEmisionCertificadoNowo1Output {

    @XmlElement(name = "OutDerechosPoliza", required = true)
    protected String outDerechosPoliza;
    @XmlElement(name = "OutExito", required = true)
    protected String outExito;
    @XmlElement(name = "OutGastosInv", required = true)
    protected String outGastosInv;
    @XmlElement(name = "OutIVARecibo", required = true)
    protected String outIVARecibo;
    @XmlElement(name = "OutIdBase", required = true)
    protected String outIdBase;
    @XmlElement(name = "OutMensaje", required = true)
    protected String outMensaje;
    @XmlElement(name = "OutPrima", required = true)
    protected String outPrima;
    @XmlElement(name = "OutPrimaTarifa", required = true)
    protected String outPrimaTarifa;
    @XmlElement(name = "OutSubTotalRecibo", required = true)
    protected String outSubTotalRecibo;
    @XmlElement(name = "OutTotalCertificado", required = true)
    protected String outTotalCertificado;
    @XmlElement(name = "OutTotalRecibo", required = true)
    protected String outTotalRecibo;

    /**
     * Obtiene el valor de la propiedad outDerechosPoliza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutDerechosPoliza() {
        return outDerechosPoliza;
    }

    /**
     * Define el valor de la propiedad outDerechosPoliza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutDerechosPoliza(String value) {
        this.outDerechosPoliza = value;
    }

    /**
     * Obtiene el valor de la propiedad outExito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutExito() {
        return outExito;
    }

    /**
     * Define el valor de la propiedad outExito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutExito(String value) {
        this.outExito = value;
    }

    /**
     * Obtiene el valor de la propiedad outGastosInv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutGastosInv() {
        return outGastosInv;
    }

    /**
     * Define el valor de la propiedad outGastosInv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutGastosInv(String value) {
        this.outGastosInv = value;
    }

    /**
     * Obtiene el valor de la propiedad outIVARecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIVARecibo() {
        return outIVARecibo;
    }

    /**
     * Define el valor de la propiedad outIVARecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIVARecibo(String value) {
        this.outIVARecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad outIdBase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIdBase() {
        return outIdBase;
    }

    /**
     * Define el valor de la propiedad outIdBase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIdBase(String value) {
        this.outIdBase = value;
    }

    /**
     * Obtiene el valor de la propiedad outMensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutMensaje() {
        return outMensaje;
    }

    /**
     * Define el valor de la propiedad outMensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutMensaje(String value) {
        this.outMensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad outPrima.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutPrima() {
        return outPrima;
    }

    /**
     * Define el valor de la propiedad outPrima.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutPrima(String value) {
        this.outPrima = value;
    }

    /**
     * Obtiene el valor de la propiedad outPrimaTarifa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutPrimaTarifa() {
        return outPrimaTarifa;
    }

    /**
     * Define el valor de la propiedad outPrimaTarifa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutPrimaTarifa(String value) {
        this.outPrimaTarifa = value;
    }

    /**
     * Obtiene el valor de la propiedad outSubTotalRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutSubTotalRecibo() {
        return outSubTotalRecibo;
    }

    /**
     * Define el valor de la propiedad outSubTotalRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutSubTotalRecibo(String value) {
        this.outSubTotalRecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad outTotalCertificado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutTotalCertificado() {
        return outTotalCertificado;
    }

    /**
     * Define el valor de la propiedad outTotalCertificado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutTotalCertificado(String value) {
        this.outTotalCertificado = value;
    }

    /**
     * Obtiene el valor de la propiedad outTotalRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutTotalRecibo() {
        return outTotalRecibo;
    }

    /**
     * Define el valor de la propiedad outTotalRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutTotalRecibo(String value) {
        this.outTotalRecibo = value;
    }

}
