
package wsdl.nowopago.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Exito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MensajeSalida" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exito",
    "mensajeSalida"
})
@XmlRootElement(name = "ASERTANowoPagoBSInsertaPago_Output")
public class ASERTANowoPagoBSInsertaPagoOutput {

    @XmlElement(name = "Exito", required = true)
    protected String exito;
    @XmlElement(name = "MensajeSalida", required = true)
    protected String mensajeSalida;

    /**
     * Obtiene el valor de la propiedad exito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExito() {
        return exito;
    }

    /**
     * Define el valor de la propiedad exito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExito(String value) {
        this.exito = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajeSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeSalida() {
        return mensajeSalida;
    }

    /**
     * Define el valor de la propiedad mensajeSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeSalida(String value) {
        this.mensajeSalida = value;
    }

}
