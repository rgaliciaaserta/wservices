
package wsdl.verificacodigoafiliado.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodigoAfiliado" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codigoAfiliado"
})
@XmlRootElement(name = "ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigo_Input")
public class ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoInput {

    @XmlElement(name = "CodigoAfiliado", required = true)
    protected String codigoAfiliado;

    /**
     * Obtiene el valor de la propiedad codigoAfiliado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAfiliado() {
        return codigoAfiliado;
    }

    /**
     * Define el valor de la propiedad codigoAfiliado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAfiliado(String value) {
        this.codigoAfiliado = value;
    }

}
