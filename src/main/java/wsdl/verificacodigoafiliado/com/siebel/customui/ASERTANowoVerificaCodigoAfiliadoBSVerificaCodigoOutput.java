
package wsdl.verificacodigoafiliado.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AfiliadoValido" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PromotorInmobiliario" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "afiliadoValido",
    "mensaje",
    "promotorInmobiliario"
})
@XmlRootElement(name = "ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigo_Output")
public class ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoOutput {

    @XmlElement(name = "AfiliadoValido", required = true)
    protected String afiliadoValido;
    @XmlElement(name = "Mensaje", required = true)
    protected String mensaje;
    @XmlElement(name = "PromotorInmobiliario", required = true)
    protected String promotorInmobiliario;

    /**
     * Obtiene el valor de la propiedad afiliadoValido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAfiliadoValido() {
        return afiliadoValido;
    }

    /**
     * Define el valor de la propiedad afiliadoValido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAfiliadoValido(String value) {
        this.afiliadoValido = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad promotorInmobiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotorInmobiliario() {
        return promotorInmobiliario;
    }

    /**
     * Define el valor de la propiedad promotorInmobiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotorInmobiliario(String value) {
        this.promotorInmobiliario = value;
    }

}
