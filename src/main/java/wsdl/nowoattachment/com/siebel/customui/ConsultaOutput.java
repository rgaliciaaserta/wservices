
package wsdl.nowoattachment.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import wsdl.nowoattachment.com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io.ListOfAsertaConvierteArchivoBaseNowoIo;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Error_spcCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Error_spcMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Exito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element ref="{http://www.siebel.com/xml/ASERTA%20Convierte%20Archivo%20Base%20Nowo%20IO}ListOfAsertaConvierteArchivoBaseNowoIo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "errorSpcCode",
    "errorSpcMessage",
    "exito",
    "mensaje",
    "listOfAsertaConvierteArchivoBaseNowoIo"
})
@XmlRootElement(name = "Consulta_Output")
public class ConsultaOutput {

    @XmlElement(name = "Error_spcCode", required = true)
    protected String errorSpcCode;
    @XmlElement(name = "Error_spcMessage", required = true)
    protected String errorSpcMessage;
    @XmlElement(name = "Exito", required = true)
    protected String exito;
    @XmlElement(name = "Mensaje", required = true)
    protected String mensaje;
    @XmlElement(name = "ListOfAsertaConvierteArchivoBaseNowoIo", namespace = "http://www.siebel.com/xml/ASERTA%20Convierte%20Archivo%20Base%20Nowo%20IO", required = true)
    protected ListOfAsertaConvierteArchivoBaseNowoIo listOfAsertaConvierteArchivoBaseNowoIo;

    /**
     * Obtiene el valor de la propiedad errorSpcCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorSpcCode() {
        return errorSpcCode;
    }

    /**
     * Define el valor de la propiedad errorSpcCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorSpcCode(String value) {
        this.errorSpcCode = value;
    }

    /**
     * Obtiene el valor de la propiedad errorSpcMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorSpcMessage() {
        return errorSpcMessage;
    }

    /**
     * Define el valor de la propiedad errorSpcMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorSpcMessage(String value) {
        this.errorSpcMessage = value;
    }

    /**
     * Obtiene el valor de la propiedad exito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExito() {
        return exito;
    }

    /**
     * Define el valor de la propiedad exito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExito(String value) {
        this.exito = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad listOfAsertaConvierteArchivoBaseNowoIo.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAsertaConvierteArchivoBaseNowoIo }
     *     
     */
    public ListOfAsertaConvierteArchivoBaseNowoIo getListOfAsertaConvierteArchivoBaseNowoIo() {
        return listOfAsertaConvierteArchivoBaseNowoIo;
    }

    /**
     * Define el valor de la propiedad listOfAsertaConvierteArchivoBaseNowoIo.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAsertaConvierteArchivoBaseNowoIo }
     *     
     */
    public void setListOfAsertaConvierteArchivoBaseNowoIo(ListOfAsertaConvierteArchivoBaseNowoIo value) {
        this.listOfAsertaConvierteArchivoBaseNowoIo = value;
    }

}
