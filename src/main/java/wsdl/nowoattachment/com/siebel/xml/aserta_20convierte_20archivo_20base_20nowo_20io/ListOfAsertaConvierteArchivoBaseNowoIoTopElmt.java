
package wsdl.nowoattachment.com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListOfAsertaConvierteArchivoBaseNowoIoTopElmt complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListOfAsertaConvierteArchivoBaseNowoIoTopElmt"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ListOfAsertaConvierteArchivoBaseNowoIo" type="{http://www.siebel.com/xml/ASERTA%20Convierte%20Archivo%20Base%20Nowo%20IO}ListOfAsertaConvierteArchivoBaseNowoIo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfAsertaConvierteArchivoBaseNowoIoTopElmt", propOrder = {
    "listOfAsertaConvierteArchivoBaseNowoIo"
})
public class ListOfAsertaConvierteArchivoBaseNowoIoTopElmt {

    @XmlElement(name = "ListOfAsertaConvierteArchivoBaseNowoIo", required = true)
    protected ListOfAsertaConvierteArchivoBaseNowoIo listOfAsertaConvierteArchivoBaseNowoIo;

    /**
     * Obtiene el valor de la propiedad listOfAsertaConvierteArchivoBaseNowoIo.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAsertaConvierteArchivoBaseNowoIo }
     *     
     */
    public ListOfAsertaConvierteArchivoBaseNowoIo getListOfAsertaConvierteArchivoBaseNowoIo() {
        return listOfAsertaConvierteArchivoBaseNowoIo;
    }

    /**
     * Define el valor de la propiedad listOfAsertaConvierteArchivoBaseNowoIo.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAsertaConvierteArchivoBaseNowoIo }
     *     
     */
    public void setListOfAsertaConvierteArchivoBaseNowoIo(ListOfAsertaConvierteArchivoBaseNowoIo value) {
        this.listOfAsertaConvierteArchivoBaseNowoIo = value;
    }

}
