
package wsdl.nowoattachment.com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOfAsertaConvierteArchivoBaseNowoIo_QNAME = new QName("http://www.siebel.com/xml/ASERTA%20Convierte%20Archivo%20Base%20Nowo%20IO", "ListOfAsertaConvierteArchivoBaseNowoIo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOfAsertaConvierteArchivoBaseNowoIo }
     * 
     */
    public ListOfAsertaConvierteArchivoBaseNowoIo createListOfAsertaConvierteArchivoBaseNowoIo() {
        return new ListOfAsertaConvierteArchivoBaseNowoIo();
    }

    /**
     * Create an instance of {@link ListOfAsertaConvierteArchivoBaseNowoIoTopElmt }
     * 
     */
    public ListOfAsertaConvierteArchivoBaseNowoIoTopElmt createListOfAsertaConvierteArchivoBaseNowoIoTopElmt() {
        return new ListOfAsertaConvierteArchivoBaseNowoIoTopElmt();
    }

    /**
     * Create an instance of {@link AsertaConvierteArchivoBaseBc }
     * 
     */
    public AsertaConvierteArchivoBaseBc createAsertaConvierteArchivoBaseBc() {
        return new AsertaConvierteArchivoBaseBc();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfAsertaConvierteArchivoBaseNowoIo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListOfAsertaConvierteArchivoBaseNowoIo }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/ASERTA%20Convierte%20Archivo%20Base%20Nowo%20IO", name = "ListOfAsertaConvierteArchivoBaseNowoIo")
    public JAXBElement<ListOfAsertaConvierteArchivoBaseNowoIo> createListOfAsertaConvierteArchivoBaseNowoIo(ListOfAsertaConvierteArchivoBaseNowoIo value) {
        return new JAXBElement<ListOfAsertaConvierteArchivoBaseNowoIo>(_ListOfAsertaConvierteArchivoBaseNowoIo_QNAME, ListOfAsertaConvierteArchivoBaseNowoIo.class, null, value);
    }

}
