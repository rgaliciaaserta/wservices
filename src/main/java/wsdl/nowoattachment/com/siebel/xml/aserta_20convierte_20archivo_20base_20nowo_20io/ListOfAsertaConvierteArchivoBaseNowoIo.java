
package wsdl.nowoattachment.com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListOfAsertaConvierteArchivoBaseNowoIo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListOfAsertaConvierteArchivoBaseNowoIo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AsertaConvierteArchivoBaseBc" type="{http://www.siebel.com/xml/ASERTA%20Convierte%20Archivo%20Base%20Nowo%20IO}AsertaConvierteArchivoBaseBc" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfAsertaConvierteArchivoBaseNowoIo", propOrder = {
    "asertaConvierteArchivoBaseBc"
})
public class ListOfAsertaConvierteArchivoBaseNowoIo {

    @XmlElement(name = "AsertaConvierteArchivoBaseBc")
    protected List<AsertaConvierteArchivoBaseBc> asertaConvierteArchivoBaseBc;

    /**
     * Gets the value of the asertaConvierteArchivoBaseBc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asertaConvierteArchivoBaseBc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsertaConvierteArchivoBaseBc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AsertaConvierteArchivoBaseBc }
     * 
     * 
     */
    public List<AsertaConvierteArchivoBaseBc> getAsertaConvierteArchivoBaseBc() {
        if (asertaConvierteArchivoBaseBc == null) {
            asertaConvierteArchivoBaseBc = new ArrayList<AsertaConvierteArchivoBaseBc>();
        }
        return this.asertaConvierteArchivoBaseBc;
    }

}
