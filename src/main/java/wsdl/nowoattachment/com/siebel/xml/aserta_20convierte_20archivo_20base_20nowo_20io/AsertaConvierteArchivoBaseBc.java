
package wsdl.nowoattachment.com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AsertaConvierteArchivoBaseBc complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AsertaConvierteArchivoBaseBc"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Searchspec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityFileExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityFileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttachmentCategory" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AttachmentType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LlaveNowo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PasswordNowo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="VectorNowo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityFileBuffer" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Operation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsertaConvierteArchivoBaseBc", propOrder = {
    "id",
    "searchspec",
    "activityId",
    "activityComments",
    "activityFileExt",
    "activityFileName",
    "attachmentCategory",
    "attachmentType",
    "llaveNowo",
    "passwordNowo",
    "status",
    "vectorNowo",
    "activityFileBuffer"
})
public class AsertaConvierteArchivoBaseBc {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Searchspec")
    protected String searchspec;
    @XmlElement(name = "ActivityId")
    protected String activityId;
    @XmlElement(name = "ActivityComments")
    protected String activityComments;
    @XmlElement(name = "ActivityFileExt")
    protected String activityFileExt;
    @XmlElement(name = "ActivityFileName")
    protected String activityFileName;
    @XmlElement(name = "AttachmentCategory", required = true)
    protected String attachmentCategory;
    @XmlElement(name = "AttachmentType", required = true)
    protected String attachmentType;
    @XmlElement(name = "LlaveNowo")
    protected String llaveNowo;
    @XmlElement(name = "PasswordNowo")
    protected String passwordNowo;
    @XmlElement(name = "Status", required = true)
    protected String status;
    @XmlElement(name = "VectorNowo")
    protected String vectorNowo;
    @XmlElement(name = "ActivityFileBuffer")
    protected byte[] activityFileBuffer;
    @XmlAttribute(name = "Operation")
    protected String operation;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad searchspec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchspec() {
        return searchspec;
    }

    /**
     * Define el valor de la propiedad searchspec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchspec(String value) {
        this.searchspec = value;
    }

    /**
     * Obtiene el valor de la propiedad activityId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityId() {
        return activityId;
    }

    /**
     * Define el valor de la propiedad activityId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityId(String value) {
        this.activityId = value;
    }

    /**
     * Obtiene el valor de la propiedad activityComments.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityComments() {
        return activityComments;
    }

    /**
     * Define el valor de la propiedad activityComments.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityComments(String value) {
        this.activityComments = value;
    }

    /**
     * Obtiene el valor de la propiedad activityFileExt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityFileExt() {
        return activityFileExt;
    }

    /**
     * Define el valor de la propiedad activityFileExt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityFileExt(String value) {
        this.activityFileExt = value;
    }

    /**
     * Obtiene el valor de la propiedad activityFileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityFileName() {
        return activityFileName;
    }

    /**
     * Define el valor de la propiedad activityFileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityFileName(String value) {
        this.activityFileName = value;
    }

    /**
     * Obtiene el valor de la propiedad attachmentCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachmentCategory() {
        return attachmentCategory;
    }

    /**
     * Define el valor de la propiedad attachmentCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachmentCategory(String value) {
        this.attachmentCategory = value;
    }

    /**
     * Obtiene el valor de la propiedad attachmentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachmentType() {
        return attachmentType;
    }

    /**
     * Define el valor de la propiedad attachmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachmentType(String value) {
        this.attachmentType = value;
    }

    /**
     * Obtiene el valor de la propiedad llaveNowo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLlaveNowo() {
        return llaveNowo;
    }

    /**
     * Define el valor de la propiedad llaveNowo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLlaveNowo(String value) {
        this.llaveNowo = value;
    }

    /**
     * Obtiene el valor de la propiedad passwordNowo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordNowo() {
        return passwordNowo;
    }

    /**
     * Define el valor de la propiedad passwordNowo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordNowo(String value) {
        this.passwordNowo = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad vectorNowo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVectorNowo() {
        return vectorNowo;
    }

    /**
     * Define el valor de la propiedad vectorNowo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVectorNowo(String value) {
        this.vectorNowo = value;
    }

    /**
     * Obtiene el valor de la propiedad activityFileBuffer.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getActivityFileBuffer() {
        return activityFileBuffer;
    }

    /**
     * Define el valor de la propiedad activityFileBuffer.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setActivityFileBuffer(byte[] value) {
        this.activityFileBuffer = value;
    }

    /**
     * Obtiene el valor de la propiedad operation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Define el valor de la propiedad operation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

}
