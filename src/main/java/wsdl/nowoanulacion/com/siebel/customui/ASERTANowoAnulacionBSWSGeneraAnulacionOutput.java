
package wsdl.nowoanulacion.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OutIdNotaCredito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutMensaje" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutSalida" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "outIdNotaCredito",
    "outMensaje",
    "outSalida"
})
@XmlRootElement(name = "ASERTANowo-AnulacionBSWSGeneraAnulacion_Output")
public class ASERTANowoAnulacionBSWSGeneraAnulacionOutput {

    @XmlElement(name = "OutIdNotaCredito", required = true)
    protected String outIdNotaCredito;
    @XmlElement(name = "OutMensaje", required = true)
    protected String outMensaje;
    @XmlElement(name = "OutSalida", required = true)
    protected String outSalida;

    /**
     * Obtiene el valor de la propiedad outIdNotaCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIdNotaCredito() {
        return outIdNotaCredito;
    }

    /**
     * Define el valor de la propiedad outIdNotaCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIdNotaCredito(String value) {
        this.outIdNotaCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad outMensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutMensaje() {
        return outMensaje;
    }

    /**
     * Define el valor de la propiedad outMensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutMensaje(String value) {
        this.outMensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad outSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutSalida() {
        return outSalida;
    }

    /**
     * Define el valor de la propiedad outSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutSalida(String value) {
        this.outSalida = value;
    }

}
