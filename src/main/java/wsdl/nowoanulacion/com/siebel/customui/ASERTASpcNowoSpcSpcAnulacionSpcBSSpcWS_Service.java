package wsdl.nowoanulacion.com.siebel.customui;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.3.2
 * 2022-01-19T12:36:58.739-06:00
 * Generated source version: 3.3.2
 *
 */
@WebServiceClient(name = "ASERTA_spcNowo_spc-_spcAnulacion_spcBS_spcWS",
                  targetNamespace = "http://siebel.com/CustomUI")
public class ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service extends Service {

    public static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://siebel.com/CustomUI", "ASERTA_spcNowo_spc-_spcAnulacion_spcBS_spcWS");
    public final static QName ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS = new QName("http://siebel.com/CustomUI", "ASERTA_spcNowo_spc-_spcAnulacion_spcBS_spcWS");

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service() {
        super(WSDL_LOCATION, SERVICE);
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS
     */
    @WebEndpoint(name = "ASERTA_spcNowo_spc-_spcAnulacion_spcBS_spcWS")
    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS getASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS() {
        return super.getPort(ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS, ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS
     */
    @WebEndpoint(name = "ASERTA_spcNowo_spc-_spcAnulacion_spcBS_spcWS")
    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS getASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS(WebServiceFeature... features) {
        return super.getPort(ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS, ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS.class, features);
    }

}
