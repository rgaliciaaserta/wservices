
package wsdl.nowoanulacion.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InPolizaId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Clavefiscal" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inPolizaId",
    "clavefiscal"
})
@XmlRootElement(name = "ASERTANowo-AnulacionBSWSGeneraAnulacion_Input")
public class ASERTANowoAnulacionBSWSGeneraAnulacionInput {

    @XmlElement(name = "InPolizaId", required = true)
    protected String inPolizaId;
    @XmlElement(name = "Clavefiscal", required = true)
    protected String clavefiscal;

    /**
     * Obtiene el valor de la propiedad inPolizaId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInPolizaId() {
        return inPolizaId;
    }

    /**
     * Define el valor de la propiedad inPolizaId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInPolizaId(String value) {
        this.inPolizaId = value;
    }

    /**
     * Obtiene el valor de la propiedad clavefiscal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClavefiscal() {
        return clavefiscal;
    }

    /**
     * Define el valor de la propiedad clavefiscal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClavefiscal(String value) {
        this.clavefiscal = value;
    }

}
