
package wsdl.nowoemision.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Exito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MsgError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OutFolioFianza" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutFolioRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIdMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIdPoliza" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutIdRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutNumPoliza" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutReferenciaCaja" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutReferenciaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutSerieFianza" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutSerieRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OutTipoMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exito",
    "msgError",
    "outFolioFianza",
    "outFolioRecibo",
    "outIdMovimiento",
    "outIdPoliza",
    "outIdRecibo",
    "outNumPoliza",
    "outReferenciaCaja",
    "outReferenciaCliente",
    "outSerieFianza",
    "outSerieRecibo",
    "outTipoMovimiento"
})
@XmlRootElement(name = "ASERTACXCoreEmisionNowoEmisionNowo_Output")
public class ASERTACXCoreEmisionNowoEmisionNowoOutput {

    @XmlElement(name = "Exito", required = true)
    protected String exito;
    @XmlElement(name = "MsgError")
    protected String msgError;
    @XmlElement(name = "OutFolioFianza", required = true)
    protected String outFolioFianza;
    @XmlElement(name = "OutFolioRecibo", required = true)
    protected String outFolioRecibo;
    @XmlElement(name = "OutIdMovimiento", required = true)
    protected String outIdMovimiento;
    @XmlElement(name = "OutIdPoliza", required = true)
    protected String outIdPoliza;
    @XmlElement(name = "OutIdRecibo", required = true)
    protected String outIdRecibo;
    @XmlElement(name = "OutNumPoliza", required = true)
    protected String outNumPoliza;
    @XmlElement(name = "OutReferenciaCaja", required = true)
    protected String outReferenciaCaja;
    @XmlElement(name = "OutReferenciaCliente", required = true)
    protected String outReferenciaCliente;
    @XmlElement(name = "OutSerieFianza", required = true)
    protected String outSerieFianza;
    @XmlElement(name = "OutSerieRecibo", required = true)
    protected String outSerieRecibo;
    @XmlElement(name = "OutTipoMovimiento", required = true)
    protected String outTipoMovimiento;

    /**
     * Obtiene el valor de la propiedad exito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExito() {
        return exito;
    }

    /**
     * Define el valor de la propiedad exito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExito(String value) {
        this.exito = value;
    }

    /**
     * Obtiene el valor de la propiedad msgError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgError() {
        return msgError;
    }

    /**
     * Define el valor de la propiedad msgError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgError(String value) {
        this.msgError = value;
    }

    /**
     * Obtiene el valor de la propiedad outFolioFianza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutFolioFianza() {
        return outFolioFianza;
    }

    /**
     * Define el valor de la propiedad outFolioFianza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutFolioFianza(String value) {
        this.outFolioFianza = value;
    }

    /**
     * Obtiene el valor de la propiedad outFolioRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutFolioRecibo() {
        return outFolioRecibo;
    }

    /**
     * Define el valor de la propiedad outFolioRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutFolioRecibo(String value) {
        this.outFolioRecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad outIdMovimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIdMovimiento() {
        return outIdMovimiento;
    }

    /**
     * Define el valor de la propiedad outIdMovimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIdMovimiento(String value) {
        this.outIdMovimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad outIdPoliza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIdPoliza() {
        return outIdPoliza;
    }

    /**
     * Define el valor de la propiedad outIdPoliza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIdPoliza(String value) {
        this.outIdPoliza = value;
    }

    /**
     * Obtiene el valor de la propiedad outIdRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutIdRecibo() {
        return outIdRecibo;
    }

    /**
     * Define el valor de la propiedad outIdRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutIdRecibo(String value) {
        this.outIdRecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad outNumPoliza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutNumPoliza() {
        return outNumPoliza;
    }

    /**
     * Define el valor de la propiedad outNumPoliza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutNumPoliza(String value) {
        this.outNumPoliza = value;
    }

    /**
     * Obtiene el valor de la propiedad outReferenciaCaja.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutReferenciaCaja() {
        return outReferenciaCaja;
    }

    /**
     * Define el valor de la propiedad outReferenciaCaja.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutReferenciaCaja(String value) {
        this.outReferenciaCaja = value;
    }

    /**
     * Obtiene el valor de la propiedad outReferenciaCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutReferenciaCliente() {
        return outReferenciaCliente;
    }

    /**
     * Define el valor de la propiedad outReferenciaCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutReferenciaCliente(String value) {
        this.outReferenciaCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad outSerieFianza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutSerieFianza() {
        return outSerieFianza;
    }

    /**
     * Define el valor de la propiedad outSerieFianza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutSerieFianza(String value) {
        this.outSerieFianza = value;
    }

    /**
     * Obtiene el valor de la propiedad outSerieRecibo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutSerieRecibo() {
        return outSerieRecibo;
    }

    /**
     * Define el valor de la propiedad outSerieRecibo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutSerieRecibo(String value) {
        this.outSerieRecibo = value;
    }

    /**
     * Obtiene el valor de la propiedad outTipoMovimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutTipoMovimiento() {
        return outTipoMovimiento;
    }

    /**
     * Define el valor de la propiedad outTipoMovimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutTipoMovimiento(String value) {
        this.outTipoMovimiento = value;
    }

}
