package wsdl.nowoemision.com.siebel.customui;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.3.2
 * 2019-12-20T10:12:40.716-06:00
 * Generated source version: 3.3.2
 *
 */
@WebServiceClient(name = "ASERTA_spcCX_spcCore_spcEmision_spcNowo",
                  targetNamespace = "http://siebel.com/CustomUI")
public class ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service extends Service {

    public static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://siebel.com/CustomUI", "ASERTA_spcCX_spcCore_spcEmision_spcNowo");
    public final static QName ASERTASpcCXSpcCoreSpcEmisionSpcNowo = new QName("http://siebel.com/CustomUI", "ASERTA_spcCX_spcCore_spcEmision_spcNowo");

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service() {
        super(WSDL_LOCATION, SERVICE);
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns ASERTASpcCXSpcCoreSpcEmisionSpcNowo
     */
    @WebEndpoint(name = "ASERTA_spcCX_spcCore_spcEmision_spcNowo")
    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo getASERTASpcCXSpcCoreSpcEmisionSpcNowo() {
        return super.getPort(ASERTASpcCXSpcCoreSpcEmisionSpcNowo, ASERTASpcCXSpcCoreSpcEmisionSpcNowo.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ASERTASpcCXSpcCoreSpcEmisionSpcNowo
     */
    @WebEndpoint(name = "ASERTA_spcCX_spcCore_spcEmision_spcNowo")
    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo getASERTASpcCXSpcCoreSpcEmisionSpcNowo(WebServiceFeature... features) {
        return super.getPort(ASERTASpcCXSpcCoreSpcEmisionSpcNowo, ASERTASpcCXSpcCoreSpcEmisionSpcNowo.class, features);
    }

}
