
package wsdl.nowoemision.com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Base_spcId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "baseSpcId"
})
@XmlRootElement(name = "ASERTACXCoreEmisionNowoEmisionNowo_Input")
public class ASERTACXCoreEmisionNowoEmisionNowoInput {

    @XmlElement(name = "Base_spcId", required = true)
    protected String baseSpcId;

    /**
     * Obtiene el valor de la propiedad baseSpcId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseSpcId() {
        return baseSpcId;
    }

    /**
     * Define el valor de la propiedad baseSpcId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseSpcId(String value) {
        this.baseSpcId = value;
    }

}
