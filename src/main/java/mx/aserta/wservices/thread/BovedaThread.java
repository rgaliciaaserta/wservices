/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.thread;

import java.io.FileOutputStream;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.BovedaVO;
import mx.aserta.wservices.vo.NowoFileProcessVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wsdl.serviceconsultas.org.tempuri.ServiceConsultas;

/**
 *
 * @author rgalicia
 */
public class BovedaThread extends Thread {
    
    private boolean success;
    
    private boolean finished;
    
    private String pathFile;
    
    private String fileName;
    
    private String extFile;
    
    private String completePathFile;
    
    private BovedaVO bovedaVO;
    
    private ServiceConsultas serviceConsultas;
    
    private NowoFileProcessVO nowoFileProcessVO;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    public BovedaThread(BovedaVO bovedaVO) {
        this.finished = false;
        this.success = false;
        
        this.bovedaVO = new BovedaVO();
        this.bovedaVO.setFolio(bovedaVO.getFolio());
        this.bovedaVO.setFormato(bovedaVO.getFormato());
        this.bovedaVO.setIdMovimiento(bovedaVO.getIdMovimiento());
        this.bovedaVO.setIdPoliza(bovedaVO.getIdPoliza());
        this.bovedaVO.setRfcEmisor(bovedaVO.getRfcEmisor());
        this.bovedaVO.setSerie(bovedaVO.getSerie());
        this.bovedaVO.setTipoDocumento(bovedaVO.getTipoDocumento());
        
        this.fileName = Util.getRandomUID() + bovedaVO.getIdPoliza() + bovedaVO.getTipoDocumento();
        
        this.nowoFileProcessVO = new NowoFileProcessVO();
        this.nowoFileProcessVO.setId(-1L);
        this.nowoFileProcessVO.setEstatus(Util.ESTATUS_INITIATED);
        this.nowoFileProcessVO.setFolio(bovedaVO.getFolio());
        this.nowoFileProcessVO.setFormato(bovedaVO.getFormato());
        this.nowoFileProcessVO.setIdMovimiento(bovedaVO.getIdMovimiento());
        this.nowoFileProcessVO.setIdPoliza(bovedaVO.getIdPoliza());
        this.nowoFileProcessVO.setRfcEmisor(bovedaVO.getRfcEmisor());
        this.nowoFileProcessVO.setSerie(bovedaVO.getSerie());
        this.nowoFileProcessVO.setTipoDocumento(bovedaVO.getTipoDocumento());
        this.nowoFileProcessVO.setUsuario(Util.SYSTEM);
    }
    
    public void run() {
        log.debug("\t+ INICIANDO PROCESO BOVEDA +");
        log.debug(TransformObject.getStringFromObject(this.bovedaVO));
        
        //bitacora
        this.nowoFileProcessVO.setEstatus(Util.ESTATUS_IN_PROGRESS);
        this.nowoFileProcessVO.setMessage("Iniciando consulta en la boveda.");
        
        FileOutputStream fos = null;
        this.completePathFile = this.pathFile + this.fileName + Util.POINT + this.extFile;
        try {
            byte[] arrByte = this.serviceConsultas.getServiceConsultasSoap12()
                .getPdf(null,
                        this.bovedaVO.getIdPoliza(),
                        this.bovedaVO.getIdMovimiento(),
                        this.bovedaVO.getTipoDocumento(),
                        this.bovedaVO.getSerie(),
                        this.bovedaVO.getFolio(),
                        this.bovedaVO.getFormato());
            
            if (arrByte == null) {
                this.success = false;
                //bitacora
                this.nowoFileProcessVO.setEstatus(Util.ESTATUS_ERROR);
                this.nowoFileProcessVO.setMessage("Archivo no encontrado: Arreglo de bytes vacio.");
            } else {
                fos = new FileOutputStream(this.completePathFile);
                fos.write(arrByte);

                this.success = true;
                //bitacora
                this.nowoFileProcessVO.setEstatus(Util.ESTATUS_FINISHED);
                this.nowoFileProcessVO.setMessage("Archivo encontrado y descargado.");
                this.nowoFileProcessVO.setPathFile(this.completePathFile);
            }
            
            log.debug("\t+ SUCCESS:" + this.success);
        } catch (Exception ex) {
            log.debug("\tError al generar archivo.");
            //bitacora
            this.nowoFileProcessVO.setEstatus(Util.ESTATUS_ERROR);
            this.nowoFileProcessVO.setMessage("Error generando el archivo:"
                    + ((ex.getMessage() == null)? null:ex.getMessage().substring(0, 100)));
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception ex) {
                    log.debug("\tError al cerrar archivo.");
                }
            }
        }
        
        log.debug("\t+ FIN PROCESO BOVEDA +");
        
        this.finished = true;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isFinished() {
        return finished;
    }

    public BovedaVO getBovedaVO() {
        return bovedaVO;
    }

    public void setExtFile(String extFile) {
        this.extFile = extFile;
    }

    public void setServiceConsultas(ServiceConsultas serviceConsultas) {
        this.serviceConsultas = serviceConsultas;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getCompletePathFile() {
        return completePathFile;
    }

    public NowoFileProcessVO getNowoFileProcessVO() {
        return nowoFileProcessVO;
    }
}
