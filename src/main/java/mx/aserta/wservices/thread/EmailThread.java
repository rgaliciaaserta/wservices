/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.thread;

import java.util.List;
import java.util.Map;
import mx.aserta.wservices.service.EmailMgun;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rgalicia
 */
public class EmailThread extends Thread {
    
    private String emailTo;
    
    private String emailCc;
    
    private String emailBcc;
    
    private String asunto;
    
    private String mensaje;
    
    private String emailFrom;
    
    private String emailContext;
    
    private Map<String, String> mapFile;
    
    private List<String> images;
    
    private EmailMgun emailMgun;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Constructor
     * @param emailTo
     * @param emailCc
     * @param emailBcc
     * @param asunto
     * @param mensaje
     * @param emailFrom
     * @param emailContext
     * @param mapFile
     * @param images 
     * @param emailMgun
     */
    public EmailThread(String emailTo, String emailCc, String emailBcc,
            String asunto, String mensaje, String emailFrom, String emailContext,
            Map<String, String> mapFile, List<String> images,
            EmailMgun emailMgun) {
        this.emailTo = emailTo;
        this.emailCc = emailCc;
        this.emailBcc = emailBcc;
        this.asunto = asunto;
        this.mensaje = mensaje;
        this.emailFrom = emailFrom;
        this.emailContext = emailContext;
        this.mapFile = mapFile;
        this.images = images;
        this.emailMgun = emailMgun;
    }
    
    public void run() {
        log.debug("\tIniciando hilo de envio de correo:");
        
        try {
            String msgError = this.validParameters(emailTo, asunto);
            if (msgError != null && !msgError.isEmpty()) {
                log.debug("\tError al validar los parametros de entrada:" + msgError);
                return;
            }
            
            if (this.emailMgun == null) {
                log.debug("\tObjeto de instancia EmailMgun null.");
                return;
            }
            
            log.debug("\tEnviando correo por MailGun...");
            this.emailMgun.sendHTMLMailAttachment(null, null, this.emailTo, this.emailCc, this.emailBcc,
                    this.mensaje,
                    this.asunto, this.emailFrom, this.emailContext, this.mapFile,
                    (this.images == null || this.images.isEmpty())?
                            null:this.images.get(0));
            log.debug("\tCorreo enviado.");
        } catch (Exception ex) {
            log.error("\tError al enviar el correo:" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Valid parameters
     * @param emailTo
     * @param asunto
     * @return 
     */
    private String validParameters(String emailTo, String asunto) {
        StringBuilder msg = new StringBuilder();
        if (emailTo == null || emailTo.isEmpty()) {
            msg.append("Debe indicar el correo electrónico del destinatario.");
        }
        
        if (asunto == null || asunto.isEmpty()) {
            msg.append("Debe indicar el correo electrónico del destinatario.");
        }
        
        return msg.toString();
    }
}
