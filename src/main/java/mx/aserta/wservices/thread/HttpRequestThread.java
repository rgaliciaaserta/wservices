/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.thread;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import mx.aserta.wservices.sps.LogRequestService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.BucketDocumentRequestVO;
import mx.aserta.wservices.vo.BucketDocumentResponseVO;
import mx.aserta.wservices.vo.LogRequestVO;
import mx.aserta.wservices.vo.LoginOauthResponseVO;
import mx.aserta.wservices.vo.LoginOauthVO;
import mx.aserta.wservices.vo.NowoHmacSignatureResponseVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

/**
 *
 * @author rgalicia
 */
public class HttpRequestThread<T, R> implements Runnable {
    
    private boolean secure;
    
    private boolean success;
    
    private boolean finished;
    
    private boolean urlEncode;
    
    private boolean paramsEncode;
    
    private int op;
    
    private long idThread;
    
    private String urlString;
    
    private String method;
    
    private boolean mapOutput;
    
    private List<String> pathFile;
    
    private String boundary;
    
    private URL url;
    
    private T objectRequestJson;
    
    private R objectResponseJson;
    
    private Class classObjectResponse;
    
    private Gson gson;
    
    private HttpURLConnection httpConnection;
    
    private BufferedReader br;
    
    private StringBuilder jsonObj;
    
    private String inputStream;
    
    private Map<String, List<String>> header;
    
    private LogRequestVO logVO;
    
    private LogRequestService logRequestService;
    
    private Logger log = LogManager.getLogger("WPDF001PR");
    
    public HttpRequestThread(String urlString, boolean secure, T objectRequestJson,
            Class classObjectResponse, int op, long idThread, Map<String, List<String>> header,
            boolean urlEncode, List<String> pathFile, String boundary, boolean paramsEncode,
            String method, boolean mapOutput, String inputStream, LogRequestService logRequestService) {
        this.secure = secure;
        this.success = false;
        this.finished = false;
        this.op = op;
        this.idThread = idThread;
        this.urlString = urlString;
        this.objectRequestJson = objectRequestJson;
        this.gson = new Gson();
        this.httpConnection = null;
        this.br = null;
        this.jsonObj = new StringBuilder();
        this.objectResponseJson = null;
        this.classObjectResponse = classObjectResponse;
        this.header = header;
        this.urlEncode = urlEncode;
        this.pathFile = pathFile;
        this.boundary = boundary;
        this.paramsEncode = paramsEncode;
        this.method = method;
        this.mapOutput = mapOutput;
        this.inputStream = inputStream;
        this.logRequestService = logRequestService;
        this.logVO = new LogRequestVO();
        this.logVO.setId(-1L);
    }
    
    /**
     * Init thread
     * @param urlString
     * @param secure
     * @param objectRequestJson
     * @param classObjectResponse
     * @param op
     * @param idThread
     * @param header
     * @param urlEncode
     * @param pathFile
     * @param boundary
     * @param paramsEncode
     * @param method
     * @param mapOutput
     * @param logRequestService
     */
    public void initThread(String urlString, boolean secure, T objectRequestJson,
            Class classObjectResponse, int op, long idThread, Map<String, List<String>> header,
            boolean urlEncode, List<String> pathFile, String boundary, boolean paramsEncode,
            String method, boolean mapOutput, String inputStream, LogRequestService logRequestService) {
        this.secure = secure;
        this.success = false;
        this.finished = false;
        this.op = op;
        this.idThread = idThread;
        this.urlString = urlString;
        this.objectRequestJson = objectRequestJson;
        if (this.gson == null) {
            this.gson = new Gson();
        }
        this.httpConnection = null;
        this.br = null;
        if (this.jsonObj != null && this.jsonObj.length() > 0) {
            this.jsonObj.delete(0, this.jsonObj.length());
        }
        this.objectResponseJson = null;
        this.classObjectResponse = classObjectResponse;
        this.header = header;
        this.urlEncode = urlEncode;
        this.pathFile = pathFile;
        this.boundary = boundary;
        this.paramsEncode = paramsEncode;
        this.method = method;
        this.mapOutput = mapOutput;
        this.inputStream = inputStream;
        this.logRequestService = logRequestService;
        this.logVO = new LogRequestVO();
        this.logVO.setId(-1L);
    }
    
    /**
     * run
     */
    @Override
    public void run() {
        this.logVO.setTransaction(Util.HTTP_TRANSACTION + Util.getRandomString());
        
        if (this.urlString == null || this.urlString.isEmpty()) {
            this.finished = true;
            this.success = false;
            log.debug("\tError:La URL es incorrecta:" + this.urlString);
            return;
        }
        
        if (!this.secure) {
            log.debug("\t************* Http *************");
            try {
                String urlParamsRequest = "";
                if (this.paramsEncode) {
                    urlParamsRequest = this.getStringFromObjectProperty();
                    if (urlParamsRequest != null && !urlParamsRequest.isEmpty()) {
                        urlParamsRequest = Util.QUERY_CLOSE + urlParamsRequest;
                    }
                }
                
                this.url = new URL(this.urlString + urlParamsRequest);
                this.logVO.setUrl(this.urlString);
                this.logVO.setUrlParameters(urlParamsRequest);
                
                log.debug("\t\t**Conectandose a la URL:" + this.urlString + urlParamsRequest);
                System.out.println("\t\t**Conectandose a la URL:" + this.urlString + urlParamsRequest);
                this.httpConnection = (HttpURLConnection) this.url.openConnection();
                
                StringBuilder headStr = new StringBuilder();
                if (this.header != null && !this.header.isEmpty()) {
                    Iterator<Map.Entry<String, List<String>>> iter = this.header.entrySet().iterator();
                    Map.Entry<String, List<String>> entry;
                    while(iter.hasNext()) {
                        entry = iter.next();
                        log.debug("\tHeader:" + entry.getKey());
                        System.out.println("\tHeader:" + entry.getKey());
                        headStr.append("[");
                        headStr.append(entry.getKey());
                        headStr.append(":");
                        
                        for (String valHeader : entry.getValue()) {
                            log.debug("\tValue:" + valHeader);
                            System.out.println("\tValue:" + valHeader);
                            headStr.append(valHeader);
                            headStr.append("-|-");
                            this.httpConnection.setRequestProperty(entry.getKey(), valHeader);
                        }
                    }
                }
                
                this.logVO.setHeaders(headStr.toString());
                this.httpConnection.setRequestMethod(this.method);
                
                //this.httpConnection.setRequestProperty(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
                
                this.httpConnection.setDoInput(true);
                
                String json = this.getStringJsonFromObject();
                String urlEncodeData = this.getStringFromObjectProperty();
                String fileName = null;
                StringBuilder bodyStr = new StringBuilder();
                if (HttpMethod.POST.name().equalsIgnoreCase(this.method)) {
                    if (this.urlEncode) {
                        this.httpConnection.setDoOutput(true);
                        log.debug("\t\t**Objeto de entrada URLENCODED:" + urlEncodeData);
                        bodyStr.append(urlEncodeData);
                        this.httpConnection.getOutputStream().write(urlEncodeData.getBytes());
                        
                    } else if (this.pathFile != null && !this.pathFile.isEmpty()) {
                        log.debug("\tArchivos a enviar:" + this.pathFile.size());

                        this.httpConnection.setUseCaches(false);
                        this.httpConnection.setDoOutput(true);
                        log.debug("\t\t**Objeto de entrada MULTIPART:" + this.pathFile + ":" + fileName
                        + "BOUNDARY:" + this.boundary);
                        System.out.println("\t\t**Objeto de entrada MULTIPART:" + this.pathFile + ":" + fileName
                        + "BOUNDARY:" + this.boundary);
                        log.debug("CD:" + Util.HTTP_HEADER_CONTENT_DISPOSITION + Util.TWO_DOTS
                                + String.format(Util.HTTP_HEADER_FORM_DATA, Util.quitExtFile(fileName), fileName) + Util.CARRY_RETURN);
                        System.out.println("CD:" + Util.HTTP_HEADER_CONTENT_DISPOSITION + Util.TWO_DOTS
                                + String.format(Util.HTTP_HEADER_FORM_DATA, Util.quitExtFile(fileName), fileName) + Util.CARRY_RETURN);

                        bodyStr.append("Lista de archivos enviados:[");
                        StringBuilder logMSg = new StringBuilder();
                        for (String fName : this.pathFile) {
                            log.debug("\t\tArchivo a enviar:" + fName);
                            System.out.println("\t\tArchivo a enviar:" + fName);
                            bodyStr.append(fName);
                            bodyStr.append("-|-");
                            fileName = Util.getFileNameFromPath(fName);

                            logMSg.append(Util.DOUBLE_GUION + this.boundary + Util.CARRY_RETURN);
                            logMSg.append((Util.HTTP_HEADER_CONTENT_DISPOSITION + Util.TWO_DOTS
                                    + String.format(Util.HTTP_HEADER_FORM_DATA, Util.quitExtFile(fileName), fileName) + Util.CARRY_RETURN).getBytes());
                            logMSg.append((Util.HTTP_HEADER_CONTENT_TYPE + Util.TWO_DOTS
                                    + Util.TYPE_TEXT_CSV
                                    + Util.CARRY_RETURN).getBytes());
                            logMSg.append(Util.CARRY_RETURN.getBytes());
                            logMSg.append(Util.getBytesFromFile(fName));
                            logMSg.append(Util.CARRY_RETURN.getBytes());
                            
                            this.httpConnection.getOutputStream().write((Util.DOUBLE_GUION + this.boundary + Util.CARRY_RETURN).getBytes());
                            this.httpConnection.getOutputStream().write((Util.HTTP_HEADER_CONTENT_DISPOSITION + Util.TWO_DOTS
                                    + String.format(Util.HTTP_HEADER_FORM_DATA, Util.quitExtFile(fileName), fileName) + Util.CARRY_RETURN).getBytes());
                            this.httpConnection.getOutputStream().write((Util.HTTP_HEADER_CONTENT_TYPE + Util.TWO_DOTS
                                    + Util.TYPE_TEXT_CSV
                                    + Util.CARRY_RETURN).getBytes());
                            this.httpConnection.getOutputStream().write(Util.CARRY_RETURN.getBytes());
                            this.httpConnection.getOutputStream().write(Util.getBytesFromFile(fName));
                            this.httpConnection.getOutputStream().write(Util.CARRY_RETURN.getBytes());

                            /*this.httpConnection.getOutputStream().write((Util.DOUBLE_GUION + this.boundary
                                + Util.DOUBLE_GUION + Util.CARRY_RETURN).getBytes());*/
                        }
                        
                        bodyStr.append("]");

                        logMSg.append((Util.DOUBLE_GUION + this.boundary
                                + Util.DOUBLE_GUION + Util.CARRY_RETURN).getBytes());
                        
                        this.httpConnection.getOutputStream().write((Util.DOUBLE_GUION + this.boundary
                                + Util.DOUBLE_GUION + Util.CARRY_RETURN).getBytes());

                        this.httpConnection.getOutputStream().flush();
                        log.debug("PAYLOAD:" + logMSg.toString());
                    } else if (json != null) {
                        this.httpConnection.setDoOutput(true);
                        this.httpConnection.getOutputStream().write(json.getBytes());
                        bodyStr.append(json);
                        
                        log.debug("\t\t**Objeto entrada JSON:" + json);
                    } else if (this.inputStream != null && !this.inputStream.isEmpty()) {
                        this.httpConnection.setDoOutput(true);
                        this.httpConnection.getOutputStream().write(this.inputStream.getBytes());
                        bodyStr.append(this.inputStream);
                        
                        log.debug("\t\t**Objeto entrada INPUTSTREAM:" + this.inputStream);
                        
                    } else {
                        this.httpConnection.setDoOutput(false);
                    }
                }
                
                this.logVO.setBody(bodyStr.toString());
                
                StringBuilder respStr = new StringBuilder();
                log.debug(this.httpConnection.getResponseCode() + ":" + this.httpConnection.getResponseMessage());
                System.out.println(this.httpConnection.getResponseCode() + ":" + this.httpConnection.getResponseMessage());
                respStr.append(this.httpConnection.getResponseCode());
                respStr.append(":");
                respStr.append(this.httpConnection.getResponseMessage());
                
                if (this.httpConnection.getResponseCode() != HttpStatus.OK.value()) {
                    log.debug("\tERROR:Servicio respondio con error.");
                    this.logVO.setResponse(respStr.toString());
                    this.jsonObj.append(this.httpConnection.getResponseCode());
                    this.jsonObj.append(":");
                    this.jsonObj.append(this.httpConnection.getResponseMessage());
                    return;
                }
                
                this.br = new BufferedReader(new InputStreamReader(this.httpConnection.getInputStream()));
                
                String cad;
                
                while((cad = this.br.readLine()) != null) {
                    this.jsonObj.append(cad);
                }
                
                log.debug("\t::::JSON RESPONSE:" + ((this.jsonObj != null)?
                        ((this.jsonObj.length() < 1000)?
                                this.jsonObj.toString():this.jsonObj.substring(0, 1000)):""));
                System.out.println("JSON:" + this.jsonObj.toString());
                respStr.append(this.jsonObj);
                this.logVO.setResponse(respStr.toString());
                
                if (this.mapOutput) {
                    this.getObjectFromJsonString();
                }
                
                this.success = true;
                
            } catch (Exception ex) {
                System.out.println("Errro:" + ex.getMessage());
                log.error("\tError:HttpRequestThread:" + ex.getMessage(), ex);
                this.success = false;
            } finally {
                this.finished = true;
                if (this.br != null) {
                    try {
                        this.br.close();
                    } catch (Exception ex) {
                        ;
                    }
                }
                if (this.httpConnection != null) {
                    try {
                        this.httpConnection.disconnect();
                    } catch (Exception ex) {
                        ;
                    }
                }
                
                this.logRequest();
            }
        }
    }
    
    /**
     * Log Request
     */
    private void logRequest() {
        if (this.logRequestService == null) {
            log.debug("\t\tEl Servicio para logueo es null.");
            return;
        }
        
        RespuestaJson resp = this.logRequestService.execute(this.logVO);
        if (resp == null || !Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())
                || resp.getDatos() == null) {
            log.debug("\t\tEl servicio de logueo no respondio como se esperaba.");
            return;
        }
        
        this.logVO = (LogRequestVO) resp.getDatos();
    }
    
    /**
     * Get JSON object as String
     * @return 
     */
    public String getStringJsonFromObject() {
        if (this.objectRequestJson == null) {
            return null;
        }
        
        try {
            return this.gson.toJson(this.objectRequestJson);
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * Get Object R from JSON string
     */
    public void getObjectFromJsonString() {
        if (this.jsonObj == null || this.jsonObj.length() == 0) {
            return;
        }
        
        try {
            this.objectResponseJson = (R) this.gson.fromJson(this.jsonObj.toString(), this.classObjectResponse);
        } catch (Exception ex) {
            log.error("\tError GSON:" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Get String from Object properties
     * @return 
     */
    public String getStringFromObjectProperty() throws Exception {
        if (this.objectRequestJson == null) {
            return null;
        }
        
        Field[] fields = this.objectRequestJson.getClass().getDeclaredFields();
        if (fields == null || fields.length == 0) {
            return null;
        }
        
        StringBuilder str = new StringBuilder();
        
        for (int i= 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            str.append(fields[i].getName());
            str.append(Util.EQUAL);
            
            if (fields[i].getType().getName().contains("java.lang.String")) {
                try {
                    str.append(
                            (String) fields[i].get(this.objectRequestJson));
                } catch (Exception ex) {
                    ;
                }
            }
            
            str.append(Util.AMPERSAND);
        }
        
        if (str.length() > 0) {
            str.deleteCharAt(str.length() - 1);
        }
        
        return str.toString();
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isFinished() {
        return finished;
    }

    public StringBuilder getJsonObj() {
        return jsonObj;
    }

    public R getObjectResponseJson() {
        return objectResponseJson;
    }
    
    public static void main(String[] args) {
        main2();
        /*main2(args);
        main2(args);
        main2(args);
        main2(args);
        main2(args);*/
    }
    
    public static void main2() {
        LoginOauthVO login = new LoginOauthVO();
        login.setClient_id("27ea677a05e84ba49102bd3257be32ec");
        login.setClient_secret("2eb2523c-0451-4330-bc5f-050ec4caa7d7");
        login.setGrant_type("client_credentials");
        login.setScope("NOWOAserta");
        
        Map<String, List<String>> header = new HashMap<>();
        List<String> ccontrol = new ArrayList<>();
        List<String> ctype = new ArrayList<>();
        ccontrol.add("no-cache");
        ctype.add("application/x-www-form-urlencoded");
        
        header.put("Cache-Control", ccontrol);
        header.put("Content-Type", ctype);
        
        List<HttpRequestThread> lista = new ArrayList<>();
        HttpRequestThread<LoginOauthVO, LoginOauthResponseVO> t;
        for (int j = 0; j < 1; j++) {
            t = new HttpRequestThread<LoginOauthVO, LoginOauthResponseVO>(
            "https://idcs-5d903747faa34a5e9f767fcb854f3e79.identity.oraclecloud.com/oauth2/v1/token",
            false,
            login, LoginOauthResponseVO.class, 1, 1, header, true, null,
            null, false, HttpMethod.POST.name(), true, null, null);
            
            t.run();

            lista.add(t);
        }
        
        int i = 10;
        boolean f = false;
        while(!f && i > 0) {
            
            f = true;
            for (int k = 0; k < lista.size(); k++) {
                if (!lista.get(k).isFinished()) {
                    f = false;
                    break;
                }
            }
            
            try {
                Thread.sleep(1000L);
            } catch (Exception ex) {
                System.out.println("Error:" + ex.getMessage());
            }
            i --;
        }
        
        for (int k = 0; k < lista.size(); k++) {
            System.out.println("-------" + k + "-------");
            System.out.println(((LoginOauthResponseVO) lista.get(k).getObjectResponseJson()).getAccess_token());
            System.out.println(((LoginOauthResponseVO) lista.get(k).getObjectResponseJson()).getExpires_in());
            System.out.println(((LoginOauthResponseVO) lista.get(k).getObjectResponseJson()).getToken_type());
        }
        
        //main3(lista);
    }
    
    public static void main3(List<HttpRequestThread> listaDatos) {
        BucketDocumentRequestVO bucket = new BucketDocumentRequestVO();
        bucket.setXincodehardwareid("9a1bb65f896141809b6376f4d21f02c8");
        
        Map<String, List<String>> header = new HashMap<>();
        List<String> ctype = new ArrayList<>();
        ctype.add("application/json");
        header.put("Content-Type", ctype);
        
        List<HttpRequestThread> lista = new ArrayList<>();
        HttpRequestThread<BucketDocumentRequestVO, BucketDocumentResponseVO> t;
        for (int j = 0; j < 1; j++) {
            t = new HttpRequestThread<BucketDocumentRequestVO, BucketDocumentResponseVO>(
            "https://apidev.aserta.com.mx/Aserta/bucket/bucket/getDocumento",
            false,
            bucket, BucketDocumentResponseVO.class, 1, 1, header, false, null,
            null, false, HttpMethod.POST.name(), true, null, null);
            
            t.run();

            lista.add(t);
        }
        
        
        List<String> auth = new ArrayList<>();
        ctype.add("application/json; charset=utf-8");
        auth.add(((LoginOauthResponseVO) listaDatos.get(0).getObjectResponseJson()).getToken_type()
                + " " + ((LoginOauthResponseVO) listaDatos.get(0).getObjectResponseJson()).getAccess_token());
        
        List<String> stripeList = new ArrayList<>();
        stripeList.add("t=1640289126,v1=400853648b82b7d119eb4fcc388c9353e40ad2fa6ebd630d5fc4de3327fa9ed9,v0=51f5bc76b84f53d28bbb36396662cef48b4634ab1f0215ca8e8743fa817d9a65");
        header.put("Stripe-Signature", stripeList);
        header.put("Content-Type", ctype);
        header.put("Authorization", auth);
        
        /*List<HttpRequestThread> lista = new ArrayList<>();
        HttpRequestThread<Object, NowoHmacSignatureResponseVO> t;
        for (int j = 0; j < 1; j++) {
            t = new HttpRequestThread<Object, NowoHmacSignatureResponseVO>(
            "https://apidev.aserta.com.mx/Aserta/Public/nwesp/pagos/events",
            false,
            null, NowoHmacSignatureResponseVO.class, 1, 1, header, false, null,
            null, false, HttpMethod.POST.name(), true, "{\n" +
"}", null);
            
            t.run();

            lista.add(t);
        }*/
        
        int i = 10;
        boolean f = false;
        while(!f && i > 0) {
            
            f = true;
            for (int k = 0; k < lista.size(); k++) {
                if (!lista.get(k).isFinished()) {
                    f = false;
                    break;
                }
            }
            
            try {
                Thread.sleep(1000L);
            } catch (Exception ex) {
                System.out.println("Error:" + ex.getMessage());
            }
            i --;
        }
        
        for (int k = 0; k < lista.size(); k++) {
            System.out.println("-------" + k + "-------");
            System.out.println(((NowoHmacSignatureResponseVO) lista.get(k).getObjectResponseJson()).isReceived());
        }
        
        //main3(lista);
        /*main3(lista);
        main3(lista);
        main3(lista);
        main3(lista);
        main3(lista);*/
    }
    
    public long getIdThread() {
        return idThread;
    }

    public String getUrlString() {
        return urlString;
    }

    public LogRequestVO getLogVO() {
        return logVO;
    }
}

