/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.config;

import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author rgalicia
 */
public class PropSource {
    
    @Value("${jndi.siebelaserta}")
    private String jndiSiebelAserta;
    
    @Value("${jndi.siebelinsurgentes}")
    private String jndiSiebelInsurgentes;
    
    @Value("${jndi.afiandwh}")
    private String jndiAfiandwh;
    
    @Value("${jndi.reporteador}")
    private String jndiReporteador;
    
    @Value("${oam.access}")
    private String oamAccess;
    
    @Value("${email.host}")
    private String emailHost;
    
    @Value("${email.port}")
    private String emailPort;
    
    @Value("${email.user}")
    private String emailUser;
    
    @Value("${email.password}")
    private String emailPass;
    
    @Value("${email.firma}")
    private String emailFirma;
    
    @Value("${email.from}")
    private String emailFrom;
    
    @Value("${email.support}")
    private String emailSupport;
    
    @Value("${email.size.mail}")
    private String emailSizeMail;
    
    @Value("${auth.credential.user}")
    private String authCredentialUser;
    
    @Value("${auth.credential.pass}")
    private String authCredentialPass;
    
    @Value("${ws.header.user}")
    private String wsHeaderUser;
    
    @Value("${ws.header.pass}")
    private String wsHeaderPass;
    
    @Value("${ws.header.session}")
    private String wsHeadersession;
    
    @Value("${url.webservice.altacliente.aserta}")
    private String wsAltaClienteAserta;
    
    @Value("${url.webservice.altacliente.insurgentes}")
    private String wsAltaClienteInsurgentes;
    
    @Value("${url.webservice.nowopagobs.aserta}")
    private String wsNowoPagoBsAserta;
    
    @Value("${url.webservice.nowopagobs.insurgentes}")
    private String wsNowoPagoBsInsurgentes;
    
    @Value("${url.contextpublished.arrenda}")
    private String urlContextPublishedArrenda;
    
    @Value("${url.webservice.nowocreabase.aserta}")
    private String wsNowoCreaBaseAserta;
    
    @Value("${url.webservice.nowocreabase.insurgentes}")
    private String wsNowoCreaBaseInsurgentes;
    
    @Value("${url.webservice.nowoanulacion.aserta}")
    private String wsNowoAnulacionAserta;
    
    @Value("${url.webservice.nowoanulacion.insurgentes}")
    private String wsNowoAnulacionInsurgentes;
    
    @Value("${url.webservice.nowocancelacion.aserta}")
    private String wsNowoCancelacionAserta;
    
    @Value("${url.webservice.nowocancelacion.insurgentes}")
    private String wsNowoCancelacionInsurgentes;
    
    @Value("${url.webservice.nowoemision.aserta}")
    private String wsNowoEmisionAserta;
    
    @Value("${url.webservice.nowoemision.insurgentes}")
    private String wsNowoEmisionInsurgentes;
    
    @Value("${url.webservice.nowoattachment.aserta}")
    private String wsNowoAttachmentAserta;
    
    @Value("${url.webservice.nowoattachment.insurgentes}")
    private String wsNowoAttachmentInsurgentes;
    
    @Value("${file.pdf.extension}")
    private String filePdfExtension;
    
    @Value("${file.pathtmp}")
    private String filePathTmp;
    
    @Value("${prop.buro.clavepais}")
    private String propBuroClavePais;
    
    @Value("${prop.buro.claveunidadmonetaria}")
    private String propBuroClaveUnidadMonetaria;
    
    @Value("${prop.buro.claveusuario}")
    private String propBuroClaveUsuario;
    
    @Value("${prop.buro.identificadorburo}")
    private String propBuroIdentificadorBuro;
    
    @Value("${prop.buro.idioma}")
    private String propBuroIdioma;
    
    @Value("${prop.buro.importecontrato}")
    private String propBuroImporteContrato;
    
    @Value("${prop.buro.numeroreferenciaoperador}")
    private String propBuroNumeroReferenciaOperador;
    
    @Value("${prop.buro.password}")
    private String propBuroPassword;
    
    @Value("${prop.buro.productorequerido}")
    private String propBuroProductoRequerido;
    
    @Value("${prop.buro.tipoconsulta}")
    private String propBuroTipoConsulta;
    
    @Value("${prop.buro.tipocontrato}")
    private String propBuroTipoContrato;
    
    @Value("${prop.buro.tiposalida}")
    private String propBuroTipoSalida;
    
    @Value("${prop.buro.version}")
    private String propBuroVersion;
    
    @Value("${url.webservice.nowoverificaagente.aserta}")
    private String wsNowoVerificaAgenteAserta;
    
    @Value("${url.webservice.nowoverificaagente.insurgentes}")
    private String wsNowoVerificaAgenteInsurgentes;
    
    @Value("${url.webservice.verificacodigoafiliacion.aserta}")
    private String wsVerificaCodigoAfiliacionAserta;
    
    @Value("${url.webservice.verificacodigoafiliacion.insurgentes}")
    private String wsVerificaCodigoAfiliacionInsurgentes;
    
    @Value("${nowo.afiliacion.path}")
    private String nowoAfiliacionPath;
    
    @Value("${email.support.nowo}")
    private String emailSupportNowo;
    
    @Value("${nowo.plantilla.emailpf}")
    private String nowoPlantillaEmailPf;
    
    @Value("${nowo.plantilla.emailpm}")
    private String nowoPlantillaEmailPm;
    
    @Value("${email.nowo.subject}")
    private String emailNowoSubject;
    
    @Value("${email.nowo.to}")
    private String emailNowoTo;
    
    @Value("${email.nowo.img}")
    private String emailNowoImg;
    
    @Value("${mailgum.host}")
    private String malgumHost;
    
    @Value("${mailgum.port}")
    private String malgumPort;
    
    @Value("${mailgum.user}")
    private String malgumUser;
    
    @Value("${mailgum.password}")
    private String malgumPassword;
    
    @Value("${mailgum.firma}")
    private String malgumFirma;
    
    @Value("${mailgum.from}")
    private String malgumFrom;
    
    @Value("${mailgum.siniestros.from}")
    private String mailgumSiniestrosFrom;
    
    @Value("${mailgum.size.mail}")
    private String malgumSizeMail;
    
    @Value("${jasper.path}")
    private String jasperPath;
    
    @Value("${jasper.nowo.cartaaceptacion}")
    private String jasperNowoCartaAceptacion;
    
    @Value("${email.nowo.carta.title}")
    private String emailNowoCartaTitle;
    
    @Value("${email.nowo.carta.body}")
    private String emailNowoCartaBody;
    
    @Value("${email.nowo.carta.emailsupport}")
    private String emailNowoCartaEmailSupport;
    
    @Value("${nowo.siniestro.path}")
    private String nowoSiniestroPath;
    
    @Value("${email.nowo.siniestro.to}")
    private String emailNowoSiniestroTo;
    
    @Value("${email.support.nowo.siniestro}")
    private String emailSupportNowoSiniestro;
    
    @Value("${nowo.plantilla.siniestro}")
    private String nowoPlantillaSiniestro;
    
    @Value("${email.nowo.siniestro.subject}")
    private String emailNowoSiniestroSubject;
    
    @Value("${email.nowo.siniestro.img}")
    private String emailNowoSiniestroImg;
    
    @Value("${mailgun.siniestro.user}")
    private String mailgunSiniestroUser;
    
    @Value("${mailgun.siniestro.password}")
    private String mailgunSiniestroPassword;
    
    @Value("${oic.nowo.clientid}")
    private String oicNowoClientId;
    
    @Value("${oic.nowo.clientsecret}")
    private String oicNowoClientSecret;
    
    @Value("${oic.nowo.granttype}")
    private String oicNowoGrantType;
    
    @Value("${oic.nowo.scope}")
    private String oicNowoScope;
    
    @Value("${oic.nowo.urloauth}")
    private String oicNowoUrlOauth;
    
    @Value("${oic.nowo.urlbucketoic}")
    private String oicNowoUrlBucketOic;
    
    public String getJndiSiebelAserta() {
        return jndiSiebelAserta;
    }

    public String getJndiSiebelInsurgentes() {
        return jndiSiebelInsurgentes;
    }

    public String getJndiAfiandwh() {
        return jndiAfiandwh;
    }

    public String getJndiReporteador() {
        return jndiReporteador;
    }

    public String getOamAccess() {
        return oamAccess;
    }

    public String getEmailHost() {
        return emailHost;
    }

    public String getEmailPort() {
        return emailPort;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public String getEmailPass() {
        return emailPass;
    }

    public String getEmailFirma() {
        return emailFirma;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public String getEmailSupport() {
        return emailSupport;
    }

    public String getEmailSizeMail() {
        return emailSizeMail;
    }

    public String getAuthCredentialUser() {
        return authCredentialUser;
    }

    public String getAuthCredentialPass() {
        return authCredentialPass;
    }

    public String getWsHeaderUser() {
        return wsHeaderUser;
    }

    public String getWsHeaderPass() {
        return wsHeaderPass;
    }

    public String getWsHeadersession() {
        return wsHeadersession;
    }

    public String getWsAltaClienteAserta() {
        return wsAltaClienteAserta;
    }

    public String getWsAltaClienteInsurgentes() {
        return wsAltaClienteInsurgentes;
    }

    public String getWsNowoPagoBsAserta() {
        return wsNowoPagoBsAserta;
    }

    public String getWsNowoPagoBsInsurgentes() {
        return wsNowoPagoBsInsurgentes;
    }

    public String getUrlContextPublishedArrenda() {
        return urlContextPublishedArrenda;
    }

    public String getWsNowoCreaBaseAserta() {
        return wsNowoCreaBaseAserta;
    }

    public String getWsNowoCreaBaseInsurgentes() {
        return wsNowoCreaBaseInsurgentes;
    }

    public String getWsNowoAnulacionAserta() {
        return wsNowoAnulacionAserta;
    }

    public String getWsNowoAnulacionInsurgentes() {
        return wsNowoAnulacionInsurgentes;
    }

    public String getWsNowoCancelacionAserta() {
        return wsNowoCancelacionAserta;
    }

    public String getWsNowoCancelacionInsurgentes() {
        return wsNowoCancelacionInsurgentes;
    }

    public String getWsNowoEmisionAserta() {
        return wsNowoEmisionAserta;
    }

    public String getWsNowoEmisionInsurgentes() {
        return wsNowoEmisionInsurgentes;
    }

    public String getWsNowoAttachmentAserta() {
        return wsNowoAttachmentAserta;
    }

    public String getWsNowoAttachmentInsurgentes() {
        return wsNowoAttachmentInsurgentes;
    }

    public String getFilePdfExtension() {
        return filePdfExtension;
    }

    public String getFilePathTmp() {
        return filePathTmp;
    }

    public String getPropBuroClavePais() {
        return propBuroClavePais;
    }

    public String getPropBuroClaveUnidadMonetaria() {
        return propBuroClaveUnidadMonetaria;
    }

    public String getPropBuroClaveUsuario() {
        return propBuroClaveUsuario;
    }

    public String getPropBuroIdentificadorBuro() {
        return propBuroIdentificadorBuro;
    }

    public String getPropBuroIdioma() {
        return propBuroIdioma;
    }

    public String getPropBuroImporteContrato() {
        return propBuroImporteContrato;
    }

    public String getPropBuroNumeroReferenciaOperador() {
        return propBuroNumeroReferenciaOperador;
    }

    public String getPropBuroPassword() {
        return propBuroPassword;
    }

    public String getPropBuroProductoRequerido() {
        return propBuroProductoRequerido;
    }

    public String getPropBuroTipoConsulta() {
        return propBuroTipoConsulta;
    }

    public String getPropBuroTipoContrato() {
        return propBuroTipoContrato;
    }

    public String getPropBuroTipoSalida() {
        return propBuroTipoSalida;
    }

    public String getPropBuroVersion() {
        return propBuroVersion;
    }

    public String getWsNowoVerificaAgenteAserta() {
        return wsNowoVerificaAgenteAserta;
    }

    public String getWsNowoVerificaAgenteInsurgentes() {
        return wsNowoVerificaAgenteInsurgentes;
    }

    public String getWsVerificaCodigoAfiliacionAserta() {
        return wsVerificaCodigoAfiliacionAserta;
    }

    public String getWsVerificaCodigoAfiliacionInsurgentes() {
        return wsVerificaCodigoAfiliacionInsurgentes;
    }

    public String getNowoAfiliacionPath() {
        return nowoAfiliacionPath;
    }

    public String getEmailSupportNowo() {
        return emailSupportNowo;
    }

    public String getEmailNowoSubject() {
        return emailNowoSubject;
    }

    public String getEmailNowoTo() {
        return emailNowoTo;
    }

    public String getEmailNowoImg() {
        return emailNowoImg;
    }

    public String getNowoPlantillaEmailPf() {
        return nowoPlantillaEmailPf;
    }

    public String getNowoPlantillaEmailPm() {
        return nowoPlantillaEmailPm;
    }

    public String getMalgumHost() {
        return malgumHost;
    }

    public String getMalgumPort() {
        return malgumPort;
    }

    public String getMalgumUser() {
        return malgumUser;
    }

    public String getMalgumPassword() {
        return malgumPassword;
    }

    public String getMalgumFirma() {
        return malgumFirma;
    }

    public String getMalgumFrom() {
        return malgumFrom;
    }

    public String getMalgumSizeMail() {
        return malgumSizeMail;
    }

    public String getJasperPath() {
        return jasperPath;
    }

    public String getJasperNowoCartaAceptacion() {
        return jasperNowoCartaAceptacion;
    }

    public String getEmailNowoCartaTitle() {
        return emailNowoCartaTitle;
    }

    public String getEmailNowoCartaBody() {
        return emailNowoCartaBody;
    }

    public String getEmailNowoCartaEmailSupport() {
        return emailNowoCartaEmailSupport;
    }

    public String getNowoSiniestroPath() {
        return nowoSiniestroPath;
    }

    public String getEmailNowoSiniestroTo() {
        return emailNowoSiniestroTo;
    }

    public String getEmailSupportNowoSiniestro() {
        return emailSupportNowoSiniestro;
    }

    public String getNowoPlantillaSiniestro() {
        return nowoPlantillaSiniestro;
    }

    public String getEmailNowoSiniestroSubject() {
        return emailNowoSiniestroSubject;
    }

    public String getEmailNowoSiniestroImg() {
        return emailNowoSiniestroImg;
    }

    public String getMailgumSiniestrosFrom() {
        return mailgumSiniestrosFrom;
    }

    public String getMailgunSiniestroUser() {
        return mailgunSiniestroUser;
    }

    public String getMailgunSiniestroPassword() {
        return mailgunSiniestroPassword;
    }

    public String getOicNowoClientId() {
        return oicNowoClientId;
    }

    public String getOicNowoClientSecret() {
        return oicNowoClientSecret;
    }

    public String getOicNowoGrantType() {
        return oicNowoGrantType;
    }

    public String getOicNowoScope() {
        return oicNowoScope;
    }

    public String getOicNowoUrlOauth() {
        return oicNowoUrlOauth;
    }

    public String getOicNowoUrlBucketOic() {
        return oicNowoUrlBucketOic;
    }
}
