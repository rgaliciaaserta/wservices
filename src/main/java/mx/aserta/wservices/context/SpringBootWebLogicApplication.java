/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.context;

import javax.naming.NamingException;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.sql.DataSource;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.endpoint.AnulacionWSEndpoint;
import mx.aserta.wservices.endpoint.AonGruposWSEndpoint;
import mx.aserta.wservices.endpoint.ArrendamientoWSEndpoint;
import mx.aserta.wservices.endpoint.CancelacionWSEndpoint;
import mx.aserta.wservices.endpoint.ClientesMoralWSEndpoint;
import mx.aserta.wservices.endpoint.ClientesWSEndpoint;
import mx.aserta.wservices.endpoint.ConsultaArchivoNowoWSEndpoint;
import mx.aserta.wservices.endpoint.ConsultaBuroSSWSEndpoint;
import mx.aserta.wservices.endpoint.ConsultaBuroWSEndpoint;
import mx.aserta.wservices.endpoint.ConsultaDocsWSEndpoint;
import mx.aserta.wservices.endpoint.ConsultaDocumentosWSEndpoint;
import mx.aserta.wservices.endpoint.CreaBaseVWSEndpoint;
import mx.aserta.wservices.endpoint.CreaBaseWSEndpoint;
import mx.aserta.wservices.endpoint.EmisionWSEndpoint;
import mx.aserta.wservices.endpoint.NowoCartaAceptacionWSEndpoint;
import mx.aserta.wservices.endpoint.NowoCheckIdentityWSEndpoint;
import mx.aserta.wservices.endpoint.NowoEmisionAsincronaCallbackWSEndpoint;
import mx.aserta.wservices.endpoint.NowoEmisionAsyncEspWSEndpoint;
import mx.aserta.wservices.endpoint.NowoPagoWSEndpoint;
import mx.aserta.wservices.endpoint.NowoRatingWSEndpoint;
import mx.aserta.wservices.endpoint.NowoRentDecisionWSEndpoint;
import mx.aserta.wservices.endpoint.NowoRentEvaluateWSEndpoint;
import mx.aserta.wservices.endpoint.NowoUpdateRentDecisionWSEndpoint;
import mx.aserta.wservices.endpoint.RecibePagoWSEndpoint;
import mx.aserta.wservices.endpoint.VerificaAgenteWSEndpoint;
import mx.aserta.wservices.endpoint.VerificaCodigoAfiliacionWSEndpoint;
import mx.aserta.wservices.ws.AnulacionWSImpl;
import mx.aserta.wservices.ws.AonGruposWSImpl;
import mx.aserta.wservices.ws.ArrendamientoWSImpl;
import mx.aserta.wservices.ws.CancelacionWSImpl;
import mx.aserta.wservices.ws.ClientesMoralWSImpl;
import mx.aserta.wservices.ws.ClientesWSImpl;
import mx.aserta.wservices.ws.ConsultaArchivoNowoWSImpl;
import mx.aserta.wservices.ws.ConsultaBuroWSImpl;
import mx.aserta.wservices.ws.ConsultaDocsWSImpl;
import mx.aserta.wservices.ws.ConsultaDocumentosWSImpl;
import mx.aserta.wservices.ws.CreaBaseVWSImpl;
import mx.aserta.wservices.ws.CreaBaseWSImpl;
import mx.aserta.wservices.ws.EmisionWSImpl;
import mx.aserta.wservices.ws.NowoCartaAceptacionWSImpl;
import mx.aserta.wservices.ws.NowoCheckIdentityWSImpl;
import mx.aserta.wservices.ws.NowoEmisionAsincronaCallbackWSImpl;
import mx.aserta.wservices.ws.NowoEmisionAsyncEspWSImpl;
import mx.aserta.wservices.ws.NowoPagoWSImpl;
import mx.aserta.wservices.ws.NowoRatingWSImpl;
import mx.aserta.wservices.ws.NowoRentDecisionWSImpl;
import mx.aserta.wservices.ws.NowoRentEvaluateWSImpl;
import mx.aserta.wservices.ws.NowoUpdateRentDecisionWSImpl;
import mx.aserta.wservices.ws.RecibePagoWSImpl;
import mx.aserta.wservices.ws.VerificaAgenteWSImpl;
import mx.aserta.wservices.ws.VerificaCodigoAfiliadoWSImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 * @author rgalicia
 */
@SpringBootApplication(scanBasePackages = {"mx.aserta.wservices.context",
    "mx.aserta.wservices.handler", "mx.aserta.wservices.interceptor", "mx.aserta.wservices.sps",
    "mx.aserta.pdfgenertor.component",
    "wsdl.serviceconsultas.org.tempuri", "wsdl.buro.consulta.com.bc.ws.consulta.bean", "mx.aserta.wservices.service",
    "mx.aserta.wservices.controller"})
@PropertySources(value = {@PropertySource("file:./iPad/conf/wservices.properties")})
public class SpringBootWebLogicApplication extends SpringBootServletInitializer
    implements WebApplicationInitializer {
    
    private Logger log = LogManager.getLogger("WSERVICES");
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Autowired
    private PropSource prop;
    
    /**
     * Bean para cargar archivo de propiedades
     * @return 
     */
    @Bean(name = "props")
    public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholder() {
        return new PropertySourcesPlaceholderConfigurer();
    }
    
    /**
     * Bean de propiedades
     * @return 
     */
    @Bean(name = "propSource")
    public PropSource getPropertySource() {
        return new PropSource();
    }
    
    @Bean(name = "dataSourceAfiandwh")
    public DataSource getDataSourceAfiandwh() {
        log.debug("Creando datasource Afiandwh...");
        try {
            JndiObjectFactoryBean jndiObjectFactoryBean =new JndiObjectFactoryBean();
            log.debug("props:" + prop.getJndiAfiandwh());
            jndiObjectFactoryBean.setJndiName(prop.getJndiAfiandwh());
            jndiObjectFactoryBean.setResourceRef(true);
            jndiObjectFactoryBean.setProxyInterface(DataSource.class);
            jndiObjectFactoryBean.afterPropertiesSet();
            log.debug("Datasource Afiandwh obtenido:" + jndiObjectFactoryBean.getObject().toString());
            return (DataSource) jndiObjectFactoryBean.getObject();
        } catch (IllegalArgumentException | NamingException ex) {
            ;
        }
        return null;
    }
    
    @Bean(name = "dataSourceSiebelAserta")
    public DataSource getDataSourceSiebelAserta() {
        log.debug("Creando datasource Siebel Aserta...");
        try {
            JndiObjectFactoryBean jndiObjectFactoryBean =new JndiObjectFactoryBean();
            log.debug("props:" + prop.getJndiSiebelAserta());
            jndiObjectFactoryBean.setJndiName(prop.getJndiSiebelAserta());
            jndiObjectFactoryBean.setResourceRef(true);
            jndiObjectFactoryBean.setProxyInterface(DataSource.class);
            jndiObjectFactoryBean.afterPropertiesSet();
            log.debug("Datasource Siebel Aserta obtenido:" + jndiObjectFactoryBean.getObject().toString());
            return (DataSource) jndiObjectFactoryBean.getObject();
        } catch (IllegalArgumentException ex) {
            ;
        } catch (NamingException ex) {
            ;
        }
        return null;
    }
    
    @Bean(name = "dataSourceSiebelInsurgentes")
    public DataSource getDataSourceSiebelInsurgentes() {
        log.debug("Creando datasource Siebel Insurgentes...");
        try {
            JndiObjectFactoryBean jndiObjectFactoryBean =new JndiObjectFactoryBean();
            log.debug("props:" + prop.getJndiSiebelInsurgentes());
            jndiObjectFactoryBean.setJndiName(prop.getJndiSiebelInsurgentes());
            jndiObjectFactoryBean.setResourceRef(true);
            jndiObjectFactoryBean.setProxyInterface(DataSource.class);
            jndiObjectFactoryBean.afterPropertiesSet();
            log.debug("Datasource Siebel Insurgentes obtenido:" + jndiObjectFactoryBean.getObject().toString());
            return (DataSource) jndiObjectFactoryBean.getObject();
        } catch (IllegalArgumentException ex) {
            ;
        } catch (NamingException ex) {
            ;
        }
        return null;
    }
    
    @Bean(name = "dataSourceReporteador")
    public DataSource getDataSourceReporteador() {
        log.debug("Creando datasource Reporteador...");
        try {
            JndiObjectFactoryBean jndiObjectFactoryBean =new JndiObjectFactoryBean();
            log.debug("props:" + prop.getJndiReporteador());
            jndiObjectFactoryBean.setJndiName(prop.getJndiReporteador());
            jndiObjectFactoryBean.setResourceRef(true);
            jndiObjectFactoryBean.setProxyInterface(DataSource.class);
            jndiObjectFactoryBean.afterPropertiesSet();
            log.debug("Datasource Reporteador obtenido:" + jndiObjectFactoryBean.getObject().toString());
            return (DataSource) jndiObjectFactoryBean.getObject();
        } catch (IllegalArgumentException ex) {
            ;
        } catch (NamingException ex) {
            ;
        }
        return null;
    }
    
    /**
     * Cargar el contexto de los servlets
     * @param sc 
     */
    @Override
    public void onStartup(ServletContext sc) {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        //Generar contexto para CXFServlet (asi no apareceran en el Servlet dispatcher de Spring boot)
        rootContext.register(SpringBootWebLogicApplication.class,
                //BovedaWSImpl.class,
                //BovedaWSEndpoint.class,
                ClientesWSImpl.class,
                ClientesWSEndpoint.class,
                ArrendamientoWSImpl.class,
                ArrendamientoWSEndpoint.class,
                RecibePagoWSImpl.class,
                RecibePagoWSEndpoint.class,
                CreaBaseWSImpl.class,
                CreaBaseWSEndpoint.class,
                AnulacionWSImpl.class,
                AnulacionWSEndpoint.class,
                CancelacionWSImpl.class,
                CancelacionWSEndpoint.class,
                EmisionWSImpl.class,
                EmisionWSEndpoint.class,
                ConsultaArchivoNowoWSImpl.class,
                ConsultaArchivoNowoWSEndpoint.class,
                ConsultaDocumentosWSImpl.class,
                ConsultaDocumentosWSEndpoint.class,
                ConsultaBuroWSImpl.class,
                ConsultaBuroWSEndpoint.class,
                AonGruposWSImpl.class,
                AonGruposWSEndpoint.class,
                ClientesMoralWSImpl.class,
                ClientesMoralWSEndpoint.class,
                CreaBaseVWSImpl.class,
                CreaBaseVWSEndpoint.class,
                VerificaAgenteWSImpl.class,
                VerificaAgenteWSEndpoint.class,
                VerificaCodigoAfiliadoWSImpl.class,
                VerificaCodigoAfiliacionWSEndpoint.class,
                ConsultaDocsWSImpl.class,
                ConsultaDocsWSEndpoint.class,
                NowoCartaAceptacionWSImpl.class,
                NowoCartaAceptacionWSEndpoint.class,
                NowoCheckIdentityWSImpl.class,
                NowoCheckIdentityWSEndpoint.class,
                NowoRatingWSImpl.class,
                NowoRatingWSEndpoint.class,
                NowoRentDecisionWSEndpoint.class,
                NowoRentDecisionWSImpl.class,
                NowoUpdateRentDecisionWSEndpoint.class,
                NowoUpdateRentDecisionWSImpl.class,
                ConsultaBuroSSWSEndpoint.class,
                NowoPagoWSEndpoint.class,
                NowoPagoWSImpl.class,
                NowoEmisionAsincronaCallbackWSEndpoint.class,
                NowoEmisionAsincronaCallbackWSImpl.class,
                NowoRentEvaluateWSEndpoint.class,
                NowoRentEvaluateWSImpl.class,
                NowoEmisionAsyncEspWSEndpoint.class,
                NowoEmisionAsyncEspWSImpl.class);
        sc.addListener(new ContextLoaderListener(rootContext));
        
        ServletRegistration.Dynamic dispatcher = sc.addServlet("dispatcher", new DispatcherServlet(rootContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
        
        ServletRegistration.Dynamic cxfDispatcher = sc.addServlet("cxf", CXFServlet.class);
        cxfDispatcher.setLoadOnStartup(1);
        cxfDispatcher.addMapping("/ws/*");
        
        CharacterEncodingFilter cef = new CharacterEncodingFilter();
        cef.setEncoding("UTF-8");
        cef.setForceEncoding(true);
        
        FilterRegistration.Dynamic frd = sc.addFilter("characterEncodingFilter", cef);
        frd.addMappingForUrlPatterns(null, false, "/*");
    }
    
    /**
     * 
     * @param application
     * @return 
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootWebLogicApplication.class);
    }
    
    /**
     * Main
     * @param args 
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringBootWebLogicApplication.class, args);
    }
}
