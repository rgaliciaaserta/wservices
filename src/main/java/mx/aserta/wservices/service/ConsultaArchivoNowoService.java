/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ConsultaArchivoBase64VO;
import mx.aserta.wservices.vo.ConsultaArchivoNowoResponseVO;
import mx.aserta.wservices.vo.ConsultaArchivoNowoVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.nowoattachment.com.siebel.customui.ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service;
import wsdl.nowoattachment.com.siebel.customui.ConsultaInput;
import wsdl.nowoattachment.com.siebel.customui.ConsultaOutput;
import wsdl.nowoattachment.com.siebel.xml.aserta_20convierte_20archivo_20base_20nowo_20io.AsertaConvierteArchivoBaseBc;

/**
 *
 * @author rgalicia
 */
@Service
public class ConsultaArchivoNowoService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service wsConsultaArchivoNowoAserta;
    
    private ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service wsConsultaArchivoNowoInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsConsultaArchivoNowoAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoAttachmentAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsConsultaArchivoNowoAserta = new ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service(asertaUrl);
                this.wsConsultaArchivoNowoAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsConsultaArchivoNowoAserta = new ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsConsultaArchivoNowoInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoAttachmentInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsConsultaArchivoNowoInsurgentes = new ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service(insurgentesUrl);
                this.wsConsultaArchivoNowoInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsConsultaArchivoNowoInsurgentes = new ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service();
            }
        }
    }

    public ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service getWsConsultaArchivoNowoAserta() {
        if (this.wsConsultaArchivoNowoAserta == null) {
            this.initAserta();
        }
        return this.wsConsultaArchivoNowoAserta;
    }

    public ASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service getWsConsultaArchivoNowoInsurgentes() {
        if (this.wsConsultaArchivoNowoInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsConsultaArchivoNowoInsurgentes;
    }
    
    /**
     * Consumir el servicio de consulta documentos nowo
     * @param empresa
     * @param consulta
     * @return 
     */
    public ConsultaArchivoNowoResponseVO consultaNowoAttachment(String empresa, ConsultaArchivoNowoVO consulta) {
        log.debug("\tASERTASpcConsultaSpcArchivosSpcNowoSpcWS_Service.consultaNowoAttachment");
        
        ConsultaArchivoNowoResponseVO resp = new ConsultaArchivoNowoResponseVO();
        
        if (consulta == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ConsultaInput input = new ConsultaInput();
            input.setIdPoliza(consulta.getIdPoliza());
            input.setTipoDocumento(consulta.getTipoDocumento());
            
            ConsultaOutput output;
            //TODO
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsConsultaArchivoNowoAserta()
                        .getASERTASpcConsultaSpcArchivosSpcNowoSpcWS().consulta(input);
            } else {
                output = this.getWsConsultaArchivoNowoInsurgentes()
                        .getASERTASpcConsultaSpcArchivosSpcNowoSpcWS().consulta(input);
            }
            
            if (output == null || output.getExito() == null
                    || output.getExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getMensaje() == null)? null:(String) output.getMensaje());
            resp.setErrorCode((output.getErrorSpcCode() == null)? null:(String) output.getErrorSpcCode());
            resp.setErrorMessage((output.getErrorSpcMessage() == null)? null:(String) output.getErrorSpcMessage());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getExito())) {
                resp.setExito(Util.SPS_EXITO);
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
            
            if (output.getListOfAsertaConvierteArchivoBaseNowoIo()!= null
                    && output.getListOfAsertaConvierteArchivoBaseNowoIo().getAsertaConvierteArchivoBaseBc() != null
                    && !output.getListOfAsertaConvierteArchivoBaseNowoIo().getAsertaConvierteArchivoBaseBc().isEmpty()) {
                
                resp.setArchivos(new ArrayList<>());
                ConsultaArchivoBase64VO archBase;
                
                for (AsertaConvierteArchivoBaseBc objArch
                        : output.getListOfAsertaConvierteArchivoBaseNowoIo().getAsertaConvierteArchivoBaseBc()) {
                    if (objArch == null) {
                        continue;
                    }
                    
                    archBase = new ConsultaArchivoBase64VO();
                    
                    archBase.setActivityComments(objArch.getActivityComments());
                    //archBase.setActivityFileBuffer(objArch.getActivityFileBuffer());
                    archBase.setActivityFileExt(objArch.getActivityFileExt());
                    archBase.setActivityFileName(objArch.getActivityFileName());
                    archBase.setActivityId(objArch.getActivityId());
                    archBase.setAttachmentCategory(objArch.getAttachmentCategory());
                    archBase.setAttachmentType(objArch.getAttachmentType());
                    archBase.setId(objArch.getId());
                    //archBase.setLlave(objArch.getLlaveNowo());
                    //archBase.setLlave(null);
                    archBase.setOperation(objArch.getOperation());
                    //archBase.setPassword(objArch.getPasswordNowo());
                    //archBase.setPassword(null);
                    archBase.setSearchSpec(objArch.getSearchspec());
                    archBase.setStatus(objArch.getStatus());
                    //archBase.setVector(objArch.getVectorNowo());
                    //archBase.setVector(null);
                    
                    archBase.setActivityFileBuffer(
                            Util.desencryptBytePDF(objArch.getActivityFileBuffer(),
                                    objArch.getPasswordNowo()));
                    
                    resp.getArchivos().add(archBase);
                }
            }
        } catch (Exception ex) {
            log.error("Error consultaNowoAttachment:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
