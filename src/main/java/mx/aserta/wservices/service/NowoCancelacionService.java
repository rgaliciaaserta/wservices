/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoCancelacionResponseVO;
import mx.aserta.wservices.vo.NowoCancelacionVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.nowocancelacion.com.siebel.customui.ASERTANowoCancelacionBSGeneraCancelacionInput;
import wsdl.nowocancelacion.com.siebel.customui.ASERTANowoCancelacionBSGeneraCancelacionOutput;
import wsdl.nowocancelacion.com.siebel.customui.ASERTASpcNowoSpcCancelacionSpcBS_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoCancelacionService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcCancelacionSpcBS_Service wsNowoCancelacionAserta;
    
    private ASERTASpcNowoSpcCancelacionSpcBS_Service wsNowoCancelacionInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsNowoCancelacionAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoCancelacionAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoCancelacionAserta = new ASERTASpcNowoSpcCancelacionSpcBS_Service(asertaUrl);
                this.wsNowoCancelacionAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoCancelacionAserta = new ASERTASpcNowoSpcCancelacionSpcBS_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsNowoCancelacionInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoCancelacionInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoCancelacionInsurgentes = new ASERTASpcNowoSpcCancelacionSpcBS_Service(insurgentesUrl);
                this.wsNowoCancelacionInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoCancelacionInsurgentes = new ASERTASpcNowoSpcCancelacionSpcBS_Service();
            }
        }
    }

    public ASERTASpcNowoSpcCancelacionSpcBS_Service getWsNowoCancelacionAserta() {
        if (this.wsNowoCancelacionAserta == null) {
            this.initAserta();
        }
        return this.wsNowoCancelacionAserta;
    }

    public ASERTASpcNowoSpcCancelacionSpcBS_Service getWsNowoCancelacionInsurgentes() {
        if (this.wsNowoCancelacionInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsNowoCancelacionInsurgentes;
    }
    
    /**
     * Consumir el servicio de cancelacion
     * @param empresa
     * @param datos
     * @return 
     */
    public NowoCancelacionResponseVO nowoCancelacion(String empresa, NowoCancelacionVO datos) {
        log.debug("\tNowoCancelacionService.nowoCancelacion");
        
        NowoCancelacionResponseVO resp = new NowoCancelacionResponseVO();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoCancelacionBSGeneraCancelacionInput input = new ASERTANowoCancelacionBSGeneraCancelacionInput();
            
            input.setInPolizaId(datos.getPolizaId());
            
            ASERTANowoCancelacionBSGeneraCancelacionOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsNowoCancelacionAserta().getASERTASpcNowoSpcCancelacionSpcBS()
                        .asertaNowoCancelacionBSGeneraCancelacion(input);
            } else {
                output = this.getWsNowoCancelacionInsurgentes().getASERTASpcNowoSpcCancelacionSpcBS()
                        .asertaNowoCancelacionBSGeneraCancelacion(input);
            }
            
            if (output == null || output.getOutExito() == null
                    || output.getOutExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje((output != null && output.getOutMensaje() != null && !output.getOutMensaje().isEmpty())?
                        (String) output.getOutMensaje():"Ocurrió un error en el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getOutMensaje() == null)? null:(String) output.getOutMensaje());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getOutExito())) {
                resp.setExito(Util.SPS_EXITO);
                
                resp.setIdNotaCredito(output.getOutIdNotaCredito());
                resp.setIvaDevolucion(output.getOutIvaDevolucion());
                resp.setPrimaDevolucion(output.getOutPrimaDevolucion());
                resp.setTotalDevolucion(output.getOutTotalDevolucion());
                
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error nowoCancelacion:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
