/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoEmisionResponseVO;
import mx.aserta.wservices.vo.NowoEmisionVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.nowoemision.com.siebel.customui.ASERTACXCoreEmisionNowoEmisionNowoInput;
import wsdl.nowoemision.com.siebel.customui.ASERTACXCoreEmisionNowoEmisionNowoOutput;
import wsdl.nowoemision.com.siebel.customui.ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoEmisionService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service wsNowoEmisionAserta;
    
    private ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service wsNowoEmisionInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsNowoEmisionAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoEmisionAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoEmisionAserta = new ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(asertaUrl);
                this.wsNowoEmisionAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoEmisionAserta = new ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsNowoEmisionInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoEmisionInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoEmisionInsurgentes = new ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service(insurgentesUrl);
                this.wsNowoEmisionInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoEmisionInsurgentes = new ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service();
            }
        }
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service getWsNowoEmisionAserta() {
        if (this.wsNowoEmisionAserta == null) {
            this.initAserta();
        }
        return this.wsNowoEmisionAserta;
    }

    public ASERTASpcCXSpcCoreSpcEmisionSpcNowo_Service getWsNowoEmisionInsurgentes() {
        if (this.wsNowoEmisionInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsNowoEmisionInsurgentes;
    }
    
    /**
     * Consumir el servicio de anulacion
     * @param empresa
     * @param datos
     * @return 
     */
    public NowoEmisionResponseVO nowoEmision(String empresa, NowoEmisionVO datos) {
        log.debug("\tNowoEmisionService.nowoEmision");
        
        NowoEmisionResponseVO resp = new NowoEmisionResponseVO();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTACXCoreEmisionNowoEmisionNowoInput input = new ASERTACXCoreEmisionNowoEmisionNowoInput();
            
            input.setBaseSpcId(datos.getBaseId());
            
            ASERTACXCoreEmisionNowoEmisionNowoOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsNowoEmisionAserta().getASERTASpcCXSpcCoreSpcEmisionSpcNowo()
                        .asertacxCoreEmisionNowoEmisionNowo(input);
            } else {
                output = this.getWsNowoEmisionInsurgentes().getASERTASpcCXSpcCoreSpcEmisionSpcNowo()
                        .asertacxCoreEmisionNowoEmisionNowo(input);
            }
            
            if (output == null || output.getExito() == null
                    || output.getExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getMsgError() == null)? null:(String) output.getMsgError());
            
            resp.setFolioFianza(output.getOutFolioFianza());
            resp.setFolioRecibo(output.getOutFolioRecibo());
            resp.setIdMovimiento(output.getOutIdMovimiento());
            resp.setIdPoliza(output.getOutIdPoliza());
            resp.setIdRecibo(output.getOutIdRecibo());
            resp.setNumPoliza(output.getOutNumPoliza());
            resp.setReferenciaCaja(output.getOutReferenciaCaja());
            resp.setReferenciaCliente(output.getOutReferenciaCliente());
            resp.setSerieFianza(output.getOutSerieFianza());
            resp.setSerieRecibo(output.getOutSerieRecibo());
            resp.setTipoMovimiento(output.getOutTipoMovimiento());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getExito())) {
                resp.setExito(Util.SPS_EXITO);
                
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error nowoEmision:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
