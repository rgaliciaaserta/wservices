/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.sps.GetDatosBovAsertaService;
import mx.aserta.wservices.sps.GetDatosBovInsurgentesService;
import mx.aserta.wservices.sps.GetNowoConfService;
import mx.aserta.wservices.sps.GetPaquetesAsertaService;
import mx.aserta.wservices.sps.GetPaquetesInsurgentesService;
import mx.aserta.wservices.sps.GetSerieAsertaService;
import mx.aserta.wservices.sps.GetSerieInsurgentesService;
import mx.aserta.wservices.sps.LogBitacoraFileService;
import mx.aserta.wservices.sps.LogBitacoraService;
import mx.aserta.wservices.thread.BovedaThread;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.BovedaVO;
import mx.aserta.wservices.vo.ConsultaDocumentosResponseVO;
import mx.aserta.wservices.vo.ConsultaDocumentosVO;
import mx.aserta.wservices.vo.NowoProcessVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.serviceconsultas.org.tempuri.ServiceConsultas;

/**
 *
 * @author rgalicia
 */
@Service
public class ConsultaDocumentoService {
    
    @Autowired
    private ServiceConsultas serviceConsultas;
    
    //@Autowired
    //private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private GetDatosBovAsertaService getDatosBovAsertaService;
    
    @Autowired
    private GetDatosBovInsurgentesService getDatosBovInsurgentesService;
    
    @Autowired
    private GetPaquetesAsertaService getPaquetesAsertaService;
    
    @Autowired
    private GetPaquetesInsurgentesService getPaquetesInsurgentesService;
    
    @Autowired
    private GetSerieAsertaService getSerieAsertaService;
    
    @Autowired
    private GetSerieInsurgentesService getSerieInsurgentesService;
    
    @Autowired
    private GetNowoConfService getNowoConfService;
    
    @Autowired
    private LogBitacoraService logBitacoraService;
    
    @Autowired
    private LogBitacoraFileService logBitacoraFileService;
    
    @Autowired
    private PropSource prop;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Consulta operacion de boveda
     * @param bov
     * @param metodo
     * @return 
     */
    /*public byte[] consultaServicioBoveda(BovedaVO bov, String metodo) {
        
        log.debug("\tConsultaDocumentoService.consultaServicioBoveda");
        
        if (this.serviceConsultas.getHandlerResolver() == null) {
            this.serviceConsultas.setHandlerResolver((PortInfo portInfo) -> {
                List<Handler> list = new ArrayList<>();
                list.add(this.soapMessageLogHandler);

                return list;
            });
        }
        
        if (Util.GET_PDF.equalsIgnoreCase(metodo)) {
            return this.serviceConsultas.getServiceConsultasSoap12().getPdf(
                    bov.getRfcEmisor(),
                    bov.getIdPoliza(),
                    bov.getIdMovimiento(),
                    bov.getTipoDocumento(),
                    bov.getSerie(),
                    bov.getFolio(),
                    bov.getFormato());
        } else if (Util.GET_XML.equalsIgnoreCase(metodo)) {
            return this.serviceConsultas.getServiceConsultasSoap12().getXml(
                    bov.getRfcEmisor(),
                    bov.getIdPoliza(),
                    bov.getIdMovimiento(),
                    bov.getTipoDocumento(),
                    bov.getSerie(),
                    bov.getFolio());
        } else if (Util.GET_PDF_ACUSE.equalsIgnoreCase(metodo)) {
            return this.serviceConsultas.getServiceConsultasSoap12().getPdfAcuse(
                    bov.getIdMovimiento());
        } else if (Util.GET_XML_ACUSE.equalsIgnoreCase(metodo)) {
            return this.serviceConsultas.getServiceConsultasSoap12().getXmlAcuse(
                    bov.getIdMovimiento());
        }
        
        return null;
    }*/
    
    /**
     * Consulta paquete de documentos
     * @param empresa
     * @param consulta
     * @return 
     */
    public ConsultaDocumentosResponseVO consultaPaquete(String empresa, ConsultaDocumentosVO consulta) {
        ConsultaDocumentosResponseVO resp = new ConsultaDocumentosResponseVO();
        log.debug("\tconsultaPaquete:Empresa:" + empresa);
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa) && !Util.INSURGENTES.equalsIgnoreCase(empresa))
                || consulta == null) {
            log.debug("\tDebe indicar la empresa y el objeto request.");
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa [ASERTA|INSURGENTES] y el objeto request.");
            return resp;
        }
        
        if (consulta.getIdPoliza() == null || consulta.getIdPoliza().isEmpty()
                || consulta.getTipoPaquete() == null || consulta.getTipoPaquete().isEmpty()) {
            log.debug("\tDebe indicar el id de póliza y el tipo de paquete.");
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el id de póliza y el tipo de paquete.");
            return resp;
        }
        
        log.debug("\tConsultaPaquete:idPoliza:" + consulta.getIdPoliza());
        List<BovedaVO> listBov = this.buildBovedaVO(empresa, consulta.getIdPoliza());
        if (listBov == null || listBov.isEmpty()
                || listBov.size() != 2) {
            log.debug("\tNo hay configuración para los documentos de la poliza " + consulta.getIdPoliza());
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("No se encontró información para los documentos de la póliza " + consulta.getIdPoliza());
            return resp;
        }
        
        List<String> paquetes = this.getPaqueteDocumentos(empresa, consulta.getTipoPaquete());
        if (paquetes == null || paquetes.isEmpty()) {
            log.debug("\tNo hay configuración para los paquetes de documentos del tipo " + consulta.getTipoPaquete());
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("No se encontró información para los paquetes de documentos del tipo " + consulta.getTipoPaquete());
            return resp;
        }
        
        //Obtener valor de serie para folleto
        
        //listBov.get(0) Poliza
        //listBov.get(1) Recibo
        this.logBitacoraService.compile();
        
        NowoProcessVO nowoProcess = new NowoProcessVO();
        nowoProcess.setId(-1L);
        nowoProcess.setLogBitacoraService(this.logBitacoraService);
        nowoProcess.setIdPoliza(consulta.getIdPoliza());
        nowoProcess.setTipoPaquete(consulta.getTipoPaquete());
        nowoProcess.setEstatus(Util.ESTATUS_INITIATED);
        nowoProcess.setUsuario(Util.SYSTEM);
        nowoProcess.saveUpdate();
        
        List<BovedaThread> listProcssBov = this.launchProcess(listBov, paquetes, nowoProcess);
        
        if (listProcssBov == null || listProcssBov.isEmpty()) {
            log.debug("\tLa lista de procesos de boveda esta vacia.");
            nowoProcess.setEstatus(Util.ESTATUS_ERROR);
            nowoProcess.setMessage("La lista de procesos de boveda esta vacia.");
            nowoProcess.saveUpdate();
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("No se puede generar el documento con la información solicitada.");
            return resp;
        }
        
        //Wait process
        this.waitTillFinish(listProcssBov);
        
        //Equal process vs packs count
        if (listProcssBov.size() != paquetes.size()) {
            log.debug("\tEL número de procesos lanzados no coincide con los documentos del paquete.");
            nowoProcess.setEstatus(Util.ESTATUS_ERROR);
            nowoProcess.setMessage("EL número de procesos lanzados no coincide con los documentos del paquete:["
                    + listProcssBov.size() + "-" + paquetes.size() + "].");
            nowoProcess.saveUpdate();
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje(nowoProcess.getMessage());
            return resp;
        }
        
        //Return Base 64 pdf document
        String fileName = this.buildPdfDocument(listProcssBov);
        if (fileName == null || fileName.isEmpty()
                || fileName.startsWith(Util.ESTATUS_ERROR)) {
            nowoProcess.setEstatus(Util.ESTATUS_ERROR);
            nowoProcess.setMessage("Error al generar el archivo:" + fileName);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error al generar el archivo.");
        } else {
            nowoProcess.setEstatus(Util.ESTATUS_FINISHED);
            nowoProcess.setMessage(Util.MSG_EXITO);
            nowoProcess.setPathFile(fileName);
            resp.setExito(Util.SPS_EXITO);
            resp.setMensaje(Util.MSG_EXITO);
            log.debug("\tArchivo:" + fileName);
            resp.setData(Util.getFileByteArray(fileName, true));
        }
        
        nowoProcess.saveUpdate();
        return resp;
    }
    
    /**
     * Obtener folio de folleto
     * @return 
     */
    public String getFolioFolleto() {
        log.debug("\tObteniendo el valor de folio para folleto.");
        
        try {
            RespuestaJson resp = this.getNowoConfService.execute(Util.KEY_FOLIO_FOLLETO);
            
            if (resp == null || resp.getCodigo() == null
                    || !Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())
                    || resp.getDatos() == null
                    || ((List<Map<String, String>>) resp.getDatos()).isEmpty()) {
                return Util.FOLIO_FOLLETO;
            }
            
            List<Map<String, String>> lista = (List<Map<String, String>>) resp.getDatos();
            if (lista.get(0) == null || lista.get(0).isEmpty()
                    || lista.get(0).get(Util.DATO + "1") == null
                    || lista.get(0).get(Util.DATO + "1").isEmpty()) {
                return Util.FOLIO_FOLLETO;
            }
            
            return lista.get(0).get(Util.DATO + "1");
        } catch (Exception ex) {
            log.error("\tError:getFolioFolleto:" + ex.getMessage(), ex);
            return Util.FOLIO_FOLLETO;
        }
    }
    
    /**
     * Launch boveda process
     * @param listBov
     * @param listPackage
     * @param nowoProcess
     * @return 
     */
    public List<BovedaThread> launchProcess(List<BovedaVO> listBov, List<String> listPackage,
            NowoProcessVO nowoProcess) {
        if (listPackage == null || listPackage.isEmpty()
                || listBov == null || listBov.isEmpty() || listBov.size() != 2) {
            log.debug("\tParametros incorrectos para la boveda.");
            return null;
        }
        
        String folioFolleto = this.getFolioFolleto();
        log.debug("\tFOLIO_FOLLETO::" + folioFolleto);
        
        List<BovedaThread> listaBoveda = new ArrayList<>();
        BovedaVO bovObj = null;
        BovedaThread bovThread;
        this.serviceConsultas.getClass();
        this.logBitacoraFileService.compile();
        
        for (String pack : listPackage) {
            if (pack == null || pack.isEmpty()) {
                continue;
            }
            
            log.debug("\tLanzando hilo para tipoDocumento:" + pack);
            
            if (Util.DOC_POLIZA.equalsIgnoreCase(pack)
                    || Util.DOC_CERTIFICADO.equalsIgnoreCase(pack)
                    || Util.DOC_POLIZACONTRATO.equalsIgnoreCase(pack)
                    || Util.DOC_SOLICITUD.equalsIgnoreCase(pack)) {
                bovObj = this.buildBovedaObject(listBov.get(0));
            } else if (Util.DOC_RECIBO.equalsIgnoreCase(pack)) {
                bovObj = this.buildBovedaObject(listBov.get(1));
            } else if (Util.DOC_FOLLETO.equalsIgnoreCase(pack)) {
                bovObj = this.buildBovedaObject(listBov.get(0));
                bovObj.setFolio(folioFolleto);
                bovObj.setSerie(pack);
                bovObj.setIdMovimiento("1-" + pack);
                bovObj.setIdPoliza("1-" + pack);
            } else {
                continue;
            }
            
            bovObj.setTipoDocumento(pack);
            
            bovThread = new BovedaThread(bovObj);
            bovThread.setExtFile(this.prop.getFilePdfExtension());
            bovThread.setPathFile(this.prop.getFilePathTmp());
            bovThread.setServiceConsultas(this.serviceConsultas);
            bovThread.getNowoFileProcessVO().setNowoProcessId(nowoProcess.getId());
            bovThread.getNowoFileProcessVO().setLogBitacoraFileService(this.logBitacoraFileService);
            
            listaBoveda.add(bovThread);
            
            bovThread.start();
        }
        
        return listaBoveda;
    }
    
    /**
     * Wait untill list Boveda finish
     * @param listBoveda 
     */
    private void waitTillFinish(List<BovedaThread> listBoveda) {
        if (listBoveda == null || listBoveda.isEmpty()) {
            return;
        }
        
        boolean finish = false;
        int count = 100;
        while (!finish && count > 0) {
            finish = true;
            for (BovedaThread bovObj : listBoveda) {
                if (!bovObj.isFinished()) {
                    finish = false;
                }
            }
            
            try {
                Thread.sleep(1000L);
            } catch (Exception ex) {
                log.debug("\tError sleep:" + ex.getMessage());
            }
            
            count --;
        }
        
        //Update log
        for (BovedaThread bovObj : listBoveda) {
            bovObj.getNowoFileProcessVO().saveUpdate();
        }
    }
    
    /**
     * Build PDF to file
     * @param listBoveda
     * @return 
     */
    private String buildPdfDocument(List<BovedaThread> listBoveda) {
        if (listBoveda == null || listBoveda.isEmpty()) {
            log.debug("\tNo hay procesos para armar archivo de salida.");
            return Util.ESTATUS_ERROR + ":No hay procesos para armar archivo de salida.";
        }
        
        PDDocument document = new PDDocument();
        List<PDDocument> lPart = new ArrayList<>();
        PDDocument part = null;
        String fileName = null;
        File origen;
        StringBuilder msg = new StringBuilder();
        log.debug("\tUniendo archivos en uno solo:" + listBoveda.size());
        for (BovedaThread bovObj : listBoveda) {
            if (!bovObj.isSuccess()) {
                log.debug("\t++Boveda Process Error: Not Success!");
                msg.append(Util.ESTATUS_ERROR);
                msg.append(":Generando ");
                msg.append(bovObj.getBovedaVO().getTipoDocumento());
                msg.append("-");
                continue;
            }
            
            fileName = bovObj.getBovedaVO().getIdPoliza();
            part = null;
            origen = null;
            try {
                origen = new File(bovObj.getCompletePathFile());
                part = PDDocument.load(origen);
                lPart.add(part);
                
                PDPageTree pgtree = part.getPages();
                Iterator<PDPage> iter = pgtree.iterator();
                while(iter.hasNext()) {
                    document.addPage(iter.next());
                }
                
            } catch (Exception ex) {
                msg.append(Util.ESTATUS_ERROR);
                msg.append(":Generando ");
                msg.append(bovObj.getBovedaVO().getTipoDocumento());
                msg.append("-");
                log.error("\tError generando pdf File:" + bovObj.getCompletePathFile() + ":" + ex.getMessage());
            } finally {
                /*if (part != null) {
                    try {
                        part.close();
                    } catch (Exception ex) {
                        log.error("\tError cerrando PDF:" + ex.getMessage());
                    }
                }*/
                
                if (origen != null) {
                    origen.delete();
                }
            }
        }
        
        if (msg.length() > 0) {
            try {
                log.debug("\tCerrando documento PDF.");
                document.close();
            } catch (Exception ex) {
                log.error("\tError cerrando BAOS del PDF");
            }
            
            return msg.toString();
        }
        
        fileName = this.prop.getFilePathTmp()
                + Util.getRandomUID() + fileName + Util.POINT + this.prop.getFilePdfExtension();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileName);
            document.save(fos);
            
            return fileName;
        } catch (Exception ex) {
            log.error("\tError escribiendo PDF final:" + ex.getMessage());
            return Util.ESTATUS_ERROR + ": escribiendo PDF final:" + ex.getMessage();
        } finally {
            try {
                document.close();
            } catch (Exception ex) {
                log.error("\tError cerrando BAOS del PDF");
            }
            
            if (!lPart.isEmpty()) {
                for (PDDocument p : lPart) {
                    try {
                        part.close();
                    } catch (Exception ex) {
                        log.error("\tError cerrando PDF:" + ex.getMessage());
                    }
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception ex) {
                    log.error("\tError cerrando BAOS del PDF");
                }
            }
        }
    }
    
    /**
     * Construir objeto para boveda
     * @param empresa
     * @param idPoliza
     * @return 
     */
    private List<BovedaVO> buildBovedaVO(String empresa, String idPoliza) {
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                    && !Util.INSURGENTES.equalsIgnoreCase(empresa))
                || idPoliza == null || idPoliza.isEmpty()) {
            return null;
        }
        
        RespuestaJson resp, respSerie;
        log.debug("\tbuildBoveda:empresa:" + empresa);
        if (Util.ASERTA.equalsIgnoreCase(empresa)) {
            resp = this.getDatosBovAsertaService.execute(idPoliza);
            respSerie = this.getSerieAsertaService.execute(idPoliza);
        } else {
            resp = this.getDatosBovInsurgentesService.execute(idPoliza);
            respSerie = this.getSerieInsurgentesService.execute(idPoliza);
        }
        
        if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())
                || (List<Map<String, String>>) resp.getDatos() == null
                || ((List<Map<String, String>>) resp.getDatos()).isEmpty()
                || ((List<Map<String, String>>) resp.getDatos()).get(0) == null) {
            log.debug("\tNo se obtuvieron datos del SP para consultar al boveda.");
            return null;
        }
        
        if (!Util.COD_EXITO.equalsIgnoreCase(respSerie.getCodigo())
                || respSerie.getDatos() == null
                || ((List<String>)respSerie.getDatos()).isEmpty()
                || ((List<String>)respSerie.getDatos()).size() != 2) {
            log.debug("\tNo se obtuvieron datos del SP Serie para consultar la boveda.");
            return null;
        }
        
        List<String> serieList = (List<String>) respSerie.getDatos();
        List<BovedaVO> listObjBov = new ArrayList<>();
        Map<String, String> mapDatos = ((List<Map<String, String>>) resp.getDatos()).get(0);
        BovedaVO objPoliza, objRecibo;
        
        objPoliza = new BovedaVO();
        objPoliza.setFolio(mapDatos.get(Util.DATO + "4"));
        objPoliza.setIdMovimiento(mapDatos.get(Util.DATO + "5"));
        objPoliza.setIdPoliza(mapDatos.get(Util.DATO + "1"));
        //objPoliza.setSerie(serieList.get(0));
        objPoliza.setSerie(Util.extractChar(mapDatos.get(Util.DATO + "2")));
        objPoliza.setFormato(Util.ORIGINAL_BOND);
        //objPoliza.setRfcEmisor();
        
        objRecibo = new BovedaVO();
        objRecibo.setFolio(mapDatos.get(Util.DATO + "10"));
        objRecibo.setIdMovimiento(mapDatos.get(Util.DATO + "7"));
        objRecibo.setIdPoliza(mapDatos.get(Util.DATO + "1"));
        //objRecibo.setSerie(serieList.get(1));
        objRecibo.setSerie(Util.extractChar(mapDatos.get(Util.DATO + "8")));
        objRecibo.setFormato(Util.ORIGINAL_BOND);
        //objRecibo.setRfcEmisor();
        
        try {
            //log de objetos
            log.debug("\tobjPoliza:" + TransformObject.getStringFromObject(objPoliza));
            log.debug("\tobjRecibo:" + TransformObject.getStringFromObject(objRecibo));
        } catch (Exception ex) {
            log.error("\tError en log de objeto para consultar boveda:" + ex.getMessage(), ex);
        }
        
        listObjBov.add(objPoliza);
        listObjBov.add(objRecibo);
        return listObjBov;
    }
    
    /**
     * Get the document list from package
     * @param empresa
     * @param tipoPaquete
     * @return 
     */
    private List<String> getPaqueteDocumentos(String empresa, String tipoPaquete) {
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                    && !Util.INSURGENTES.equalsIgnoreCase(empresa))
                || tipoPaquete == null || tipoPaquete.isEmpty()) {
            return null;
        }
        
        RespuestaJson resp;
        if (Util.ASERTA.equalsIgnoreCase(empresa)) {
            resp = this.getPaquetesAsertaService.execute(tipoPaquete);
        } else {
            resp = this.getPaquetesInsurgentesService.execute(tipoPaquete);
        }
        
        if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())
                || resp.getDatos() == null || (List<Map<String, String>>) resp.getDatos() == null
                || ((List<Map<String, String>>) resp.getDatos()).isEmpty()) {
            return null;
        }
        
        List<String> lista = new ArrayList<>();
        for (Map<String, String> map : (List<Map<String, String>>) resp.getDatos()) {
            if (map == null || map.isEmpty() || map.get(Util.DATO + "1") == null
                    || map.get(Util.DATO + "1").isEmpty()) {
                continue;
            }
            
            lista.add(map.get(Util.DATO + "1"));
        }
        
        return lista;
    }
    
    /**
     * Build object from BovedaVO
     * @param obj
     * @return 
     */
    public BovedaVO buildBovedaObject(BovedaVO obj) {
        if (obj == null) {
            log.debug("\tError:buildBovedaObject:objeto null.");
            return null;
        }
        
        BovedaVO bobj = new BovedaVO();
        bobj.setFolio(obj.getFolio());
        bobj.setFormato(obj.getFormato());
        bobj.setIdMovimiento(obj.getIdMovimiento());
        bobj.setIdPoliza(obj.getIdPoliza());
        bobj.setRfcEmisor(obj.getRfcEmisor());
        bobj.setSerie(obj.getSerie());
        bobj.setTipoDocumento(obj.getTipoDocumento());
        
        try {
            log.debug("\tAsignacion de objeto:FROM:" + TransformObject.getStringFromObject(obj));
            log.debug("\tAsignacion de objeto:TO:" + TransformObject.getStringFromObject(bobj));
        } catch (Exception ex) {
            ;
        }
        
        return bobj;
    }
}
