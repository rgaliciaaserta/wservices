/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoCreaBaseResponseVO;
import mx.aserta.wservices.vo.NowoCreaBaseVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.creabase.com.siebel.customui.ASERTANowoCreaBaseEmisionCertificadoNowo1Input;
import wsdl.creabase.com.siebel.customui.ASERTANowoCreaBaseEmisionCertificadoNowo1Output;
import wsdl.creabase.com.siebel.customui.ASERTASpcNowoSpcSpcCreaSpcBase_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoCreaBaseService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcSpcCreaSpcBase_Service wsNowoCreaBaseAserta;
    
    private ASERTASpcNowoSpcSpcCreaSpcBase_Service wsNowoCreaBaseInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsNowoCreaBaseAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoCreaBaseAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoCreaBaseAserta = new ASERTASpcNowoSpcSpcCreaSpcBase_Service(asertaUrl);
                this.wsNowoCreaBaseAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoCreaBaseAserta = new ASERTASpcNowoSpcSpcCreaSpcBase_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsNowoCreaBaseInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoCreaBaseInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoCreaBaseInsurgentes = new ASERTASpcNowoSpcSpcCreaSpcBase_Service(insurgentesUrl);
                this.wsNowoCreaBaseInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoCreaBaseInsurgentes = new ASERTASpcNowoSpcSpcCreaSpcBase_Service();
            }
        }
    }

    public ASERTASpcNowoSpcSpcCreaSpcBase_Service getWsNowoCreaBaseAserta() {
        if (this.wsNowoCreaBaseAserta == null) {
            this.initAserta();
        }
        return this.wsNowoCreaBaseAserta;
    }

    public ASERTASpcNowoSpcSpcCreaSpcBase_Service getWsNowoCreaBaseInsurgentes() {
        if (this.wsNowoCreaBaseInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsNowoCreaBaseInsurgentes;
    }
    
    /**
     * Consumir el servicio de crea base
     * @param empresa
     * @param datos
     * @return 
     */
    public NowoCreaBaseResponseVO nowoCreaBase(String empresa, NowoCreaBaseVO datos) {
        log.debug("\tNowoCreaBaseService.nowoCreaBase");
        
        NowoCreaBaseResponseVO resp = new NowoCreaBaseResponseVO();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoCreaBaseEmisionCertificadoNowo1Input input = new ASERTANowoCreaBaseEmisionCertificadoNowo1Input();
            
            input.setHocOperationId(datos.getHocOperationId());
            input.setInAgenteReferenciador(datos.getAgenteReferenciador());
            input.setInBucket(datos.getBucket());
            input.setInCodigoPropiedad(datos.getCodigoPropiedad());
            input.setInEsquema(datos.getEsquema());
            input.setInFechaInicio(Util.formatDateSiebel(datos.getFechaInicio()));
            input.setInIdAsegurado(datos.getIdAsegurado());
            input.setInIdContratante(datos.getIdContratante());
            input.setInIdObligado(datos.getIdObligado());
            input.setInMeses(((datos.getMeses() == null || datos.getMeses() == 0)? null: datos.getMeses() + ""));
            input.setInMoneda(datos.getMoneda());
            input.setInMontoRenta((datos.getMontoRenta() == null)? null:datos.getMontoRenta() + "");
            input.setRefInmNowo(datos.getReferenciaInmobiliariaNowo());
            
            ASERTANowoCreaBaseEmisionCertificadoNowo1Output output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsNowoCreaBaseAserta().getASERTASpcNowoSpcSpcCreaSpcBase()
                        .asertaNowoCreaBaseEmisionCertificadoNowo1(input);
            } else {
                output = this.getWsNowoCreaBaseInsurgentes().getASERTASpcNowoSpcSpcCreaSpcBase()
                        .asertaNowoCreaBaseEmisionCertificadoNowo1(input);
            }
            
            if (output == null || output.getOutExito() == null
                    || output.getOutExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getOutMensaje() == null)? null:(String) output.getOutMensaje());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getOutExito())) {
                resp.setExito(Util.SPS_EXITO);
                
                /*resp.setComisionAgente(Util.fromString(output.g));
                resp.setComisionHocelot(Util.fromString(output.getOutComisionHocelot()));
                resp.setComisionInmobiliaria(Util.fromString(output.getOutComisionInmobiliaria()));
                resp.setComisionSucursal(Util.fromString(output.getOutComisionSucursal()));*/
                
                resp.setDerechosPoliza(Util.fromString(output.getOutDerechosPoliza()));
                resp.setGastosInvestigacion(Util.fromString(output.getOutGastosInv()));
                resp.setIdBase(output.getOutIdBase());
                resp.setIvaRecibo(Util.fromString(output.getOutIVARecibo()));
                resp.setPrima(Util.fromString(output.getOutPrima()));
                resp.setPrimaTarifa(Util.fromString(output.getOutPrimaTarifa()));
                resp.setSubtotalRecibo(Util.fromString(output.getOutSubTotalRecibo()));
                resp.setTotalCertificado(Util.fromString(output.getOutTotalCertificado()));
                resp.setTotalRecibo(Util.fromString(output.getOutTotalRecibo()));
                
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error nowoCreaBase:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
