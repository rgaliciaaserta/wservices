/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import mx.aserta.pdfgenerator.util.Util;
import mx.aserta.pdfgenertor.component.PDFGeneratorNowo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.sps.GetTextoService;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class PdfNowoService {
    
    @Autowired
    private PDFGeneratorNowo pDFGeneratorNowo;
    
    @Autowired
    private GetTextoService getTextoService;
    
    @Autowired
    private PropSource prop;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Servicio para la generacion de pdf Nowo
     * @param map
     * @param res 
     */
    public void pfgGenerator(Map<String, Object> map, HttpServletResponse res) {
        log.debug("\tPdfNowoService.pfgGenerator");

        log.debug("Carpeta donde buscar los jasper:" + this.prop.getJasperPath());
        //Parametros del jasper
        Map<String, Object> params = new HashMap<>();
        params.put(mx.aserta.pdfgenerator.util.Util.SUBREPORT_FOLDER, this.prop.getJasperPath());
        
        map.put(mx.aserta.pdfgenerator.util.Util.JASPER_MAP, params);
        
        //Lista de jasper
        List<String> jasperList = new ArrayList();
        
        //Lista de Listas de Mapas
        List<List<Map<String, String>>> listaListMapas = new ArrayList<>();
        
        //Datos
        listaListMapas.add(this.getDatos((String) map.get(mx.aserta.pdfgenerator.util.Util.PLANTILLA),
                (String) map.get(mx.aserta.pdfgenerator.util.Util.EMPRESA),
                (String) map.get(mx.aserta.pdfgenerator.util.Util.ROW_ID),
                map));
        
        //Validar datos vacios
        map.put(mx.aserta.pdfgenerator.util.Util.EMPTY_DATA, false);
        
        if (!listaListMapas.isEmpty()) {
            map.put(mx.aserta.pdfgenerator.util.Util.EMPTY_DATA, true);
            
            for (List<Map<String, String>> mapLista : listaListMapas) {
                if (mapLista != null && !mapLista.isEmpty()) {
                    map.put(mx.aserta.pdfgenerator.util.Util.EMPTY_DATA, false);
                }
            }
            
        } else {
            map.put(mx.aserta.pdfgenerator.util.Util.EMPTY_DATA, true);
            
        }
        
        map.put(mx.aserta.pdfgenerator.util.Util.LISTA_MAP,
                listaListMapas);
        
        List<String> textos = new ArrayList();
        
        if (mx.aserta.pdfgenerator.util.Util.PLANTILLA_NOWO_CARTA_CONFIRMACION.equalsIgnoreCase((String) map.get(mx.aserta.pdfgenerator.util.Util.PLANTILLA))) {
            jasperList.add(this.prop.getJasperPath() + this.prop.getJasperNowoCartaAceptacion());
            
            textos.add(mx.aserta.pdfgenerator.util.Util.TXT_NOWO_CARTACONF_HEAD_ASERTA);
            textos.add(mx.aserta.pdfgenerator.util.Util.TXT_NOWO_CARTACONF_HEAD_INSURGENTES);
            textos.add(mx.aserta.pdfgenerator.util.Util.TXT_NOWO_CARTACONF_TXT_ASERTA);
            textos.add(mx.aserta.pdfgenerator.util.Util.TXT_NOWO_CARTACONF_TXT_INSURGENTES);
            
        }
        
        map.put(mx.aserta.pdfgenerator.util.Util.JASPER_LIST, jasperList);
        
        map.put(mx.aserta.pdfgenerator.util.Util.LISTA_TEXTO_MAP, this.getTexto(textos));
        
        try {
            this.pDFGeneratorNowo.generaPdf(map, res);
        } catch (Exception ex) {
            log.error("Error generando el PDF:" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Consultar Datos de Siebel
     * @param tipoConsulta
     * @param empresa
     * @param rowId
     * @param map
     * @return 
     */
    public List<Map<String, String>> getDatos(String tipoConsulta, String empresa, String rowId,
            Map<String, Object> map) {
        if (tipoConsulta == null || tipoConsulta.isEmpty()
                || !mx.aserta.pdfgenerator.util.Util.PLANTILLA_LIST_NOWO.contains(tipoConsulta)) {
            log.debug("Debe indicar el tipo de consulta:" + tipoConsulta);
            return null;
        }
        
        if (rowId == null || rowId.isEmpty()) {
            log.debug("Debe indicar el identificador:" + rowId);
            return null;
        }
        
        log.debug("\trowId:" + rowId);
        RespuestaJson resp = new RespuestaJson();
        log.debug("\tConsultando datos\n");
        if (mx.aserta.pdfgenerator.util.Util.PLANTILLA_NOWO_CARTA_CONFIRMACION.equalsIgnoreCase(tipoConsulta)) {
            resp.setCodigo(Util.COD_OK);
            Map<String, String> mapDatos = new HashMap<>();
            mapDatos.put(mx.aserta.pdfgenerator.util.Util.DATO + "1", (String) map.get(mx.aserta.wservices.util.Util.NOWO_ASEGURADO));
            mapDatos.put(mx.aserta.pdfgenerator.util.Util.DATO + "2", (String) map.get(mx.aserta.wservices.util.Util.NOWO_CONTRATANTE));
            mapDatos.put(mx.aserta.pdfgenerator.util.Util.DATO + "3", (String) map.get(mx.aserta.wservices.util.Util.NOWO_FECHA));
            List<Map<String, String>> list = new ArrayList<>();
            list.add(mapDatos);
            resp.setDatos(list);
            
        } else {
            //resp = this.getPolizaCaucionService.execute(rowId);
            resp.setCodigo(Util.COD_OK);
        }
        
        //Valida peticion exitosa a BD
        if (Util.COD_OK.equalsIgnoreCase(resp.getCodigo())) {
            return (resp.getDatos() == null)? null:(List<Map<String, String>>) resp.getDatos();
        }
        
        return null;
    }
    
    /**
     * Obtener los textos
     * @param ids
     * @return 
     */
    private Map<String, List<LinkedHashMap<String, String>>> getTexto(List<String> ids) {
        log.debug("\tObtener textos...");
        Map<String, List<LinkedHashMap<String, String>>> map = new HashMap<>();
        if (ids == null || ids.isEmpty()) {
            log.debug("No se mandaron id's para obtener los textos.");
            return map;
        }
        
        RespuestaJson resp;
        for (String id : ids) {
            log.debug("\tObteniendo texto:" + id);
            resp = this.getTextoService.execute(id);
            if (Util.COD_OK.equalsIgnoreCase(resp.getCodigo())
                    && resp.getDatos() != null
                    && !((List<Map<String, Object>>) resp.getDatos()).isEmpty()) {
                map.put(id, (List<LinkedHashMap<String, String>>) resp.getDatos());
                log.debug("Textos encontrados:" + id + ":" + ((List<LinkedHashMap<String, String>>) resp.getDatos()).size());
            } else {
                map.put(id, new ArrayList<>());
                log.debug("Textos no encontrados:" + id);
            }
        }
        
        return map;
    }
}
