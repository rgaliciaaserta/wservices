/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.AltaClienteResponseVO;
import mx.aserta.wservices.vo.AltaClienteVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.altacliente.com.siebel.customui.ASERTANowoAltaClienteNowoAltaClienteNowoInput;
import wsdl.altacliente.com.siebel.customui.ASERTANowoAltaClienteNowoAltaClienteNowoOutput;
import wsdl.altacliente.com.siebel.customui.ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class AltaClienteService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service wsAltaClienteAserta;
    
    private ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service wsAltaClienteInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsAltaClienteAserta == null) {
            try {
                File file = new File(this.prop.getWsAltaClienteAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsAltaClienteAserta = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service(asertaUrl);
                this.wsAltaClienteAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsAltaClienteAserta = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsAltaClienteInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsAltaClienteInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsAltaClienteInsurgentes = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service(insurgentesUrl);
                this.wsAltaClienteInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsAltaClienteInsurgentes = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service();
            }
        }
    }

    public ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service getWsAltaClienteAserta() {
        if (this.wsAltaClienteAserta == null) {
            this.initAserta();
        }
        return this.wsAltaClienteAserta;
    }

    public ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service getWsAltaClienteInsurgentes() {
        if (this.wsAltaClienteInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsAltaClienteInsurgentes;
    }
    
    /**
     * Consumir el servicio de prendas
     * @param empresa
     * @param alta
     * @return 
     */
    public AltaClienteResponseVO altaCliente(String empresa, AltaClienteVO alta) {
        log.debug("\tASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service.altacliente");
        
        AltaClienteResponseVO resp = new AltaClienteResponseVO();
        
        if (alta == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoAltaClienteNowoAltaClienteNowoInput input = new ASERTANowoAltaClienteNowoAltaClienteNowoInput();
            
            input.setBanco(alta.getBanco());
            input.setCLABE(alta.getClabe());
            input.setCalleSpcDomicilioSpcaSpcrentar(alta.getCalleDomicilioRentar());
            input.setCalleSpcDomiclioSpcCliente(alta.getCalleDomicilioCliente());
            input.setCiudadSpcDomicilioSpcCliente(alta.getCiudadDomicilioCliente());
            input.setCiudadSpcDomicilioSpcaSpcrentar(alta.getCiudadDomicilioRentar());
            input.setCodigoSpcPostalSpcDomicilioSpcCliente(alta.getCpDomicilioCliente());
            input.setCodigoSpcPostalSpcDomicilioSpcaSpcrentar(alta.getCpDomicilioRentar());
            input.setColoniaSpcDomicilioSpcCliente(alta.getColoniaDomicilioCliente());
            input.setColoniaSpcDomicilioSpcClienteSpcaSpcrentar(alta.getColoniaDomicilioRentar());
            input.setCorreo(alta.getCorreo());
            input.setCuenta(alta.getCuenta());
            input.setCurp(alta.getCurp());
            input.setDelegacionSlhMunicipioSpcDomicilioSpcCliente(alta.getDelMunDomicilioCliente());
            input.setDelegacionSlhMunicipioSpcDomicilioSpcaSpcrentar(alta.getDelMunDomicilioRentar());
            input.setDiaSpcLimiteSpcPagoSpcRenta(alta.getDiaLimitePagoRenta());
            input.setEstadoSpcDomicilioSpcCliente(alta.getEstadoDomicilioCliente());
            input.setEstadoSpcDomicilioSpcaSpcrentar(alta.getEstadoDomicilioRentar());
            input.setFechaSpcNacimiento(Util.formatDateSiebel(alta.getFechaNacimiento()));
            input.setFechaVencimientoTarjeta(Util.formatDateSiebel(alta.getFechaVencimientoTarjeta()));
            input.setFinSpcVigenciaSpcContrato(Util.formatDateSiebel(alta.getFinVigenciaContrato()));
            input.setFinSpcVigenciaSpcGarantia(Util.formatDateSiebel(alta.getFinVigenciaGarantia()));
            input.setInCodigoAgenteNowo(alta.getCodigoAgenteNowo());
            input.setInCodigoPropiedad(alta.getCodigoPropiedad());
            input.setInPerfil(alta.getPerfil());
            input.setInTipoPersona(alta.getTipoPersona());
            input.setInicioSpcVigenciaSpcContrato(Util.formatDateSiebel(alta.getInicioVigenciaContrato()));
            input.setInicioSpcVigenciaSpcGarantia(Util.formatDateSiebel(alta.getInicioVigenciaGarantia()));
            input.setLugarSpcNacimiento(alta.getLugarNacimiento());
            input.setM2SpcDomicilioSpcRentarSpcCliente(
                    (alta.getM2DomicilioRentar() == null)?
                            "":alta.getM2DomicilioRentar() + "");
            input.setMontoSpcRenta(
                    (alta.getMontoRenta() == null)?
                            "":alta.getMontoRenta() + "");
            input.setNacionalidad(alta.getNacionalidad());
            input.setNombre(alta.getNombre());
            input.setNUE(alta.getNue());
            input.setNumeroSpcExteriorSpcDomicilioSpcCliente(alta.getNumExteriorDomicilioCliente());
            input.setNumeroSpcExteriorSpcDomicilioSpcaSpcrentar(alta.getNumExteriorDomicilioRentar());
            input.setNumeroSpcIdentificacionSpcCliente(alta.getNumeroIdentificacionCliente());
            input.setNumeroSpcInteriorSpcDomicilioSpcCliente(alta.getNumInteriorDomicilioCliente());
            input.setNumeroSpcInteriorSpcDomicilioSpcaSpcrentar(alta.getNumInteriorDomicilioRentar());
            input.setOcupacion(alta.getOcupacion());
            input.setPaisSpcDomicilioSpcCliente(alta.getPaisDomicilioCliente());
            input.setPaisSpcDomicilioSpcaSpcrentar(alta.getPaisDomicilioRentar());
            input.setPrimerSpcApellido(alta.getPrimerApellido());
            input.setRFC(alta.getRfc());
            input.setScoreHocelot(alta.getScoreHocelot());
            input.setSegundoSpcApellido(alta.getSegundoApellido());
            input.setTarjetaSpcCredito(alta.getTarjetaCredito());
            input.setTelefono(alta.getTelefono());
            input.setTipoResidencia(alta.getTipoResidencia());
            input.setTipoSpcIdentificacionSpcCliente(alta.getTipoIdentificacionCliente());
            
            ASERTANowoAltaClienteNowoAltaClienteNowoOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsAltaClienteAserta()
                        .getASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo().asertaNowoAltaClienteNowoAltaClienteNowo(input);
            } else {
                output = this.getWsAltaClienteInsurgentes()
                        .getASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo().asertaNowoAltaClienteNowoAltaClienteNowo(input);
            }
            
            if (output == null || output.getOutExito() == null
                    || output.getOutExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMsgError((output.getOutMsgError() == null)? null:(String) output.getOutMsgError());
            resp.setIdCliente((output.getOutIDCliente() == null)? null:(String) output.getOutIDCliente());
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getOutExito())) {
                resp.setExito(Util.SPS_EXITO);
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error altaCliente:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
