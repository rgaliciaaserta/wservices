/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class EmailMgun {
    
    private Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    private PropSource prop;
    
    public void sendHTMLMailAttachment(String login, String empresa, String emailS, String emailsCC, String emailsBCC, String msg, String asunto, String from, String context, Map<String, String> mapFile,
            String img1) throws MessagingException {
        log.debug("===================================================");
        log.debug("Enviando correo......");
        log.debug("Mensaje " + msg);
        log.debug("Asunto " + asunto);
        log.debug("===================================================");
        
        Session mailSession = getSession(context);
        MimeMessage message = new MimeMessage(mailSession);
        //String asuntoT = Util.getRandomToken(Util.strBT.toString());
        message.setSubject(asunto, "UTF-8");
        message.addHeader("X-Mailgun-Native-Send", "true");
        message.setHeader("X-Mailgun-Native-Send", "true");
        //message.setSubject(asuntoT, "UTF-8");
        message.setSentDate(new Date());
        message.setSender(new InternetAddress(from));
        message.setFrom(new InternetAddress(from));
        /*String nom = Util.getRandomToken(Util.strB.toString()).toLowerCase() + "@aserta.com.mx";
        message.setSender(new InternetAddress(nom));
        message.setFrom(new InternetAddress(nom));*/
        
        if (emailS != null) {
            StringTokenizer st = new StringTokenizer(emailS, ",");
            List<String> email = new ArrayList<String>();
            while (st.hasMoreTokens()) {
                email.add(st.nextToken());
            }

            for (int i = 0; i < email.size(); i++) {
                String mail = email.get(i);
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(mail));
                log.debug("Correo salida:" + mail);

            }
        }
        
        if (emailsCC != null) {
            StringTokenizer stcc = new StringTokenizer(emailsCC, ",");
            List<String> emailCc = new ArrayList<String>();
            while (stcc.hasMoreTokens()) {
                emailCc.add(stcc.nextToken());
            }

            for (int i = 0; i < emailCc.size(); i++) {
                String mail = emailCc.get(i);
                message.addRecipient(Message.RecipientType.CC, new InternetAddress(mail));
                log.debug("Correo salida CC:" + mail);

            }
        }
        
        if (emailsBCC != null) {
            StringTokenizer stbcc = new StringTokenizer(emailsBCC, ",");
            List<String> emailBCc = new ArrayList<String>();
            while (stbcc.hasMoreTokens()) {
                emailBCc.add(stbcc.nextToken());
            }

            for (int i = 0; i < emailBCc.size(); i++) {
                String mail = emailBCc.get(i);
                message.addRecipient(Message.RecipientType.BCC, new InternetAddress(mail));
                log.debug("Correo salida CC:" + mail);

            }
        }
        
        StringBuilder htmlMessage = new StringBuilder();
        htmlMessage.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"");
        htmlMessage.append("<html>");
        htmlMessage.append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head>");
        htmlMessage.append("<body>");
        htmlMessage.append(msg);
        //htmlMessage.append(Util.getTexto());
        htmlMessage.append("</body>");
        
        Multipart mpart = new MimeMultipart();
        BodyPart bodyMensaje = new MimeBodyPart();
        
        bodyMensaje.setContent(htmlMessage.toString(), "text/html; charset=UTF-8");
        mpart.addBodyPart(bodyMensaje);
        
        int idxFile = 0;
        BodyPart body;
        if (mapFile != null && !mapFile.isEmpty()) {
            try {
                Iterator<Map.Entry<String, String>> iterFile = mapFile.entrySet().iterator();
                while(iterFile.hasNext()) {
                    Map.Entry<String, String> entry = iterFile.next();
                    if (entry.getValue() == null || entry.getValue().isEmpty()) {
                        continue;
                    }

                    body = new MimeBodyPart();
                    DataSource ds = new FileDataSource(entry.getValue());
                    body.setDataHandler(new DataHandler(ds));
                    body.setFileName((entry.getKey() == null || entry.getKey().isEmpty())?
                            ((idxFile++) + ""):entry.getKey());
                    mpart.addBodyPart(body);
                }
            } catch (Exception ex) {
                log.debug("Error adjuto:" + ex.getMessage());
            }
        }
        
        if (img1 != null && !img1.isEmpty()) {
            try{
                MimeBodyPart resource = new MimeBodyPart();
                resource.attachFile(img1);
                resource.setContentID("<" + "img1" + ">");
                resource.setDisposition(MimeBodyPart.INLINE);
                mpart.addBodyPart(resource);
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
            }
        }
        
        message.setContent(mpart);
        
        log.debug("Enviando mensaje . . . . . . . . . . ");
        Transport.send(message);
        log.debug("El mensaje ha sido enviado");
    }
    
    private Session getSession(String context) {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        
        properties.setProperty("mail.smtp.host", this.prop.getMalgumHost());
        properties.setProperty("mail.smtp.port", this.prop.getMalgumPort());
        properties.setProperty("mail.smtp.starttls", "true");
        properties.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");
        
        log.debug("MailgunHost:" + this.prop.getMalgumHost());
        log.debug("MailgunPort:" + this.prop.getMalgumPort());
        
        SMTPAuthenticator authenticator = null;
        if (context != null && context.equalsIgnoreCase(Util.SINIESTRO)) {
            log.debug("MailgunUser:" + this.prop.getMailgunSiniestroUser());
            log.debug("MailgunPassword:" + this.prop.getMailgunSiniestroPassword());
            authenticator = new SMTPAuthenticator(this.prop.getMailgunSiniestroUser(), this.prop.getMailgunSiniestroPassword());
        } else {
            log.debug("MailgunUser:" + this.prop.getMalgumUser());
            log.debug("MailgunPassword:" + this.prop.getMalgumPassword());
            authenticator = new SMTPAuthenticator(this.prop.getMalgumUser(), this.prop.getMalgumPassword());
        }
        
        return Session.getInstance(properties, authenticator);
    }
    
    private Address[] listAddress(List<String> mails) throws AddressException {
        Address[] list = new Address[mails.size()];
        int c = 0;
        for (String m : mails) {
            list[c] = new InternetAddress(m);
            c++;
        }
        return list;
    }
    
    public Address[] getEmail(String emails) {
        List<String> emailsList = new ArrayList<String>();
//        String emails="jcrodriguez@aserta.com.mx, ,,,dhernandez@aserta.com.mx, ,jurasec@gmail.com";
        String mail;
        StringTokenizer st = new StringTokenizer(emails, ",");
        
        log.debug("Tokenizando: " + emails);
        while (st.hasMoreTokens()) {
            mail = st.nextToken();
            log.debug("|=> Token mail: " + mail);
            if (!mail.trim().isEmpty()) {
                emailsList.add(mail.trim());
            }
        }
        
        Address[] list = new Address[emailsList.size()];
        log.debug("|=> Resultado: ");
        for (int i = 0; i < emailsList.size(); i++) {
            log.debug("|---> " + emailsList.get(i));
            try {
                list[i] = new InternetAddress(emailsList.get(i));
            } catch (AddressException ex) {
                log.error("      Error en formato de email: "+ex.getMessage()+". "+ex.toString(), ex);
            }
        }
        
        return list;
    }
    
    class SMTPAuthenticator extends Authenticator {
        
        private String userName = null;
        private String password = null;
        
        public SMTPAuthenticator(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(userName, password);
        }
    }
}
