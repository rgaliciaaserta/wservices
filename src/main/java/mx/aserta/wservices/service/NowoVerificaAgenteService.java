/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoVerificaAgenteResponseVO;
import mx.aserta.wservices.vo.NowoVerificaAgenteVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.nowoverificaagente.com.siebel.customui.ASERTANowoVerificaAgenteWSVerificaAgenteInput;
import wsdl.nowoverificaagente.com.siebel.customui.ASERTANowoVerificaAgenteWSVerificaAgenteOutput;
import wsdl.nowoverificaagente.com.siebel.customui.ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoVerificaAgenteService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service wsNowoVerificaAgenteAserta;
    
    private ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service wsNowoVerificaAgenteInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsNowoVerificaAgenteAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoVerificaAgenteAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoVerificaAgenteAserta = new ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service(asertaUrl);
                this.wsNowoVerificaAgenteAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoVerificaAgenteAserta = new ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsNowoVerificaAgenteInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoVerificaAgenteInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoVerificaAgenteInsurgentes = new ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service(insurgentesUrl);
                this.wsNowoVerificaAgenteInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoVerificaAgenteInsurgentes = new ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service();
            }
        }
    }

    public ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service getWsNowoVerificaAgenteAserta() {
        if (this.wsNowoVerificaAgenteAserta == null) {
            this.initAserta();
        }
        return this.wsNowoVerificaAgenteAserta;
    }

    public ASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS_Service getWsNowoVerificaAgenteInsurgentes() {
        if (this.wsNowoVerificaAgenteInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsNowoVerificaAgenteInsurgentes;
    }
    
    /**
     * Consumir el servicio de verifica agente
     * @param empresa
     * @param datos
     * @return 
     */
    public NowoVerificaAgenteResponseVO nowoVerificaAgente(String empresa, NowoVerificaAgenteVO datos) {
        log.debug("\tNowoVerificaAgenteService.nowoVerificaAgente");
        
        NowoVerificaAgenteResponseVO resp = new NowoVerificaAgenteResponseVO();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoVerificaAgenteWSVerificaAgenteInput input = new ASERTANowoVerificaAgenteWSVerificaAgenteInput();
            
            input.setNoAgente(datos.getNoAgente());
            
            ASERTANowoVerificaAgenteWSVerificaAgenteOutput output;
            
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsNowoVerificaAgenteAserta().getASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS()
                        .asertaNowoVerificaAgenteWSVerificaAgente(input);
            } else {
                output = this.getWsNowoVerificaAgenteInsurgentes().getASERTASpcNowoSpcSpcVerificaSpcAgenteSpcWS()
                        .asertaNowoVerificaAgenteWSVerificaAgente(input);
            }
            
            if (output == null || output.getExito()== null
                    || output.getExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getErrorMsg() == null)? null:(String) output.getErrorMsg());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getExito())) {
                resp.setExito(Util.SPS_EXITO);
                
                resp.setResultado(output.getResultado());
                
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error nowoVerificaAgente:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
