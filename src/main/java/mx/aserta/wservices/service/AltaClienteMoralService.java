/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.AltaClienteMoralResponseVO;
import mx.aserta.wservices.vo.AltaClienteMoralVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.altacliente.com.siebel.customui.ASERTANowoAltaClienteNowoAltaClienteMoralNowoInput;
import wsdl.altacliente.com.siebel.customui.ASERTANowoAltaClienteNowoAltaClienteMoralNowoOutput;
import wsdl.altacliente.com.siebel.customui.ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class AltaClienteMoralService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service wsAltaClienteAserta;
    
    private ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service wsAltaClienteInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsAltaClienteAserta == null) {
            try {
                File file = new File(this.prop.getWsAltaClienteAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsAltaClienteAserta = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service(asertaUrl);
                this.wsAltaClienteAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsAltaClienteAserta = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsAltaClienteInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsAltaClienteInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsAltaClienteInsurgentes = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service(insurgentesUrl);
                this.wsAltaClienteInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsAltaClienteInsurgentes = new ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service();
            }
        }
    }

    public ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service getWsAltaClienteAserta() {
        if (this.wsAltaClienteAserta == null) {
            this.initAserta();
        }
        return this.wsAltaClienteAserta;
    }

    public ASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service getWsAltaClienteInsurgentes() {
        if (this.wsAltaClienteInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsAltaClienteInsurgentes;
    }
    
    /**
     * Consumir el servicio de prendas
     * @param empresa
     * @param alta
     * @return 
     */
    public AltaClienteMoralResponseVO altaCliente(String empresa, AltaClienteMoralVO alta) {
        log.debug("\tASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo_Service.altacliente");
        
        AltaClienteMoralResponseVO resp = new AltaClienteMoralResponseVO();
        
        if (alta == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoAltaClienteNowoAltaClienteMoralNowoInput input = new ASERTANowoAltaClienteNowoAltaClienteMoralNowoInput();
            
            input.setBanco(alta.getBanco());
            input.setCLABE(alta.getClabe());
            input.setCalleDomicilioaRentar(alta.getCalleDomicilioaRentar());
            input.setCalleDomiclioCliente(alta.getCalleDomiclioCliente());
            input.setCiudadDomicilioCliente(alta.getCiudadDomicilioCliente());
            input.setCiudadDomicilioaRentar(alta.getCiudadDomicilioaRentar());
            input.setCodigoPostalDomicilioCliente(alta.getCodigoPostalDomicilioCliente());
            input.setCodigoPostalDomicilioaRentar(alta.getCodigoPostalDomicilioaRentar());
            input.setColoniaDomicilioCliente(alta.getColoniaDomicilioCliente());
            input.setColoniaDomicilioClienteaRentar(alta.getColoniaDomicilioClienteaRentar());
            input.setCorreo(alta.getCorreo());
            input.setCuenta(alta.getCuenta());
            input.setDelegacionMunicipioDomicilioCliente(alta.getDelegacionMunicipioDomicilioCliente());
            input.setDelegacionMunicipioDomicilioaRentar(alta.getDelegacionMunicipioDomicilioaRentar());
            input.setDiaLimitePagoRenta(alta.getDiaLimitePagoRenta());
            input.setEstadoDomicilioCliente(alta.getEstadoDomicilioCliente());
            input.setEstadoDomicilioaRentar(alta.getEstadoDomicilioaRentar());
            input.setFechaConstitucion(Util.formatDateSiebel(Util.parseDateString(alta.getFechaConstitucion())));
            input.setFinVigenciaContrato(Util.formatDateSiebel(Util.parseDateString(alta.getFinVigenciaContrato())));
            input.setFinVigenciaGarantia(Util.formatDateSiebel(Util.parseDateString(alta.getFinVigenciaGarantia())));
            input.setInCodigoPropiedad(alta.getCodigoPropiedad());
            input.setInicioVigenciaContrato(Util.formatDateSiebel(Util.parseDateString(alta.getInicioVigenciaContrato())));
            input.setInicioVigenciaGarantia(Util.formatDateSiebel(Util.parseDateString(alta.getInicioVigenciaGarantia())));
            input.setMetrosCuadradosDomicilioaRentar(alta.getMetrosCuadradosDomicilioaRentar());
            input.setMontoRenta(alta.getMontoRenta());
            input.setNumeroExteriorDomicilioCliente(alta.getNumeroExteriorDomicilioCliente());
            input.setNumeroExteriorDomicilioaRentar(alta.getNumeroExteriorDomicilioaRentar());
            input.setNumeroInteriorDomicilioCliente(alta.getNumeroInteriorDomicilioCliente());
            input.setNumeroInteriorDomicilioaRentar(alta.getNumeroInteriorDomicilioaRentar());
            input.setPaisDomicilioCliente(alta.getPaisDomicilioCliente());
            input.setPaisDomicilioaRentar(alta.getPaisDomicilioaRentar());
            input.setPerfil(alta.getPerfil());
            input.setRFC(alta.getRfc());
            input.setRazonSocial(alta.getRazonSocial());
            
            ASERTANowoAltaClienteNowoAltaClienteMoralNowoOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsAltaClienteAserta()
                        .getASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo().asertaNowoAltaClienteNowoAltaClienteMoralNowo(input);
            } else {
                output = this.getWsAltaClienteInsurgentes()
                        .getASERTASpcNowoSpcSpcAltaSpcClienteSpcNowo().asertaNowoAltaClienteNowoAltaClienteMoralNowo(input);
            }
            
            if (output == null || output.getExito() == null
                    || output.getExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMsgError((output.getErrorMsg() == null)? null:(String) output.getErrorMsg());
            resp.setIdCliente((output.getIdCliente() == null)? null:(String) output.getIdCliente());
            resp.setIdInmueble((output.getIdInmueble() == null)? null:(String) output.getIdInmueble());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getExito())) {
                resp.setExito(Util.SPS_EXITO);
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error altaCliente:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
