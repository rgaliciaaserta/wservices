/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoCartaAceptacionRequestVO;
import mx.aserta.wservices.vo.NowoCartaAceptacionResponseVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoCartaAceptacionService {
    
    @Autowired
    private PdfNowoService pdfNowoService;
    
    @Autowired
    private Email email;
    
    @Autowired
    private PropSource prop;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Get Plantilla
     * @param empresa
     * @param plantilla
     * @return 
     */
    public NowoCartaAceptacionResponseVO getPlantilla(String empresa, NowoCartaAceptacionRequestVO plantilla) {
        log.debug("\t\t>>getPlantilla");
        
        NowoCartaAceptacionResponseVO response = new NowoCartaAceptacionResponseVO();
        
        if (plantilla == null) {
            log.debug("\tDebe indicar los parámetros de entrada.");
            response.setCodigo(Util.COD_ERROR);
            response.setMensaje("Debe indicar los parámetros de entrada.");
            return response;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa) && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            log.debug("\tDebe indicar la empresa:" + empresa);
            response.setCodigo(Util.COD_ERROR);
            response.setMensaje("Debe indicar la empresa:[" + Util.ASERTA + " | " + Util.INSURGENTES + "]");
            return response;
        }
        
        String validaMsg = this.validaParametros(plantilla);
        if (validaMsg != null && !validaMsg.isEmpty()) {
            log.debug("\tParametros:" + validaMsg);
            response.setCodigo(Util.COD_ERROR);
            response.setMensaje("Debe indicar los parámetros de entrada:" + validaMsg);
            return response;
        }
        
        log.debug("\tPlantlla:" + mx.aserta.pdfgenerator.util.Util.PLANTILLA_NOWO_CARTA_CONFIRMACION);
        log.debug("\tAsegurado:" + plantilla.getAsegurado());
        log.debug("\tContratante:" + plantilla.getContratante());
        log.debug("\tEmailDestino:" + plantilla.getEmailDestino());
        log.debug("\tFecha:" + plantilla.getFecha());
        log.debug("\tEmpresa:" + empresa);
        
        //log.debug("\temail:" + email);
        //log.debug("\temailcc:" + emailcc);
        //log.debug("\temailAsunto:" + emailAsunto);
        //log.debug("\tfirmelec:" + plantilla.getFirmelec());
        //log.debug("\tIdFirmante:" + plantilla.getIdFirmante());
        
        Map<String, String> mapFile = new HashMap<>();
        try {
            //Parametros para construir la plantilla
            Map<String, Object> map = new HashMap<>();
            
            //Empresa
            map.put(mx.aserta.pdfgenerator.util.Util.EMPRESA, empresa.toUpperCase());
            //Tipo de plantilla
            map.put(mx.aserta.pdfgenerator.util.Util.PLANTILLA, mx.aserta.pdfgenerator.util.Util.PLANTILLA_NOWO_CARTA_CONFIRMACION);
            //Tipo de persona
            //map.put(Util.TIPO_PERSONA, tipoPersona.toUpperCase());
            //Row Id
            map.put(mx.aserta.pdfgenerator.util.Util.ROW_ID, "1");
            
            map.put(mx.aserta.pdfgenerator.util.Util.EMAIL, plantilla.getEmailDestino());
            
            String randomUid = Util.getRandomUID();
            map.put(mx.aserta.pdfgenerator.util.Util.FILE_NAME, randomUid + "_"
                    + ((plantilla.getAsegurado() == null || plantilla.getAsegurado().isEmpty())?
                        "":plantilla.getAsegurado().trim()));
            
            map.put(mx.aserta.wservices.util.Util.NOWO_ASEGURADO, plantilla.getAsegurado());
            map.put(mx.aserta.wservices.util.Util.NOWO_CONTRATANTE, plantilla.getContratante());
            map.put(mx.aserta.wservices.util.Util.NOWO_FECHA, plantilla.getFecha());
            
            //Datos de email
            /*map.put(Util.EMAIL, email);
            map.put(Util.EMAILCC, emailcc);
            map.put(Util.EMAIL_ASUNTO, emailAsunto);*/
            
            //Datos firma electronica
            /*List<String> datPfx = this.getListPfx(plantilla.getIdFirmante());
            map.put(mx.aserta.pdfgenerator.util.Util.FIRMA_ELECTRONICA, datPfx.get(1));//pfx file
            //map.put(Util.FIRMA_ELECTRONICA, "RLAUS - Software - ANF AC Test.pfx");
            map.put(mx.aserta.pdfgenerator.util.Util.PATH_SIGNING, this.prop.getSignPathSigning());
            map.put(mx.aserta.pdfgenerator.util.Util.FILE_NOT_SIGNED, this.prop.getSignFileNotSigned());
            //map.put(Util.SIGN_PWD, this.prop.getSignPwd());
            map.put(mx.aserta.pdfgenerator.util.Util.SIGN_PWD, datPfx.get(3)); //pass
            */
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            map.put(mx.aserta.pdfgenerator.util.Util.BYTE_ARRAY_OUTPUT_STREAM, baos);
            
            map.put(mx.aserta.pdfgenerator.util.Util.CODIGO_RESPONSE, mx.aserta.pdfgenerator.util.Util.COD_OK);
            
            this.pdfNowoService.pfgGenerator(map, null);
            
            if (mx.aserta.pdfgenerator.util.Util.COD_OK.equalsIgnoreCase((String) map.get(mx.aserta.pdfgenerator.util.Util.CODIGO_RESPONSE))) {
                response.setCodigo(Util.COD_EXITO);
                response.setDatos(Util.encodeBase64(baos.toByteArray()));
            } else {
                response.setCodigo(Util.COD_ERROR);
                response.setMensaje((String) map.get(mx.aserta.pdfgenerator.util.Util.MSG_RESPONSE));
            }
            
            if (plantilla.getEmailDestino() != null && !plantilla.getEmailDestino().isEmpty()) {
                String email = plantilla.getEmailDestino().replaceAll(";", ",");
                String filePath = (map.get(mx.aserta.pdfgenerator.util.Util.EMAIL_FILEPATH) == null)? null:(String) map.get(mx.aserta.pdfgenerator.util.Util.EMAIL_FILEPATH);
                if (filePath == null || filePath.isEmpty()) {
                    //res.sendError(Util.HTTP_SERVER_INTERNAL_ERROR, "No existe archivo para enviar por correo.");
                    //res.getOutputStream().print(String.format(this.pdfService.getPlantilla(this.prop.getPlantillaCorreoError()), "No existe archivo para enviar por correo."));
                    //return;
                }
                
                String keyFile = (String) map.get(mx.aserta.pdfgenerator.util.Util.FILE_NAME);
                keyFile = (keyFile.endsWith(".pdf"))? keyFile:keyFile + ".pdf";
                mapFile.put(
                        ((((String) map.get(mx.aserta.pdfgenerator.util.Util.FILE_NAME)).endsWith(".pdf"))?
                        (String) map.get(mx.aserta.pdfgenerator.util.Util.FILE_NAME):(String) map.get(mx.aserta.pdfgenerator.util.Util.FILE_NAME) + ".pdf").replaceAll(randomUid, Util.NOWO_CARTA_ACEPTACION)
                        , filePath);
                log.debug("Archivo adjunto emial:" + keyFile + ":" + filePath);
                
                this.email.sendHTMLMailAttachment(null,
                        empresa.toUpperCase(),
                        email,
                        null,
                        this.prop.getEmailNowoCartaEmailSupport(),
                        this.prop.getEmailNowoCartaBody(),
                        this.prop.getEmailNowoCartaTitle(),
                        mapFile,
                        null/*this.prop.getEmailNowoImg()*/);
                
                response.setCodigo(Util.COD_EXITO);
                response.setMensaje("Correo enviado.");
            }
        } catch (Exception ex) {
            log.error("Error:" + ex.getMessage(), ex);
            try {
                response.setCodigo(Util.COD_ERROR);
                response.setMensaje("Error:" + ex.getMessage());
            } catch (Exception e) {
                log.error("Error:" + ex.getMessage(), ex);
            }
        } finally {
            if (!mapFile.isEmpty()) {
                Iterator<Entry<String, String>> iter = mapFile.entrySet().iterator();
                File fileDelete;
                while(iter.hasNext()) {
                    Entry<String, String> entry = iter.next();
                    
                    try {
                        fileDelete = new File(entry.getValue());
                        if (fileDelete.exists() && fileDelete.isFile()) {
                            fileDelete.delete();
                        }
                    } catch (Exception e) {
                        log.error("Error borrando archivo:" + e.getMessage(), e);
                    }
                }
            }
        }
        
        return response;
    }
    
    /**
     * Parameters validation
     * @param datos
     * @return 
     */
    private String validaParametros(NowoCartaAceptacionRequestVO datos) {
        log.debug("\tNowoCartaAceptacionService.validaParametros");
        if (datos == null) {
            return "Debe indicar los datos de entrada.";
        }
        
        if (datos.getAsegurado() == null || datos.getAsegurado().isEmpty()) {
            return "Debe indicar el asegurado.";
        }
        
        if (datos.getContratante() == null || datos.getContratante().isEmpty()) {
            return "Debe indicar el contratante.";
        }
        
        if (datos.getEmailDestino() == null || datos.getEmailDestino().isEmpty()) {
            return "Debe indicar el email destino.";
        }
        
        if (datos.getFecha() == null || datos.getFecha().isEmpty()
                || Util.isFormatFecha(datos.getFecha(), Util.FORMAT_DDMMYYYY) != null) {
            return "Debe indicar la fecha en formato [dd-mm-yyyy]";
        }
        
        return null;
    }
}
