/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.util.ArrayList;
import java.util.List;
import mx.aserta.wservices.model.AonGruposResponseModel;
import mx.aserta.wservices.sps.GetBlackSmithService;
import mx.aserta.wservices.sps.GetGrupoPeniolesService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.AonGruposResponseVO;
import mx.aserta.wservices.vo.BlackSmithResponseVO;
import mx.aserta.wservices.vo.GrupoPeniolesResponseVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class AonGruposService {
    
    @Autowired
    private GetGrupoPeniolesService getGrupoPeniolesService;
    
    @Autowired
    private GetBlackSmithService getBlackSmithService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Grupo Penioles
     * @return 
     */
    public AonGruposResponseVO grupoPenioles() {
        log.debug("\tAonGrupoPenioles.grupoPenioles");
        
        AonGruposResponseVO respVo = new AonGruposResponseVO();
        RespuestaJson resp = new RespuestaJson();
        
        try {
            
            resp = this.getGrupoPeniolesService.execute();
            
            if (Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                respVo.setExito(Util.SPS_EXITO);
                respVo.setDatos(
                        (resp.getDatos() == null)? null:(List<AonGruposResponseModel>) resp.getDatos()
                );
            } else {
                respVo.setExito(Util.SPS_ERROR);
            }
            
            log.debug("\tAonGrupoService.Exito:" + respVo.getExito());
            log.debug("\tAonGrupoService.Mensaje:" + respVo.getMensaje());
        } catch (Exception ex) {
            log.error("\tError al invocar el servicio AonGrupoService.getGrupoPenioles:" + ex.getMessage(), ex);
            resp.setCodigo(Util.SPS_ERROR);
            resp.setMensaje("Error al invocar el servicio de grupo peñoles");
        }
        
        return respVo;
    }
    
    /**
     * Black Smith
     * @return 
     */
    public AonGruposResponseVO blackSmith() {
        log.debug("\tAonGrupoPenioles.blackSmith");
        
        AonGruposResponseVO respVo = new AonGruposResponseVO();
        RespuestaJson resp = new RespuestaJson();
        
        try {
            
            resp = this.getBlackSmithService.execute();
            
            if (Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                respVo.setExito(Util.SPS_EXITO);
                respVo.setDatos(
                        (resp.getDatos() == null)? null:(List<AonGruposResponseModel>) resp.getDatos()
                );
            } else {
                respVo.setExito(Util.SPS_ERROR);
            }
            
            log.debug("\tAonGrupoService.Exito:" + respVo.getExito());
            log.debug("\tAonGrupoService.Mensaje:" + respVo.getMensaje());
        } catch (Exception ex) {
            log.error("\tError al invocar el servicio AonGrupoService.getBlackSmith:" + ex.getMessage(), ex);
            resp.setCodigo(Util.SPS_ERROR);
            resp.setMensaje("Error al invocar el servicio de Black Smith");
        }
        
        return respVo;
    }
    
    /**
     * AonGrupos
     * @return 
     */
    public AonGruposResponseVO aonGrupos() {
        log.debug("\tAonGruposService.aonGrupos");
        
        AonGruposResponseVO resp = new AonGruposResponseVO();
        AonGruposResponseVO respbs = new AonGruposResponseVO();
        
        try {
            resp = this.grupoPenioles();
            
            if (!Util.SPS_EXITO.equalsIgnoreCase(resp.getExito())
                    || resp.getDatos() == null) {
                resp.setDatos(new ArrayList<>());
            }
            
            respbs = this.blackSmith();
            
            if (!Util.SPS_EXITO.equalsIgnoreCase(respbs.getExito())
                    || respbs.getDatos() == null) {
                respbs.setDatos(new ArrayList<>());
            }
            
            resp.setMensaje(
                    ((resp.getMensaje() == null || resp.getMensaje().isEmpty())?
                            "":resp.getMensaje() + ":")
                    + ((respbs.getMensaje() == null || respbs.getMensaje().isEmpty())?
                            "":respbs.getMensaje())
            );
            
            if (!(Util.SPS_EXITO.equalsIgnoreCase(resp.getExito())
                    || Util.SPS_EXITO.equalsIgnoreCase(respbs.getExito()))) {
                return resp;
            }
            
            resp.getDatos().addAll(respbs.getDatos());
            
        } catch (Exception ex) {
            log.error("\tError AonGruposService.aonGrupos:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error al invocar el servicio de AON Grupos");
        }
        
        return resp;
    }
}
