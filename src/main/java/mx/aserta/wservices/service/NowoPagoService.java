/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoPagoInsertaResponse;
import mx.aserta.wservices.vo.NowoPagoInsertaVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.nowopago.com.siebel.customui.ASERTANowoPagoBSInsertaPagoInput;
import wsdl.nowopago.com.siebel.customui.ASERTANowoPagoBSInsertaPagoOutput;
import wsdl.nowopago.com.siebel.customui.ASERTASpcNowoSpcPagoSpcBS_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoPagoService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcPagoSpcBS_Service wsNowoPagoAserta;
    
    private ASERTASpcNowoSpcPagoSpcBS_Service wsNowoPagoInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsNowoPagoAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoPagoBsAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoPagoAserta = new ASERTASpcNowoSpcPagoSpcBS_Service(asertaUrl);
                this.wsNowoPagoAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoPagoAserta = new ASERTASpcNowoSpcPagoSpcBS_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsNowoPagoInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoPagoBsInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoPagoInsurgentes = new ASERTASpcNowoSpcPagoSpcBS_Service(insurgentesUrl);
                this.wsNowoPagoInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoPagoInsurgentes = new ASERTASpcNowoSpcPagoSpcBS_Service();
            }
        }
    }

    public ASERTASpcNowoSpcPagoSpcBS_Service getWsAltaClienteAserta() {
        if (this.wsNowoPagoAserta == null) {
            this.initAserta();
        }
        return this.wsNowoPagoAserta;
    }

    public ASERTASpcNowoSpcPagoSpcBS_Service getWsAltaClienteInsurgentes() {
        if (this.wsNowoPagoInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsNowoPagoInsurgentes;
    }
    
    /**
     * Consumir el servicio de prendas
     * @param empresa
     * @param datos
     * @return 
     */
    public NowoPagoInsertaResponse nowoPagoInserta(String empresa, NowoPagoInsertaVO datos) {
        log.debug("\tNowoPagoService.nowoPagoInserta");
        
        NowoPagoInsertaResponse resp = new NowoPagoInsertaResponse();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoPagoBSInsertaPagoInput input = new ASERTANowoPagoBSInsertaPagoInput();
            
            input.setFecha(datos.getFecha());
            input.setHora(datos.getHora());
            input.setIdentificadorMoneda(datos.getIdMoneda());
            input.setMensaje(datos.getMensaje());
            input.setMonto(datos.getMonto() + "");
            input.setNumAutorizacion(datos.getNumeroAutorizacion());
            input.setReferenciaPago(datos.getReferenciaPago());
            
            ASERTANowoPagoBSInsertaPagoOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsAltaClienteAserta()
                        .getASERTASpcNowoSpcPagoSpcBS().asertaNowoPagoBSInsertaPago(input);
            } else {
                output = this.getWsAltaClienteInsurgentes()
                        .getASERTASpcNowoSpcPagoSpcBS().asertaNowoPagoBSInsertaPago(input);
            }
            
            if (output == null || output.getExito() == null
                    || output.getExito().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getMensajeSalida() == null)? null:(String) output.getMensajeSalida());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getExito())) {
                resp.setExito(Util.SPS_EXITO);
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error nogoPagoBS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
