/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoAnulacionResponseVO;
import mx.aserta.wservices.vo.NowoAnulacionVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.nowoanulacion.com.siebel.customui.ASERTANowoAnulacionBSWSGeneraAnulacionInput;
import wsdl.nowoanulacion.com.siebel.customui.ASERTANowoAnulacionBSWSGeneraAnulacionOutput;
import wsdl.nowoanulacion.com.siebel.customui.ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoAnulacionService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service wsNowoAnulacionAserta;
    
    private ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service wsNowoAnulacionInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsNowoAnulacionAserta == null) {
            try {
                File file = new File(this.prop.getWsNowoAnulacionAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoAnulacionAserta = new ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(asertaUrl);
                this.wsNowoAnulacionAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoAnulacionAserta = new ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsNowoAnulacionInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsNowoAnulacionInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsNowoAnulacionInsurgentes = new ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service(insurgentesUrl);
                this.wsNowoAnulacionInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsNowoAnulacionInsurgentes = new ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service();
            }
        }
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service getWsNowoAnulacionAserta() {
        if (this.wsNowoAnulacionAserta == null) {
            this.initAserta();
        }
        return this.wsNowoAnulacionAserta;
    }

    public ASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS_Service getWsNowoAnulacionInsurgentes() {
        if (this.wsNowoAnulacionInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsNowoAnulacionInsurgentes;
    }
    
    /**
     * Consumir el servicio de anulacion
     * @param empresa
     * @param datos
     * @return 
     */
    public NowoAnulacionResponseVO nowoAnulacion(String empresa, NowoAnulacionVO datos) {
        log.debug("\tNowoAnulacionService.nowoAnulacion");
        
        NowoAnulacionResponseVO resp = new NowoAnulacionResponseVO();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoAnulacionBSWSGeneraAnulacionInput input = new ASERTANowoAnulacionBSWSGeneraAnulacionInput();
            
            input.setInPolizaId(datos.getPolizaId());
            input.setClavefiscal(datos.getClaveFiscal());
            
            ASERTANowoAnulacionBSWSGeneraAnulacionOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsNowoAnulacionAserta().getASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS()
                        .asertaNowoAnulacionBSWSGeneraAnulacion(input);
            } else {
                output = this.getWsNowoAnulacionInsurgentes().getASERTASpcNowoSpcSpcAnulacionSpcBSSpcWS()
                        .asertaNowoAnulacionBSWSGeneraAnulacion(input);
            }
            
            if (output == null || output.getOutSalida() == null
                    || output.getOutSalida().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getOutMensaje() == null)? null:(String) output.getOutMensaje());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getOutSalida())) {
                resp.setExito(Util.SPS_EXITO);
                
                resp.setIdNotaCredito(output.getOutIdNotaCredito());
                
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error nowoAnulacion:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
