/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import mx.aserta.wservices.sps.ArrendaCotizaAsertaService;
import mx.aserta.wservices.sps.ArrendaCotizaInsurgentesService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ArrendamientoResponse;
import mx.aserta.wservices.vo.ArrendamientoVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class ArrendamientoService {
    
    @Autowired
    private ArrendaCotizaAsertaService arrendaCotizaAsertaService;
    
    @Autowired
    private ArrendaCotizaInsurgentesService arrendaCotizaInsurgentesService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Cotizacion de arrendamiento
     * @param arrenda
     * @return 
     */
    public ArrendamientoResponse cotizaArrendamiento(ArrendamientoVO arrenda) {
        this.log.debug("\t::::::: ArrendamientoService.cotizaArrendamiento :::::::::");
        
        ArrendamientoResponse resp = new ArrendamientoResponse();
        
        if (arrenda == null) {
            resp.setFlgExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar los parametros de entrada.");
            return resp;
        }
        
        /*if (arrenda.getEmpresa() == null || arrenda.getEmpresa().isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(arrenda.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(arrenda.getEmpresa()))) {
            resp.setFlgExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa [" + Util.ASERTA + " | " + Util.INSURGENTES + "]");
            return resp;
        }*/
        
        try {
            /*if (Util.ASERTA.equalsIgnoreCase(arrenda.getEmpresa())) {
                return this.arrendaCotizaAsertaService.execute(arrenda);
            } else {*/
                return this.arrendaCotizaInsurgentesService.execute(arrenda);
            //}
        } catch (Exception ex) {
            this.log.error("Error:ArrendamientoService.cotizaArrendamiento:" + ex.getMessage(), ex);
            resp.setFlgExito(Util.SPS_ERROR);
            resp.setMensaje("Error al consumir el servicio ArrendamientoService.cotizaArrendamiento:" + ex.getMessage());
        }
        
        return resp;
    }
}
