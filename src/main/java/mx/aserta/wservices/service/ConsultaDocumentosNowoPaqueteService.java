/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.util.Base64;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ConsultaDocumentosNowoPaqueteResponseVO;
import mx.aserta.wservices.vo.ConsultaDocumentosNowoPaqueteVO;
import mx.aserta.wservices.vo.ConsultaDocumentosResponseVO;
import mx.aserta.wservices.vo.ConsultaDocumentosVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class ConsultaDocumentosNowoPaqueteService {
    
    @Autowired
    private ConsultaDocumentoService consultaDocumentoService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Consulta documentos Nowo
     * @param datos
     * @return 
     */
    public ConsultaDocumentosNowoPaqueteResponseVO nowoConsultaDocumentos(ConsultaDocumentosNowoPaqueteVO datos) {
        log.debug("\tConsultaDocumentosNowoPaqueteService.nowoConsultaDocumentos");
        
        ConsultaDocumentosNowoPaqueteResponseVO resp = new ConsultaDocumentosNowoPaqueteResponseVO();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        if ((datos.getIdTransaccion() == null || datos.getIdTransaccion().isEmpty())
                && (datos.getIdPoliza() == null || datos.getIdPoliza().isEmpty())) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el idTransaccion o el idPoliza.");
            return resp;
        }
        
        ConsultaDocumentosResponseVO consultaResp = new ConsultaDocumentosResponseVO();
        ConsultaDocumentosVO consulta = new ConsultaDocumentosVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(consulta));
            
            consulta.setIdPoliza(datos.getIdPoliza());
            consulta.setTipoPaquete(datos.getTipoPaquete());
            
            /*if (base.getEmpresa() == null || base.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(base.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(base.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            consultaResp = this.consultaDocumentoService.consultaPaquete(Util.INSURGENTES, consulta);
            resp.setMensaje(consultaResp.getMensaje());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(consultaResp.getExito())) {
                resp.setCodigo(Util.COD_EXITO);
                resp.setData(Base64.getEncoder().encodeToString(consultaResp.getData()));
                resp.setHashcode(Util.hashCodeGen(consultaResp.getData()));
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
        } catch (Exception ex) {
            log.error("Error:ConsultaDocumentosNowoPaqueteService.nowoConsultaDocumentos:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al generar el paquete:" + ex.getMessage());
        }
        
        return resp;
        
    }
}
