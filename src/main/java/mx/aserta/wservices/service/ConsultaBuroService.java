/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.sps.GetBCCredentialService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.ConsultaBC;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.Direccion;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.EncabezadoBC;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.ErrorRespBC;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.PersonaRespBC;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.PersonasRespBC;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.RespuestaBC;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.UR;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.WSConsultaService;

/**
 *
 * @author rgalicia
 */
@Service
public class ConsultaBuroService {
    
    @Autowired
    private WSConsultaService wSConsultaService;
    
    @Autowired
    private GetBCCredentialService getBCCredentialService;
    
    @Autowired
    private PropSource prop;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Consumir el servicio de consulta buro
     * @param consulta
     * @return 
     */
    public RespuestaBC consultaBuroXml(ConsultaBC consulta) {
        log.debug("\tConsultaBuroService.consultaBuro");
        
        RespuestaBC resp = new RespuestaBC();
        
        if (consulta == null) {
            resp = this.buildErrorResponse(resp, "Debe indicar el objeto de entrada.");
            return resp;
        }
        
        try {
            return this.deleteEncabezado(this.wSConsultaService.getWSConsultaPort().consultaXML(this.buildEncabezado(consulta)));
        } catch (Exception ex) {
            log.error("Error consultaBuro:" + ex.getMessage(), ex);
            resp = this.buildErrorResponse(resp, "Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
    
    /**
     * Get credentials
     * @return 
     */
    private List<String> getCredentials(){
        log.debug("\tgetCredentials");
        
        List<String> lista = new ArrayList<>();
        RespuestaJson resp = this.getBCCredentialService.execute();
        
        if (Util.SPS_EXITO.equalsIgnoreCase(resp.getCodigo())
                && resp.getDatos() != null && ((List<Map<String, String>>) resp.getDatos()).size() > 0) {
            lista.add(((List<Map<String, String>>) resp.getDatos()).get(0).get(Util.DATO + "1"));
            lista.add(((List<Map<String, String>>) resp.getDatos()).get(0).get(Util.DATO + "2"));
        }
        
        return lista;
    }
    
    /**
     * Build Encabezado
     * @param consulta
     * @return 
     */
    private ConsultaBC buildEncabezado(ConsultaBC consulta) {
        log.debug("\tBuild Encabezado:::");
        if (consulta == null || consulta.getPersonas() == null
                || consulta.getPersonas().getPersona() == null) {
            log.debug("\tbuildEncabezado:null");
            return consulta;
        }
        
        List<String> cred = this.getCredentials();
        
        EncabezadoBC enc = new EncabezadoBC();
        enc.setClavePais(this.prop.getPropBuroClavePais());
        enc.setClaveUnidadMonetaria(this.prop.getPropBuroClaveUnidadMonetaria());
        enc.setIdentificadorBuro(this.prop.getPropBuroIdentificadorBuro());
        enc.setIdioma(this.prop.getPropBuroIdioma());
        enc.setImporteContrato(this.prop.getPropBuroImporteContrato());
        enc.setNumeroReferenciaOperador(this.prop.getPropBuroNumeroReferenciaOperador());
        
        if (cred != null && cred.size() > 1) {
            enc.setClaveUsuario(cred.get(0));
            enc.setPassword(cred.get(1));
        } else {
            enc.setClaveUsuario(this.prop.getPropBuroClaveUsuario());
            enc.setPassword(this.prop.getPropBuroPassword());
        }
        
        this.log.debug("Credenciales de Buro de credito:" + enc.getClaveUsuario() + ":" + enc.getPassword());
        
        enc.setProductoRequerido(this.prop.getPropBuroProductoRequerido());
        enc.setTipoConsulta(this.prop.getPropBuroTipoConsulta());
        enc.setTipoContrato(this.prop.getPropBuroTipoContrato());
        enc.setTipoSalida(this.prop.getPropBuroTipoSalida());
        enc.setVersion(this.prop.getPropBuroVersion());
        
        consulta.getPersonas().getPersona().setEncabezado(enc);
        log.debug("\tSet encabezado:" + consulta.getPersonas().getPersona().getEncabezado().getClaveUsuario());
        log.debug("\tSet encabezado:" + consulta.getPersonas().getPersona().getEncabezado().getPassword());
        
        return this.limpiaAcentos(consulta);
    }
    
    /**
     * Limpiar acentos
     * @param consulta
     * @return 
     */
    private ConsultaBC limpiaAcentos(ConsultaBC consulta) {
        if (consulta == null) {
            return consulta;
        }
        
        if (consulta.getPersonas() == null || consulta.getPersonas().getPersona() == null) {
            return consulta;
        }
        
        if (consulta.getPersonas().getPersona().getNombre() != null) {
            consulta.getPersonas().getPersona().getNombre().setPrimerNombre(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getPrimerNombre())
            );
            consulta.getPersonas().getPersona().getNombre().setSegundoNombre(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getSegundoNombre())
            );
            consulta.getPersonas().getPersona().getNombre().setApellidoPaterno(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getApellidoPaterno())
            );
            consulta.getPersonas().getPersona().getNombre().setApellidoMaterno(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getApellidoMaterno())
            );
            consulta.getPersonas().getPersona().getNombre().setApellidoAdicional(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getApellidoAdicional())
            );
            consulta.getPersonas().getPersona().getNombre().setEstadoCivil(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getEstadoCivil())
            );
            consulta.getPersonas().getPersona().getNombre().setNacionalidad(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getNacionalidad())
            );
            consulta.getPersonas().getPersona().getNombre().setRFC(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getRFC())
            );
            consulta.getPersonas().getPersona().getNombre().setResidencia(
                    Util.limpiarAcentos(consulta.getPersonas().getPersona().getNombre().getResidencia())
            );
        }
        
        if (consulta.getPersonas().getPersona().getDomicilios() != null
                && consulta.getPersonas().getPersona().getDomicilios().getDomicilio() != null) {
            
            List<Direccion> listDireccion = new ArrayList<>();
            Direccion direcObj;
            for (Direccion direc : consulta.getPersonas().getPersona().getDomicilios().getDomicilio()) {
                if (direc == null) {
                    continue;
                }
                
                direcObj = new Direccion();
                direcObj.setCP(Util.limpiarAcentos(direc.getCP()));
                direcObj.setCiudad(Util.limpiarAcentos(direc.getCiudad()));
                direcObj.setCodPais(Util.limpiarAcentos(direc.getCodPais()));
                direcObj.setColoniaPoblacion(Util.limpiarAcentos(direc.getColoniaPoblacion()));
                direcObj.setDelegacionMunicipio(Util.limpiarAcentos(direc.getDelegacionMunicipio()));
                direcObj.setDireccion1(Util.limpiarAcentos(direc.getDireccion1()));
                direcObj.setDireccion2(Util.limpiarAcentos(direc.getDireccion2()));
                direcObj.setEstado(Util.limpiarAcentos(direc.getEstado()));
                direcObj.setExtension(Util.limpiarAcentos(direc.getExtension()));
                direcObj.setFax(Util.limpiarAcentos(direc.getFax()));
                direcObj.setFechaResidencia(direc.getFechaResidencia());
                direcObj.setIndicadorEspecialDomicilio(Util.limpiarAcentos(direc.getIndicadorEspecialDomicilio()));
                direcObj.setNumeroTelefono(Util.limpiarAcentos(direc.getNumeroTelefono()));
                direcObj.setTipoDomicilio(Util.limpiarAcentos(direc.getTipoDomicilio()));
                
                listDireccion.add(direcObj);
            }
            
            consulta.getPersonas().getPersona().getDomicilios().getDomicilio().clear();
            consulta.getPersonas().getPersona().getDomicilios().getDomicilio().addAll(listDireccion);
        }
        
        return consulta;
    }
    
    /**
     * Delete Encabezado
     * @param resp 
     */
    private RespuestaBC deleteEncabezado(RespuestaBC resp) {
        log.debug("\tDelete Encabezado::");
        if (resp == null || resp.getPersonas() == null
                || resp.getPersonas().getPersona() == null
                || resp.getPersonas().getPersona().isEmpty()) {
            log.debug("\tdeleteEncabezado:null");
            return resp;
        }
        
        for (PersonaRespBC prbc : resp.getPersonas().getPersona()) {
            if (prbc == null) {
                continue;
            }
            
            prbc.setEncabezado(null);
            log.debug("\tdelete Encabezado:" + prbc);
        }
        log.debug(resp.getPersonas().getPersona().get(0).getEncabezado());
        return resp;
    }
    
    /**
     * Build Error Response
     * @param resp
     * @param msgError
     * @return 
     */
    private RespuestaBC buildErrorResponse(RespuestaBC resp, String msgError) {
        log.debug("\tBuildErrorResponse::");
        if (resp == null) {
            resp = new RespuestaBC();
        }
        
        if (resp.getPersonas() == null) {
            resp.setPersonas(new PersonasRespBC());
        }
        
        PersonaRespBC prbc = new PersonaRespBC();
        ErrorRespBC err = new ErrorRespBC();
        UR ur = new UR();
        ur.setErrorSistemaBuroCredito(msgError);
        ur.setFaltaCampoRequerido(msgError);
        
        err.setUR(ur);
        prbc.setError(err);
        resp.getPersonas().getPersona().add(prbc);
        
        return resp;
    }
}
