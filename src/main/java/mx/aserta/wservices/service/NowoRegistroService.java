/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.sps.RegDocumentoService;
import mx.aserta.wservices.sps.RegPersonaService;
import mx.aserta.wservices.thread.EmailThread;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoDocumentoVO;
import mx.aserta.wservices.vo.NowoPersonaFisicaVO;
import mx.aserta.wservices.vo.NowoPersonaMoralVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoRegistroService {
    
    @Autowired
    private RegPersonaService regPersonaService;
    
    @Autowired
    private RegDocumentoService regDocumentoService;
    
    @Autowired
    private EmailMgun emailMgun;
    
    @Autowired
    private PropSource prop;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Registro persona fisica
     * @param datos
     * @return 
     */
    public RespuestaJson nowoRegistroPf(NowoPersonaFisicaVO datos) {
        log.debug("\tNowoRegistroService.nowoRegistroPf");
        
        RespuestaJson resp = new RespuestaJson();
        
        String validacion = this.validaParamsPf(datos);
        if (validacion != null && !validacion.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje(validacion);
            return resp;
        }
        
        try {
            //Registro en BD
            resp = this.regPersonaService.execute(datos);
            
            if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                log.debug("\tError al registrar la persona:" + resp.getMensaje());
                resp.setMensaje("No fue posible realizar el registro de la persona. Contacte al administrador del sistema.");
                return resp;
            }
            
            //Almacen de documentos
            List<String> listFiles = this.savingDocuments(datos.getDocumentos(), datos.getRfc());
            //Armar mapa para email
            Map<String, String> mapFiles = this.buildAttachmentFile(datos.getDocumentos(), listFiles);
            
            if (mapFiles.containsKey(Util.COD_ERROR)) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(mapFiles.get(Util.COD_ERROR));
                return resp;
            }
            
            StringBuilder strDoc = new StringBuilder();
            BigDecimal idPersona = (BigDecimal) resp.getDatos();
            for (NowoDocumentoVO docNowo : datos.getDocumentos()) {
                resp = this.regDocumentoService.execute(idPersona, docNowo);
                
                if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                    strDoc.append("El siguiente documento no se guardó correctamente:[");
                    strDoc.append(docNowo.getTipo());
                    strDoc.append("] (");
                    strDoc.append(docNowo.getNombre());
                    strDoc.append(".");
                    strDoc.append(docNowo.getFormato());
                    strDoc.append(").");
                }
            }
            
            if (strDoc.length() > 0) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(strDoc.toString());
                return resp;
            }
            
            try{
                /*this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoTo(), null, this.prop.getEmailSupportNowo(),
                        this.replaceValuesPf(this.prop.getNowoPlantillaEmailPf(), datos),
                        this.prop.getEmailNowoSubject(), mapFiles, this.prop.getEmailNowoImg());
                */
                this.emailMgun.getClass();
                
                List<String> listImages = new ArrayList<>();
                listImages.add(this.prop.getEmailNowoImg());
                
                EmailThread emailThread = new EmailThread(
                        this.prop.getEmailNowoTo(),
                        null,
                        this.prop.getEmailSupportNowo(),
                        this.prop.getEmailNowoSubject(),
                        this.replaceValuesPf(this.prop.getNowoPlantillaEmailPf(), datos),
                        this.prop.getMalgumFrom(),
                        null,
                        mapFiles,
                        listImages,
                        this.emailMgun
                );
                
                emailThread.start();
                
            } catch (Exception ex) {
                log.error("\t\t*********ERROR al enviar el correo con los archivos adjuntos.***********:" + ex.getMessage(),
                ex);
                this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoTo(), null, this.prop.getEmailSupportNowo(),
                        "Error al enviar el correo por Mailgun (Persona fisica):<br/>"
                                + "CURP:" + datos.getCurp()
                                + "<br/>RFC:" + datos.getRfc()
                                + "<br/>Error:"
                                + ex.getMessage(),
                        "Error:" + this.prop.getEmailNowoSubject(),
                        this.prop.getMalgumFrom(),
                        null,
                        null, this.prop.getEmailNowoImg());
            }
            
            resp.setCodigo(Util.COD_EXITO);
            resp.setMensaje("Registro guardado.");
            
        } catch (Exception ex) {
            log.error("\tError NowoRegistroService.nowoRegistroPf:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("No fue posible realizar el registro de la persona. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Valida parametros de persona fisica
     * @param datos
     * @return 
     */
    private String validaParamsPf(NowoPersonaFisicaVO datos) {
        log.debug("\t\tValidando parametros de persona fisica.");
        
        StringBuilder str = new StringBuilder();
        if (datos == null) {
            str.append("Debe indicar los datos de la persona.");
            return str.toString();
        }
        
        if (datos.getNombre() == null || datos.getNombre().isEmpty()) {
            str.append("Debe indicar el nombre de la persona.");
        }
        
        /*if (datos.getApellidoPaterno() == null || datos.getApellidoPaterno().isEmpty()) {
            str.append("Debe indicar el apellido paterno de la persona.");
        }*/
        
        /*if (datos.getApellidoMaterno() == null || datos.getApellidoMaterno().isEmpty()) {
            str.append("Debe indicar el apellido materno de la persona.");
        }*/
        
        if (datos.getRfc( )== null || datos.getRfc().isEmpty()) {
            str.append("Debe indicar el RFC de la persona.");
        }
        
        return str.toString();
    }
    
    /**
     * Registro persona moral
     * @param datos
     * @return 
     */
    public RespuestaJson nowoRegistroPm(NowoPersonaMoralVO datos) {
        log.debug("\tNowoRegistroService.nowoRegistroPm");
        
        RespuestaJson resp = new RespuestaJson();
        
        String validacion = this.validaParamsPm(datos);
        if (validacion != null && !validacion.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje(validacion);
            return resp;
        }
        
        try {
            //Registro en BD
            resp = this.regPersonaService.execute(datos);
            
            if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                log.debug("\tError al registrar la persona:" + resp.getMensaje());
                resp.setMensaje("No fue posible realizar el registro de la persona. Contacte al administrador del sistema.");
                return resp;
            }
            
            //Almacen de documentos
            List<String> listFiles = this.savingDocuments(datos.getDocumentos(), datos.getRfc());
            //Armar mapa para email
            Map<String, String> mapFiles = this.buildAttachmentFile(datos.getDocumentos(), listFiles);
            
            if (mapFiles.containsKey(Util.COD_ERROR)) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(mapFiles.get(Util.COD_ERROR));
                return resp;
            }
            
            StringBuilder strDoc = new StringBuilder();
            BigDecimal idPersona = (BigDecimal) resp.getDatos();
            for (NowoDocumentoVO docNowo : datos.getDocumentos()) {
                resp = this.regDocumentoService.execute(idPersona, docNowo);
                
                if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                    strDoc.append("El siguiente documento no se guardó correctamente:[");
                    strDoc.append(docNowo.getTipo());
                    strDoc.append("] (");
                    strDoc.append(docNowo.getNombre());
                    strDoc.append(".");
                    strDoc.append(docNowo.getFormato());
                    strDoc.append(").");
                }
            }
            
            if (strDoc.length() > 0) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(strDoc.toString());
                return resp;
            }
            
            try{
                /*this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoTo(), null, this.prop.getEmailSupportNowo(),
                        this.replaceValuesPm(this.prop.getNowoPlantillaEmailPm(), datos),
                        this.prop.getEmailNowoSubject(), mapFiles, this.prop.getEmailNowoImg());*/
                
                this.emailMgun.getClass();
                
                List<String> listImages = new ArrayList<>();
                listImages.add(this.prop.getEmailNowoImg());
                
                EmailThread emailThread = new EmailThread(
                        this.prop.getEmailNowoTo(),
                        null,
                        this.prop.getEmailSupportNowo(),
                        this.prop.getEmailNowoSubject(),
                        this.replaceValuesPm(this.prop.getNowoPlantillaEmailPm(), datos),
                        this.prop.getMalgumFrom(),
                        null,
                        mapFiles,
                        listImages,
                        this.emailMgun
                );
                
                emailThread.start();
                
            } catch (Exception ex) {
                log.error("\t\t*********ERROR al enviar el correo con los archivos adjuntos.***********:" + ex.getMessage(), ex);
                this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoTo(), null, this.prop.getEmailSupportNowo(),
                        "Error al enviar el correo por Mailgun (Persona moral):<br/>"
                                + "Denominacion social:" + datos.getDenominacionSocial()
                                + "<br/>RFC:" + datos.getRfc()
                                + "<br/>Error:"
                                + ex.getMessage(),
                        "Error:" + this.prop.getEmailNowoSubject(),
                        this.prop.getMalgumFrom(),
                        null,
                        null, this.prop.getEmailNowoImg());
            }
            
            resp.setCodigo(Util.COD_EXITO);
            resp.setMensaje("Registro guardado.");
            
        } catch (Exception ex) {
            log.error("\tError NowoRegistroService.nowoRegistroPf:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("No fue posible realizar el registro de la persona. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Valida parametros de persona moral
     * @param datos
     * @return 
     */
    private String validaParamsPm(NowoPersonaMoralVO datos) {
        log.debug("\t\tValidando parametros de persona moral.");
        
        StringBuilder str = new StringBuilder();
        if (datos == null) {
            str.append("Debe indicar los datos de la persona.");
            return str.toString();
        }
        
        if (datos.getDenominacionSocial() == null || datos.getDenominacionSocial().isEmpty()) {
            str.append("Debe indicar la razón social de la persona.");
        }
        
        if (datos.getRfc() == null || datos.getRfc().isEmpty()) {
            str.append("Debe indicar el rfc de la persona.");
        }
        
        if (datos.getRepresentanteLegal() == null
                || datos.getRepresentanteLegal().getNombre() == null || datos.getRepresentanteLegal().getNombre().isEmpty()
                /*|| datos.getRepresentanteLegal().getApellidoPaterno() == null || datos.getRepresentanteLegal().getApellidoPaterno().isEmpty()
                || datos.getRepresentanteLegal().getApellidoMaterno() == null || datos.getRepresentanteLegal().getApellidoMaterno().isEmpty()*/) {
            str.append("Debe indicar nombre y apellidos del representante legal.");
        }
        
        return str.toString();
    }
    
    /**
     * Saving documentos
     * @param documentos
     * @param folder
     * @return 
     */
    private List<String> savingDocuments(List<NowoDocumentoVO> documentos, String folder) {
        log.debug("\t\tNowoRegistroService.savingDocuments");
        if (documentos == null || documentos.isEmpty()) {
            log.debug("\tNo hay documentos adjuntos.");
            return null;
        }
        
        List<String> result = new ArrayList<>();
        String msg;
        String body = "";
        for (NowoDocumentoVO doc : documentos) {
            if (doc == null) {
                result.add(null);
                continue;
            }
            
            msg = new String();
            body = "{tipo:" + doc.getTipo() +", formato:" + doc.getFormato()
                    + ", nombre:" + doc.getNombre() + ", documento:"
                    + ((doc.getDocumento() == null || doc.getDocumento().isEmpty())? "null":
                        ((doc.getDocumento().length() > 50)? doc.getDocumento().substring(0, 50): doc.getDocumento()))
                    + "}";
            
            if (doc.getTipo() == null || doc.getTipo().isEmpty()) {
                msg += "Debe indicar el tipo del documento:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            if (doc.getFormato() == null
                    || doc.getFormato().isEmpty()
                    || (!Util.FORMATO_DOC.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_DOCX.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_JPEG.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_JPG.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_PDF.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_PNG.equalsIgnoreCase(doc.getFormato())
                    )) {
                msg += "Debe indicar un formato válido del documento [ ";
                msg += Util.FORMATO_DOC;
                msg += " | ";
                msg += Util.FORMATO_DOCX;
                msg += " | ";
                msg += Util.FORMATO_JPEG;
                msg += " | ";
                msg += Util.FORMATO_JPG;
                msg += " | ";
                msg += Util.FORMATO_PDF;
                msg += " | ";
                msg += Util.FORMATO_PNG;
                msg += " ]:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            if (doc.getNombre() == null || doc.getNombre().isEmpty()) {
                msg += "Debe indicar el nombre del documento:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            if (doc.getDocumento() == null || doc.getDocumento().isEmpty()) {
                msg += "Debe indicar el documento en base 64:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            //Crear carpeta para documentos
            String nameFolder = Util.createFolder(this.prop.getNowoAfiliacionPath(), folder);
            
            result.add(Util.PATH_TMP + Util.saveFile(doc.getNombre(), doc.getFormato(),
                    this.prop.getNowoAfiliacionPath() + nameFolder + "/", doc.getDocumento()));
        }
        
        return result;
    }
    
    /**
     * Build attachment file
     * @param documentos
     * @param pathList
     * @return 
     */
    private Map<String, String> buildAttachmentFile(List<NowoDocumentoVO> documentos, List<String> pathList) {
        log.debug("\t\tNowoRegistroService.buildAttachmentFile");
        
        Map<String, String> map = new HashMap<>();
        if (documentos == null || documentos.isEmpty()) {
            return null;
        }
        
        if (pathList == null || pathList.isEmpty()) {
            map.put(Util.COD_ERROR, "Algunos documentos no se guardaron correctamente, la lista de documentos está vacia.");
            return map;
        }
        
        if (documentos.size() != pathList.size()) {
            map.put(Util.COD_ERROR, "Algunos documentos no se guardaron correctamente, la lista de documentos guardados no coincide con la original.");
            return map;
        }
        
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < documentos.size(); i++) {
            if (pathList.get(i) == null || pathList.get(i).isEmpty()
                    || !pathList.get(i).startsWith(Util.PATH_TMP)) {
                msg.append("Error al guardar el documento ");
                msg.append(documentos.get(i).getTipo());
                msg.append(":");
                msg.append(pathList.get(i));
                msg.append(". | .");
                continue;
            }
            
            log.debug("\tGuardando documento:" + pathList.get(i).substring(Util.PATH_TMP.length()));
            map.put(documentos.get(i).getTipo() + "_" + documentos.get(i).getNombre() + "." + documentos.get(i).getFormato(),
                    pathList.get(i).substring(Util.PATH_TMP.length()));
        }
        
        if (msg.length() > 0) {
            map.put(Util.COD_ERROR, msg.toString());
        }
        
        return map;
    }
    
    /**
     * Replace strings values
     * @param plantilla
     * @param datos
     * @return 
     */
    private String replaceValuesPf(String plantilla, NowoPersonaFisicaVO datos) {
        if (plantilla == null || plantilla.isEmpty() || datos == null) {
            return plantilla;
        }
        
        return Util.formatStringv2(plantilla, datos.getNombre(),
                datos.getNombre(),
                datos.getApellidoPaterno(),
                datos.getApellidoMaterno(),
                datos.getRfc(),
                datos.getCurp(),
                datos.getGiro(),
                datos.getEmail(),
                datos.getTelefono(),
                datos.getNumeroAgente(),
                (datos.isAceptoAvisoPrivacidad())? "S&iacute;":"No",
                (datos.isAceptoTerminosCondiciones())? "S&iacute;":"No",
                (datos.isAceptoComunicacionComercial())? "S&iacute;":"No"
                );
    }
    
    /**
     * Replace strings values
     * @param plantilla
     * @param datos
     * @return 
     */
    private String replaceValuesPm(String plantilla, NowoPersonaMoralVO datos) {
        if (plantilla == null || plantilla.isEmpty() || datos == null) {
            return plantilla;
        }
        
        String nrl = (datos.getRepresentanteLegal() == null)? "-":
                (datos.getRepresentanteLegal().getNombre() + " "
                + datos.getRepresentanteLegal().getApellidoPaterno() + " "
                + datos.getRepresentanteLegal().getApellidoMaterno()).trim();
        nrl = (nrl.isEmpty())? "-":nrl;
        
        String ncc = (datos.getContactoComercial() == null)? "-":
                (datos.getContactoComercial().getNombre() + " "
                + datos.getContactoComercial().getApellidoPaterno() + " "
                + datos.getContactoComercial().getApellidoMaterno()).trim();
        ncc = (ncc.isEmpty())? "-":ncc;
        
        String nca = (datos.getContactoAdministrativo() == null)? "-":
                (datos.getContactoAdministrativo().getNombre() + " "
                + datos.getContactoAdministrativo().getApellidoPaterno() + " "
                + datos.getContactoAdministrativo().getApellidoMaterno()).trim();
        nca = (nca.isEmpty())? "-":nca;
        
        return Util.formatStringv2(plantilla, datos.getDenominacionSocial(),
                datos.getDenominacionSocial(),
                datos.getRfc(),
                nrl,
                (datos.getRepresentanteLegal() == null)? "-":
                        (datos.getRepresentanteLegal().getEmail() == null
                                || datos.getRepresentanteLegal().getEmail().isEmpty())?
                                "-":datos.getRepresentanteLegal().getEmail(),
                (datos.getRepresentanteLegal() == null)? "-":
                        (datos.getRepresentanteLegal().getTelefono() == null
                                || datos.getRepresentanteLegal().getTelefono().isEmpty())?
                                "-":datos.getRepresentanteLegal().getTelefono(),
                ncc,
                (datos.getContactoComercial() == null)? "-":
                        (datos.getContactoComercial().getEmail() == null
                                || datos.getContactoComercial().getEmail().isEmpty())?
                                "-":datos.getContactoComercial().getEmail(),
                (datos.getContactoComercial() == null)? "-":
                        (datos.getContactoComercial().getTelefono() == null
                                || datos.getContactoComercial().getTelefono().isEmpty())?
                                "-":datos.getContactoComercial().getTelefono(),
                nca,
                (datos.getContactoAdministrativo() == null)? "-":
                        (datos.getContactoAdministrativo().getEmail() == null
                                || datos.getContactoAdministrativo().getEmail().isEmpty())?
                                "-":datos.getContactoAdministrativo().getEmail(),
                (datos.getContactoAdministrativo() == null)? "-":
                        (datos.getContactoAdministrativo().getTelefono() == null
                                || datos.getContactoAdministrativo().getTelefono().isEmpty())?
                                "-":datos.getContactoAdministrativo().getTelefono(),
                datos.getNumeroAgente(),
                (datos.isAceptoAvisoPrivacidad())? "S&iacute;":"No",
                (datos.isAceptoTerminosCondiciones())? "S&iacute;":"No",
                (datos.isAceptoComunicacionComercial())? "S&iacute;":"No"
                );
    }
}
