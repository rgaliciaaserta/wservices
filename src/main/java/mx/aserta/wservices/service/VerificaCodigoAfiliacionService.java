/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.handler.SoapMessageLogHandler;
import mx.aserta.wservices.handler.SoapMessageSiebelHandler;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.VerificaCodigoAfiliadoRequestVO;
import mx.aserta.wservices.vo.VerificaCodigoAfiliadoResponseVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wsdl.verificacodigoafiliado.com.siebel.customui.ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoInput;
import wsdl.verificacodigoafiliado.com.siebel.customui.ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoOutput;
import wsdl.verificacodigoafiliado.com.siebel.customui.ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service;

/**
 *
 * @author rgalicia
 */
@Service
public class VerificaCodigoAfiliacionService {
    
    @Autowired
    private SoapMessageSiebelHandler soapMessageSiebelHandler;
    
    @Autowired
    private SoapMessageLogHandler soapMessageLogHandler;
    
    @Autowired
    private PropSource prop;
    
    private ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service wsVerificaCodigoAserta;
    
    private ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service wsVerificaCodigoInsurgentes;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Inicializa aserta ws
     */
    private void initAserta() {
        if (this.wsVerificaCodigoAserta == null) {
            try {
                File file = new File(this.prop.getWsVerificaCodigoAfiliacionAserta());
                URL asertaUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsVerificaCodigoAserta = new ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service(asertaUrl);
                this.wsVerificaCodigoAserta.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
                
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsVerificaCodigoAserta = new ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service();
            }
        }
    }
    
    /**
     * Inicilaiza insurgentes ws
     */
    private void initInsurgentes() {
        if (this.wsVerificaCodigoInsurgentes == null) {
            try {
                File file = new File(this.prop.getWsVerificaCodigoAfiliacionInsurgentes());
                URL insurgentesUrl = file.getCanonicalFile().toURI().toURL();
                
                this.wsVerificaCodigoInsurgentes = new ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service(insurgentesUrl);
                this.wsVerificaCodigoInsurgentes.setHandlerResolver((PortInfo portInfo) -> {
                    List<Handler> list = new ArrayList<>();
                    list.add(this.soapMessageSiebelHandler);
                    list.add(this.soapMessageLogHandler);
                    
                    return list;
                });
            } catch (Exception ex) {
                log.error("Error:" + ex.getMessage(), ex);
                this.wsVerificaCodigoInsurgentes = new ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service();
            }
        }
    }

    public ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service getWsVerificaCodigoAserta() {
        if (this.wsVerificaCodigoAserta == null) {
            this.initAserta();
        }
        return this.wsVerificaCodigoAserta;
    }

    public ASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS_Service getWsVerificaCodigoInsurgentes() {
        if (this.wsVerificaCodigoInsurgentes == null) {
            this.initInsurgentes();
        }
        return this.wsVerificaCodigoInsurgentes;
    }
    
    /**
     * Consumir el servicio de anulacion
     * @param empresa
     * @param datos
     * @return 
     */
    public VerificaCodigoAfiliadoResponseVO verificaCodigoAfiliacion(String empresa, VerificaCodigoAfiliadoRequestVO datos) {
        log.debug("\tVerificaCodigoAfiliacionService.verificaCodigoAfiliacion");
        
        VerificaCodigoAfiliadoResponseVO resp = new VerificaCodigoAfiliadoResponseVO();
        
        if (datos == null) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        if (empresa == null || empresa.isEmpty()
                || (!Util.ASERTA.equalsIgnoreCase(empresa)
                && !Util.INSURGENTES.equalsIgnoreCase(empresa))) {
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar la empresa (" + Util.ASERTA + " | " + Util.INSURGENTES + ").");
            return resp;
        }
        
        try {
            ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoInput input = new ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoInput();
            
            input.setCodigoAfiliado(datos.getCodigoAfiliado());
            
            ASERTANowoVerificaCodigoAfiliadoBSVerificaCodigoOutput output;
            if (Util.ASERTA.equalsIgnoreCase(empresa)) {
                output = this.getWsVerificaCodigoAserta().getASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS()
                        .asertaNowoVerificaCodigoAfiliadoBSVerificaCodigo(input);
            } else {
                output = this.getWsVerificaCodigoInsurgentes().getASERTASpcNowoSpcVerificaSpcCodigoSpcAfiliadoSpcBS()
                        .asertaNowoVerificaCodigoAfiliadoBSVerificaCodigo(input);
            }
            
            if (output == null || output.getAfiliadoValido() == null
                    || output.getAfiliadoValido().isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.getMensaje() == null)? null:(String) output.getMensaje());
            
            if (Util.SPS_EXITO.equalsIgnoreCase(output.getAfiliadoValido())) {
                resp.setExito(Util.SPS_EXITO);
                
                resp.setPromotorInmobiliario(output.getPromotorInmobiliario());
                
            } else {
                resp.setExito(Util.SPS_ERROR);
            }
        } catch (Exception ex) {
            log.error("Error verificaCodigoAfiliacion:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Ocurrió un error en el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
