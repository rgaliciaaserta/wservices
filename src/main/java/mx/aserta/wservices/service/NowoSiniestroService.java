/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.service;

import com.google.gson.Gson;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.sps.NowoSiniestroBiometriaParamService;
import mx.aserta.wservices.sps.NowoSiniestroGetSinRepDocsService;
import mx.aserta.wservices.sps.NowoSiniestroRepDocumentoService;
import mx.aserta.wservices.sps.NowoSiniestroRepService;
import mx.aserta.wservices.thread.EmailThread;
import mx.aserta.wservices.thread.HttpRequestThread;
import mx.aserta.wservices.vo.NowoSiniestroReporteVO;
import mx.aserta.wservices.vo.RespuestaJson;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.BucketDocumentRequestVO;
import mx.aserta.wservices.vo.BucketDocumentResponseVO;
import mx.aserta.wservices.vo.BucketDocumentVO;
import mx.aserta.wservices.vo.LoginOauthResponseVO;
import mx.aserta.wservices.vo.LoginOauthVO;
import mx.aserta.wservices.vo.NowoDocumentoVO;
import mx.aserta.wservices.vo.NowoSiniestroPropietarioVO;
import mx.aserta.wservices.vo.NowoSiniestroReporteSendVO;
import mx.aserta.wservices.vo.NowoSiniestroVO;
import mx.aserta.wservices.vo.NowoSinistroBiometriaVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author rgalicia
 */
@Service
public class NowoSiniestroService {
    
    @Autowired
    private NowoSiniestroRepService nowoSiniestroRepService;
    
    @Autowired
    private NowoSiniestroRepDocumentoService nowoSiniestroRepDocumentoService;
    
    @Autowired
    private NowoSiniestroGetSinRepDocsService nowoSiniestroGetSinRepDocsService;
    
    @Autowired
    private NowoSiniestroBiometriaParamService nowoSiniestroBiometriaParamService;
    
    @Autowired
    private EmailMgun emailMgun;
    
    @Autowired
    private PropSource prop;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Registro siniestro
     * @param datos
     * @return 
     */
    public RespuestaJson nowoSiniestroReporte(NowoSiniestroReporteVO datos) {
        log.debug("\tNowoSiniestroService.nowoSiniestroReporte");
        
        RespuestaJson resp = new RespuestaJson();
        NowoSinistroBiometriaVO datBiom = new NowoSinistroBiometriaVO();
        
        String validacion = this.validaParamsSiniestro(datos);
        if (validacion != null && !validacion.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje(validacion);
            return resp;
        }
        
        try {
            //Registro en BD
            resp = this.nowoSiniestroRepService.execute(datos);
            
            if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                log.debug("\tError al registrar el siniestro:" + resp.getMensaje());
                resp.setMensaje("No fue posible realizar el registro del siniestro. Contacte al administrador del sistema.");
                return resp;
            }
            
            if (resp.getDatos() == null || ((BigDecimal) resp.getDatos()).intValue() <= BigDecimal.ZERO.intValue()) {
                log.debug("\tError al registrar el siniestro:No se obtuvo un identificador válido en el repositorio.");
                resp.setMensaje("No fue posible obtener un identificador válido en el repositorio de siniestros. Contacte al administrador del sistema.");
                return resp;
            }
            
            //Almacen de documentos
            List<String> listFiles = this.savingDocuments(datos.getDocumentos(), datos.getSiniestro().getNumeroPoliza());
            //Armar mapa para email
            Map<String, String> mapFiles = this.buildAttachmentFile(datos.getDocumentos(), listFiles);
            
            if (mapFiles != null && mapFiles.containsKey(Util.COD_ERROR)) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(mapFiles.get(Util.COD_ERROR));
                return resp;
            }
            
            StringBuilder strDoc = new StringBuilder();
            BigDecimal idSiniestro = (BigDecimal) resp.getDatos();
            if (datos.getDocumentos() != null && !datos.getDocumentos().isEmpty()) {
                for (NowoDocumentoVO docNowo : datos.getDocumentos()) {
                    resp = this.nowoSiniestroRepDocumentoService.execute(idSiniestro, docNowo);

                    if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                        strDoc.append("El siguiente documento no se guardó correctamente:[");
                        strDoc.append(docNowo.getTipo());
                        strDoc.append("] (");
                        strDoc.append(docNowo.getNombre());
                        strDoc.append(".");
                        strDoc.append(docNowo.getFormato());
                        strDoc.append(").");
                    }
                }
            }
            
            if (strDoc.length() > 0) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(strDoc.toString());
                datBiom.setIdentificador(idSiniestro.longValue());
                resp.setDatos(datBiom);
                return resp;
            }
            
            RespuestaJson respBiom = (RespuestaJson) this.nowoSiniestroBiometriaParamService.execute(Util.SINISTRO_ESTATUS_PENDIENTE, idSiniestro.longValue());
            
            if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                log.debug("\tError al buscar url de biometria siniestro:" + resp.getMensaje());
                datBiom.setIdentificador(-1);
            } else {
                datBiom.setIdentificador(idSiniestro.longValue());
                if (respBiom.getDatos() != null && !((List<Map<String, String>>) respBiom.getDatos()).isEmpty()
                        && ((List<Map<String, String>>) respBiom.getDatos()).get(0) != null) {
                    datBiom.setUrlBiometria(((List<Map<String, String>>) respBiom.getDatos()).get(0).get(Util.DATO + "7"));
                }
                resp.setDatos(datBiom);
            }
            
            /*try{
                //this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoSiniestroTo(), null, this.prop.getEmailSupportNowoSiniestro(),
                //        this.replaceValuesSiniestro(this.prop.getNowoPlantillaSiniestro(), datos),
                //        this.prop.getEmailNowoSiniestroSubject(), mapFiles, this.prop.getEmailNowoSiniestroImg());
                
                this.emailMgun.getClass();
                
                List<String> listImages = new ArrayList<>();
                listImages.add(this.prop.getEmailNowoSiniestroImg());
                
                EmailThread emailThread = new EmailThread(
                        this.prop.getEmailNowoSiniestroTo(),
                        null,
                        this.prop.getEmailSupportNowoSiniestro(),
                        this.prop.getEmailNowoSiniestroSubject(),
                        this.replaceValuesSiniestro(this.prop.getNowoPlantillaSiniestro(), datos),
                        this.prop.getMailgumSiniestrosFrom(),
                        Util.SINIESTRO,
                        mapFiles,
                        listImages,
                        this.emailMgun
                );
                
                emailThread.start();
                
            } catch (Exception ex) {
                log.error("\t\t*********ERROR al enviar el correo con los archivos adjuntos.***********:" + ex.getMessage(),
                ex);
                
                this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoSiniestroTo(), null, this.prop.getEmailSupportNowoSiniestro(),
                        "Error al enviar el correo por Mailgun (Siniestro):<br/>"
                                + "N&uacute;mero de p&oacute;liza:" + datos.getSiniestro().getNumeroPoliza()
                                + "<br/>Inquilino:" + datos.getSiniestro().getNombreInquilino()
                                + "<br/>Error:"
                                + ex.getMessage(),
                        "Error:" + this.prop.getEmailNowoSiniestroSubject(),
                        this.prop.getMailgumSiniestrosFrom(),
                        Util.SINIESTRO,
                        null, this.prop.getEmailNowoSiniestroImg());
            }*/
            
            resp.setCodigo(Util.COD_EXITO);
            resp.setMensaje("Registro guardado.");
            
        } catch (Exception ex) {
            log.error("\tError NowoSiniestroService.nowoSiniestroReporte:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("No fue posible realizar el registro del siniestro. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Valida parametros de siniestro
     * @param datos
     * @return 
     */
    private String validaParamsSiniestro(NowoSiniestroReporteVO datos) {
        log.debug("\t\tValidando parametros de siniestro.");
        
        StringBuilder str = new StringBuilder();
        if (datos == null) {
            str.append("Debe indicar los datos del siniestro.");
            return str.toString();
        }
        
        //Propietario
        /*if (datos.getPropietario() == null) {
            str.append("Debe indicar los datos del propietario.");
            return str.toString();
        }
        
        if (datos.getPropietario().getNombre() == null || datos.getPropietario().getNombre().isEmpty()) {
            str.append("Debe indicar el nombre del propietario.");
        }*/
        
        if (datos.getPropietario().getEmail() == null || datos.getPropietario().getEmail().isEmpty()) {
            str.append("Debe indicar el correo electrónico del propietario.");
        }
        
        if (datos.getPropietario().getTelefono() == null || datos.getPropietario().getTelefono().isEmpty()) {
            str.append("Debe indicar el teléfono del propietario.");
        }
        
        //Siniestro
        /*if (datos.getSiniestro() == null) {
            str.append("Debe indicar los datos del siniestro.");
            return str.toString();
        }
        
        if (datos.getSiniestro().getNombreInquilino() == null || datos.getSiniestro().getNombreInquilino().isEmpty()) {
            str.append("Debe indicar el nombre del inquilino.");
        }*/
        
        if (datos.getSiniestro().getNumeroPoliza() == null || datos.getSiniestro().getNumeroPoliza().isEmpty()) {
            str.append("Debe indicar el número de póliza.");
        }
        
        /*if (datos.getSiniestro().getTipoSiniestro() == null || datos.getSiniestro().getTipoSiniestro().isEmpty()) {
            str.append("Debe indicar el tipo de siniestro.");
        }
        
        if (datos.getSiniestro().getProblema() == null || datos.getSiniestro().getProblema().isEmpty()) {
            str.append("Debe indicar el problema del siniestro.");
        }*/
        
        return str.toString();
    }
    
    /**
     * Saving documentos
     * @param documentos
     * @param folder
     * @return 
     */
    private List<String> savingDocuments(List<NowoDocumentoVO> documentos, String folder) {
        log.debug("\t\tNowoSiniestroService.savingDocuments");
        if (documentos == null || documentos.isEmpty()) {
            log.debug("\tNo hay documentos adjuntos.");
            return null;
        }
        
        List<String> result = new ArrayList<>();
        String msg;
        String body = "";
        
        for (NowoDocumentoVO doc : documentos) {
            if (doc == null) {
                result.add(null);
                continue;
            }
            
            msg = new String();
            body = "{tipo:" + doc.getTipo() +", formato:" + doc.getFormato()
                    + ", nombre:" + doc.getNombre() + ", documento:"
                    + ((doc.getDocumento() == null || doc.getDocumento().isEmpty())? "null":
                        ((doc.getDocumento().length() > 50)? doc.getDocumento().substring(0, 50): doc.getDocumento()))
                    + "}";
            
            if (doc.getTipo() == null || doc.getTipo().isEmpty()) {
                msg += "Debe indicar el tipo del documento:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            if (doc.getFormato() == null
                    || doc.getFormato().isEmpty()
                    || (!Util.FORMATO_DOC.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_DOCX.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_JPEG.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_JPG.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_PDF.equalsIgnoreCase(doc.getFormato())
                        && !Util.FORMATO_PNG.equalsIgnoreCase(doc.getFormato())
                    )) {
                msg += "Debe indicar un formato válido del documento [ ";
                msg += Util.FORMATO_DOC;
                msg += " | ";
                msg += Util.FORMATO_DOCX;
                msg += " | ";
                msg += Util.FORMATO_JPEG;
                msg += " | ";
                msg += Util.FORMATO_JPG;
                msg += " | ";
                msg += Util.FORMATO_PDF;
                msg += " | ";
                msg += Util.FORMATO_PNG;
                msg += " ]:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            if (doc.getNombre() == null || doc.getNombre().isEmpty()) {
                msg += "Debe indicar el nombre del documento:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            if (doc.getDocumento() == null || doc.getDocumento().isEmpty()) {
                msg += "Debe indicar el documento en base 64:";
                msg += body;
                msg += ".";
                result.add(msg);
                continue;
            }
            
            //Crear carpeta para documentos
            String nameFolder = Util.createFolder(this.prop.getNowoSiniestroPath(), folder);
            String namePath = Util.PATH_TMP + Util.saveFile(doc.getNombre(), doc.getFormato(),
                    this.prop.getNowoSiniestroPath() + nameFolder + "/", doc.getDocumento());
            result.add(namePath);
            doc.setUrl(namePath);
        }
        
        return result;
    }
    
    /**
     * Build attachment file
     * @param documentos
     * @param pathList
     * @return 
     */
    private Map<String, String> buildAttachmentFile(List<NowoDocumentoVO> documentos, List<String> pathList) {
        log.debug("\t\tNowoSiniestroService.buildAttachmentFile");
        
        Map<String, String> map = new HashMap<>();
        if (documentos == null || documentos.isEmpty()) {
            return null;
        }
        
        if (pathList == null || pathList.isEmpty()) {
            map.put(Util.COD_ERROR, "Algunos documentos no se guardaron correctamente, la lista de documentos está vacia.");
            return map;
        }
        
        if (documentos.size() != pathList.size()) {
            map.put(Util.COD_ERROR, "Algunos documentos no se guardaron correctamente, la lista de documentos guardados no coincide con la original.");
            return map;
        }
        
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < documentos.size(); i++) {
            if (pathList.get(i) == null || pathList.get(i).isEmpty()
                    || !pathList.get(i).startsWith(Util.PATH_TMP)) {
                msg.append("Error al guardar el documento ");
                msg.append(documentos.get(i).getTipo());
                msg.append(":");
                msg.append(pathList.get(i));
                msg.append(". | .");
                continue;
            }
            
            log.debug("\tGuardando documento:" + pathList.get(i).substring(Util.PATH_TMP.length()));
            map.put(documentos.get(i).getTipo() + "_" + documentos.get(i).getNombre() + "." + documentos.get(i).getFormato(),
                    pathList.get(i).substring(Util.PATH_TMP.length()));
        }
        
        if (msg.length() > 0) {
            map.put(Util.COD_ERROR, msg.toString());
        }
        
        return map;
    }
    
    /**
     * Replace strings values
     * @param plantilla
     * @param datos
     * @return 
     */
    private String replaceValuesSiniestro(String plantilla, NowoSiniestroReporteVO datos) {
        if (plantilla == null || plantilla.isEmpty() || datos == null) {
            return plantilla;
        }
        
        return Util.formatStringv2(plantilla,
                (datos.getPropietario() == null)? "":datos.getPropietario().getNombre(),
                (datos.getPropietario() == null)? "":datos.getPropietario().getEmail(),
                (datos.getPropietario() == null)? "":datos.getPropietario().getTelefono(),
                //(datos.getSiniestro() == null)? "":datos.getSiniestro().getNombreInquilino(),
                (datos.getSiniestro() == null)? "":datos.getSiniestro().getNumeroPoliza(),
                (datos.getSiniestro() == null)? "":datos.getSiniestro().getTipoSiniestro(),
                (datos.getSiniestro() == null)? "":datos.getSiniestro().getProblema()
                );
    }
    
    /**
     * Registro siniestro send
     * @param datos
     * @return 
     */
    public RespuestaJson nowoSiniestroReporteSend(NowoSiniestroReporteSendVO datos) {
        log.debug("\tNowoSiniestroService.nowoSiniestroReporteSend");
        
        RespuestaJson resp = new RespuestaJson();
        
        String validacion = this.validaParamsSiniestroSend(datos);
        if (validacion != null && !validacion.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje(validacion);
            return resp;
        }
        
        Map<String, String> mapFiles = null;
        NowoSiniestroReporteVO siniestroDatos = null;
        
        try {
            if (!Util.SINISTRO_ESTATUS_ERROR.equalsIgnoreCase(datos.getEstatus())) {
                /*if (resp.getDatos() == null || ((List<Map<String, String>>) resp.getDatos()).isEmpty()) {
                    log.debug("\tError al realizar el envío del siniestro:No se obtuvo un identificador válido en el repositorio.");
                    resp.setMensaje("No fue posible obtener un identificador válido en el repositorio de siniestros. Contacte al administrador del sistema.");
                    return resp;
                }*/

                //Recuperar biometria
                NowoDocumentoVO docBiometria = null;
                try {
                    LoginOauthResponseVO loginResponse = this.loginOauth();
                    if (loginResponse != null) {
                        log.debug("\tOauth correcto.");
                        BucketDocumentRequestVO bucketRequest = new BucketDocumentRequestVO();
                        bucketRequest.setXincodehardwareid(datos.getSesion());

                        BucketDocumentResponseVO bucketResponse = this.getDocumentoBucket(bucketRequest, loginResponse);
                        if (bucketResponse != null && bucketResponse.getCodigo() != null
                                && Util.COD_EXITO_OIC.equalsIgnoreCase(bucketResponse.getCodigo())
                                && bucketResponse.getData() != null) {
                            Gson gsonBucket = new Gson();
                            BucketDocumentVO jsonBucket = (BucketDocumentVO) gsonBucket.fromJson(new String(Base64.getDecoder().decode(bucketResponse.getData())), BucketDocumentVO.class);
                            
                            //Almacen de documentos
                            docBiometria = new NowoDocumentoVO();
                            docBiometria.setDocumento(jsonBucket.getCroppedBackID());
                            docBiometria.setFormato("jpg");
                            docBiometria.setNombre("Biometria_" + datos.getIdSiniestro());
                            docBiometria.setTipo("Biometria");
                            List<NowoDocumentoVO> listaBiometria = new ArrayList<NowoDocumentoVO>();
                            listaBiometria.add(docBiometria);
                            
                            List<String> listFiles = this.savingDocuments(listaBiometria, "" + datos.getIdSiniestro());
                            
                            log.debug("PATH:" + listFiles);
                            
                            RespuestaJson respBiometria = this.nowoSiniestroRepDocumentoService.execute(new BigDecimal(datos.getIdSiniestro()), docBiometria);
                            
                        } else {
                            log.debug("\tError al obtener el documento de biometría del siniestro:" + datos.getIdSiniestro());
                            resp.setMensaje("Error al obtener el documento de biometría del siniestro:" + datos.getIdSiniestro() + ". Contacte al administrador del sistema.");
                            resp.setDatos(null);
                            return resp;
                        }
                    } else {
                        log.debug("\tError al autenticarse para recuperar documentos de biometría del siniestro:" + datos.getIdSiniestro());
                        resp.setMensaje("Error al autenticarse para recuperar documentos de biometría del siniestro:" + datos.getIdSiniestro() + ". Contacte al administrador del sistema.");
                        resp.setDatos(null);
                        return resp;
                    }
                } catch (Exception ex) {
                    log.debug("\tError al obtener el documento de biometría del siniestro:" + datos.getIdSiniestro());
                    resp.setMensaje("Error al obtener el documento de biometría del siniestro:" + datos.getIdSiniestro() + ". Contacte al administrador del sistema.");
                    resp.setDatos(null);
                    return resp;
                }
                
                //Registro en BD
                resp = this.nowoSiniestroGetSinRepDocsService.execute(datos.getIdSiniestro());

                if (!Util.COD_EXITO.equalsIgnoreCase(resp.getCodigo())) {
                    log.debug("\tError al registrar el siniestro:" + resp.getMensaje());
                    resp.setMensaje("No fue posible realizar el envío del siniestro. Contacte al administrador del sistema.");
                    resp.setDatos(null);
                    return resp;
                }
                
                //Almacen de documentos
                siniestroDatos = this.savingDocumentsSend((List<Map<String, String>>) resp.getDatos());
                
                if (siniestroDatos == null) {
                    log.debug("\tError al obtener documentos del siniestro:" + datos.getIdSiniestro());
                    resp.setMensaje("Error al obtener documentos del siniestro:" + datos.getIdSiniestro() + ". Contacte al administrador del sistema.");
                    resp.setDatos(null);
                    return resp;
                }
                
                //Armar mapa para email
                mapFiles = this.buildAttachmentFileSend(siniestroDatos.getDocumentos());

                if (mapFiles.containsKey(Util.COD_ERROR)) {
                    resp.setCodigo(Util.COD_ERROR);
                    resp.setMensaje(mapFiles.get(Util.COD_ERROR));
                    resp.setDatos(null);
                    return resp;
                }
            }
            
            try{
                /*this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoSiniestroTo(), null, this.prop.getEmailSupportNowoSiniestro(),
                        this.replaceValuesSiniestro(this.prop.getNowoPlantillaSiniestro(), datos),
                        this.prop.getEmailNowoSiniestroSubject(), mapFiles, this.prop.getEmailNowoSiniestroImg());*/
                
                this.emailMgun.getClass();
                
                List<String> listImages = new ArrayList<>();
                listImages.add(this.prop.getEmailNowoSiniestroImg());
                
                if (Util.SINISTRO_ESTATUS_ERROR.equalsIgnoreCase(datos.getEstatus())) {
                    EmailThread emailThread = new EmailThread(
                            this.prop.getEmailNowoSiniestroTo(),
                            null,
                            this.prop.getEmailSupportNowoSiniestro(),
                            this.prop.getEmailNowoSiniestroSubject(),
                            datos.getError(),//this.replaceValuesSiniestro(this.prop.getNowoPlantillaSiniestro(), siniestroDatos),
                            this.prop.getMailgumSiniestrosFrom(),
                            Util.SINIESTRO,
                            mapFiles,
                            listImages,
                            this.emailMgun
                    );
                    
                    emailThread.start();
                    
                } else {
                    
                    EmailThread emailThread = new EmailThread(
                            this.prop.getEmailNowoSiniestroTo(),
                            null,
                            this.prop.getEmailSupportNowoSiniestro(),
                            this.prop.getEmailNowoSiniestroSubject(),
                            this.replaceValuesSiniestro(this.prop.getNowoPlantillaSiniestro(), siniestroDatos),
                            this.prop.getMailgumSiniestrosFrom(),
                            Util.SINIESTRO,
                            mapFiles,
                            listImages,
                            this.emailMgun
                    );

                    emailThread.start();
                    
                }
                
            } catch (Exception ex) {
                log.error("\t\t*********ERROR al enviar el correo con los archivos adjuntos.***********:" + ex.getMessage(),
                ex);
                
                this.emailMgun.sendHTMLMailAttachment(null, null, this.prop.getEmailNowoSiniestroTo(), null, this.prop.getEmailSupportNowoSiniestro(),
                        "Error al enviar el correo por Mailgun (Siniestro):<br/>"
                                + "N&uacute;mero de p&oacute;liza:" + siniestroDatos.getSiniestro().getNumeroPoliza()
                                + "<br/>Inquilino:" + siniestroDatos.getSiniestro().getNombreInquilino()
                                + "<br/>Error:"
                                + ex.getMessage(),
                        "Error:" + this.prop.getEmailNowoSiniestroSubject(),
                        this.prop.getMailgumSiniestrosFrom(),
                        Util.SINIESTRO,
                        null, this.prop.getEmailNowoSiniestroImg());
            }
            
            resp.setCodigo(Util.COD_EXITO);
            resp.setMensaje("Correo enviado.");
            resp.setDatos(null);
            
        } catch (Exception ex) {
            log.error("\tError NowoSiniestroService.nowoSiniestroReporteSend:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("No fue posible realizar el registro del siniestro. Contacte al administrador del sistema.");
            resp.setDatos(null);
        }
        
        return resp;
    }
    
    /**
     * Valida parametros de siniestro send
     * @param datos
     * @return 
     */
    private String validaParamsSiniestroSend(NowoSiniestroReporteSendVO datos) {
        log.debug("\t\tValidando parametros de siniestro.");
        
        StringBuilder str = new StringBuilder();
        if (datos == null) {
            str.append("Debe indicar los datos del siniestro.");
            return str.toString();
        }
        
        //Propietario
        if (datos.getIdSiniestro() == null) {
            str.append("Debe indicar los datos del identificador.");
            return str.toString();
        }
        
        if (datos.getEstatus() == null || !(Util.SINISTRO_ESTATUS_PENDIENTE.equalsIgnoreCase(datos.getEstatus())
                || Util.SINISTRO_ESTATUS_ERROR.equalsIgnoreCase(datos.getEstatus()))) {
            str.append("Debe indicar el estatus del siniestro.");
        }
        
        return str.toString();
    }
    
    /**
     * Get list files
     * @param listMap
     * @return 
     */
    private NowoSiniestroReporteVO savingDocumentsSend(List<Map<String, String>> listMap) {
        log.debug("\tNowoSiniestroService.savingDocumentsSend");
        
        if (listMap == null || listMap.isEmpty()) {
            log.debug("No existen registros para el siniestro.");
            return null;
        }
        
        NowoSiniestroReporteVO datos = new NowoSiniestroReporteVO();
        
        Map<String, String> map;
        boolean fill = true;
        NowoDocumentoVO doc;
        
        for (int i = 0; i < listMap.size(); i++) {
            map = listMap.get(i);
            
            if (map == null) {
                continue;
            }
            
            if (fill) {
                NowoSiniestroPropietarioVO prop = new NowoSiniestroPropietarioVO();
                prop.setEmail(map.get(Util.DATO + "3"));
                prop.setNombre(map.get(Util.DATO + "2"));
                prop.setTelefono(map.get(Util.DATO + "4"));
                
                NowoSiniestroVO siniestro = new NowoSiniestroVO();
                siniestro.setNombreInquilino(map.get(Util.DATO + "5"));
                siniestro.setNumeroPoliza(map.get(Util.DATO + "6"));
                siniestro.setProblema(map.get(Util.DATO + "8"));
                siniestro.setTipoSiniestro(map.get(Util.DATO + "7"));
                
                datos.setPropietario(prop);
                datos.setSiniestro(siniestro);
                datos.setDocumentos(new ArrayList<NowoDocumentoVO>());
                
                fill = false;
            }
            
            doc = new NowoDocumentoVO();
            doc.setDocumento(map.get(Util.DATO + "12"));
            doc.setFormato(map.get(Util.DATO + "11"));
            doc.setNombre(map.get(Util.DATO + "10"));
            doc.setTipo(map.get(Util.DATO + "9"));
            doc.setUrl(map.get(Util.DATO + "13"));
            
            datos.getDocumentos().add(doc);
        }
        
        return datos;
    }
    
    /**
     * Build attachment file
     * @param documentos
     * @return 
     */
    private Map<String, String> buildAttachmentFileSend(List<NowoDocumentoVO> documentos) {
        log.debug("\t\tNowoSiniestroService.buildAttachmentFileSend");
        
        Map<String, String> map = new HashMap<>();
        if (documentos == null || documentos.isEmpty()) {
            return null;
        }
        
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < documentos.size(); i++) {
            if (documentos.get(i) == null || documentos.get(i).getUrl() == null
                    || documentos.get(i).getUrl().isEmpty()) {
                log.debug("Error al guardar el documento " + documentos.get(i).getUrl());
                msg.append("Error al guardar el documento ");
                msg.append(documentos.get(i).getTipo());
                msg.append(":");
                msg.append(documentos.get(i).getNombre());
                msg.append(".");
                msg.append(documentos.get(i).getFormato());
                msg.append("<br/>");
                
                continue;
            }
            
            log.debug("\tGuardando documento:" + documentos.get(i).getUrl());
            map.put(documentos.get(i).getTipo() + "_" + documentos.get(i).getNombre() + "." + documentos.get(i).getFormato(),
                    documentos.get(i).getUrl());
        }
        
        if (msg.length() > 0) {
            map.put(Util.COD_ERROR, msg.toString());
        }
        
        return map;
    }
    
    /**
     * Login Oauth Bucket
     * @return 
     */
    private LoginOauthResponseVO loginOauth() {
        log.debug("\tNowoSiniestroService.loginOuth");
        
        LoginOauthVO login = new LoginOauthVO();
        login.setClient_id(this.prop.getOicNowoClientId());
        login.setClient_secret(this.prop.getOicNowoClientSecret());
        login.setGrant_type(this.prop.getOicNowoGrantType());
        login.setScope(this.prop.getOicNowoScope());
        
        Map<String, List<String>> header = new HashMap<>();
        List<String> ccontrol = new ArrayList<>();
        List<String> ctype = new ArrayList<>();
        ccontrol.add("no-cache");
        ctype.add("application/x-www-form-urlencoded");
        
        header.put("Cache-Control", ccontrol);
        header.put("Content-Type", ctype);
        
        List<HttpRequestThread> lista = new ArrayList<>();
        HttpRequestThread<LoginOauthVO, LoginOauthResponseVO> t;
        for (int j = 0; j < 1; j++) {
            t = new HttpRequestThread<LoginOauthVO, LoginOauthResponseVO>(
            this.prop.getOicNowoUrlOauth(),
            false,
            login, LoginOauthResponseVO.class, 1, 1, header, true, null,
            null, false, HttpMethod.POST.name(), true, null, null);
            
            t.run();

            lista.add(t);
        }
        
        int i = 10;
        boolean f = false;
        while(!f && i > 0) {
            
            f = true;
            for (int k = 0; k < lista.size(); k++) {
                if (!lista.get(k).isFinished()) {
                    f = false;
                    break;
                }
            }
            
            try {
                Thread.sleep(1000L);
            } catch (Exception ex) {
                System.out.println("Error:" + ex.getMessage());
            }
            i --;
        }
        
        if (!lista.isEmpty() && lista.get(0) != null) {
            return (LoginOauthResponseVO) lista.get(0).getObjectResponseJson();
        }
        
        return null;
    }
    
    /**
     * Get document from bucket
     * @param bucket
     * @param loginResponse
     * @return 
     */
    private BucketDocumentResponseVO getDocumentoBucket(BucketDocumentRequestVO bucket,
            LoginOauthResponseVO loginResponse) {
        log.debug("\tNowoSiniestroService.getDocumentoBucket");
        
        Map<String, List<String>> header = new HashMap<>();
        List<String> auth = new ArrayList<>();
        List<String> ctype = new ArrayList<>();
        ctype.add("application/json");
        auth.add(loginResponse.getToken_type()
                + " " + loginResponse.getAccess_token());
        
        header.put("Content-Type", ctype);
        header.put("Authorization", auth);
        
        List<HttpRequestThread> lista = new ArrayList<>();
        HttpRequestThread<BucketDocumentRequestVO, BucketDocumentResponseVO> t;
        for (int j = 0; j < 1; j++) {
            t = new HttpRequestThread<BucketDocumentRequestVO, BucketDocumentResponseVO>(
            this.prop.getOicNowoUrlBucketOic(),
            false,
            bucket, BucketDocumentResponseVO.class, 1, 1, header, false, null,
            null, false, HttpMethod.POST.name(), true, null, null);
            
            t.run();

            lista.add(t);
        }
        
        int i = 10;
        boolean f = false;
        while(!f && i > 0) {
            
            f = true;
            for (int k = 0; k < lista.size(); k++) {
                if (!lista.get(k).isFinished()) {
                    f = false;
                    break;
                }
            }
            
            try {
                Thread.sleep(1000L);
            } catch (Exception ex) {
                System.out.println("Error:" + ex.getMessage());
            }
            i --;
        }
        
        /*for (int k = 0; k < lista.size(); k++) {
            log.debug("-------" + k + "-------");
            log.debug(((BucketDocumentResponseVO) lista.get(k).getObjectResponseJson()).getCodigo());
            log.debug(((BucketDocumentResponseVO) lista.get(k).getObjectResponseJson()).getMensaje());
            log.debug(((BucketDocumentResponseVO) lista.get(k).getObjectResponseJson()).getData());
        }*/
        
        if (!lista.isEmpty() && lista.get(0) != null) {
            return (BucketDocumentResponseVO) lista.get(0).getObjectResponseJson();
        }
        
        return null;
    }
}
