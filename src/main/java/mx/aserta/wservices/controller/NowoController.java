/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.aserta.wservices.service.ConsultaDocumentoService;
import mx.aserta.wservices.service.ConsultaDocumentosNowoPaqueteService;
import mx.aserta.wservices.service.NowoCartaAceptacionService;
import mx.aserta.wservices.service.NowoRegistroService;
import mx.aserta.wservices.service.NowoSiniestroService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ConsultaDocumentosNowoPaqueteResponseVO;
import mx.aserta.wservices.vo.ConsultaDocumentosNowoPaqueteVO;
import mx.aserta.wservices.vo.NowoCartaAceptacionRequestVO;
import mx.aserta.wservices.vo.NowoCartaAceptacionResponseVO;
import mx.aserta.wservices.vo.NowoPersonaFisicaVO;
import mx.aserta.wservices.vo.NowoPersonaMoralVO;
import mx.aserta.wservices.vo.NowoSiniestroReporteSendVO;
import mx.aserta.wservices.vo.NowoSiniestroReporteVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rgalicia
 */
@RestController
public class NowoController {
    
    @Autowired
    private NowoRegistroService nowoRegistroService;
    
    @Autowired
    private NowoCartaAceptacionService nowoCartaAceptacionService;
    
    @Autowired
    private NowoSiniestroService nowoSiniestroService;
    
    @Autowired
    private ConsultaDocumentosNowoPaqueteService consultaDocumentosNowoPaqueteService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * Registro persona fisica
     * @param datos
     * @return 
     */
    @RequestMapping("/nowo/registro/pf")
    public RespuestaJson nowoRegistroPf(@RequestBody(required = true) NowoPersonaFisicaVO datos) {
        log.debug("\t\t>>/nowo/registro/pf");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        try {
            return this.nowoRegistroService.nowoRegistroPf(datos);
            
        } catch (Exception ex) {
            log.error("\t\tError:/nowo/registro/pf:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:/nowo/registro/pf. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Registro persona moral
     * @param datos
     * @return 
     */
    @RequestMapping("/nowo/registro/pm")
    public RespuestaJson nowoRegistroPm(@RequestBody(required = true) NowoPersonaMoralVO datos) {
        log.debug("\t\t>>/nowo/registro/pm");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        try {
            return this.nowoRegistroService.nowoRegistroPm(datos);
            
        } catch (Exception ex) {
            log.error("\t\tError:/nowo/registro/pm:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:/nowo/registro/pm. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Nowo Carta Aceptacion
     * @param datos
     * @return 
     */
    @RequestMapping("/nowo/carta_aceptacion")
    public NowoCartaAceptacionResponseVO nowoCartaAceptacion(@RequestBody(required = true) NowoCartaAceptacionRequestVO datos) {
        log.debug("\t\t>>/nowo/carta_aceptacion");
        
        NowoCartaAceptacionResponseVO resp = new NowoCartaAceptacionResponseVO();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        try {
            return this.nowoCartaAceptacionService.getPlantilla(Util.INSURGENTES, datos);
            
        } catch (Exception ex) {
            log.error("\t\tError:/nowo/carta_aceptacion:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:/nowo/carta_aceptacion. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Registro persona moral
     * @param datos
     * @return 
     */
    @RequestMapping("/nowo/siniestro_reporte")
    public RespuestaJson nowoSiniestroReporte(@RequestBody(required = true) NowoSiniestroReporteVO datos) {
        log.debug("\t\t>>/nowo/siniestro_reporte");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        try {
            return this.nowoSiniestroService.nowoSiniestroReporte(datos);
            
        } catch (Exception ex) {
            log.error("\t\tError:/nowo/siniestro_reporte:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:/nowo/siniestro_reporte. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Registro persona moral
     * @param datos
     * @return 
     */
    @RequestMapping(value = "/nowo/siniestro_reporte_send")
    public RespuestaJson nowoSiniestroReporteSend(@RequestBody(required = true) NowoSiniestroReporteSendVO datos) {
        log.debug("\t\t>>/nowo/siniestro_reporte_send");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        /*String xincodehardwareid = httpRequest.getHeader("X-Incode-Hardware-Id");
        
        if (xincodehardwareid == null || xincodehardwareid.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el header [X-Incode-Hardware-Id].");
            return resp;
        }*/
        
        try {
            return this.nowoSiniestroService.nowoSiniestroReporteSend(datos);
            
        } catch (Exception ex) {
            log.error("\t\tError:/nowo/siniestro_reporte_send:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:/nowo/siniestro_reporte_send. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
    
    /**
     * Consulta Documentos
     * @param datos
     * @return 
     */
    @RequestMapping("/nowo/consulta_documentos")
    public ConsultaDocumentosNowoPaqueteResponseVO nowoConsultaDocumentos(@RequestBody(required = true) ConsultaDocumentosNowoPaqueteVO datos) {
        log.debug("\t\t>>/nowo/consulta_documentos");
        
        ConsultaDocumentosNowoPaqueteResponseVO resp = new ConsultaDocumentosNowoPaqueteResponseVO();
        
        if (datos == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        try {
            return this.consultaDocumentosNowoPaqueteService.nowoConsultaDocumentos(datos);
            
        } catch (Exception ex) {
            log.error("\t\tError:/nowo/consulta_documentos:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:/nowo/consulta_documentos. Contacte al administrador del sistema.");
        }
        
        return resp;
    }
}
