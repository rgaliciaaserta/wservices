/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.LogRequestVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class LogRequestService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    public static final String PI_ID = "PI_ID";
    public static final String PI_APPLICATION = "PI_APPLICATION";
    public static final String PI_TRANSACTION = "PI_TRANSACTION";
    public static final String PI_URLREQUEST = "PI_URLREQUEST";
    public static final String PI_URLPARAMETERS = "PI_URLPARAMETERS";
    public static final String PI_HEADERREQUEST = "PI_HEADERREQUEST";
    public static final String PI_BODYREQUEST = "PI_BODYREQUEST";
    public static final String PI_RESPONSE = "PI_RESPONSE";
    public static final String PI_USUARIOCARGA = "PI_USUARIOCARGA";
    public static final String PO_FLG = "PO_FLG";
    public static final String PO_MSG = "PO_MSG";
    public static final String PO_ID = "PO_ID";
    
    static {
        sql = getSQL("pkg.httprequest", "SP_LOG_REQUEST");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public LogRequestService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_APPLICATION, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_TRANSACTION, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_URLREQUEST, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_URLPARAMETERS, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_HEADERREQUEST, Types.CLOB));
        this.declareParameter(new SqlParameter(PI_BODYREQUEST, Types.CLOB));
        this.declareParameter(new SqlParameter(PI_RESPONSE, Types.CLOB));
        this.declareParameter(new SqlParameter(PI_USUARIOCARGA, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_ID, Types.NUMERIC));
        
        try {
            this.compile();
        } catch(Exception ex) {
            log.error("\t\tError compile:" + sql + ":" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Consume SP
     * @param req
     * @return 
     */
    public RespuestaJson execute(LogRequestVO req) {
        log.debug("\t\tLogRequestService:" + sql);
        
        RespuestaJson resp = new RespuestaJson();
        if (req == null ) {
            log.debug("\t\tDebe indicar el objeto request para logueo.");
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto request para logueo.");
            return resp;
        }
        
        log.debug(":" + req.getApplication());
        log.debug(":" + req.getTransaction());
        log.debug(":" + req.getId());
        
        Map<String, Object> input = new HashMap<>();
        try {
            input.put(PI_ID, req.getId());
            input.put(PI_APPLICATION, req.getApplication());
            input.put(PI_TRANSACTION, req.getTransaction());
            input.put(PI_URLREQUEST, req.getUrl());
            input.put(PI_URLPARAMETERS, req.getUrlParameters());
            input.put(PI_HEADERREQUEST, req.getHeaders());
            input.put(PI_BODYREQUEST, req.getBody());
            input.put(PI_RESPONSE, req.getResponse());
            input.put(PI_USUARIOCARGA, req.getUsuario());
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\t\tOcurrió un error al ejecutar el servicio.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? "":(String) output.get(PO_MSG));
            if (Util.SPS_EXITO.equalsIgnoreCase((String) output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                
                req.setId((output.get(PO_ID) == null)? null:((BigDecimal) output.get(PO_ID)).longValue());
                resp.setDatos(req);
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tcodigo:" + resp.getCodigo());
            log.debug("\tmensaje:" + resp.getMensaje());
        } catch (Exception ex) {
            log.error("\t\tError service:" + sql + ":" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al invocar el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
