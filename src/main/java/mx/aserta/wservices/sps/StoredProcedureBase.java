/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author rgalicia
 */
public class StoredProcedureBase extends StoredProcedure {
    
    public StoredProcedureBase(DataSource ds, String SQL) {
        super(ds, SQL);
    }

    public static String getSQL(String propertyName, String SP) {
        Properties general = new Properties();
        try {
            InputStream io = new FileInputStream("./iPad/conf/wservices.properties");
            general.load(io);
        } catch (IOException ex) {
                LogManager.getLogger("WSERVICES").error("Error leyendo properties:" + ex.getMessage(), ex);
        }
        LogManager.getLogger("WSERVICES").debug(general.toString());
        return String.format("%s.%s", general.getProperty(propertyName), SP);

    }
}
