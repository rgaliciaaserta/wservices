/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.model.AonGruposResponseModel;
import mx.aserta.wservices.model.GrupoPeniolesResponseModel;
import static mx.aserta.wservices.sps.StoredProcedureBase.getSQL;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class GetGrupoPeniolesService extends StoredProcedureBase {

    private static String sql = "REP_RESUMEN.?";
    
    private static final String PO_FLG = "PO_FLG";
    private static final String PO_MSG = "PO_MSG";
    private static final String PO_DATOS = "PO_DATOS";
    
    static {
        sql = getSQL("pkg.aon_grupo_penioles", "SP_GET_GRUPO_PENIOLES");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public GetGrupoPeniolesService(@Qualifier("dataSourceReporteador") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_DATOS, oracle.jdbc.OracleTypes.CURSOR,
                (rs, numRow) -> {
                    AonGruposResponseModel datos = new AonGruposResponseModel();
                    
                    try {
                        datos.setBeneficiario(Util.emptyStringIfNull(rs.getString("BENEFICIARIO")));
                        datos.setCentroCostos(Util.emptyStringIfNull(rs.getString("CENTRO_COSTOS")));
                        datos.setEmpresa(Util.emptyStringIfNull(rs.getString("NOMBRE_EMPRESA")));
                        datos.setEstatusFianza(Util.emptyStringIfNull(rs.getString("STATUS_FIANZA")));
                        datos.setEstatusPago(Util.emptyStringIfNull(rs.getString("ESTATUS_PAGO")));
                        datos.setFechaFinVigencia(Util.emptyStringIfNull(rs.getString("FFIN_VIG")));
                        datos.setFechaInicioVigencia(Util.emptyStringIfNull(rs.getString("FINI_VIG")));
                        datos.setFechaMovimiento(Util.emptyStringIfNull(rs.getString("FECHA_MOVIMIENTO")));
                        datos.setFiado(Util.emptyStringIfNull(rs.getString("FIADO")));
                        datos.setFianza(Util.emptyStringIfNull(rs.getString("FIANZA")));
                        datos.setInclusion(Util.emptyStringIfNull(rs.getString("INCLUSION")));
                        datos.setMoneda(Util.emptyStringIfNull(rs.getString("MONEDA")));
                        datos.setMontoAfianzadoMovimiento(Util.emptyStringIfNull(rs.getString("MONTO_AFIANZADO_MOVTO")));
                        datos.setMontoReclamacion(Util.emptyStringIfNull(rs.getString("MONTO_RECLAMACION")));
                        datos.setNoContrato(Util.emptyStringIfNull(rs.getString("NUM_CONTRATO")));
                        datos.setObligacion(Util.emptyStringIfNull(rs.getString("OBLIGACION")));
                        datos.setPrimaNeta(Util.emptyStringIfNull(rs.getString("PRIMA_NETA")));
                        datos.setPrimaTotal(Util.emptyStringIfNull(rs.getString("PRIMA_TOTAL")));
                        datos.setRamoTipo(Util.emptyStringIfNull(rs.getString("RAMO_TIPO")));
                        datos.setTipoMovimiento(Util.emptyStringIfNull(rs.getString("TIPO_MOVIMIENTO")));
                        
                        return datos;
                    } catch(Exception ex) {
                        return null;
                    }
                }
        ));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Consume SP
     * @return 
     */
    public RespuestaJson execute() {
        log.debug("\tGetGrupoPeniolesService.execute:"+ sql);
        
        RespuestaJson resp = new RespuestaJson();
        
        try {
            Map<String, Object> input = new HashMap<>();
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tError al ejecutar el servicio:" + sql);
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("Error al ejecutar el servicio:" + sql);
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? "":(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String) output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                resp.setDatos(
                        (output.get(PO_DATOS) == null)? null:
                                (List<AonGruposResponseModel>) output.get(PO_DATOS));
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
        } catch (Exception ex) {
            log.error("\tError:" + sql + ":" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al ejecutar el servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
