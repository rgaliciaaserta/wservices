/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ArrendamientoDetalleResponse;
import mx.aserta.wservices.vo.ArrendamientoDetalleResponseV2;
import mx.aserta.wservices.vo.ArrendamientoResponse;
import mx.aserta.wservices.vo.ArrendamientoVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class ArrendaCotizaAsertaService extends StoredProcedureBase {
    
    private static String sql = "SIEBEL.?";
    
    private static final String PI_MONTO_RENTA = "PIMONTORENTA";
    private static final String PI_ESQUEMA = "PIESQUEMA";
    private static final String PI_BUCKET = "PIBUCKET";
    private static final String PI_MESES = "PIMESES";
    private static final String PI_USR_LOGIN = "PIUSRLOGIN";
    private static final String PI_LOG_ID_SIEBEL = "PILOGIDSIEBEL";
    
    private static final String PO_FLG_EXITO = "POFLGEXITO";
    private static final String PO_MSG = "POMSG";
    private static final String PO_CALCULOS = "POCALCULOS";
    
    static {
        sql = getSQL("pkg.scarrnowo_arrenda", "SP_COTIZA_2");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public ArrendaCotizaAsertaService(@Qualifier("dataSourceSiebelAserta") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_MONTO_RENTA, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_ESQUEMA, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_BUCKET, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_MESES, Types.INTEGER));
        this.declareParameter(new SqlParameter(PI_USR_LOGIN, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_LOG_ID_SIEBEL, Types.VARCHAR));
        
        this.declareParameter(new SqlOutParameter(PO_FLG_EXITO, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_CALCULOS, oracle.jdbc.OracleTypes.CURSOR, (ResultSet rs, int numRow) -> {
            ArrendamientoDetalleResponse datos = new ArrendamientoDetalleResponse();
            
            try {                
                datos.setComisionAgentePesos(rs.getBigDecimal("COMI_AGTE_PESOS"));
                datos.setComisionHocelotPesos(rs.getBigDecimal("COMI_HOCELOT_PESOS"));
                datos.setComisionInmobiliariaPesos(rs.getBigDecimal("COMI_INMOBILIARIA_PESOS"));
                datos.setComisionSucursalPesos(rs.getBigDecimal("COMI_SUC_PESOS"));
                datos.setDerechosPoliza(rs.getBigDecimal("DERECHOS_POLIZA"));
                datos.setGastosInvestigacion(rs.getBigDecimal("GASTOS_INVES"));
                datos.setIvaRecibo(rs.getBigDecimal("IVA_RECIBO"));
                datos.setMeses(rs.getInt("MESES"));
                datos.setPctComisionAgente(rs.getBigDecimal("PCT_COMI_AGTE"));
                datos.setPctComisionHocelot(rs.getBigDecimal("PCT_COMI_HOCELOT"));
                datos.setPctComisionInmobiliario(rs.getBigDecimal("PCT_COMI_INMOBILIARIO"));
                datos.setPctComisionSucursal(rs.getBigDecimal("PCT_COMI_SUCURSAL"));
                datos.setPctGa(rs.getBigDecimal("PCT_GA"));
                datos.setPctPrima(rs.getBigDecimal("PCT_PRIMA"));
                datos.setPrimaPesos(rs.getBigDecimal("PRIMA_PESOS"));
                datos.setPrimaTarifa(rs.getBigDecimal("PRIMA_DE_TARIFA"));
                datos.setSubtotalRecibo(rs.getBigDecimal("SUBTOTAL_RECIBO"));
                datos.setTotalCertificadoPesos(rs.getBigDecimal("TOTAL_CERTIFIADO_PESOS"));
                datos.setTotalRecibo(rs.getBigDecimal("TOTAL_RECIBO"));
                
                return datos;
            } catch (Exception ex) {
                this.log.error("Error:" + sql + ":" + ex.getMessage(), ex);
                return null;
            }
        }));
        
        try {
            this.compile();
        } catch (Exception ex) {
            this.log.error("Error ArrendaCotizaAsertaService:compile:" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Consumir el SP
     * @param arrenda
     * @return 
     */
    public ArrendamientoResponse execute(ArrendamientoVO arrenda) {
        this.log.debug("\t:::::::: " + sql + " :::::");
        
        ArrendamientoResponse resp = new ArrendamientoResponse();
        
        if (arrenda == null) {
            resp.setFlgExito(Util.SPS_ERROR);
            resp.setMensaje("Debe indicar los datos de entrada.");
            return resp;
        }
        
        log.debug("\tmontoRenta:" + arrenda.getMontoRenta());
        log.debug("\tesquema:" + arrenda.getEsquema());
        log.debug("\tmeses:" + arrenda.getMeses());
        log.debug("\tbucket:" + arrenda.getBucket());
        
        Map<String, Object> input = new HashMap<>();
        try {
            input.put(PI_MONTO_RENTA, arrenda.getMontoRenta());
            input.put(PI_ESQUEMA, arrenda.getEsquema());
            input.put(PI_BUCKET, arrenda.getBucket());
            input.put(PI_MESES, arrenda.getMeses());
            input.put(PI_USR_LOGIN, Util.SIEBEL);
            input.put(PI_LOG_ID_SIEBEL, "1");
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG_EXITO) == null) {
                resp.setFlgExito(Util.SPS_ERROR);
                resp.setMensaje("Ocurrió un error al ejecutar el servicio:" + sql);
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            if (Util.SPS_EXITO.equalsIgnoreCase((String) output.get(PO_FLG_EXITO))) {
                resp.setFlgExito(Util.SPS_EXITO);
                
                if (output.get(PO_CALCULOS) != null
                        && !((List<ArrendamientoDetalleResponse>) output.get(PO_CALCULOS)).isEmpty()) {
                    List<ArrendamientoDetalleResponse> listaDatos = (List<ArrendamientoDetalleResponse>) output.get(PO_CALCULOS);
                    
                    if (listaDatos != null && !listaDatos.isEmpty()) {
                        List<ArrendamientoDetalleResponseV2> listaDetalleV2 = new ArrayList<>();
                        ArrendamientoDetalleResponseV2 rdetv2;
                        
                        for (ArrendamientoDetalleResponse rdet : listaDatos) {
                            rdetv2 = new ArrendamientoDetalleResponseV2();
                            rdetv2.setMeses(rdet.getMeses());
                            rdetv2.setMonto(rdet.getTotalRecibo());
                            
                            listaDetalleV2.add(rdetv2);
                        }
                        
                        resp.setDetalle(listaDetalleV2);
                    }
                }
            } else {
                resp.setFlgExito(Util.SPS_ERROR);
            }
            
        } catch (Exception ex) {
            this.log.error("Error:" + sql + ":" + ex.getMessage(), ex);
            resp.setFlgExito(Util.SPS_ERROR);
            resp.setMensaje("Error en el servicio:" + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
