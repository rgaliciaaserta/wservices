/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoSiniestroReporteVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class NowoSiniestroRepService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    private static final String PI_ID = "PI_ID";
    private static final String PI_PROP_NOMBRE = "PI_PROP_NOMBRE";
    private static final String PI_PROP_EMAIL = "PI_PROP_EMAIL";
    private static final String PI_PROP_TEL = "PI_PROP_TEL";
    private static final String PI_INQ_NOMBRE = "PI_INQ_NOMBRE";
    private static final String PI_NUM_POLIZA = "PI_NUM_POLIZA";
    private static final String PI_TIPO_SINIESTRO = "PI_TIPO_SINIESTRO";
    private static final String PI_PROBLEMA = "PI_PROBLEMA";
    private static final String PO_FLG = "PO_FLG";
    private static final String PO_MSG = "PO_MSG";
    private static final String PO_ID = "PO_ID";
    
    static {
        sql = getSQL("pkg.afiandwh.nowo_regpersona", "SP_SINIESTRO_REP");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public NowoSiniestroRepService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_PROP_NOMBRE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_PROP_EMAIL, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_PROP_TEL, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_INQ_NOMBRE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_NUM_POLIZA, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_TIPO_SINIESTRO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_PROBLEMA, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_ID, Types.NUMERIC));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consumir el SP
     * @param reg
     * @return 
     */
    public RespuestaJson execute(NowoSiniestroReporteVO reg) {
        log.debug("\tNowoSiniestroRepService.execute");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (reg == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        
        try {
            input.put(PI_ID, null);
            input.put(PI_PROP_NOMBRE, (reg.getPropietario() == null)? null:reg.getPropietario().getNombre());
            input.put(PI_PROP_EMAIL, (reg.getPropietario() == null)? null:reg.getPropietario().getEmail());
            input.put(PI_PROP_TEL, (reg.getPropietario() == null)? null:reg.getPropietario().getTelefono());
            input.put(PI_INQ_NOMBRE, (reg.getSiniestro() == null)? null:reg.getSiniestro().getNombreInquilino());
            input.put(PI_NUM_POLIZA, (reg.getSiniestro() == null)? null:reg.getSiniestro().getNumeroPoliza());
            input.put(PI_TIPO_SINIESTRO, (reg.getSiniestro() == null)? null:reg.getSiniestro().getTipoSiniestro());
            input.put(PI_PROBLEMA, (reg.getSiniestro() == null)? null:reg.getSiniestro().getProblema());
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tEl servicio no respondió como se esperaba.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("El servicio no respondió como se esperaba.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String)output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                resp.setDatos((BigDecimal) output.get(PO_ID));
                
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tPO_FLG:" + resp.getCodigo());
            log.debug("\tPO_MSG:" + resp.getMensaje());
            log.debug("\tPO_ID:" + resp.getDatos());
            
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio " + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
