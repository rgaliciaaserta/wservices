/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoDocumentoVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class RegDocumentoService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    private static final String PI_REGPERSONA_ID = "PI_REGPERSONA_ID";
    private static final String PI_TIPO = "PI_TIPO";
    private static final String PI_NOMBRE = "PI_NOMBRE";
    private static final String PI_FORMATO = "PI_FORMATO";
    private static final String PI_DOCUMENTO = "PI_DOCUMENTO";
    private static final String PO_FLG = "PO_FLG";
    private static final String PO_MSG = "PO_MSG";
    
    static {
        sql = getSQL("pkg.afiandwh.nowo_regpersona", "SP_REGDOCUMENTO");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public RegDocumentoService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_REGPERSONA_ID, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_TIPO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_NOMBRE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_FORMATO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_DOCUMENTO, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consumir el SP
     * @param idPersona
     * @param reg
     * @return 
     */
    public RespuestaJson execute(BigDecimal idPersona, NowoDocumentoVO reg) {
        log.debug("\tRegDocumentoService.execute");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (reg == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        
        try {
            input.put(PI_REGPERSONA_ID, idPersona);
            input.put(PI_TIPO, reg.getTipo());
            input.put(PI_NOMBRE, reg.getNombre());
            input.put(PI_FORMATO, reg.getFormato());
            input.put(PI_DOCUMENTO, reg.getDocumento());
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tEl servicio no respondió como se esperaba.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("El servicio no respondió como se esperaba.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String)output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tPO_FLG:" + resp.getCodigo());
            log.debug("\tPO_MSG:" + resp.getMensaje());
            
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio " + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
