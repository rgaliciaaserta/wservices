/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import static mx.aserta.wservices.sps.StoredProcedureBase.getSQL;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class GetDatosBovAsertaService extends StoredProcedureBase {
    
    private static String sql = "SIEBEL.?";
    
    private static final String PI_POLIZAID = "PI_POLIZAID";
    
    private static final String PO_FLG = "PO_FLG";
    private static final String PO_MSG = "PO_MSG";
    private static final String PO_DATOS = "PO_DATOS";
    
    static {
        sql = getSQL("pkg.nowoconf", "SP_GET_DATOS_BOV");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public GetDatosBovAsertaService(@Qualifier("dataSourceSiebelAserta") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_POLIZAID, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_DATOS, oracle.jdbc.OracleTypes.CURSOR, (ResultSet rs, int numRow) -> {
            Map<String, String> datos = new LinkedHashMap(30, 0.7f, false);
            
            try {
                ResultSetMetaData rsmd = rs.getMetaData();
                
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    datos.put(Util.DATO + i, rs.getString(i));
                }
                
                return datos;
            } catch (Exception ex) {
                ;
            }
            
            return null;
        }));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consumir el SP
     * @param idPoliza
     * @return 
     */
    public RespuestaJson execute(String idPoliza) {
        log.debug("\tGetDatosBovAsertaService.execute");
        
        log.debug("\tidPoliza:" + idPoliza);
        RespuestaJson resp = new RespuestaJson();
        
        if (idPoliza == null || idPoliza.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el id de la póliza.");
            
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        
        try {
            input.put(PI_POLIZAID, idPoliza);
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tEl servicio no respondió como se esperaba.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("El servicio no respondió como se esperaba.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String)output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                
                if (output.get(PO_DATOS) != null) {
                    resp.setDatos((List<Map<String, String>>) output.get(PO_DATOS));
                }
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tFLG:" + resp.getCodigo());
            log.debug("\tMSG:" + resp.getMensaje());
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio " + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
