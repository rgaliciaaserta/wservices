/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoProcessVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class LogBitacoraService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    public static final String PI_ID = "PI_ID";
    public static final String PI_ID_POLIZA = "PI_ID_POLIZA";
    public static final String PI_TIPO_PAQ = "PI_TIPO_PAQ";
    public static final String PI_ESTATUS = "PI_ESTATUS";
    public static final String PI_PATH_FILE = "PI_PATH_FILE";
    public static final String PI_MESSAGE = "PI_MESSAGE";
    public static final String PI_USUARIO = "PI_USUARIO";
    public static final String PO_FLG = "PO_FLG";
    public static final String PO_MSG = "PO_MSG";
    public static final String PO_ID = "PO_ID";
    
    static {
        sql = getSQL("pkg.afiandwh.nowo_conf", "SP_LOG_BITACORA");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public LogBitacoraService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_ID_POLIZA, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_TIPO_PAQ, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_ESTATUS, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_PATH_FILE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_MESSAGE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_USUARIO, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_ID, Types.NUMERIC));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("\tError compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consume the SP
     * @param process
     * @return 
     */
    public RespuestaJson execute(NowoProcessVO process) {
        log.debug("\tLogBitacoraService.execute");
        
        RespuestaJson resp = new RespuestaJson();
        if (process == null) {
            log.debug("\tDebe indicar el objeto de entrada.");
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        log.debug("\tPI_ID:" + process.getId());
        try {
            input.put(PI_ID, process.getId());
            input.put(PI_ID_POLIZA, process.getIdPoliza());
            input.put(PI_TIPO_PAQ, process.getTipoPaquete());
            input.put(PI_ESTATUS, process.getEstatus());
            input.put(PI_PATH_FILE, process.getPathFile());
            input.put(PI_MESSAGE, process.getMessage());
            input.put(PI_USUARIO, process.getUsuario());
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tError al consumir el servicio:" + sql);
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("Error al consumir el servicio:" + sql);
                return resp;
            }
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String) output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                
                try {
                    process.setId((output.get(PO_ID) == null)? null:((BigDecimal) output.get(PO_ID)).longValue());
                } catch (Exception e) {
                    log.error("Error transform:" + e.getMessage());
                }
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tPO_FLG:" + resp.getCodigo());
            log.debug("\tPO_MSG:" + resp.getMensaje());
            log.debug("\tPO_ID:" + process.getId());
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio:" + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
