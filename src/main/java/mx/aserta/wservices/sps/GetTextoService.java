/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;
import mx.aserta.wservices.vo.RespuestaJson;

/**
 *
 * @author rgalicia
 */
@Repository
public class GetTextoService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?.SP_GET_TEXTO";

    public static final String PI_ID = "PI_ID";
    
    public static final String PO_FLG = "PO_FLG";
    public static final String PO_MSG = "PO_MSG";
    public static final String PO_DATOS = "PO_DATOS";
    
    static {
        sql = getSQL("bd.pkgtextos", "SP_GET_TEXTO");
    }
    
    //private int i, totalColumnas;
    
    //private ResultSetMetaData rsmd;
    
    private Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public GetTextoService(@Qualifier("dataSourceAfiandwh") DataSource ds){
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_DATOS, oracle.jdbc.OracleTypes.CURSOR, (ResultSet rs, int numRow) -> {
            Map<String, String> datos = new LinkedHashMap<>(30, 0.7f, false);
            
            try {
                //this.rsmd = rs.getMetaData();
                ResultSetMetaData rsmd = rs.getMetaData();
                int totalColumnas = rsmd.getColumnCount();
                /*if (numRow <= 1) {
                    this.totalColumnas = this.rsmd.getColumnCount();
                }*/
                
                //for (this.i = 1; this.i <= this.totalColumnas; this.i++) {
                for(int i = 1; i <= totalColumnas;i++) {
                    //datos.put(Util.DATO + this.i, rs.getString(this.i));
                    datos.put(Util.DATO + i, rs.getString(i));
                }
                
                return datos;
            } catch (Exception ex) {
                log.error("Conversion error:" + ((ex == null || ex.getMessage() == null)? null:ex.getMessage().substring(100)), ex);
            }
            
            return null;
        }));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + ex.getMessage());
        }
    }
    
    /**
     * Consumo del SP
     * @param id
     * @return 
     */
    public RespuestaJson execute(String id) {
        log.debug("\tGetTextoService");
        
        log.debug("Plantilla:" + id);
        RespuestaJson resp = new RespuestaJson();
        
        Map<String, Object> input = new HashMap<>();
        try {
            input.put(PI_ID, id);
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.error("Error al consumir el sp " + sql);
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("Error al consumir el sp " + sql);
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? "":(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String) output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                if (output.get(PO_DATOS) != null) {
                    resp.setDatos((List<LinkedHashMap<String, Object>>) output.get(PO_DATOS));
                }
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tRespuesta sql:" + sql + ":" + resp.getCodigo());
        } catch (Exception ex) {
            log.error("Error sql:" + sql + ":" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error sql:" + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
