/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class GetSerieAsertaService extends StoredProcedureBase {
    
    private static String sql = "SIEBEL.?";
    
    private static final String PI_POLIZAID = "PI_POLIZAID";
    
    private static final String PO_FLG = "PO_FLG";
    private static final String PO_MSG = "PO_MSG";
    private static final String PO_SERIEPOL = "PO_SERIEPOL";
    private static final String PO_SERIEREC = "PO_SERIEREC";
    
    static {
        sql = getSQL("pkg.nowoconf", "SP_GET_SERIE");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public GetSerieAsertaService(@Qualifier("dataSourceSiebelAserta") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_POLIZAID, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_SERIEPOL, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_SERIEREC, Types.VARCHAR));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consumir el SP
     * @param idPoliza
     * @return 
     */
    public RespuestaJson execute(String idPoliza) {
        log.debug("\tGetSerieAsertaService.execute");
        
        log.debug("\tidPoliza:" + idPoliza);
        RespuestaJson resp = new RespuestaJson();
        
        if (idPoliza == null || idPoliza.isEmpty()) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el id de la póliza.");
            
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        
        try {
            input.put(PI_POLIZAID, idPoliza);
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tEl servicio no respondió como se esperaba.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("El servicio no respondió como se esperaba.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            List<String> serieList = new ArrayList<>();
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String)output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                
                serieList.add((output.get(PO_SERIEPOL) == null)? "":(String) output.get(PO_SERIEPOL));
                serieList.add((output.get(PO_SERIEREC) == null)? "":(String) output.get(PO_SERIEREC));
                resp.setDatos(serieList);
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tFLG:" + resp.getCodigo());
            log.debug("\tMSG:" + resp.getMensaje());
            log.debug("\tSERIE:" + resp.getDatos());
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio " + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
