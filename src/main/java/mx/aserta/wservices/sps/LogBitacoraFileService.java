/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import static mx.aserta.wservices.sps.StoredProcedureBase.getSQL;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoFileProcessVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class LogBitacoraFileService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    public static final String PI_ID = "PI_ID";
    public static final String PI_ID_PROC = "PI_ID_PROC";
    public static final String PI_PATH_FILE = "PI_PATH_FILE";
    public static final String PI_ESTATUS = "PI_ESTATUS";
    public static final String PI_MESSAGE = "PI_MESSAGE";
    public static final String PI_RFC_EMISOR = "PI_RFC_EMISOR";
    public static final String PI_ID_POLIZA = "PI_ID_POLIZA";
    public static final String PI_ID_MOVIMIENTO = "PI_ID_MOVIMIENTO";
    public static final String PI_TIPO_DOCUMENTO = "PI_TIPO_DOCUMENTO";
    public static final String PI_SERIE = "PI_SERIE";
    public static final String PI_FOLIO = "PI_FOLIO";
    public static final String PI_FORMATO = "PI_FORMATO";
    public static final String PI_USUARIO = "PI_USUARIO";
    public static final String PO_FLG = "PO_FLG";
    public static final String PO_MSG = "PO_MSG";
    public static final String PO_ID = "PO_ID";
    
    static {
        sql = getSQL("pkg.afiandwh.nowo_conf", "SP_LOG_BITACORA_FILE");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public LogBitacoraFileService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_ID_PROC, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_PATH_FILE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_ESTATUS, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_MESSAGE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RFC_EMISOR, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_ID_POLIZA, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_ID_MOVIMIENTO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_TIPO_DOCUMENTO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_SERIE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_FOLIO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_FORMATO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_USUARIO, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_ID, Types.NUMERIC));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consume the SP
     * @param process
     * @return 
     */
    public RespuestaJson execute(NowoFileProcessVO process) {
        log.debug("\tLogBitacoraFileService.execute");
        
        RespuestaJson resp = new RespuestaJson();
        if (process == null) {
            log.debug("\tDebe indicar el objeto de entrada.");
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        log.debug("\tPI_ID:" + process.getId());
        log.debug("\tPI_ID_PROC:" + process.getNowoProcessId());
        try {
            input.put(PI_ID, process.getId());
            input.put(PI_ID_PROC, process.getNowoProcessId());
            input.put(PI_PATH_FILE, process.getPathFile());
            input.put(PI_ESTATUS, process.getEstatus());
            input.put(PI_MESSAGE, process.getMessage());
            input.put(PI_RFC_EMISOR, process.getRfcEmisor());
            input.put(PI_ID_POLIZA, process.getIdPoliza());
            input.put(PI_ID_MOVIMIENTO, process.getIdMovimiento());
            input.put(PI_TIPO_DOCUMENTO, process.getTipoDocumento());
            input.put(PI_SERIE, process.getSerie());
            input.put(PI_FOLIO, process.getFolio());
            input.put(PI_FORMATO, process.getFormato());
            input.put(PI_USUARIO, process.getUsuario());
    
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tError al consumir el servicio:" + sql);
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("Error al consumir el servicio:" + sql);
                return resp;
            }
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String) output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                
                try {
                    process.setId((output.get(PO_ID) == null)? null:((BigDecimal) output.get(PO_ID)).longValue());
                } catch (Exception e) {
                    log.error("Error transform:" + e.getMessage());
                }
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tPO_FLG:" + resp.getCodigo());
            log.debug("\tPO_MSG:" + resp.getMensaje());
            log.debug("\tPO_ID:" + process.getId());
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio:" + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
