/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.LogVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class LogService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    public static final String PI_ID = "PI_ID";
    public static final String PI_PARAMS = "PI_PARAMS";
    public static final String PI_TARGET = "PI_TARGET";
    public static final String PI_ESTATUS = "PI_ESTATUS";
    public static final String PI_MSG = "PI_MSG";
    public static final String PI_USUARIO_CARGA = "PI_USUARIO_CARGA";
    public static final String PO_FLG = "PO_FLG";
    public static final String PO_MSG = "PO_MSG";
    
    static {
        sql = getSQL("pkg.stp_cnfg", "SP_LOG");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public LogService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_PARAMS, Types.CLOB));
        this.declareParameter(new SqlParameter(PI_TARGET, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_ESTATUS, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_MSG, Types.CLOB));
        this.declareParameter(new SqlParameter(PI_USUARIO_CARGA, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage(), ex);
        }
    }
    
    /**
     * Consumir el SP
     * @param logvo
     * @return 
     */
    public RespuestaJson execute(LogVO logvo) {
        log.debug("\t\tLogService:" + sql);
        
        RespuestaJson resp = new RespuestaJson();
        
        try {
            Map<String, Object> input = new HashMap<>();
            
            input.put(PI_ID, logvo.getId());
            input.put(PI_PARAMS, logvo.getParams().toString());
            input.put(PI_TARGET, logvo.getTarget());
            input.put(PI_ESTATUS, logvo.getEstatus());
            input.put(PI_MSG, logvo.getMsg().toString());
            input.put(PI_USUARIO_CARGA, logvo.getUsuario());
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tError:El servicio no respondió como se esperaba:" + sql);
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje(Util.MSG_ERROR);
                
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? "":(String) output.get(PO_MSG));
            log.debug("\t\tRespuesta:" + output.get(PO_FLG));
            log.debug("\t\tMensaje:" + output.get(PO_MSG));
            
            resp.setCodigo((String) output.get(PO_FLG));
            
        } catch (Exception ex) {
            log.error("Error:tGetResponseService:" + sql + ":" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al ejecutar el servicio:" + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
