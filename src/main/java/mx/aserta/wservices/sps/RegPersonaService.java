/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.sps;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoPersonaFisicaVO;
import mx.aserta.wservices.vo.NowoPersonaMoralVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rgalicia
 */
@Repository
public class RegPersonaService extends StoredProcedureBase {
    
    private static String sql = "AFIANDWH.?";
    
    private static final String PI_ID = "PI_ID";
    private static final String PI_TIPO_PERSONA = "PI_TIPO_PERSONA";
    private static final String PI_NOMBRERAZON = "PI_NOMBRERAZON";
    private static final String PI_APELLIDOPAT = "PI_APELLIDOPAT";
    private static final String PI_APELLIDOMAT = "PI_APELLIDOMAT";
    private static final String PI_RFC = "PI_RFC";
    private static final String PI_CURP = "PI_CURP";
    private static final String PI_GIRO = "PI_GIRO";
    private static final String PI_EMAIL = "PI_EMAIL";
    private static final String PI_TELEFONO = "PI_TELEFONO";
    private static final String PI_NUMERO_AGENTE = "PI_NUMERO_AGENTE";
    private static final String PI_RL_NOMBRE = "PI_RL_NOMBRE";
    private static final String PI_RL_APELLIDOPAT = "PI_RL_APELLIDOPAT";
    private static final String PI_RL_APELLIDOMAT = "PI_RL_APELLIDOMAT";
    private static final String PI_RL_EMAIL = "PI_RL_EMAIL";
    private static final String PI_RL_TELEFONO = "PI_RL_TELEFONO";
    private static final String PI_CC_NOMBRE = "PI_CC_NOMBRE";
    private static final String PI_CC_APELLIDOPAT = "PI_CC_APELLIDOPAT";
    private static final String PI_CC_APELLIDOMAT = "PI_CC_APELLIDOMAT";
    private static final String PI_CC_EMAIL = "PI_CC_EMAIL";
    private static final String PI_CC_TELEFONO = "PI_CC_TELEFONO";
    private static final String PI_CA_NOMBRE = "PI_CA_NOMBRE";
    private static final String PI_CA_APELLIDOPAT = "PI_CA_APELLIDOPAT";
    private static final String PI_CA_APELLIDOMAT = "PI_CA_APELLIDOMAT";
    private static final String PI_CA_EMAIL = "PI_CA_EMAIL";
    private static final String PI_CA_TELEFONO = "PI_CA_TELEFONO";
    private static final String PI_ACEPTO_AVP = "PI_ACEPTO_AVP";
    private static final String PI_ACEPTO_TYC = "PI_ACEPTO_TYC";
    private static final String PI_ACEPTO_CCP = "PI_ACEPTO_CCP";
    private static final String PO_FLG = "PO_FLG";
    private static final String PO_MSG = "PO_MSG";
    private static final String PO_ID = "PO_ID";
    
    static {
        sql = getSQL("pkg.afiandwh.nowo_regpersona", "SP_REGPERSONA");
    }
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public RegPersonaService(@Qualifier("dataSourceAfiandwh") DataSource ds) {
        super(ds, sql);
        
        this.declareParameter(new SqlParameter(PI_ID, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_TIPO_PERSONA, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_NOMBRERAZON, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_APELLIDOPAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_APELLIDOMAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RFC, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CURP, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_GIRO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_EMAIL, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_TELEFONO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_NUMERO_AGENTE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RL_NOMBRE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RL_APELLIDOPAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RL_APELLIDOMAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RL_EMAIL, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_RL_TELEFONO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CC_NOMBRE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CC_APELLIDOPAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CC_APELLIDOMAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CC_EMAIL, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CC_TELEFONO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CA_NOMBRE, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CA_APELLIDOPAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CA_APELLIDOMAT, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CA_EMAIL, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_CA_TELEFONO, Types.VARCHAR));
        this.declareParameter(new SqlParameter(PI_ACEPTO_AVP, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_ACEPTO_TYC, Types.NUMERIC));
        this.declareParameter(new SqlParameter(PI_ACEPTO_CCP, Types.NUMERIC));
        this.declareParameter(new SqlOutParameter(PO_FLG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_MSG, Types.VARCHAR));
        this.declareParameter(new SqlOutParameter(PO_ID, Types.NUMERIC));
        
        try {
            this.compile();
        } catch (Exception ex) {
            log.error("Error compile:" + sql + ":" + ex.getMessage());
        }
    }
    
    /**
     * Consumir el SP pf
     * @param reg
     * @return 
     */
    public RespuestaJson execute(NowoPersonaFisicaVO reg) {
        log.debug("\tRegPersonaService.execute pf");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (reg == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        
        try {
            input.put(PI_ID, null);
            input.put(PI_TIPO_PERSONA, Util.PERSONA_FISICA);
            input.put(PI_NOMBRERAZON, reg.getNombre());
            input.put(PI_APELLIDOPAT, reg.getApellidoPaterno());
            input.put(PI_APELLIDOMAT, reg.getApellidoMaterno());
            input.put(PI_RFC, reg.getRfc());
            input.put(PI_CURP, reg.getCurp());
            input.put(PI_GIRO, reg.getGiro());
            input.put(PI_EMAIL, reg.getEmail());
            input.put(PI_TELEFONO, reg.getTelefono());
            input.put(PI_NUMERO_AGENTE, reg.getNumeroAgente());
            input.put(PI_RL_NOMBRE, null);
            input.put(PI_RL_APELLIDOPAT, null);
            input.put(PI_RL_APELLIDOMAT, null);
            input.put(PI_RL_EMAIL, null);
            input.put(PI_RL_TELEFONO, null);
            input.put(PI_CC_NOMBRE, null);
            input.put(PI_CC_APELLIDOPAT, null);
            input.put(PI_CC_APELLIDOMAT, null);
            input.put(PI_CC_EMAIL, null);
            input.put(PI_CC_TELEFONO, null);
            input.put(PI_CA_NOMBRE, null);
            input.put(PI_CA_APELLIDOPAT, null);
            input.put(PI_CA_APELLIDOMAT, null);
            input.put(PI_CA_EMAIL, null);
            input.put(PI_CA_TELEFONO, null);
            input.put(PI_ACEPTO_AVP, (reg.isAceptoAvisoPrivacidad())? BigDecimal.ONE:BigDecimal.ZERO);
            input.put(PI_ACEPTO_TYC, (reg.isAceptoTerminosCondiciones())? BigDecimal.ONE:BigDecimal.ZERO);
            input.put(PI_ACEPTO_CCP, (reg.isAceptoComunicacionComercial())? BigDecimal.ONE:BigDecimal.ZERO);
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tEl servicio no respondió como se esperaba.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("El servicio no respondió como se esperaba.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String)output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                resp.setDatos((BigDecimal) output.get(PO_ID));
                
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tPO_FLG:" + resp.getCodigo());
            log.debug("\tPO_MSG:" + resp.getMensaje());
            log.debug("\tPO_ID:" + resp.getDatos());
            
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio " + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
    
    /**
     * Consumir el SP pm
     * @param reg
     * @return 
     */
    public RespuestaJson execute(NowoPersonaMoralVO reg) {
        log.debug("\tRegPersonaService.execute pm");
        
        RespuestaJson resp = new RespuestaJson();
        
        if (reg == null) {
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Debe indicar el objeto de entrada.");
            
            return resp;
        }
        
        Map<String, Object> input = new HashMap<>();
        
        try {
            input.put(PI_ID, null);
            input.put(PI_TIPO_PERSONA, Util.PERSONA_MORAL);
            input.put(PI_NOMBRERAZON, reg.getDenominacionSocial());
            input.put(PI_APELLIDOPAT, null);
            input.put(PI_APELLIDOMAT, null);
            input.put(PI_RFC, reg.getRfc());
            input.put(PI_CURP, null);
            input.put(PI_GIRO, null);
            input.put(PI_EMAIL, null);
            input.put(PI_TELEFONO, null);
            input.put(PI_NUMERO_AGENTE, reg.getNumeroAgente());
            input.put(PI_RL_NOMBRE, (reg.getRepresentanteLegal() == null)?
                    null:reg.getRepresentanteLegal().getNombre());
            input.put(PI_RL_APELLIDOPAT, (reg.getRepresentanteLegal() == null)?
                    null:reg.getRepresentanteLegal().getApellidoPaterno());
            input.put(PI_RL_APELLIDOMAT, (reg.getRepresentanteLegal() == null)?
                    null:reg.getRepresentanteLegal().getApellidoMaterno());
            input.put(PI_RL_EMAIL, (reg.getRepresentanteLegal() == null)?
                    null:reg.getRepresentanteLegal().getEmail());
            input.put(PI_RL_TELEFONO, (reg.getRepresentanteLegal() == null)?
                    null:reg.getRepresentanteLegal().getTelefono());
            input.put(PI_CC_NOMBRE, (reg.getContactoComercial() == null)?
                    null:reg.getContactoComercial().getNombre());
            input.put(PI_CC_APELLIDOPAT, (reg.getContactoComercial() == null)?
                    null:reg.getContactoComercial().getApellidoPaterno());
            input.put(PI_CC_APELLIDOMAT, (reg.getContactoComercial() == null)?
                    null:reg.getContactoComercial().getApellidoMaterno());
            input.put(PI_CC_EMAIL, (reg.getContactoComercial() == null)?
                    null:reg.getContactoComercial().getEmail());
            input.put(PI_CC_TELEFONO, (reg.getContactoComercial() == null)?
                    null:reg.getContactoComercial().getTelefono());
            input.put(PI_CA_NOMBRE, (reg.getContactoAdministrativo() == null)?
                    null:reg.getContactoAdministrativo().getNombre());
            input.put(PI_CA_APELLIDOPAT, (reg.getContactoAdministrativo() == null)?
                    null:reg.getContactoAdministrativo().getApellidoPaterno());
            input.put(PI_CA_APELLIDOMAT, (reg.getContactoAdministrativo() == null)?
                    null:reg.getContactoAdministrativo().getApellidoMaterno());
            input.put(PI_CA_EMAIL, (reg.getContactoAdministrativo() == null)?
                    null:reg.getContactoAdministrativo().getEmail());
            input.put(PI_CA_TELEFONO, (reg.getContactoAdministrativo() == null)?
                    null:reg.getContactoAdministrativo().getTelefono());
            input.put(PI_ACEPTO_AVP, (reg.isAceptoAvisoPrivacidad())? BigDecimal.ONE:BigDecimal.ZERO);
            input.put(PI_ACEPTO_TYC, (reg.isAceptoTerminosCondiciones())? BigDecimal.ONE:BigDecimal.ZERO);
            input.put(PI_ACEPTO_CCP, (reg.isAceptoComunicacionComercial())? BigDecimal.ONE:BigDecimal.ZERO);
            
            Map<String, Object> output = super.execute(input);
            
            if (output == null || output.get(PO_FLG) == null) {
                log.debug("\tEl servicio no respondió como se esperaba.");
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("El servicio no respondió como se esperaba.");
                return resp;
            }
            
            resp.setMensaje((output.get(PO_MSG) == null)? null:(String) output.get(PO_MSG));
            
            if (Util.SPS_EXITO.equalsIgnoreCase((String)output.get(PO_FLG))) {
                resp.setCodigo(Util.COD_EXITO);
                resp.setDatos((BigDecimal) output.get(PO_ID));
                
            } else {
                resp.setCodigo(Util.COD_ERROR);
            }
            
            log.debug("\tPO_FLG:" + resp.getCodigo());
            log.debug("\tPO_MSG:" + resp.getMensaje());
            log.debug("\tPO_ID:" + resp.getDatos());
            
        } catch (Exception ex) {
            log.error("Error:" + sql + ":" + ex.getMessage());
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error al consumir el servicio " + sql + ":" + ex.getMessage());
        }
        
        return resp;
    }
}
