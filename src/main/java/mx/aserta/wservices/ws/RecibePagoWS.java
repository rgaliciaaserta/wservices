/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoPagoInsertaResponse;
import mx.aserta.wservices.vo.NowoPagoInsertaVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface RecibePagoWS {
    
    @WebResult(name = "recibePagoResponse", partName = "recibePagoResponse")
    NowoPagoInsertaResponse recibePago(
        @WebParam(name = "recibePagoRequest", partName = "recibePagoRequest") NowoPagoInsertaVO recibo);
}
