/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.ConsultaArchivoNowoService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ConsultaArchivoNowoResponseVO;
import mx.aserta.wservices.vo.ConsultaArchivoNowoVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.ConsultaArchivoNowoWS",
        serviceName = "ConsultaArchivoNowoWS")
public class ConsultaArchivoNowoWSImpl implements ConsultaArchivoNowoWS {
    
    @Autowired
    private ConsultaArchivoNowoService consultaArchivoNowoService;
    
    public Logger log = LogManager.getLogger("WSERVICES");

    @Override
    public ConsultaArchivoNowoResponseVO consultaArchivo(ConsultaArchivoNowoVO consulta) {
        log.debug("\t\t:::::::::::::::::::::: ConsultaArchivoNowoWS:consultaArchivo ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        ConsultaArchivoNowoResponseVO resp = new ConsultaArchivoNowoResponseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(consulta));
            
            if (consulta == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (alta.getEmpresa() == null || alta.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(alta.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(alta.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.consultaArchivoNowoService.consultaNowoAttachment(Util.INSURGENTES, consulta);
            
        } catch (Exception ex) {
            log.error("Error:WS:ConsultaArchivoNowoWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
