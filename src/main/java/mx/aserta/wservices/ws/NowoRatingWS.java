/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoRatingRequestVO;
import mx.aserta.wservices.vo.NowoRatingResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoRatingWS {
    
    @WebResult(name = "nowoRatingResponse", partName = "nowoRatingResponse")
    public NowoRatingResponseVO calculate(
            @WebParam(name = "nowoRatingRequest", partName = "nowoRatingRequest") NowoRatingRequestVO request);
}
