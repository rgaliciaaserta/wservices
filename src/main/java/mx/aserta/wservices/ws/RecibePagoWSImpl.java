/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.NowoPagoService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoPagoInsertaResponse;
import mx.aserta.wservices.vo.NowoPagoInsertaVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.RecibePagoWS",
        serviceName = "RecibePagoWS")
public class RecibePagoWSImpl implements RecibePagoWS {

    @Autowired
    private NowoPagoService nowoPagoService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public NowoPagoInsertaResponse recibePago(NowoPagoInsertaVO recibo) {
        log.debug("\t\t:::::::::::::::::::::: RecibePagoWS:recibePago ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        NowoPagoInsertaResponse resp = new NowoPagoInsertaResponse();
        
        try {
            log.debug(TransformObject.getStringFromObject(recibo));
            
            if (recibo == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (alta.getEmpresa() == null || alta.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(alta.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(alta.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            String msg = this.validaParametros(recibo);
            if (msg != null && !msg.isEmpty()) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje(msg);
                return resp;
            }
            
            return this.nowoPagoService.nowoPagoInserta(Util.INSURGENTES, recibo);
            
        } catch (Exception ex) {
            log.error("Error:WS:ClientesWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
    
    /**
     * Validar los parametros de entrada
     * @param recibo
     * @return 
     */
    private String validaParametros(NowoPagoInsertaVO recibo) {
        log.debug("\tValidando parametros:");
        
        StringBuilder str = new StringBuilder();
        
        if (recibo == null) {
            str.append("Debe indicar el objeto de entrada");
            return str.toString();
        }
        
        //Valida fecha ddMMyy
        String msgErr = Util.isFormat(recibo.getFecha(), Util.FORMAT_DDMMYY);
        if (msgErr != null && !msgErr.isEmpty()) {
            return msgErr;
        }
        
        //Valida hora:minuto
        msgErr = Util.isHour(recibo.getHora());
        if (msgErr != null && !msgErr.isEmpty()) {
            return msgErr;
        }
        
        //Valida monto
        msgErr = Util.isMonto((recibo.getMonto() == null)? "":(recibo.getMonto() + ""));
        if (msgErr != null && !msgErr.isEmpty()) {
            return msgErr;
        }
        
        return null;
    }
}
