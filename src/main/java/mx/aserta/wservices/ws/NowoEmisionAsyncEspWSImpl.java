/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoEmisionAsyncEspRequestVO;
import mx.aserta.wservices.vo.NowoEmisionAsyncEspResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService(endpointInterface = "mx.aserta.wservices.ws.NowoEmisionAsyncEspWS",
        serviceName = "NowoEmisionAsyncEspWS")
public class NowoEmisionAsyncEspWSImpl implements NowoEmisionAsyncEspWS {
    
    @WebResult(name = "nowoEmisionAsyncEspResponse", partName = "nowoEmisionAsyncEspResponse")
    public NowoEmisionAsyncEspResponseVO callbackEmisionAsyncEsp(
            @WebParam(name = "nowoEmisionAsyncEspRequest", partName = "nowoEmisionAsyncEspRequest")
            NowoEmisionAsyncEspRequestVO req) {
        return null;
    }
}
