/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoRentDecisionRequestVO;
import mx.aserta.wservices.vo.NowoRentDecisionResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoRentDecisionWS {
    
    @WebResult(name = "nowoRentDecisionResponse", partName = "nowoRentDecisionResponse")
    public NowoRentDecisionResponseVO rentDecision(
            @WebParam(name = "nowoRentDecisionRequest", partName = "nowoRentDecisionRequest")
                    NowoRentDecisionRequestVO datos
    );
}
