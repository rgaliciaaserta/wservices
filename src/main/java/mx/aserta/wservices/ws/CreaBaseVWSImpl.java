/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.NowoCreaBaseService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoCreaBaseResponseVO;
import mx.aserta.wservices.vo.NowoCreaBaseVO;
import mx.aserta.wservices.vo.NowoCreaBaseVVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.CreaBaseVWS",
        serviceName = "CreaBaseVWS")
public class CreaBaseVWSImpl implements CreaBaseVWS {
    
    @Autowired
    private NowoCreaBaseService nowoCreaBaseService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public NowoCreaBaseResponseVO emisionCertificado(NowoCreaBaseVVO base) {
        log.debug("\t\t:::::::::::::::::::::: CreaBaseVWS:emisionCertificado ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        NowoCreaBaseResponseVO resp = new NowoCreaBaseResponseVO();
        NowoCreaBaseVO req = new NowoCreaBaseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(base));
            req.setAgenteReferenciador(null);
            req.setBucket(null);
            req.setEsquema(base.getEsquema());
            req.setFechaInicio(base.getFechaInicio());
            req.setIdAsegurado(base.getIdAsegurado());
            req.setIdContratante(base.getIdContratante());
            req.setIdObligado(base.getIdObligado());
            req.setMeses(base.getMeses());
            req.setMoneda(base.getMoneda());
            req.setMontoRenta(base.getMontoRenta());
            req.setCodigoPropiedad(base.getCodigoPropiedad());
            
            if (base == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (base.getEmpresa() == null || base.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(base.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(base.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.nowoCreaBaseService.nowoCreaBase(Util.INSURGENTES, req);
            
        } catch (Exception ex) {
            log.error("Error:WS:ClientesVWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
