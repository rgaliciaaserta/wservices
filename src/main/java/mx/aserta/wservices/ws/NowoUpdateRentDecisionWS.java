/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoUpdateRentDecisionRequestVO;
import mx.aserta.wservices.vo.NowoUpdateRentDecisionResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoUpdateRentDecisionWS {
    
    @WebResult(name = "nowoUpdateRentDecisionResponse", partName = "nowoUpdateRentDecisionResponse")
    NowoUpdateRentDecisionResponseVO updateRentDecision(
            @WebParam(name = "nowoUpdateRentDecisionRequest", partName = "nowoUpdateRentDecisionRequest")
            NowoUpdateRentDecisionRequestVO updRequest
    );
}
