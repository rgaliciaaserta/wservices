/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.ConsultaDocumentoService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.BovedaVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
/*@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.BovedaWS",
        serviceName = "BovedaWS")
public class BovedaWSImpl implements BovedaWS {

    @Autowired
    private ConsultaDocumentoService consultaDocumentoService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public byte[] getPdf(String rfcEmisor,
            String idPoliza,
            String idMovimiento,
            String tipoDocumento,
            String serie,
            String folio,
            String formato) {
        
        log.debug("\tgetPdf");
        
        BovedaVO bov = new BovedaVO();
        bov.setFolio(folio);
        bov.setFormato(formato);
        bov.setIdMovimiento(idMovimiento);
        bov.setIdPoliza(idPoliza);
        bov.setRfcEmisor(rfcEmisor);
        bov.setSerie(serie);
        bov.setTipoDocumento(tipoDocumento);
        
        log.debug("Params:\n" + TransformObject.getStringFromObject(bov));
        
        return this.consultaDocumentoService.consultaServicioBoveda(bov, Util.GET_PDF);
    }
    
    @Override
    public byte[] getXml(String rfcEmisor,
            String idPoliza,
            String idMovimiento,
            String tipoDocumento,
            String serie,
            String folio) {
        
        log.debug("\tgetXml");
        
        BovedaVO bov = new BovedaVO();
        bov.setFolio(folio);
        bov.setIdMovimiento(idMovimiento);
        bov.setIdPoliza(idPoliza);
        bov.setRfcEmisor(rfcEmisor);
        bov.setSerie(serie);
        bov.setTipoDocumento(tipoDocumento);
        
        log.debug("Params:\n" + TransformObject.getStringFromObject(bov));
        
        return this.consultaDocumentoService.consultaServicioBoveda(bov, Util.GET_XML);
    }
    
    @Override
    public byte[] getPdfAcuse(String idMovimiento) {
        
        log.debug("\tgetPdfAcuse");
        
        BovedaVO bov = new BovedaVO();
        bov.setIdMovimiento(idMovimiento);
        
        log.debug("Params:\n" + TransformObject.getStringFromObject(bov));
        
        return this.consultaDocumentoService.consultaServicioBoveda(bov, Util.GET_PDF_ACUSE);
    }
    
    @Override
    public byte[] getXmlAcuse(String idMovimiento) {
        
        log.debug("\tgetXmlAcuse");
        
        BovedaVO bov = new BovedaVO();
        bov.setIdMovimiento(idMovimiento);
        
        log.debug("Params:\n" + TransformObject.getStringFromObject(bov));
        
        return this.consultaDocumentoService.consultaServicioBoveda(bov, Util.GET_XML_ACUSE);
    }
}*/
