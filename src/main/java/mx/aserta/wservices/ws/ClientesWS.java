/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.AltaClienteResponseVO;
import mx.aserta.wservices.vo.AltaClienteVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ClientesWS {
    
    @WebResult(name = "altaClienteResponse", partName = "altaClienteResponse")
    AltaClienteResponseVO altaCliente(
            @WebParam(name = "altaClienteRequest", partName = "altaClienteRequest") AltaClienteVO alta);
}
