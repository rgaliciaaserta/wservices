/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.NowoCancelacionService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoCancelacionResponseVO;
import mx.aserta.wservices.vo.NowoCancelacionVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.CancelacionWS",
        serviceName = "CancelacionWS")
public class CancelacionWSImpl implements CancelacionWS {
    
    @Autowired
    private NowoCancelacionService nowoCancelacionService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public NowoCancelacionResponseVO generaCancelacion(NowoCancelacionVO cancela) {
        log.debug("\t\t:::::::::::::::::::::: CancelacionWS:generaCancelacion ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        NowoCancelacionResponseVO resp = new NowoCancelacionResponseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(cancela));
            
            if (cancela == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (base.getEmpresa() == null || base.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(base.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(base.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.nowoCancelacionService.nowoCancelacion(Util.INSURGENTES, cancela);
            
        } catch (Exception ex) {
            log.error("Error:WS:CancelacionWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
