/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.ArrendamientoResponse;
import mx.aserta.wservices.vo.ArrendamientoVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ArrendamientoWS {
    
    @WebResult(name = "arrendamientoResponse", partName = "arrendamientoResponse")
    ArrendamientoResponse cotizaArrendamiento(
            @WebParam(name = "arrendamientoRequest", partName = "arrendamientoRequest") ArrendamientoVO arrenda);
}
