/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoCartaAceptacionRequestVO;
import mx.aserta.wservices.vo.NowoCartaAceptacionResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoCartaAceptacionWS {
    
    @WebResult(name = "nowoCartaAceptacionResponse", partName = "nowoCartaAceptacionResponse")
    public NowoCartaAceptacionResponseVO cartaAceptacion(
            @WebParam(name = "nowoCartaAceptacionRequest", partName = "nowoCartaAceptacionRequest") NowoCartaAceptacionRequestVO consulta);
}
