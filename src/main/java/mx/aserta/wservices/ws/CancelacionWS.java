/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoCancelacionResponseVO;
import mx.aserta.wservices.vo.NowoCancelacionVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface CancelacionWS {
    
    @WebResult(name = "cancelacionResponse", partName = "cancelacionResponse")
    public NowoCancelacionResponseVO generaCancelacion(
        @WebParam(name = "cancelacionRequest", partName = "cancelacionRequest") NowoCancelacionVO cancela);
}
