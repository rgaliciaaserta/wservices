/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.AltaClienteService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.AltaClienteResponseVO;
import mx.aserta.wservices.vo.AltaClienteVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.ClientesWS",
        serviceName = "ClientesWS")
public class ClientesWSImpl implements ClientesWS {

    @Autowired
    private AltaClienteService altaClienteService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public AltaClienteResponseVO altaCliente(AltaClienteVO alta) {
        log.debug("\t\t:::::::::::::::::::::: ClientesWS:altaCliente ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        AltaClienteResponseVO resp = new AltaClienteResponseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(alta));
            
            if (alta == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (alta.getEmpresa() == null || alta.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(alta.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(alta.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.altaClienteService.altaCliente(Util.INSURGENTES, alta);
            
        } catch (Exception ex) {
            log.error("Error:WS:ClientesWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMsgError("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
