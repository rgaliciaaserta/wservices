/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.ConsultaDocumentosResponseVO;
import mx.aserta.wservices.vo.ConsultaDocumentosVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ConsultaDocumentosWS {
    
    @WebResult(name = "consultaPaqueteResult", partName = "consultaPaqueteResult")
    public ConsultaDocumentosResponseVO consultaPaquete(
            @WebParam(name = "consultaPaqueteRequest", partName = "consultaPaqueteRequest") ConsultaDocumentosVO consulta);
}
