/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.ConsultaBuroService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.ConsultaXML;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.ConsultaXMLResponse;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.ConsultaBuroWS",
        serviceName = "ConsultaBuroWS")
public class ConsultaBuroWSImpl implements ConsultaBuroWS {
    
    @Autowired
    private ConsultaBuroService consultaBuroService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public ConsultaXMLResponse consultaBuroXml(ConsultaXML consulta) {
        log.debug("\t\t:::::::::::::::::::::: ConsultaBuroWS:consultaBuro ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        //RespuestaJson resp = new RespuestaJson();
        //RespuestaBC resp = new RespuestaBC();
        ConsultaXMLResponse resp = new ConsultaXMLResponse();
        //wsdl.buro.consulta.com.bc.ws.consulta.bean.WSConsultaServiceStub.ConsultaXMLResponseE resp = new wsdl.buro.consulta.com.bc.ws.consulta.bean.WSConsultaServiceStub.ConsultaXMLResponseE();
        
        try {
            log.debug(TransformObject.getStringFromObject(consulta));
            
            if (consulta == null) {
                
                //resp.setExito(Util.SPS_ERROR);
                //resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            resp.setReturn(this.consultaBuroService.consultaBuroXml(consulta.getConsulta()));
            
        } catch (Exception ex) {
            log.error("Error:WS:CancelacionWS:" + ex.getMessage(), ex);
            //resp.setExito(Util.SPS_ERROR);
            //resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
