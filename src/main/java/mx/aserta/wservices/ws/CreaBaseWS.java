/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoCreaBaseResponseVO;
import mx.aserta.wservices.vo.NowoCreaBaseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface CreaBaseWS {
    
    @WebResult(name = "emisionCertificadoRequest", partName = "emisionCertificadoRequest")
    NowoCreaBaseResponseVO emisionCertificado(
            @WebParam(name = "emisionCertificadoResponse", partName = "emisionCertificadoResponse") NowoCreaBaseVO base);
}
