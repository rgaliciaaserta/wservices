/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.NowoCartaAceptacionService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoCartaAceptacionRequestVO;
import mx.aserta.wservices.vo.NowoCartaAceptacionResponseVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author rgalicia
 */
@WebService(endpointInterface = "mx.aserta.wservices.ws.NowoCartaAceptacionWS",
        serviceName = "NowoCartaAceptacionWS")
public class NowoCartaAceptacionWSImpl implements NowoCartaAceptacionWS {

    @Autowired
    private NowoCartaAceptacionService nowoCartaAceptacionService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public NowoCartaAceptacionResponseVO cartaAceptacion(NowoCartaAceptacionRequestVO consulta) {
        log.debug("\t\t:::::::::::::::::::::: NowoCartaAceptacionWS:cartaAceptacion ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        NowoCartaAceptacionResponseVO resp = new NowoCartaAceptacionResponseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(consulta));
            
            if (consulta == null) {
                resp.setCodigo(Util.COD_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (base.getEmpresa() == null || base.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(base.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(base.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.nowoCartaAceptacionService.getPlantilla(Util.INSURGENTES, consulta);
            
        } catch (Exception ex) {
            log.error("Error:WS:NowoCartaAceptacionWS:" + ex.getMessage(), ex);
            resp.setCodigo(Util.COD_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
    
}
