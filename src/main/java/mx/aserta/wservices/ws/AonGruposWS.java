/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.AonGruposResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface AonGruposWS {
    
    /*@WebResult(name = "grupoPeniolesResponse", partName = "grupoPeniolesResponse")
    public GrupoPeniolesResponseVO grupoPenioles();
    
    @WebResult(name = "blackSmithResponse", partName = "blackSmithResponse")
    public BlackSmithResponseVO blackSmith();*/
    
    @WebResult(name = "aonGruposResponse", partName = "aonGruposResponse")
    public AonGruposResponseVO aonGrupos();
}
