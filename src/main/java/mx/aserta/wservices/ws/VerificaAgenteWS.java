/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoVerificaAgenteResponseVO;
import mx.aserta.wservices.vo.NowoVerificaAgenteVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface VerificaAgenteWS {
    
    @WebResult(name = "verificaAgenteResponse", partName = "verificaAgenteResponse")
    NowoVerificaAgenteResponseVO verificaAgente(
            @WebParam(name = "verificaAgenteRequest", partName = "verificaAgenteRequest") NowoVerificaAgenteVO verifica);
}
