/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.NowoEmisionService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.NowoEmisionResponseVO;
import mx.aserta.wservices.vo.NowoEmisionVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.EmisionWS",
        serviceName = "EmisionWS")
public class EmisionWSImpl implements EmisionWS {
    
    @Autowired
    private NowoEmisionService nowoEmisionService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public NowoEmisionResponseVO coreEmision(NowoEmisionVO emision) {
        log.debug("\t\t:::::::::::::::::::::: EmisionWS:coreEmision ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        NowoEmisionResponseVO resp = new NowoEmisionResponseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(emision));
            
            if (emision == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (base.getEmpresa() == null || base.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(base.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(base.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.nowoEmisionService.nowoEmision(Util.INSURGENTES, emision);
            
        } catch (Exception ex) {
            log.error("Error:WS:EmisionWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
