/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoCheckIdentityRequestVO;
import mx.aserta.wservices.vo.NowoCheckIdentityResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoCheckIdentityWS {
    
    @WebResult(name = "nowoCheckIdentityResponse", partName = "nowoCheckIdentityResponse")
    public NowoCheckIdentityResponseVO validateByParsedName(
            @WebParam(name = "nowoCheckIdentityRequest", partName = "nowoCheckIdentityRequest")
            NowoCheckIdentityRequestVO datos);
}
