/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.EvaluateVO;
import mx.aserta.wservices.vo.ScoringResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface HocelotWS {
    
    @WebResult(name = "evaluateResponse", partName = "evaluateResponse")
    ScoringResponseVO evaluate(
        @WebParam(name = "evaluateRequest", partName = "evaluateRequest")EvaluateVO evaluateRequest);
}
