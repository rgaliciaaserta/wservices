/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoRatingRequestVO;
import mx.aserta.wservices.vo.NowoRatingResponseVO;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@WebService(endpointInterface = "mx.aserta.wservices.ws.NowoRatingWS",
        serviceName = "NowoRatingWS")
@Component
public class NowoRatingWSImpl implements NowoRatingWS {

    @Override
    public NowoRatingResponseVO calculate(NowoRatingRequestVO request) {
        return null;
    }
}
