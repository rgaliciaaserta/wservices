/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.ConsultaArchivoNowoResponseVO;
import mx.aserta.wservices.vo.ConsultaArchivoNowoVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ConsultaArchivoNowoWS {
    
    @WebResult(name = "consultaArchivoResponse", partName = "consultaArchivoResponse")
    ConsultaArchivoNowoResponseVO consultaArchivo(
            @WebParam(name = "consultaArchivoRequest", partName = "consultaArchivoRequest") ConsultaArchivoNowoVO consulta);
}
