/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.ArrendamientoService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ArrendamientoResponse;
import mx.aserta.wservices.vo.ArrendamientoVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@WebService
@Component
public class ArrendamientoWSImpl implements ArrendamientoWS {

    @Autowired
    private ArrendamientoService arrendamientoService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public ArrendamientoResponse cotizaArrendamiento(ArrendamientoVO arrenda) {
        log.debug("\t\t:::::::::::::::::::::: ArrendamientoWS ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        ArrendamientoResponse resp = new ArrendamientoResponse();
        
        try {
            log.debug(TransformObject.getStringFromObject(arrenda));
            
            return this.arrendamientoService.cotizaArrendamiento(arrenda);
            
        } catch (Exception ex) {
            log.error("Error:WS:ArrendamientoWS:" + ex.getMessage(), ex);
            resp.setFlgExito(Util.SPS_ERROR);
            resp.setMensaje("Error " + ex.getMessage());
            
            return resp;
        }
    }
}
