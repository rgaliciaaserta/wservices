/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.ConsultaXML;
import wsdl.buro.consulta.com.bc.ws.consulta.bean.ConsultaXMLResponse;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ConsultaBuroWS {
    
    @WebResult(name = "consultaBuroResponse", partName = "consultaBuroResponse")
    public ConsultaXMLResponse consultaBuroXml(
            @WebParam(name = "consultaBuroRequest", partName = "consultaBuroRequest") ConsultaXML consulta);
}
