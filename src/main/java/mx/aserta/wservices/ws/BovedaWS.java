/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 *
 * @author rgalicia
 */
/*@WebService
public interface BovedaWS {
    
    @WebResult(name = "getPdfResult", partName = "getPdfResult")
    byte[] getPdf(
            @WebParam(name = "RfcEmisor", partName = "RfcEmisor") String rfcEmisor,
            @WebParam(name = "IdPoliza", partName = "IdPoliza") String idPoliza,
            @WebParam(name = "IdMovimiento", partName = "IdMovimiento") String idMovimiento,
            @WebParam(name = "TipoDocumento", partName = "TipoDocumento") String tipoDocumento,
            @WebParam(name = "Serie", partName = "Serie") String serie,
            @WebParam(name = "Folio", partName = "Folio") String folio,
            @WebParam(name = "Formato", partName = "Formato") String formato);
    
    @WebResult(name = "getXmlResult", partName = "getXmlResult")
    byte[] getXml(
            @WebParam(name = "RfcEmisor", partName = "RfcEmisor") String rfcEmisor,
            @WebParam(name = "IdPoliza", partName = "IdPoliza") String idPoliza,
            @WebParam(name = "IdMovimiento", partName = "IdMovimiento") String idMovimiento,
            @WebParam(name = "TipoDocumento", partName = "TipoDocumento") String tipoDocumento,
            @WebParam(name = "Serie", partName = "Serie") String serie,
            @WebParam(name = "Folio", partName = "Folio") String folio);
    
    @WebResult(name = "getPdfAcuseResult", partName = "getPdfAcuseResult")
    byte[] getPdfAcuse(
            @WebParam(name = "IdMovimiento", partName = "IdMovimiento") String idMovimiento);
    
    @WebResult(name = "getXmlAcuseResult", partName = "getXmlAcuseResult")
    byte[] getXmlAcuse(
            @WebParam(name = "IdMovimiento", partName = "IdMovimiento") String idMovimiento);
}*/
