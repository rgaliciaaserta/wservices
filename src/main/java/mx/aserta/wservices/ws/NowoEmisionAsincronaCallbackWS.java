/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoEmisionAsincronaCallbackRequestVO;
import mx.aserta.wservices.vo.NowoEmisionAsincronaCallbackResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoEmisionAsincronaCallbackWS {
    
    @WebResult(name = "nowoEmisionAsincronaCallbackResponseVO", partName = "nowoEmisionAsincronaCallbackResponseVO")
    NowoEmisionAsincronaCallbackResponseVO callback(
            @WebParam(name = "nowoEmisionAsincronaCallbackRequestVO", partName = "nowoEmisionAsincronaCallbackRequestVO")
                    NowoEmisionAsincronaCallbackRequestVO datos);
}
