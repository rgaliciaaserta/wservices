/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoAnulacionResponseVO;
import mx.aserta.wservices.vo.NowoAnulacionVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface AnulacionWS {
    
    @WebResult(name = "anulacionResponse", partName = "anulacionResponse")
    public NowoAnulacionResponseVO generaAnulacion(
            @WebParam(name = "anulacionRequest", partName = "anulacionRequest") NowoAnulacionVO anula);
}
