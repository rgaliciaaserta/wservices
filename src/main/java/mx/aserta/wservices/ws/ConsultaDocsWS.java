/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.ConsultaDocsRequestVO;
import mx.aserta.wservices.vo.ConsultaDocsResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ConsultaDocsWS {
    
    @WebResult(name = "consultaDocsResponse", partName = "consultaDocsResponse")
    ConsultaDocsResponseVO consultadocs(
            @WebParam(name = "consultaDocsRequest", partName = "consultaDocsRequest")
            ConsultaDocsRequestVO req);
}
