/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.AltaClienteMoralResponseVO;
import mx.aserta.wservices.vo.AltaClienteMoralVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface ClientesMoralWS {
    
    @WebResult(name = "altaClienteMoralResponse", partName = "altaClienteMoralResponse")
    AltaClienteMoralResponseVO altaClienteMoral(
            @WebParam(name = "altaClienteMoralRequest", partName = "altaClienteMoralRequest") AltaClienteMoralVO alta);
}
