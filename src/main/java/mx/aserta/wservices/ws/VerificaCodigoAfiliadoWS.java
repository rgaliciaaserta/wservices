/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.VerificaCodigoAfiliadoRequestVO;
import mx.aserta.wservices.vo.VerificaCodigoAfiliadoResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface VerificaCodigoAfiliadoWS {
    
    @WebResult(name = "verificaCodigoAfiliadoResponse", partName = "verificaCodigoAfiliadoResponse")
    VerificaCodigoAfiliadoResponseVO verificaCodigoAfiliado(
            @WebParam(name = "verificaCodigoAfiliadoRequest", partName = "verificaCodigoAfiliadoRequest")
            VerificaCodigoAfiliadoRequestVO verificaCodigo
    );
}
