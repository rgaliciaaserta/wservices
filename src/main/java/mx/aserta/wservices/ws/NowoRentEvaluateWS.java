/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoRentEvaluateRequestVO;
import mx.aserta.wservices.vo.NowoRentEvaluateResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoRentEvaluateWS {
    
    @WebResult(name = "rentEvaluateResponse", partName = "rentEvaluateResponse")
    NowoRentEvaluateResponseVO rentEvaluate(
            @WebParam(name = "rentEvaluateRequest", partName = "rentEvaluateRequest") NowoRentEvaluateRequestVO rent);
}
