/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebService;
import mx.aserta.wservices.service.ConsultaDocumentoService;
import mx.aserta.wservices.util.TransformObject;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.ConsultaDocumentosResponseVO;
import mx.aserta.wservices.vo.ConsultaDocumentosVO;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
@WebService(endpointInterface = "mx.aserta.wservices.ws.ConsultaDocumentosWS",
        serviceName = "ConsultaDocumentosWS")
public class ConsultaDocumentosWSImpl implements ConsultaDocumentosWS {

    @Autowired
    private ConsultaDocumentoService consultaDocumentoService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public ConsultaDocumentosResponseVO consultaPaquete(ConsultaDocumentosVO consulta) {
        log.debug("\t\t:::::::::::::::::::::: ConsultaDocumentosWS:consultaPaquete ::::::::::::::::::::::::::");
        
        //Setear el id de transaccion del interceptor de entrada al de salida
        //para rastreo de la entrada y salida del web service en tabla de log
        log.debug("ID TRANSACTION.::::" + PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID) + ":::.");
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(Util.TRANSACTION_ID, PhaseInterceptorChain.getCurrentMessage().getExchange().get(Util.TRANSACTION_ID));
        
        ConsultaDocumentosResponseVO resp = new ConsultaDocumentosResponseVO();
        
        try {
            log.debug(TransformObject.getStringFromObject(consulta));
            
            if (consulta == null) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMensaje("Debe indicar el objeto de entrada.");
                return resp;
            }
            
            /*if (base.getEmpresa() == null || base.getEmpresa().isEmpty()
                    || (!Util.ASERTA.equalsIgnoreCase(base.getEmpresa())
                    && !Util.INSURGENTES.equalsIgnoreCase(base.getEmpresa()))) {
                resp.setExito(Util.SPS_ERROR);
                resp.setMsgError("Debe indicar la empresa [" + Util.ASERTA+ " | " + Util.INSURGENTES + "].");
                return resp;
            }*/
            
            return this.consultaDocumentoService.consultaPaquete(Util.INSURGENTES, consulta);
            
        } catch (Exception ex) {
            log.error("Error:WS:ConsultaDocumentosWS:" + ex.getMessage(), ex);
            resp.setExito(Util.SPS_ERROR);
            resp.setMensaje("Error del servicio:" + ex.getMessage());
        }
        
        return resp;
    }
}
