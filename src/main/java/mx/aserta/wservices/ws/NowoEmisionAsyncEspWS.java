/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoEmisionAsyncEspRequestVO;
import mx.aserta.wservices.vo.NowoEmisionAsyncEspResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoEmisionAsyncEspWS {
    
    @WebResult(name = "nowoEmisionAsyncEspResponse", partName = "nowoEmisionAsyncEspResponse")
    NowoEmisionAsyncEspResponseVO callbackEmisionAsyncEsp(
            @WebParam(name = "nowoEmisionAsyncEspRequest", partName = "nowoEmisionAsyncEspRequest")
            NowoEmisionAsyncEspRequestVO req);
}
