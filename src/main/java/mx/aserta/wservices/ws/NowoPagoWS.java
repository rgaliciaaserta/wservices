/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import mx.aserta.wservices.vo.NowoPagoRequestVO;
import mx.aserta.wservices.vo.NowoPagoResponseVO;

/**
 *
 * @author rgalicia
 */
@WebService
public interface NowoPagoWS {
    
    @WebResult(name = "nowoPagoResponse", partName = "nowoPagoResponse")
    NowoPagoResponseVO pago(@WebParam(name = "nowoPagoRequest", partName = "nowoPagoRequest") NowoPagoRequestVO pago);
}
