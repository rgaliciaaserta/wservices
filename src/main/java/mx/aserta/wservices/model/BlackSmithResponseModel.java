/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.model;

/**
 *
 * @author rgalicia
 */
public class BlackSmithResponseModel {
    
    private String empresa;
    
    private String beneficiario;
    
    private String fiado;
    
    private String fianza;
    
    private String inclusion;
    
    private String ramoTipo;
    
    private String obligacion;
    
    private String noContrato;
    
    private String tipoMovimiento;
    
    private String fechaMovimiento;
    
    private String fechaInicioVigencia;
    
    private String fechaFinVigencia;
    
    private String moneda;
    
    private String montoAfianzadoMovimiento;
    
    private String primaNeta;
    
    private String primaTotal;
    
    private String estatusPago;
    
    private String centroCostos;
    
    private String montoReclamacion;
    
    private String estatusFianza;

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getFiado() {
        return fiado;
    }

    public void setFiado(String fiado) {
        this.fiado = fiado;
    }

    public String getFianza() {
        return fianza;
    }

    public void setFianza(String fianza) {
        this.fianza = fianza;
    }

    public String getInclusion() {
        return inclusion;
    }

    public void setInclusion(String inclusion) {
        this.inclusion = inclusion;
    }

    public String getRamoTipo() {
        return ramoTipo;
    }

    public void setRamoTipo(String ramoTipo) {
        this.ramoTipo = ramoTipo;
    }

    public String getObligacion() {
        return obligacion;
    }

    public void setObligacion(String obligacion) {
        this.obligacion = obligacion;
    }

    public String getNoContrato() {
        return noContrato;
    }

    public void setNoContrato(String noContrato) {
        this.noContrato = noContrato;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(String fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    public String getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    public void setFechaInicioVigencia(String fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    public String getFechaFinVigencia() {
        return fechaFinVigencia;
    }

    public void setFechaFinVigencia(String fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMontoAfianzadoMovimiento() {
        return montoAfianzadoMovimiento;
    }

    public void setMontoAfianzadoMovimiento(String montoAfianzadoMovimiento) {
        this.montoAfianzadoMovimiento = montoAfianzadoMovimiento;
    }

    public String getPrimaNeta() {
        return primaNeta;
    }

    public void setPrimaNeta(String primaNeta) {
        this.primaNeta = primaNeta;
    }

    public String getPrimaTotal() {
        return primaTotal;
    }

    public void setPrimaTotal(String primaTotal) {
        this.primaTotal = primaTotal;
    }

    public String getEstatusPago() {
        return estatusPago;
    }

    public void setEstatusPago(String estatusPago) {
        this.estatusPago = estatusPago;
    }

    public String getCentroCostos() {
        return centroCostos;
    }

    public void setCentroCostos(String centroCostos) {
        this.centroCostos = centroCostos;
    }

    public String getMontoReclamacion() {
        return montoReclamacion;
    }

    public void setMontoReclamacion(String montoReclamacion) {
        this.montoReclamacion = montoReclamacion;
    }

    public String getEstatusFianza() {
        return estatusFianza;
    }

    public void setEstatusFianza(String estatusFianza) {
        this.estatusFianza = estatusFianza;
    }
}
