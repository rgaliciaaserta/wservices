/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.handler;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
public class SoapMessageLogHandler implements SOAPHandler<SOAPMessageContext> {

    private static Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        try {
            Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
            
            SOAPMessage message = context.getMessage();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            Source source = message.getSOAPPart().getContent();
            StreamResult sr = new StreamResult(baos);
            
            Transformer transformer;
            
            try {
                transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                transformer.transform(source, sr);
            } catch (TransformerConfigurationException ex) {
                log.error("      Error:" + ex.getMessage() + ". " + ex.toString(), ex);
            } catch (TransformerException ex) {
                log.error("      Error:" + ex.getMessage() + ". " + ex.toString(), ex);
            }
            
            log.debug("Dentro del metodo de soa");
            
            if (outboundProperty) {
                log.info("       |=> (Request) Contenido del mensaje");
                log.info("       " + baos.toString());
                context.put("REQUEST_XML", baos.toString());
                context.put("REQUEST_HORA", new Date());
            } else {
                log.info("       |=> (Response) Contenido del mensaje");
                if (baos.size() > 50000) {
                    String logString = baos.toString();
                    log.info("       " + logString.substring(0, 50000));
                } else {
                    log.info("       " + baos.toString());
                }
                QName servicio = (QName) context.get(MessageContext.WSDL_SERVICE);
            }
        } catch (SOAPException ex) {
            log.error("Error:" + ex.getMessage(), ex);
        }
        
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
        
    }
    
}
