/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.handler;

import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import mx.aserta.wservices.config.PropSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
public class SoapMessageSiebelHandler implements SOAPHandler<SOAPMessageContext> {
    
    @Autowired
    private PropSource prop;
    
    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        Boolean outboudProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        
        if (outboudProperty) {
            //Add headers
            try {
                SOAPEnvelope soapEnvelope = context.getMessage().getSOAPPart().getEnvelope();
                SOAPHeader header = soapEnvelope.getHeader();
                if (header == null) {
                    header = soapEnvelope.addHeader();
                }
                
                SOAPElement userHeader = header.addChildElement("UsernameToken", "ws", "http://siebel.com/CustomUI");
                userHeader.addTextNode(this.prop.getWsHeaderUser());
                
                SOAPElement passHeader = header.addChildElement("PasswordText", "ws", "http://siebel.com/CustomUI");
                passHeader.addTextNode(this.prop.getWsHeaderPass());
                
                SOAPElement sessionHeader = header.addChildElement("SessionType", "ws", "http://siebel.com/CustomUI");
                sessionHeader.addTextNode(this.prop.getWsHeadersession());
                
            } catch (Exception ex) {
                throw new RuntimeException("Error on sopa header: " + ex.getMessage());
            }
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
        
    }
}
