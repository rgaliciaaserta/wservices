/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.bouncycastle.util.encoders.Hex;

/**
 *
 * @author rgalicia
 */
public class Util {
    
    public static final String COD_EXITO = "0";
    public static final String COD_ERROR = "1";
    
    public static final String SPS_EXITO = "Y";
    public static final String SPS_ERROR = "N";
    
    public static final String MSG_EXITO = "EXITO";
    public static final String MSG_ERROR = "ERROR";
    
    public static final String COD_EXITO_OIC = "00";
    
    public static final String DATO = "dato";
    
    public static final String RECEIVED = "RECEIVED";
    public static final String FINISHED = "FINISHED";
    
    public static final String WEBLOGIC = "WEBLOGIC";
    public static final String TRANSACTION_ID = "TRANSACTION_ID";
    
    public static final String GET_PDF = "getPdf";
    public static final String GET_XML = "getXml";
    public static final String GET_PDF_ACUSE = "getPdfAcuse";
    public static final String GET_XML_ACUSE = "getXmlAcuse";
    
    public static final String PERSONA_FISICA = "PERSONA_FISICA";
    public static final String PERSONA_MORAL = "PERSONA_MORAL";
    
    public static final String APP_STP = "stp";
    public static final String APP_BOVEDA = "boveda";
    public static final String APP_CLIENTES = "clientes";
    public static final String APP_ARRENDA = "arrenda";
    public static final String APP_RECIBE_PAGO = "recibepago";
    public static final String APP_CREA_BASE = "creabase";
    public static final String APP_ANULACION = "anulacion";
    public static final String APP_CONSULTA_DOCUMENTOS = "consulta_documentos";
    public static final String APP_CANCELACION = "cancelacion";
    public static final String APP_EMISION = "emision";
    public static final String APP_CONSULTA_ARCHIVOS_NOWO = "consultaarchivosnowo";
    public static final String APP_CONSULTA_BURO = "consulta_buro";
    public static final String APP_AON_GRUPOS = "aon_grupos";
    public static final String APP_VERIFICA_AGENTE = "verifica_agente";
    public static final String APP_VERIFICA_CODIGO_AFILIACION = "verifica_codigo_afiliacion";
    public static final String APP_NOWO_CARTA_ACEPTA = "nowo_carta_aceptacion";
    
    public static final String ASERTA = "ASERTA";
    public static final String INSURGENTES = "INSURGENTES";
    
    public static final String SIEBEL = "SIEBEL";
    
    public static final String POLIZA = "POLIZA";
    public static final String RECIBO = "RECIBO";
    
    public static final String ORIGINAL_BOND = "OriginalBond";
    
    public static final String DOC_CERTIFICADO = "Certificado";
    public static final String DOC_FOLLETO = "Folleto";
    public static final String DOC_POLIZA = "Poliza";
    public static final String DOC_POLIZACONTRATO = "PolizaContrato";
    public static final String DOC_SOLICITUD = "Solicitud";
    public static final String DOC_RECIBO = "Recibo";
    
    /*public static final String ASEGURAD_ASERTA = "ASEGURAD_ASERTA";
    public static final String ASEGURAD_INSURG = "ASEGURAD_INSURG";*/
    
    public static final String ESTATUS_INITIATED = "INITIATED";
    public static final String ESTATUS_IN_PROGRESS = "IN_PROGRESS";
    public static final String ESTATUS_ERROR = "ERROR";
    public static final String ESTATUS_FINISHED = "FINISHED";
    
    public static final String FOLIO_FOLLETO = "3";
    public static final String KEY_FOLIO_FOLLETO = "FOLIO_FOLLETO";
    
    public static final String FORMATO_DOC = "doc";
    public static final String FORMATO_DOCX = "docx";
    public static final String FORMATO_PDF = "pdf";
    public static final String FORMATO_JPEG = "jpeg";
    public static final String FORMATO_JPG = "jpg";
    public static final String FORMATO_PNG = "png";
    
    public static final String PATH_TMP = "TMP:";
    public static final String STRING_FORMAT = "%s";
    
    public static final String SYSTEM = "SYSTEM";
    
    public static final String SINIESTRO = "SINIESTRO";
    /**Plantilla Nowo **/
    public static final String NOWO_ASEGURADO = "NOWO_ASEGURADO";
    public static final String NOWO_CONTRATANTE = "NOWO_CONTRATANTE";
    public static final String NOWO_FECHA = "NOWO_FECHA";
    public static final String NOWO_CARTA_ACEPTACION = "Carta Aseguradora Insurgentes - Confirmación de Evaluación";
    
    public static final String FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
    public static final String FORMAT_DDMMYY = "ddMMyy";
    public static final String FORMAT_YYYYMMDDTHHMISS = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String FORMAT_YYYYMMDDTHHMISSSINZ = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FORMAT_DDMMYYYY = "dd-MM-yyyy";
    
    public static final String SINISTRO_ESTATUS_PENDIENTE = "PENDIENTE";
    public static final String SINISTRO_ESTATUS_ERROR = "ERROR";
    
    public static final SimpleDateFormat sdfmmddyyyy = new SimpleDateFormat(FORMAT_MM_DD_YYYY);
    public static final SimpleDateFormat sdfddmmyy = new SimpleDateFormat(FORMAT_DDMMYY);
    public static final SimpleDateFormat sdfyyyymmddhhmiss = new SimpleDateFormat(FORMAT_YYYYMMDDTHHMISS);
    public static final SimpleDateFormat sdfddMMyyyy = new SimpleDateFormat(FORMAT_DDMMYYYY);
    public static final SimpleDateFormat sdfyyyymmddhhmisssinz = new SimpleDateFormat(FORMAT_YYYYMMDDTHHMISSSINZ);
    
    //HTTP Connection
    public static final String APPLICATION_FADAUTH = "FAD_AUTH";
    
    public static final String HTTP_TRANSACTION = "HTTPTR";
    
    public static final String GUION_BAJO = "_";
    public static final String OR = "|";
    public static final String SPACE = " ";
    public static final String DIAGONAL = "/";
    public static final String EQUAL = "=";
    public static final String AMPERSAND = "&";
    public static final String TWO_DOTS = ":";
    public static final String QUERY_CLOSE = "?";
    public static final String POINT = ".";
    public static final char COMA = ',';
    public static final char COMILLAS = '"';
    public static final String DOUBLE_GUION = "--";
    public static final String CARRY_RETURN = "\r\n";
    public static final String TYPE_TEXT_CSV = "text/csv";
    
    public static final String EXT_CSV = ".csv";
    
    public static final String HTTP_HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HTTP_HEADER_AUTHORIZATION = "Authorization";
    public static final String HTTP_HEADER_CONTENT_DISPOSITION = "Content-Disposition";
    
    public static final String HTTP_HEADER_FORM_DATA = "form-data;name=\"%s\";filename=\"%s\"";
    
    
    public static final Pattern patternFechaddmmyy = Pattern.compile("[0-9]{6}");
    public static final Pattern patternMonto = Pattern.compile("[0-9]+(\\.[0-9]{1,2})?");
    
    public static final Random random = new Random((new Date()).getTime());
    public static final Random randomNumber = new Random(new Date().getTime());
    
    public static Logger log = LogManager.getLogger("WSERVICES");
    /**
     * Random UID
     * @return 
     */
    public static String getRandomUID() {
        return (new Date()).getTime() + "" + Math.abs(random.nextLong());
    }
    
    /**
     * Get random string
     * @return 
     */
    public static String getRandomString() {
        return (Math.abs(randomNumber.nextLong()) + "" +  new Date().getTime()).replaceAll("-", "A");
    }
    
    /**
     * Formato de fecha MM/DD/YYYY
     * @param date
     * @return 
     */
    public static String formatDateSiebel(Date date) {
        if (date == null) {
            return null;
        }
        
        try {
            return sdfmmddyyyy.format(date);
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * Formato de fecha yyyy-mm-ddThh:mi:ss
     * @param date
     * @return 
     */
    public static Date parseDateString(String date) {
        if (date == null) {
            return null;
        }
        
        try {
            return sdfyyyymmddhhmisssinz.parse(date);
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * Valida si una fecha en cadena tiene el formato correcto
     * @param cad
     * @param formato
     * @return
     */
    public static String isFormat(String cad, String formato) {
        if (cad == null || cad.isEmpty() || formato == null || formato.isEmpty()) {
            return "Debe indicar la fecha.";
        }
        
        try {
            Date fecha;
            
            if (!patternFechaddmmyy.matcher(cad).matches()) {
                return "El formato de fecha es incorrecto.";
            }
            
            if (FORMAT_DDMMYY.equalsIgnoreCase(formato)) {
                fecha = sdfddmmyy.parse(cad);
                sdfddmmyy.format(fecha);
            } else if (FORMAT_MM_DD_YYYY.equalsIgnoreCase(formato)) {
                fecha = sdfmmddyyyy.parse(cad);
                sdfmmddyyyy.format(fecha);
            } else if (FORMAT_DDMMYYYY.equalsIgnoreCase(formato)) {
                fecha = sdfddMMyyyy.parse(cad);
                sdfddMMyyyy.format(fecha);
            } else {
                return "Formato de fecha no soportado.";
            }
            
            return null;
        } catch (Exception ex) {
            return "Error: el formato de la fecha es incorrecto:" + ex.getMessage();
        }
    }
    
    /**
     * Valida si una fecha en cadena tiene el formato correcto
     * @param cad
     * @param formato
     * @return
     */
    public static String isFormatFecha(String cad, String formato) {
        if (cad == null || cad.isEmpty() || formato == null || formato.isEmpty()) {
            return "Debe indicar la fecha.";
        }
        
        try {
            Date fecha;
            
            if (FORMAT_DDMMYY.equalsIgnoreCase(formato)) {
                fecha = sdfddmmyy.parse(cad);
                sdfddmmyy.format(fecha);
            } else if (FORMAT_MM_DD_YYYY.equalsIgnoreCase(formato)) {
                fecha = sdfmmddyyyy.parse(cad);
                sdfmmddyyyy.format(fecha);
            } else if (FORMAT_DDMMYYYY.equalsIgnoreCase(formato)) {
                fecha = sdfddMMyyyy.parse(cad);
                sdfddMMyyyy.format(fecha);
            } else {
                return "Formato de fecha no soportado.";
            }
            
            return null;
        } catch (Exception ex) {
            return "Error: el formato de la fecha es incorrecto:" + ex.getMessage();
        }
    }
    
    /**
     * Valida formato de hora:minuto
     * @param hora
     * @return 
     */
    public static String isHour(String hora) {
        if (hora == null || hora.isEmpty()) {
            return "Debe indicar la hora";
        }
        
        try {
            if (hora.length() != 5) {
                return "El formato de hora:minuto no corresponde.";
            }
            
            int hh = Integer.valueOf(hora.substring(0, 2));
            int mm = Integer.valueOf(hora.substring(3, 5));
            
            if (hh < 0 || hh > 23) {
                return "La hora es incorrecta: [0 - 23].";
            }
            
            if (mm < 0 || mm > 59) {
                return "El minuto es incorrecta: [0 - 59].";
            }
            
            return null;
        } catch (Exception ex) {
            return "Error de formato de hora:minuto:" + ex.getMessage();
        }
    }
    
    /**
     * Formato de monto
     * @param monto
     * @return 
     */
    public static String isMonto(String monto) {
        if (monto == null || monto.isEmpty()) {
            return "Debe indicar el monto.";
        }
        
        try {
            if (!patternMonto.matcher(monto).matches()) {
                return "El formato del monto es incorrecto.";
            }
            BigDecimal bd = new BigDecimal(monto);
            
            return null;
        } catch (Exception ex) {
            return "Error en el formato de monto:" + ex.getMessage();
        }
    }
    
    /**
     * Obtener BigDecimal from String
     * @param monto
     * @return 
     */
    public static BigDecimal fromString(String monto) {
        if (monto == null || monto.isEmpty()) {
            return null;
        }
        
        try {
            BigDecimal mnt = new BigDecimal(monto);
            mnt.setScale(2, RoundingMode.DOWN);
            return mnt;
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * Encodear a Base64 un String
     * @param arrByte
     * @return 
     */
    public static String encodeBase64(byte[] arrByte) {
        if (arrByte == null || arrByte.length == 0) {
            return null;
        }
        
        return Base64.getEncoder().encodeToString(arrByte);
    }
    
    /**
     * Encodear a Base64 un archivo
     * @param file
     * @param delete
     * @return 
     */
    public static byte[] encodeBase64(String file, boolean delete) {
        if (file == null || file.isEmpty()) {
            return null;
        }
        
        return Base64.getEncoder().encode(getFileByteArray(file, delete));
    }
    
    /**
     * Leer archivo
     * @param file
     * @param delete
     * @return 
     */
    public static byte[] getFileByteArray(String file, boolean delete) {
        if (file == null || file.isEmpty()) {
            return null;
        }
        
        BufferedInputStream bis = null;
        ByteArrayOutputStream baos = null;
        File f = null;
        try {
            f = new File(file);
            bis = new BufferedInputStream(new FileInputStream(f));
            baos = new ByteArrayOutputStream();
            byte[] arrbyte = new byte[256];
            int leido;
            
            while((leido = bis.read(arrbyte)) != -1) {
                baos.write(arrbyte, 0, leido);
            }
            
            return baos.toByteArray();
        } catch (Exception ex) {
            return null;
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (Exception ex) {
                    ;
                }
            }
            
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception ex) {
                    ;
                }
            }
            
            if (f != null && delete) {
                f.delete();
            }
        }
    }
    
    /**
     * Desencrypt bytes with password PDF
     * @param arr
     * @param pass
     * @return 
     */
    public static byte[] desencryptBytePDF(byte[] arr, String pass) {
        if (arr == null || pass == null) {
            log.debug("\tdesencryptBytePDF:null:");
            return arr;
        }
        
        PDDocument pd = null;
        ByteArrayOutputStream baos = null;
        
        try {
            pd = PDDocument.load(arr, pass);
            pd.setAllSecurityToBeRemoved(true);

            baos = new ByteArrayOutputStream();
            pd.save(baos);
            
            return baos.toByteArray();
            
        } catch(Exception ex) {
            log.error("\tdesencryptBytePDF:null:" + ex.getMessage(), ex);
            return arr;
            
        } finally {
            if (pd != null) {
                try {
                    pd.close();
                } catch (Exception e) {
                    ;
                }
            }
            
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception e) {
                    ;
                }
            }
        }
    }
    
    /**
     * Limpiar caracteres no ASCII
     * @param cadena
     * @return 
     */
    public static String limpiarAcentos(String cadena) {
        String limpio =null;
        if (cadena !=null) {
           String valor = cadena;
           // Normalizar texto para eliminar acentos, dieresis, cedillas y tildes
           limpio = Normalizer.normalize(valor, Normalizer.Form.NFD);
           // Quitar caracteres no ASCII, interrogacion que abre, exclamacion que abre, grados, U con dieresis.
           limpio = limpio.replaceAll("[^\\p{ASCII}(\u00A1)(\u00BF)(\u00B0)(U\u0308)(u\u0308)]", "");
           // Regresar a la forma compuesta, para poder comparar la enie con la tabla de valores
           limpio = Normalizer.normalize(limpio, Normalizer.Form.NFC);
        }
        return limpio;
    }
    
    /**
     * Return empty String if null
     * @param cad
     * @return 
     */
    public static String emptyStringIfNull(String cad) {
        if (cad == null) {
            return "";
        }
        
        return cad;
    }
    
    /**
     * Extraer caracteres de una cadena
     * @param folio
     * @return 
     */
    public static String extractChar(final String cadena) {
        if (cadena == null || cadena.isEmpty()) {
            return cadena;
        }
        
        char[] chars = cadena.toCharArray();
        int val;
        String tmpCadena = "";
        for (int i = 0; i < chars.length; i++) {
            val = (int) chars[i];
            if ((val >= 65 && val <= 90)
                    || (val >= 97 && val <= 122)) {
                tmpCadena += chars[i];
            }
        }
        
        return tmpCadena;
    }
    
    /**
     * Saving file
     * @param nameFile
     * @param extFile
     * @param folderFile
     * @param base64File
     * @return 
     */
    public static String saveFile(String nameFile, String extFile, String folderFile, String base64File) {
        if (nameFile == null || nameFile.isEmpty()
                || extFile == null || extFile.isEmpty()
                || folderFile == null || folderFile.isEmpty()
                || base64File == null || base64File.isEmpty()) {
            return null;
        }
        
        BufferedOutputStream bw = null;
        try {
            String pathFile = folderFile + Util.getRandomUID() + "_" + nameFile + "." + extFile;
            File file = new File(pathFile);
            bw = new BufferedOutputStream(new FileOutputStream(file, false));
            
            bw.write(Base64.getDecoder().decode(base64File));
            
            return pathFile;
            
        } catch (Exception ex) {
            log.error("Error al ecribir el archivo:" + ex.getMessage(), ex);
            return null;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (Exception ex) {
                    ;
                }
            }
        }
    }
    
    /**
     * Create folder in path
     * @param path
     * @param nameFolder
     * @return 
     */
    public static String createFolder(String path, String nameFolder) {
        if (path == null || path.isEmpty()) {
            return "";
        }
        
        if (nameFolder == null || nameFolder.isEmpty()) {
            return "";
        }
        
        File f = new File(path + nameFolder);
        f.mkdir();
        
        return nameFolder;
    }
    
    /**
     * Formato de string
     * @param cad
     * @param args
     * @return 
     */
    public static String formatStringv2(String cad, String ...args) {
        if (cad == null || cad.isEmpty() || args == null || args.length == 0) {
            return cad;
        }
        
        int idx = 0, idxfo;
        String cadTmp = "";
        
        while(idx < args.length) {
            idxfo = cad.indexOf(Util.STRING_FORMAT);
            
            if (idxfo < 0) {
                cadTmp += cad;
                idx = args.length;
                cad = null;
                break;
            }
            
            try {
                cadTmp += cad.substring(0, idxfo)
                    + ((args[idx] == null)? "":args[idx]);
            } catch (Exception ex) {
                ;
            }
            
            try {
                cad = cad.substring(idxfo + Util.STRING_FORMAT.length(), cad.length());
            } catch (Exception ex) {
                ;
            }
            
            idx ++;
        }
        
        if (cad != null && !cad.isEmpty()) {
            cadTmp += cad;
        }
        
        return cadTmp;
    }
    
    /**
     * Generate hashcode
     * @param arrByte
     * @return 
     */
    public static String hashCodeGen(byte[] arrByte) {
        if (arrByte == null) {
            return null;
        }
        
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hashbytes = digest.digest(arrByte);
            return Hex.toHexString(hashbytes);
        } catch (Exception ex) {
            log.error("ERROR:" + ex.getMessage(), ex);
            return null;
        }
    }
    
    /**
     * Quit extension file
     * @param fileName
     * @return 
     */
    public static String quitExtFile(String fileName) {
        if (fileName == null || fileName.isEmpty()) {
            return fileName;
        }
        
        int idxPoint = fileName.lastIndexOf(Util.POINT);
        if (idxPoint < 0) {
            return fileName;
        }
        
        return fileName.substring(0, idxPoint);
    }
    
    /**
     * Get file name from path
     * @param pathFile
     * @return 
     */
    public static String getFileNameFromPath(String pathFile) {
        if (pathFile == null || pathFile.isEmpty()) {
            return null;
        }
        
        int idxLast = pathFile.lastIndexOf("/");
        if (idxLast < 0) {
            return pathFile;
        }
        
        if (idxLast >= pathFile.length()) {
            return "";
        }
        
        return pathFile.substring(idxLast + 1, pathFile.length());
    }
    
    /**
     * Get bytes from file
     * @param pathFile
     * @return 
     */
    public static byte[] getBytesFromFile(String pathFile) {
        if (pathFile == null || pathFile.isEmpty()) {
            return null;
        }
        
        ByteArrayOutputStream baos = null;
        BufferedInputStream bis = null;
        try {
            baos = new ByteArrayOutputStream();
            bis = new BufferedInputStream(new FileInputStream(pathFile));
            int leido;
            byte[] arrByte = new byte[128];
            
            while((leido = bis.read(arrByte)) != -1) {
                baos.write(arrByte, 0, leido);
            }
            
            return baos.toByteArray();
        } catch (Exception ex) {
            return null;
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (Exception ex) {
                    ;
                }
            }
            
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception ex) {
                    ;
                }
            }
        }
    }
    
    /*public static void main(String[] args) {
        System.out.println(hashCodeGen(("132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789"
                + "132456789123456789123456789123456789123456789").getBytes()));
    }*/
    
    /*public static void main(String[] args) throws FileNotFoundException, IOException {
        
        byte[] bb = encodeBase64("/Users/rgalicia/Desktop/Poliza_1-62R2QI.zip");
        for (int i = 0; i < bb.length; i++) {
            System.out.println(bb[i]);
        }
        
        byte[] bbb = Base64.getDecoder().decode(bb);
        FileOutputStream fos = new FileOutputStream("/Users/rgalicia/Desktop/Poliza_1-62R2QI_2.zip");
        fos.write(bbb);
        fos.close();
    }*/
    
    /*public static void main(String[] args) {
        try {
            Pattern p = Pattern.compile("[0-9]+(\\.[0-9]{1,2})?");
            
            
            System.out.println(p.matcher("123.13").matches());
            System.out.println(p.matcher("123712837182973.13").matches());
            System.out.println(p.matcher(".23").matches());
            System.out.println(p.matcher("010.00").matches());
            System.out.println(p.matcher("111.99").matches());
            System.out.println(p.matcher("1212312312.87").matches());
            System.out.println(p.matcher("99898138912712.90").matches());
            System.out.println(p.matcher("111.10").matches());
            System.out.println(p.matcher("00.33").matches());
            System.out.println(p.matcher("0.76").matches());
            System.out.println(p.matcher("1.55").matches());
            System.out.println(p.matcher("1").matches());
            System.out.println(p.matcher("12.").matches());
            System.out.println(p.matcher("12.1").matches());
            System.out.println(p.matcher("12.10").matches());
            System.out.println(p.matcher("12.102").matches());
            
        } catch (Exception ex) {
            ;
        }
    }*/
}
