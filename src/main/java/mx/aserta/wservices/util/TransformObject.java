/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.util;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author rgalicia
 */
public class TransformObject {
    
    /**
     * Transformación de objeto a cadena JSON
     * @param obj
     * @return 
     */
    public static String getStringFromObject(Object obj) {
        if (obj == null) {
            return "";
        }
        
        StringBuilder str = new StringBuilder();
        //Validación para listas de objetos
        if (obj instanceof List) {
            str.append(getStringFromList((List) obj));
        } else {
            //Obtener los metodos del objeto
            str.append(getStringFromObjectValue(obj, obj.getClass()));
        }
        
        return str.toString();
    }
    
    /**
     * Recorrer elementos de la lista
     * @param lista
     * @return 
     */
    public static String getStringFromList(Object lista) {
        StringBuilder str = new StringBuilder();
        if (lista == null) {
            str.append("null");
            return str.toString();
        }
        
        str.append("[");
        for (Object obj : (List<?>) lista) {
            str.append(getStringFromObject(obj));
            str.append(",\n");
        }
        
        if (str.length() > 1) {
            str.deleteCharAt(str.length() - 1);
            str.deleteCharAt(str.length() - 1);
        }
        str.append("]");
        return str.toString();
    }
    
    /**
     * Recorrer los metodos de un objeto
     * @param obj
     * @return 
     */
    public static String getStringFromMethod(Object obj) {
        StringBuilder str = new StringBuilder();
        str.append("{");
        if (obj == null || obj.getClass().getMethods() == null) {
            str.append("}");
            return str.toString();
        }
        
        for (Method metodo : obj.getClass().getMethods()) {
            if (metodo == null || metodo.getName() == null) {
                continue;
            }
            
            //Validar que sea un método get de propiedades de la clase
            if (metodo.getName().startsWith("get")
                    && metodo.getParameters().length == 0
                    && !metodo.getName().equalsIgnoreCase("get") && !metodo.getName().equalsIgnoreCase("getOrDefault")
                    && !metodo.getName().equalsIgnoreCase("getClass")
                    && !metodo.getName().equalsIgnoreCase("getBytes")) {
                str.append((metodo.getName().length() > 3)? metodo.getName().substring(3, 4).toLowerCase():"");
                str.append((metodo.getName().length() > 4)? metodo.getName().substring(4).toLowerCase():"");
                str.append(":");
                
                str.append(getStringFromValueObject(obj, metodo));
                str.append(",\n");
            }
        }
        
        if (str.length() > 1) {
            str.deleteCharAt(str.length() - 1);
            str.deleteCharAt(str.length() - 1);
        }
        
        str.append("}");
        return str.toString();
    }
    
    /**
     * Obtener el valor de la propiedad de un objeto
     * @param obj
     * @param metodo
     * @return 
     */
    public static String getStringFromValueObject(Object obj, Method metodo) {
        StringBuilder str = new StringBuilder();
        
        if (obj == null || metodo == null) {
            str.append("null");
            return str.toString();
        }
        
        try {
            str.append(getStringFromObjectValue(metodo.invoke(obj), metodo.getReturnType()));
        } catch (Exception ex) {
            str.append("null");
        }
        
        return str.toString();
    }
    
    /**
     * Obtener Valor de objeto de acuerdo a su clase
     * @param obj
     * @param clase
     * @return 
     */
    public static String getStringFromObjectValue(Object obj, Class clase) {
        StringBuilder str = new StringBuilder();
        if (obj == null || clase == null) {
            str.append("null");
            return str.toString();
        }
        System.out.println(clase.getName());
        if (clase.isPrimitive()) {
            str.append((obj == null)? "null":obj);
        } else if (clase.getName().contains(String.class.getName())) {
            str.append((obj == null)? "null":"'" + (String) obj + "'");
        } else if (clase.getName().contains(List.class.getName()) || clase.isInstance(ArrayList.class)) {
            str.append(getStringFromList(clase.cast(obj)));
        } else if (clase.isInstance(Date.class)) {
            str.append((obj == null)? "null":"'" + obj.toString() + "'");
        } else if (clase.getName().contains(Map.class.getName()) || clase.isInstance(AbstractMap.class)
                || clase.getName().contains(HashMap.class.getName()) || clase.getName().contains(SortedMap.class.getName())
                || clase.getName().contains(TreeMap.class.getName())) {
            str.append(getStringFromMap(obj));
        } else {
            str.append(getStringFromMethod(obj));
        }
        
        return str.toString();
    }
    
    /**
     * Obtener valores de mapa
     * @param obj
     * @return 
     */
    public static String getStringFromMap(Object obj) {
        StringBuilder str = new StringBuilder();
        
        if (obj == null) {
            str.append("null");
            return str.toString();
        }
        
        str.append("{");
        Iterator<Map.Entry<?, ?>> iter = (Iterator<Map.Entry<?, ?>>) ((Map<?, ?>) obj).entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry<?, ?> entrada = iter.next();
            if (entrada.getKey() == null) {
                continue;
            }
            str.append(entrada.getKey().toString());
            str.append(":");
            str.append(getStringFromObject(entrada.getValue()));
            str.append(",\n");
        }
        
        if (str.length() > 1) {
            str.deleteCharAt(str.length() - 1);
            str.deleteCharAt(str.length() - 1);
        }
        str.append("}");
        return str.toString();
    }
}
