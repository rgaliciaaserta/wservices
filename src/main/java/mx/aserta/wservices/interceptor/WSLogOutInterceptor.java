/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.interceptor;

import java.io.OutputStream;
import mx.aserta.wservices.sps.LogService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.LogVO;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.phase.Phase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
public class WSLogOutInterceptor extends AbstractSoapInterceptor {

    private String usuario = "WEBLOGIC";
    
    @Autowired
    private LogService logService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    public WSLogOutInterceptor() {
        super(Phase.PRE_STREAM);
    }
    
    public class LogOutCallBack implements CachedOutputStreamCallback {

        private SoapMessage message;
        
        private LogVO logVO;
        
        public LogOutCallBack(SoapMessage t, LogVO logVO) {
            this.message = t;
            this.logVO = logVO;
        }
        
        @Override
        public void onClose(CachedOutputStream stream) {
            try {
                if (stream != null) {
                    StringBuilder str = new StringBuilder();
                    try {
                        stream.writeCacheTo(str);
                    } catch (Exception ex) {
                        log.error("\t\tError leyendo datos de stream response.");
                    }

                    if (str.length() > 50000) {
                        log.debug("\t\t" + str.substring(0, 50000));
                    } else {
                        log.debug("\t\t" + str.toString());
                    }
                    
                    this.logVO.setId((String) this.message.getExchange().get(Util.TRANSACTION_ID));
                    this.logVO.getMsg().append(str.toString());
                    
                    logService.execute(this.logVO);
                }
            } catch (Exception ex) {
                log.error("\t\tError cerrando conexion de salida.");
            }
        }

        @Override
        public void onFlush(CachedOutputStream stream) {
            ;
        }
    }
    
    @Override
    public void handleMessage(SoapMessage t) throws Fault {
        log.debug("\t\tWeb Service SOAP Response ***********************************************************************");
        
        try {
            LogVO logvo = new LogVO();
            logvo.setId(null);
            logvo.setEstatus(Util.FINISHED);
            logvo.setTarget(null);
            logvo.setUsuario(this.getUsuario());
            
            OutputStream out = t.getContent(OutputStream.class);
            CacheAndWriteOutputStream cwos = new CacheAndWriteOutputStream(out);
            t.setContent(OutputStream.class, cwos);
            
            cwos.registerCallback(new LogOutCallBack(t, logvo));
        } catch (Exception ex) {
            log.error("Error log:" + ex.getMessage(), ex);
        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
