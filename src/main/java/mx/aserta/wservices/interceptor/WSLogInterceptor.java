/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.interceptor;

import java.io.ByteArrayOutputStream;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import mx.aserta.wservices.sps.LogService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.LogVO;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

/**
 *
 * @author rgalicia
 */
@Component
public class WSLogInterceptor extends AbstractSoapInterceptor {

    private String usuario = "WEBLOGIC";
    
    @Autowired
    private LogService logService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    public WSLogInterceptor() {
        super(Phase.USER_PROTOCOL);
    }
    
    @Override
    public void handleMessage(SoapMessage t) throws Fault {
        log.debug("\t\tWeb Service SOAP Request ***********************************************************************");
        
        try {
            LogVO logvo = new LogVO();
            logvo.setEstatus(Util.RECEIVED);
            logvo.setTarget(Util.getRandomUID());
            logvo.setUsuario(this.getUsuario());
            
            SOAPMessage soapMessage = t.getContent(SOAPMessage.class);
            
            String operation = this.getWsOperation(soapMessage.getSOAPPart().getChildNodes());
            log.debug("\t.:.:Operacion:.:.::" + operation);
            
            if (operation != null && !operation.isEmpty()) {
                logvo.setUsuario(operation);
            }
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Source source = soapMessage.getSOAPPart().getContent();
            StreamResult sr = new StreamResult(baos);
            
            Transformer transformer;
            
            try {
                transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                transformer.transform(source, sr);
            } catch (TransformerConfigurationException ex) {
                log.error("      Error:" + ex.getMessage() + ". " + ex.toString(), ex);
            } catch (TransformerException ex) {
                log.error("      Error:" + ex.getMessage() + ". " + ex.toString(), ex);
            }
            
            if (baos.size() > 50000) {
                String logString = baos.toString();
                log.info("       " + logString.substring(0, 50000));
            } else {
                log.info("       " + baos.toString());
            }
            
            logvo.getParams().append(baos.toString());
            RespuestaJson resp = this.logService.execute(logvo);
            if (resp.getMensaje() != null && !resp.getMensaje().isEmpty()) {
                t.getExchange().put(Util.TRANSACTION_ID, resp.getMensaje());
            }
        } catch (Exception ex) {
            log.error("Error log:" + ex.getMessage(), ex);
        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Lista de nodos
     * @param lista
     * @return 
     */
    private String getWsOperation(NodeList lista) {
        log.debug("\t.:Lista de nodos:.");
        
        if (lista == null) {
            log.debug("\tLista de nodos vacia.");
        }
        
        String localName = null;
        try {
            NodeList primerNivel, segundoNivel;
            log.debug("\t:Lista de nodos size:" + lista.getLength());
            for (int i = 0; i < lista.getLength(); i++) {
                //Primer nivel
                primerNivel = lista.item(i).getChildNodes();
                log.debug("\tLista de nodos primer nivel:" + primerNivel.getLength());
                for (int j = 0; j < primerNivel.getLength(); j++) {
                    //Segundo nivel
                    segundoNivel = primerNivel.item(j).getChildNodes();
                    log.debug("\tLista de nodos segundo nivel:" + segundoNivel.getLength());
                    for (int k = 0; k < segundoNivel.getLength(); k++) {
                        //Tercer nivel
                        if (segundoNivel.item(k).getLocalName() != null &&
                                !segundoNivel.item(k).getLocalName().isEmpty()
                                && segundoNivel.item(k).getParentNode().getLocalName() != null
                                && !segundoNivel.item(k).getParentNode().getLocalName().isEmpty()
                                && segundoNivel.item(k).getParentNode().getLocalName().equalsIgnoreCase("body")) {
                            localName = segundoNivel.item(k).getLocalName();
                            log.debug("\tlocalname:" + localName);
                            log.debug("\tlocalname padre:" + segundoNivel.item(k).getParentNode().getLocalName());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.debug("\tError lista nodos:" + ex.getMessage());
        }
        
        return localName;
    }
}
