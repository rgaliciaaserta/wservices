/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.interceptor;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import mx.aserta.wservices.config.PropSource;
import mx.aserta.wservices.sps.GetCredentialsService;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.vo.RespuestaJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
public class ClientesInterceptorSecurity implements CallbackHandler{

    @Autowired
    private PropSource prop;
    
    @Autowired
    private GetCredentialsService getCredentialsService;
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        log.debug("\t\t::..........Clientes WS Security interceptor..........::");
        
        log.debug("\t\t::..........ClientesWS Security interceptor recover credentials..........::");
        
        String user, pass;
        //Obtener credenciales de BD
        RespuestaJson rpJson = this.getCredentialsService.execute(Util.APP_CLIENTES);
        if (Util.COD_EXITO.equalsIgnoreCase(rpJson.getCodigo())
                && rpJson.getDatos() != null && !((List<Map<String, String>>) rpJson.getDatos()).isEmpty()
                && ((List<Map<String, String>>) rpJson.getDatos()).get(0) != null) {
            
            user = (((List<Map<String, String>>) rpJson.getDatos()).get(0).get(Util.DATO + "1") == null)?
                    null: (String) ((List<Map<String, String>>) rpJson.getDatos()).get(0).get(Util.DATO + "1");
            pass = (((List<Map<String, String>>) rpJson.getDatos()).get(0).get(Util.DATO + "2") == null)?
                    null: (String) ((List<Map<String, String>>) rpJson.getDatos()).get(0).get(Util.DATO + "2");
        } else {
            user = this.prop.getAuthCredentialUser();
            pass = this.prop.getAuthCredentialPass();
        }
        
        if (callbacks != null) {
            WSPasswordCallback pc;
            for (Callback cb : callbacks) {
                pc = (WSPasswordCallback) cb;
                
                log.debug("\t\tInterceptor usage:" + pc.getUsage() + ":" + pc.getIdentifier() + ":" + pc.getPassword());
                
                if (pc.getUsage() == WSPasswordCallback.USERNAME_TOKEN) {
                    if (pc.getIdentifier() != null && pc.getIdentifier().equals(user)) {
                        pc.setPassword(pass);
                    }
                }
            }
            
        } else {
            log.error("\t\tNo se recibieron los header's en el interceptor.");
            throw new IOException("El servicio web requiere usuario y password.");
        }
    }
    
}
