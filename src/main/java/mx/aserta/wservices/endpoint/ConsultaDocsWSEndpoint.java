/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.endpoint;

import java.util.HashMap;
import java.util.Map;
import mx.aserta.wservices.interceptor.ConsultaDocsInterceptorSecurity;
import mx.aserta.wservices.interceptor.WSLogInterceptor;
import mx.aserta.wservices.interceptor.WSLogOutInterceptor;
import mx.aserta.wservices.ws.ConsultaDocsWS;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
public class ConsultaDocsWSEndpoint extends EndpointImpl implements EndpointConfig {
    
    @Autowired
    public ConsultaDocsWSEndpoint(@Qualifier(Bus.DEFAULT_BUS_ID) Bus bus,
            ConsultaDocsWS implementor,
            ConsultaDocsInterceptorSecurity consultaDocsInterceptorSecurity,
            WSLogInterceptor wsLogInterceptor,
            WSLogOutInterceptor wsLogOutInterceptor) {
        super(bus, (Object) implementor);
        
        try {
            log.debug("\t\tPublicacion de web service:\"consultaDocsWS\"");
            super.setPublishedEndpointUrl(this.getEndpointPublishedUrl() + "consultaDocsWS");
            super.publish(this.getContextPublishedUrl() + "consultaDocsWS");
            log.debug("\t\tPublicacion de web service: OK");
            log.debug("\t\tAplicando seguridad a ws \"consultaDocsWS\"");
            
            Map<String, Object> inProps = new HashMap<>();
            inProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
            inProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
            inProps.put(WSHandlerConstants.PW_CALLBACK_REF, consultaDocsInterceptorSecurity);

            WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps);
            
            wsLogInterceptor.setUsuario("");
            wsLogOutInterceptor.setUsuario("");
            
            // Le pasamos el interceptor
            this.getInInterceptors().add(wsLogInterceptor);
            this.getInInterceptors().add(wssIn);
            
            this.getOutInterceptors().add(wsLogOutInterceptor);
        } catch (Exception ex) {
            log.error("\tError publicando web service:\"consultaDocsWS\":" + ex.getMessage(), ex);
        }
    }
}
