/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.endpoint;

import mx.aserta.wservices.interceptor.NowoCartaAceptacionInterceptorSecurity;
import mx.aserta.wservices.interceptor.WSLogInterceptor;
import mx.aserta.wservices.interceptor.WSLogOutInterceptor;
import mx.aserta.wservices.util.Util;
import mx.aserta.wservices.ws.NowoUpdateRentDecisionWS;
import org.apache.cxf.Bus;
import org.apache.cxf.binding.soap.saaj.SAAJInInterceptor;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author rgalicia
 */
@Component
public class NowoUpdateRentDecisionWSEndpoint extends EndpointImpl implements EndpointConfig {
    
    private Logger log = LogManager.getLogger("WSERVICES");
    
    @Autowired
    public NowoUpdateRentDecisionWSEndpoint(@Qualifier(Bus.DEFAULT_BUS_ID) Bus bus,
            NowoUpdateRentDecisionWS implementor,
            NowoCartaAceptacionInterceptorSecurity nowoCartaAceptacionInterceptorSecurity,
            WSLogInterceptor wsLogInterceptor,
            WSLogOutInterceptor wsLogOutInterceptor) {
        super(bus, (Object) implementor);
        
        try {
            log.debug("\t\tPublicacion de web service:\"nowoUpdateRentDecisionWS\"");
            super.setPublishedEndpointUrl(this.getEndpointPublishedUrl() + "nowoUpdateRentDecisionWS");
            super.publish(this.getContextPublishedUrl() + "nowoUpdateRentDecisionWS");
            log.debug("\t\tPublicacion de web service: OK");
            log.debug("\t\tAplicando seguridad a ws \"nowoUpdateRentDecisionWS\"");
            
            SAAJInInterceptor saaj = new SAAJInInterceptor();
            wsLogInterceptor.setUsuario(Util.APP_CLIENTES);
            wsLogOutInterceptor.setUsuario(Util.APP_CLIENTES);
            
            // Le pasamos el interceptor
            this.getInInterceptors().add(wsLogInterceptor);
            this.getInInterceptors().add(saaj);
            
            this.getOutInterceptors().add(wsLogOutInterceptor);
        } catch (Exception ex) {
            log.error("\tError publicando web service:\"nowoUpdateRentDecisionWS\":" + ex.getMessage(), ex);
        }
    }
}
