/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.endpoint;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rgalicia
 */
public interface EndpointConfig {
    
    public Logger log = LogManager.getLogger("WSERVICES");
    
    /**
     * EndpointPublishedUrl
     * @return 
     */
    default public String getEndpointPublishedUrl() {
        Properties prop = new Properties();
        try {
            InputStream io = new FileInputStream("./iPad/conf/wservices.properties");
            prop.load(io);
            
            return prop.getProperty("url.publishedEndpointUrl");
        } catch (Exception ex) {
            log.error("Error al cargar archivo de configuracion:EndpointConfig:" + ex.getMessage(), ex);
            return null;
        }
    }
    
    /**
     * ContextUrlPublished
     * @return 
     */
    default public String getContextPublishedUrl() {
        Properties prop = new Properties();
        try {
            InputStream io = new FileInputStream("./iPad/conf/wservices.properties");
            prop.load(io);
            
            return prop.getProperty("url.contextpublished.arrenda");
        } catch (Exception ex) {
            log.error("Error al cargar archivo de configuracion:EndpointConfig:" + ex.getMessage(), ex);
            return null;
        }
    }
    
    /**
     * EndpointPublishedUrl
     * @return 
     */
    default public String getEndpointPublishedUrlAon() {
        Properties prop = new Properties();
        try {
            InputStream io = new FileInputStream("./iPad/conf/wservices.properties");
            prop.load(io);
            
            return prop.getProperty("url.publishedEndpointUrl.aon");
        } catch (Exception ex) {
            log.error("Error al cargar archivo de configuracion:EndpointConfig:" + ex.getMessage(), ex);
            return null;
        }
    }
    
    /**
     * ContextUrlPublished
     * @return 
     */
    default public String getContextPublishedUrlAon() {
        Properties prop = new Properties();
        try {
            InputStream io = new FileInputStream("./iPad/conf/wservices.properties");
            prop.load(io);
            
            return prop.getProperty("url.contextpublished.aon");
        } catch (Exception ex) {
            log.error("Error al cargar archivo de configuracion:EndpointConfig:" + ex.getMessage(), ex);
            return null;
        }
    }
}
