/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rgalicia
 */
@XmlRootElement(name = "altaClienteMoralRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class AltaClienteMoralVO {
    
    @XmlElement(name = "delegacionMunicipioDomicilioCliente", required = false, nillable = true)
    private String delegacionMunicipioDomicilioCliente;
    
    @XmlElement(name = "inicioVigenciaGarantia", required = false, nillable = true)
    private String inicioVigenciaGarantia;
    
    @XmlElement(name = "inicioVigenciaContrato", required = false, nillable = true)
    private String inicioVigenciaContrato;
    
    @XmlElement(name = "razonSocial", required = true, nillable = false)
    private String razonSocial;
    
    @XmlElement(name = "numeroExteriorDomicilioCliente", required = false, nillable = true)
    private String numeroExteriorDomicilioCliente;
    
    @XmlElement(name = "coloniaDomicilioCliente", required = false, nillable = true)
    private String coloniaDomicilioCliente;
    
    @XmlElement(name = "clabe", required = false, nillable = true)
    private String clabe;
    
    @XmlElement(name = "metrosCuadradosDomicilioaRentar", required = false, nillable = true)
    private String metrosCuadradosDomicilioaRentar;
    
    @XmlElement(name = "montoRenta", required = false, nillable = true)
    private String montoRenta;
    
    @XmlElement(name = "diaLimitePagoRenta", required = false, nillable = true)
    private String diaLimitePagoRenta;
    
    @XmlElement(name = "rfc", required = true, nillable = false)
    private String rfc;
    
    @XmlElement(name = "codigoPropiedad", required = false, nillable = true)
    private String codigoPropiedad;
    
    @XmlElement(name = "codigoPostalDomicilioCliente", required = false, nillable = true)
    private String codigoPostalDomicilioCliente;
    
    @XmlElement(name = "finVigenciaContrato", required = false, nillable = true)
    private String finVigenciaContrato;
    
    @XmlElement(name = "fechaConstitucion", required = false, nillable = true)
    protected String fechaConstitucion;
    
    @XmlElement(name = "estadoDomicilioCliente", required = false, nillable = true)
    private String estadoDomicilioCliente;
    
    @XmlElement(name = "ciudadDomicilioaRentar", required = false, nillable = true)
    private String ciudadDomicilioaRentar;
    
    @XmlElement(name = "codigoPostalDomicilioaRentar", required = false, nillable = true)
    private String codigoPostalDomicilioaRentar;
    
    @XmlElement(name = "paisDomicilioCliente", required = false, nillable = true)
    private String paisDomicilioCliente;
    
    @XmlElement(name = "numeroExteriorDomicilioaRentar", required = false, nillable = true)
    private String numeroExteriorDomicilioaRentar;
    
    @XmlElement(name = "estadoDomicilioaRentar", required = false, nillable = true)
    private String estadoDomicilioaRentar;
    
    @XmlElement(name = "ciudadDomicilioCliente", required = false, nillable = true)
    private String ciudadDomicilioCliente;
    
    @XmlElement(name = "delegacionMunicipioDomicilioaRentar", required = false, nillable = true)
    private String delegacionMunicipioDomicilioaRentar;
    
    @XmlElement(name = "numeroInteriorDomicilioaRentar", required = false, nillable = true)
    private String numeroInteriorDomicilioaRentar;
    
    @XmlElement(name = "cuenta", required = false, nillable = true)
    private String cuenta;
    
    @XmlElement(name = "perfil", required = true, nillable = false)
    private String perfil;
    
    @XmlElement(name = "calleDomiclioCliente", required = false, nillable = true)
    private String calleDomiclioCliente;
    
    @XmlElement(name = "calleDomicilioaRentar", required = false, nillable = true)
    private String calleDomicilioaRentar;
    
    @XmlElement(name = "coloniaDomicilioClienteaRentar", required = false, nillable = true)
    private String coloniaDomicilioClienteaRentar;
    
    @XmlElement(name = "finVigenciaGarantia", required = false, nillable = true)
    private String finVigenciaGarantia;
    
    @XmlElement(name = "banco", required = false, nillable = true)
    private String banco;
    
    @XmlElement(name = "paisDomicilioaRentar", required = false, nillable = true)
    private String paisDomicilioaRentar;
    
    @XmlElement(name = "numeroInteriorDomicilioCliente", required = false, nillable = true)
    private String numeroInteriorDomicilioCliente;

    @XmlElement(name = "correo", required = false, nillable = true)
    private String correo;
    
    public String getDelegacionMunicipioDomicilioCliente() {
        return delegacionMunicipioDomicilioCliente;
    }

    public void setDelegacionMunicipioDomicilioCliente(String delegacionMunicipioDomicilioCliente) {
        this.delegacionMunicipioDomicilioCliente = delegacionMunicipioDomicilioCliente;
    }

    public String getInicioVigenciaGarantia() {
        return inicioVigenciaGarantia;
    }

    public void setInicioVigenciaGarantia(String inicioVigenciaGarantia) {
        this.inicioVigenciaGarantia = inicioVigenciaGarantia;
    }

    public String getInicioVigenciaContrato() {
        return inicioVigenciaContrato;
    }

    public void setInicioVigenciaContrato(String inicioVigenciaContrato) {
        this.inicioVigenciaContrato = inicioVigenciaContrato;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNumeroExteriorDomicilioCliente() {
        return numeroExteriorDomicilioCliente;
    }

    public void setNumeroExteriorDomicilioCliente(String numeroExteriorDomicilioCliente) {
        this.numeroExteriorDomicilioCliente = numeroExteriorDomicilioCliente;
    }

    public String getCodigoPropiedad() {
        return codigoPropiedad;
    }

    public void setCodigoPropiedad(String codigoPropiedad) {
        this.codigoPropiedad = codigoPropiedad;
    }

    public String getColoniaDomicilioCliente() {
        return coloniaDomicilioCliente;
    }

    public void setColoniaDomicilioCliente(String coloniaDomicilioCliente) {
        this.coloniaDomicilioCliente = coloniaDomicilioCliente;
    }

    public String getClabe() {
        return clabe;
    }

    public void setClabe(String clabe) {
        this.clabe = clabe;
    }

    public String getMetrosCuadradosDomicilioaRentar() {
        return metrosCuadradosDomicilioaRentar;
    }

    public void setMetrosCuadradosDomicilioaRentar(String metrosCuadradosDomicilioaRentar) {
        this.metrosCuadradosDomicilioaRentar = metrosCuadradosDomicilioaRentar;
    }

    public String getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(String montoRenta) {
        this.montoRenta = montoRenta;
    }

    public String getDiaLimitePagoRenta() {
        return diaLimitePagoRenta;
    }

    public void setDiaLimitePagoRenta(String diaLimitePagoRenta) {
        this.diaLimitePagoRenta = diaLimitePagoRenta;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCodigoPostalDomicilioCliente() {
        return codigoPostalDomicilioCliente;
    }

    public void setCodigoPostalDomicilioCliente(String codigoPostalDomicilioCliente) {
        this.codigoPostalDomicilioCliente = codigoPostalDomicilioCliente;
    }

    public String getFinVigenciaContrato() {
        return finVigenciaContrato;
    }

    public void setFinVigenciaContrato(String finVigenciaContrato) {
        this.finVigenciaContrato = finVigenciaContrato;
    }

    public String getEstadoDomicilioCliente() {
        return estadoDomicilioCliente;
    }

    public void setEstadoDomicilioCliente(String estadoDomicilioCliente) {
        this.estadoDomicilioCliente = estadoDomicilioCliente;
    }

    public String getCiudadDomicilioaRentar() {
        return ciudadDomicilioaRentar;
    }

    public void setCiudadDomicilioaRentar(String ciudadDomicilioaRentar) {
        this.ciudadDomicilioaRentar = ciudadDomicilioaRentar;
    }

    public String getCodigoPostalDomicilioaRentar() {
        return codigoPostalDomicilioaRentar;
    }

    public void setCodigoPostalDomicilioaRentar(String codigoPostalDomicilioaRentar) {
        this.codigoPostalDomicilioaRentar = codigoPostalDomicilioaRentar;
    }

    public String getPaisDomicilioCliente() {
        return paisDomicilioCliente;
    }

    public void setPaisDomicilioCliente(String paisDomicilioCliente) {
        this.paisDomicilioCliente = paisDomicilioCliente;
    }

    public String getNumeroExteriorDomicilioaRentar() {
        return numeroExteriorDomicilioaRentar;
    }

    public void setNumeroExteriorDomicilioaRentar(String numeroExteriorDomicilioaRentar) {
        this.numeroExteriorDomicilioaRentar = numeroExteriorDomicilioaRentar;
    }

    public String getEstadoDomicilioaRentar() {
        return estadoDomicilioaRentar;
    }

    public void setEstadoDomicilioaRentar(String estadoDomicilioaRentar) {
        this.estadoDomicilioaRentar = estadoDomicilioaRentar;
    }

    public String getCiudadDomicilioCliente() {
        return ciudadDomicilioCliente;
    }

    public void setCiudadDomicilioCliente(String ciudadDomicilioCliente) {
        this.ciudadDomicilioCliente = ciudadDomicilioCliente;
    }

    public String getDelegacionMunicipioDomicilioaRentar() {
        return delegacionMunicipioDomicilioaRentar;
    }

    public void setDelegacionMunicipioDomicilioaRentar(String delegacionMunicipioDomicilioaRentar) {
        this.delegacionMunicipioDomicilioaRentar = delegacionMunicipioDomicilioaRentar;
    }

    public String getNumeroInteriorDomicilioaRentar() {
        return numeroInteriorDomicilioaRentar;
    }

    public void setNumeroInteriorDomicilioaRentar(String numeroInteriorDomicilioaRentar) {
        this.numeroInteriorDomicilioaRentar = numeroInteriorDomicilioaRentar;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getCalleDomiclioCliente() {
        return calleDomiclioCliente;
    }

    public void setCalleDomiclioCliente(String calleDomiclioCliente) {
        this.calleDomiclioCliente = calleDomiclioCliente;
    }

    public String getCalleDomicilioaRentar() {
        return calleDomicilioaRentar;
    }

    public void setCalleDomicilioaRentar(String calleDomicilioaRentar) {
        this.calleDomicilioaRentar = calleDomicilioaRentar;
    }

    public String getColoniaDomicilioClienteaRentar() {
        return coloniaDomicilioClienteaRentar;
    }

    public void setColoniaDomicilioClienteaRentar(String coloniaDomicilioClienteaRentar) {
        this.coloniaDomicilioClienteaRentar = coloniaDomicilioClienteaRentar;
    }

    public String getFinVigenciaGarantia() {
        return finVigenciaGarantia;
    }

    public void setFinVigenciaGarantia(String finVigenciaGarantia) {
        this.finVigenciaGarantia = finVigenciaGarantia;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getPaisDomicilioaRentar() {
        return paisDomicilioaRentar;
    }

    public void setPaisDomicilioaRentar(String paisDomicilioaRentar) {
        this.paisDomicilioaRentar = paisDomicilioaRentar;
    }

    public String getNumeroInteriorDomicilioCliente() {
        return numeroInteriorDomicilioCliente;
    }

    public void setNumeroInteriorDomicilioCliente(String numeroInteriorDomicilioCliente) {
        this.numeroInteriorDomicilioCliente = numeroInteriorDomicilioCliente;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFechaConstitucion() {
        return fechaConstitucion;
    }

    public void setFechaConstitucion(String fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
    }
}
