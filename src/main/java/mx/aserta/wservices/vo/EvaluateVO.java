/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class EvaluateVO {
    
    private EconomicRequest economicRequest;
    
    private List<InterveningParties> interveningParties;
    
    private TargetAddress targetAddress;

    public EconomicRequest getEconomicRequest() {
        return economicRequest;
    }

    public void setEconomicRequest(EconomicRequest economicRequest) {
        this.economicRequest = economicRequest;
    }

    public List<InterveningParties> getInterveningParties() {
        return interveningParties;
    }

    public void setInterveningParties(List<InterveningParties> interveningParties) {
        this.interveningParties = interveningParties;
    }

    public TargetAddress getTargetAddress() {
        return targetAddress;
    }

    public void setTargetAddress(TargetAddress targetAddress) {
        this.targetAddress = targetAddress;
    }
}
