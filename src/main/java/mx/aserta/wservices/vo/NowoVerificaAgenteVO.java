/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoVerificaAgenteVO {
    
    private String noAgente;

    public String getNoAgente() {
        return noAgente;
    }

    public void setNoAgente(String noAgente) {
        this.noAgente = noAgente;
    }
}
