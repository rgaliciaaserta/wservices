/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import mx.aserta.wservices.sps.LogBitacoraFileService;

/**
 *
 * @author rgalicia
 */
public class NowoFileProcessVO {
    
    private Long id;
    
    private Long nowoProcessId;
    
    private String pathFile;
    
    private String estatus;
    
    private String message;
    
    private String rfcEmisor;
    
    private String idPoliza;
    
    private String idMovimiento;
    
    private String tipoDocumento;
    
    private String serie;
    
    private String folio;
    
    private String formato;
    
    private String usuario;
    
    private LogBitacoraFileService logBitacoraFileService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNowoProcessId() {
        return nowoProcessId;
    }

    public void setNowoProcessId(Long nowoProcessId) {
        this.nowoProcessId = nowoProcessId;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRfcEmisor() {
        return rfcEmisor;
    }

    public void setRfcEmisor(String rfcEmisor) {
        this.rfcEmisor = rfcEmisor;
    }

    public String getIdPoliza() {
        return idPoliza;
    }

    public void setIdPoliza(String idPoliza) {
        this.idPoliza = idPoliza;
    }

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(String idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public LogBitacoraFileService getLogBitacoraFileService() {
        return logBitacoraFileService;
    }

    public void setLogBitacoraFileService(LogBitacoraFileService logBitacoraFileService) {
        this.logBitacoraFileService = logBitacoraFileService;
    }
    
    /**
     * Save or Update
     */
    public void saveUpdate() {
        if (this.logBitacoraFileService == null) {
            return;
        }
        
        this.logBitacoraFileService.execute(this);
    }
}
