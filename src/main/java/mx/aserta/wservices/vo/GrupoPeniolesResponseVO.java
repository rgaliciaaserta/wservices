/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;
import mx.aserta.wservices.model.GrupoPeniolesResponseModel;

/**
 *
 * @author rgalicia
 */
public class GrupoPeniolesResponseVO {
    
    private String exito;
    
    private String mensaje;
    
    private List<GrupoPeniolesResponseModel> datos;

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<GrupoPeniolesResponseModel> getDatos() {
        return datos;
    }

    public void setDatos(List<GrupoPeniolesResponseModel> datos) {
        this.datos = datos;
    }
}
