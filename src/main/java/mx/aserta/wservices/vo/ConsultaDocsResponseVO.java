/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class ConsultaDocsResponseVO {
    
    private String codigo;
    
    private String mensaje;
    
    private List<ArchivoVO> archivo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ArchivoVO> getArchivo() {
        return archivo;
    }

    public void setArchivo(List<ArchivoVO> archivo) {
        this.archivo = archivo;
    }
}
