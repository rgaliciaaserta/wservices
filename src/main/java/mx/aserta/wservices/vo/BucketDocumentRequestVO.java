/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class BucketDocumentRequestVO {
    
    private String xincodehardwareid;

    public String getXincodehardwareid() {
        return xincodehardwareid;
    }

    public void setXincodehardwareid(String xincodehardwareid) {
        this.xincodehardwareid = xincodehardwareid;
    }
}
