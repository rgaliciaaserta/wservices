/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoSiniestroReporteVO {
    
    private NowoSiniestroPropietarioVO propietario;
    
    private NowoSiniestroVO siniestro;
    
    private List<NowoDocumentoVO> documentos;

    public NowoSiniestroPropietarioVO getPropietario() {
        return propietario;
    }

    public void setPropietario(NowoSiniestroPropietarioVO propietario) {
        this.propietario = propietario;
    }

    public NowoSiniestroVO getSiniestro() {
        return siniestro;
    }

    public void setSiniestro(NowoSiniestroVO siniestro) {
        this.siniestro = siniestro;
    }

    public List<NowoDocumentoVO> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<NowoDocumentoVO> documentos) {
        this.documentos = documentos;
    }
}
