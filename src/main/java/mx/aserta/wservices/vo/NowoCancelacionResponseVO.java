/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoCancelacionResponseVO {
    
    private String exito;
    
    private String mensaje;
    
    private String idNotaCredito;
    
    private String ivaDevolucion;
    
    private String primaDevolucion;
    
    private String totalDevolucion;

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setIdNotaCredito(String idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public String getIvaDevolucion() {
        return ivaDevolucion;
    }

    public void setIvaDevolucion(String ivaDevolucion) {
        this.ivaDevolucion = ivaDevolucion;
    }

    public String getPrimaDevolucion() {
        return primaDevolucion;
    }

    public void setPrimaDevolucion(String primaDevolucion) {
        this.primaDevolucion = primaDevolucion;
    }

    public String getTotalDevolucion() {
        return totalDevolucion;
    }

    public void setTotalDevolucion(String totalDevolucion) {
        this.totalDevolucion = totalDevolucion;
    }
}
