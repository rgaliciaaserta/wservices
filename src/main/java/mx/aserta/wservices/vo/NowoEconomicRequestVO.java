/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoEconomicRequestVO {
    
    private double amount;
    
    private List<NowoVariablesVO> variables;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<NowoVariablesVO> getVariables() {
        return variables;
    }

    public void setVariables(List<NowoVariablesVO> variables) {
        this.variables = variables;
    }
}
