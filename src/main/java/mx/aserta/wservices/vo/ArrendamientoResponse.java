/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class ArrendamientoResponse {
    
    private String flgExito;
    
    private String mensaje;
    
    private List<ArrendamientoDetalleResponseV2> detalle;
    
    //private ArrendamientoDetalleResponse detalle;

    public String getFlgExito() {
        return flgExito;
    }

    public void setFlgExito(String flgExito) {
        this.flgExito = flgExito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /*public ArrendamientoDetalleResponse getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrendamientoDetalleResponse detalle) {
        this.detalle = detalle;
    }*/

    public List<ArrendamientoDetalleResponseV2> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ArrendamientoDetalleResponseV2> detalle) {
        this.detalle = detalle;
    }
}
