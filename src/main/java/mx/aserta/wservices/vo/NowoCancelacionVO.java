/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoCancelacionVO {
    
    private String polizaId;

    public String getPolizaId() {
        return polizaId;
    }

    public void setPolizaId(String polizaId) {
        this.polizaId = polizaId;
    }
}
