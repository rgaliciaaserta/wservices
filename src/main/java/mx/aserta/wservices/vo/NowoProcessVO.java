/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import mx.aserta.wservices.sps.LogBitacoraService;

/**
 *
 * @author rgalicia
 */
public class NowoProcessVO {
    
    private Long id;
    
    private String idPoliza;
    
    private String tipoPaquete;
    
    private String estatus;
    
    private String pathFile;
    
    private String message;
    
    private String usuario;
    
    private LogBitacoraService logBitacoraService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdPoliza() {
        return idPoliza;
    }

    public void setIdPoliza(String idPoliza) {
        this.idPoliza = idPoliza;
    }

    public String getTipoPaquete() {
        return tipoPaquete;
    }

    public void setTipoPaquete(String tipoPaquete) {
        this.tipoPaquete = tipoPaquete;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public LogBitacoraService getLogBitacoraService() {
        return logBitacoraService;
    }

    public void setLogBitacoraService(LogBitacoraService logBitacoraService) {
        this.logBitacoraService = logBitacoraService;
    }
    
    /**
     * Save / Update log
     */
    public void saveUpdate() {
        if (this.logBitacoraService == null) {
            return;
        }
        
        this.logBitacoraService.execute(this);
    }
}
