/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author rgalicia
 */
public class NowoCreaBaseVO {
    
    private String idContratante;
    
    private String idAsegurado;
    
    private String idObligado;
    
    private String moneda;
    
    private BigDecimal montoRenta;
    
    private String esquema;
    
    private Date fechaInicio;
    
    private Integer meses;
    
    private String agenteReferenciador;
    
    private String bucket;
    
    private String codigoPropiedad;
    
    private String hocOperationId;
    
    private String referenciaInmobiliariaNowo;

    public String getIdContratante() {
        return idContratante;
    }

    public void setIdContratante(String idContratante) {
        this.idContratante = idContratante;
    }

    public String getIdAsegurado() {
        return idAsegurado;
    }

    public void setIdAsegurado(String idAsegurado) {
        this.idAsegurado = idAsegurado;
    }

    public String getIdObligado() {
        return idObligado;
    }

    public void setIdObligado(String idObligado) {
        this.idObligado = idObligado;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(BigDecimal montoRenta) {
        this.montoRenta = montoRenta;
    }

    public String getEsquema() {
        return esquema;
    }

    public void setEsquema(String esquema) {
        this.esquema = esquema;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public String getAgenteReferenciador() {
        return agenteReferenciador;
    }

    public void setAgenteReferenciador(String agenteReferenciador) {
        this.agenteReferenciador = agenteReferenciador;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getCodigoPropiedad() {
        return codigoPropiedad;
    }

    public void setCodigoPropiedad(String codigoPropiedad) {
        this.codigoPropiedad = codigoPropiedad;
    }

    public String getHocOperationId() {
        return hocOperationId;
    }

    public void setHocOperationId(String hocOperationId) {
        this.hocOperationId = hocOperationId;
    }

    public String getReferenciaInmobiliariaNowo() {
        return referenciaInmobiliariaNowo;
    }

    public void setReferenciaInmobiliariaNowo(String referenciaInmobiliariaNowo) {
        this.referenciaInmobiliariaNowo = referenciaInmobiliariaNowo;
    }
}
