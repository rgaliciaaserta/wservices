/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoUpdateRentDecisionRequestVO {
    
    private Double montoRenta;
    
    private NowoUpdateRentDecisionInquilinoVO inquilino;
    
    private NowoUpdateRentDecisionInquilinoVO aval;

    public Double getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(Double montoRenta) {
        this.montoRenta = montoRenta;
    }

    public NowoUpdateRentDecisionInquilinoVO getInquilino() {
        return inquilino;
    }

    public void setInquilino(NowoUpdateRentDecisionInquilinoVO inquilino) {
        this.inquilino = inquilino;
    }

    public NowoUpdateRentDecisionInquilinoVO getAval() {
        return aval;
    }

    public void setAval(NowoUpdateRentDecisionInquilinoVO aval) {
        this.aval = aval;
    }
}
