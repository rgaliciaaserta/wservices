/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoAnulacionVO {
    
    private String polizaId;

    private String claveFiscal;
    
    public String getPolizaId() {
        return polizaId;
    }

    public void setPolizaId(String polizaId) {
        this.polizaId = polizaId;
    }

    public String getClaveFiscal() {
        return claveFiscal;
    }

    public void setClaveFiscal(String claveFiscal) {
        this.claveFiscal = claveFiscal;
    }
}
