/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoRentDecisionInquilinoVO {
    
    private Double buroScore;
    
    private String curp;
    
    private String nombreCompleto;
    
    private String email;
    
    private String telefono;
    
    private Double metrosCuadrados;
    
    private String trabajo;
    
    private Double numeroCuartosBanioActual;
    
    private Double metrosCasaActual;
    
    private String tipoCasaActual;
    
    private Double numeroEstacionamientoActual;
    
    private Double numeroCuartosActual;
    
    private NowoRentDecisionDireccionVO direccion;

    public Double getBuroScore() {
        return buroScore;
    }

    public void setBuroScore(Double buroScore) {
        this.buroScore = buroScore;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Double getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Double metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public Double getNumeroCuartosBanioActual() {
        return numeroCuartosBanioActual;
    }

    public void setNumeroCuartosBanioActual(Double numeroCuartosBanioActual) {
        this.numeroCuartosBanioActual = numeroCuartosBanioActual;
    }

    public Double getMetrosCasaActual() {
        return metrosCasaActual;
    }

    public void setMetrosCasaActual(Double metrosCasaActual) {
        this.metrosCasaActual = metrosCasaActual;
    }

    public String getTipoCasaActual() {
        return tipoCasaActual;
    }

    public void setTipoCasaActual(String tipoCasaActual) {
        this.tipoCasaActual = tipoCasaActual;
    }

    public Double getNumeroEstacionamientoActual() {
        return numeroEstacionamientoActual;
    }

    public void setNumeroEstacionamientoActual(Double numeroEstacionamientoActual) {
        this.numeroEstacionamientoActual = numeroEstacionamientoActual;
    }

    public Double getNumeroCuartosActual() {
        return numeroCuartosActual;
    }

    public void setNumeroCuartosActual(Double numeroCuartosActual) {
        this.numeroCuartosActual = numeroCuartosActual;
    }

    public NowoRentDecisionDireccionVO getDireccion() {
        return direccion;
    }

    public void setDireccion(NowoRentDecisionDireccionVO direccion) {
        this.direccion = direccion;
    }
}
