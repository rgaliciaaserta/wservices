/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoPersonaMoralVO {
    
    private String denominacionSocial;
    
    private String rfc;
    
    private String numeroAgente;
    
    private boolean aceptoAvisoPrivacidad;
    
    private boolean aceptoTerminosCondiciones;
    
    private boolean aceptoComunicacionComercial;
    
    private NowoPersonaContactoVO representanteLegal;
    
    private NowoPersonaContactoVO contactoComercial;
    
    private NowoPersonaContactoVO contactoAdministrativo;
    
    private List<NowoDocumentoVO> documentos;

    public String getDenominacionSocial() {
        return denominacionSocial;
    }

    public void setDenominacionSocial(String denominacionSocial) {
        this.denominacionSocial = denominacionSocial;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNumeroAgente() {
        return numeroAgente;
    }

    public void setNumeroAgente(String numeroAgente) {
        this.numeroAgente = numeroAgente;
    }

    public NowoPersonaContactoVO getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(NowoPersonaContactoVO representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public NowoPersonaContactoVO getContactoComercial() {
        return contactoComercial;
    }

    public void setContactoComercial(NowoPersonaContactoVO contactoComercial) {
        this.contactoComercial = contactoComercial;
    }

    public NowoPersonaContactoVO getContactoAdministrativo() {
        return contactoAdministrativo;
    }

    public void setContactoAdministrativo(NowoPersonaContactoVO contactoAdministrativo) {
        this.contactoAdministrativo = contactoAdministrativo;
    }

    public List<NowoDocumentoVO> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<NowoDocumentoVO> documentos) {
        this.documentos = documentos;
    }

    public boolean isAceptoAvisoPrivacidad() {
        return aceptoAvisoPrivacidad;
    }

    public void setAceptoAvisoPrivacidad(boolean aceptoAvisoPrivacidad) {
        this.aceptoAvisoPrivacidad = aceptoAvisoPrivacidad;
    }

    public boolean isAceptoTerminosCondiciones() {
        return aceptoTerminosCondiciones;
    }

    public void setAceptoTerminosCondiciones(boolean aceptoTerminosCondiciones) {
        this.aceptoTerminosCondiciones = aceptoTerminosCondiciones;
    }

    public boolean isAceptoComunicacionComercial() {
        return aceptoComunicacionComercial;
    }

    public void setAceptoComunicacionComercial(boolean aceptoComunicacionComercial) {
        this.aceptoComunicacionComercial = aceptoComunicacionComercial;
    }
}
