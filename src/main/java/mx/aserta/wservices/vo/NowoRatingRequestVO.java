/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoRatingRequestVO {
    
    private String curp;
    
    private String nombreCompleto;
    
    private Double metrosCuadrados;
    
    private String telefono;
    
    private String email;
    
    private Double buroScore;
    
    private Double montoRenta;
    
    private NowoRatingAddressRequestVO direccion;

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Double getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Double metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getBuroScore() {
        return buroScore;
    }

    public void setBuroScore(Double buroScore) {
        this.buroScore = buroScore;
    }

    public Double getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(Double montoRenta) {
        this.montoRenta = montoRenta;
    }

    public NowoRatingAddressRequestVO getDireccion() {
        return direccion;
    }

    public void setDireccion(NowoRatingAddressRequestVO direccion) {
        this.direccion = direccion;
    }
}
