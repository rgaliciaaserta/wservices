/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoPersonaFisicaVO {
    
    private String nombre;
    
    private String apellidoPaterno;
    
    private String apellidoMaterno;
    
    private String rfc;
    
    private String curp;
    
    private String giro;
    
    private String email;
    
    private String telefono;
    
    private String numeroAgente;
    
    private boolean aceptoAvisoPrivacidad;
    
    private boolean aceptoTerminosCondiciones;
    
    private boolean aceptoComunicacionComercial;
    
    private List<NowoDocumentoVO> documentos;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNumeroAgente() {
        return numeroAgente;
    }

    public void setNumeroAgente(String numeroAgente) {
        this.numeroAgente = numeroAgente;
    }

    public List<NowoDocumentoVO> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<NowoDocumentoVO> documentos) {
        this.documentos = documentos;
    }

    public boolean isAceptoAvisoPrivacidad() {
        return aceptoAvisoPrivacidad;
    }

    public void setAceptoAvisoPrivacidad(boolean aceptoAvisoPrivacidad) {
        this.aceptoAvisoPrivacidad = aceptoAvisoPrivacidad;
    }

    public boolean isAceptoTerminosCondiciones() {
        return aceptoTerminosCondiciones;
    }

    public void setAceptoTerminosCondiciones(boolean aceptoTerminosCondiciones) {
        this.aceptoTerminosCondiciones = aceptoTerminosCondiciones;
    }

    public boolean isAceptoComunicacionComercial() {
        return aceptoComunicacionComercial;
    }

    public void setAceptoComunicacionComercial(boolean aceptoComunicacionComercial) {
        this.aceptoComunicacionComercial = aceptoComunicacionComercial;
    }
}
