/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoRentEvaluateResponseVO {
    
    private int scoreRate;
    
    private String operationId;
    
    private double rentalCapacity;
    
    private String codigo;
    
    private String mensaje;

    public int getScoreRate() {
        return scoreRate;
    }

    public void setScoreRate(int scoreRate) {
        this.scoreRate = scoreRate;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public double getRentalCapacity() {
        return rentalCapacity;
    }

    public void setRentalCapacity(double rentalCapacity) {
        this.rentalCapacity = rentalCapacity;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
