/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rgalicia
 */
@XmlRootElement(name = "altaClienteRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class AltaClienteVO {
    
    @XmlElement(name = "codigoAgenteNowo", required = false, nillable = true)
    private String codigoAgenteNowo;
    
    @XmlElement(name = "tipoPersona", required = true, nillable = false)
    private String tipoPersona;
    
    @XmlElement(name = "perfil", required = true, nillable = false)
    private String perfil;
    
    @XmlElement(name = "nombre", required = true, nillable = false)
    private String nombre;
    
    @XmlElement(name = "primerApellido", required = true, nillable = false)
    private String primerApellido;
    
    @XmlElement(name = "segundoApellido", required = true, nillable = false)
    private String segundoApellido;
    
    @XmlElement(name = "fechaNacimiento", required = true, nillable = false)
    private Date fechaNacimiento;
    
    @XmlElement(name = "lugarNacimiento", required = true, nillable = false)
    private String lugarNacimiento;
    
    @XmlElement(name = "nacionalidad", required = true, nillable = false)
    private String nacionalidad;
    
    @XmlElement(name = "ocupacion", required = true, nillable = false)
    private String ocupacion;
    
    @XmlElement(name = "telefono", required = true, nillable = false)
    private String telefono;
    
    @XmlElement(name = "correo", required = true, nillable = false)
    private String correo;
    
    @XmlElement(name = "curp", required = true, nillable = false)
    private String curp;
    
    @XmlElement(name = "rfc", required = true, nillable = false)
    private String rfc;
    
    @XmlElement(name = "calleDomicilioCliente", required = true, nillable = false)
    private String calleDomicilioCliente;
    
    @XmlElement(name = "numExteriorDomicilioCliente", required = true, nillable = false)
    private String numExteriorDomicilioCliente;
    
    @XmlElement(name = "numInteriorDomicilioCliente", required = true, nillable = false)
    private String numInteriorDomicilioCliente;
    
    @XmlElement(name = "cpDomicilioCliente", required = true, nillable = false)
    private String cpDomicilioCliente;
    
    @XmlElement(name = "coloniaDomicilioCliente", required = true, nillable = false)
    private String coloniaDomicilioCliente;
    
    @XmlElement(name = "delMunDomicilioCliente", required = true, nillable = false)
    private String delMunDomicilioCliente;
    
    @XmlElement(name = "ciudadDomicilioCliente", required = true, nillable = false)
    private String ciudadDomicilioCliente;
    
    @XmlElement(name = "estadoDomicilioCliente", required = true, nillable = false)
    private String estadoDomicilioCliente;
    
    @XmlElement(name = "paisDomicilioCliente", required = true, nillable = false)
    private String paisDomicilioCliente;
    
    @XmlElement(name = "numeroIdentificacionCliente", required = true, nillable = false)
    private String numeroIdentificacionCliente;
    
    @XmlElement(name = "tipoIdentificacionCliente", required = true, nillable = false)
    private String tipoIdentificacionCliente;
    
    @XmlElement(name = "calleDomicilioRentar", required = false, nillable = true)
    private String calleDomicilioRentar;
    
    @XmlElement(name = "numExteriorDomicilioRentar", required = false, nillable = true)
    private String numExteriorDomicilioRentar;
    
    @XmlElement(name = "numInteriorDomicilioRentar", required = false, nillable = true)
    private String numInteriorDomicilioRentar;
    
    @XmlElement(name = "cpDomicilioRentar", required = false, nillable = true)
    private String cpDomicilioRentar;
    
    @XmlElement(name = "coloniaDomicilioRentar", required = false, nillable = true)
    private String coloniaDomicilioRentar;
    
    @XmlElement(name = "delMunDomicilioRentar", required = false, nillable = true)
    private String delMunDomicilioRentar;
    
    @XmlElement(name = "ciudadDomicilioRentar", required = false, nillable = true)
    private String ciudadDomicilioRentar;
    
    @XmlElement(name = "estadoDomicilioRentar", required = false, nillable = true)
    private String estadoDomicilioRentar;
    
    @XmlElement(name = "paisDomicilioRentar", required = false, nillable = true)
    private String paisDomicilioRentar;
    
    @XmlElement(name = "m2DomicilioRentar", required = false, nillable = true)
    private BigDecimal m2DomicilioRentar;
    
    @XmlElement(name = "montoRenta", required = false, nillable = true)
    private BigDecimal montoRenta;
    
    @XmlElement(name = "diaLimitePagoRenta", required = false, nillable = true)
    private String diaLimitePagoRenta;
    
    @XmlElement(name = "inicioVigenciaGarantia", required = false, nillable = true)
    private Date inicioVigenciaGarantia;
    
    @XmlElement(name = "finVigenciaGarantia", required = false, nillable = true)
    private Date finVigenciaGarantia;
    
    @XmlElement(name = "codigoPropiedad", required = false, nillable = true)
    private String codigoPropiedad;
    
    @XmlElement(name = "inicioVigenciaContrato", required = false, nillable = true)
    private Date inicioVigenciaContrato;
    
    @XmlElement(name = "finVigenciaContrato", required = false, nillable = true)
    private Date finVigenciaContrato;
    
    @XmlElement(name = "banco", required = false, nillable = true)
    private String banco;
    
    @XmlElement(name = "tarjetaCredito", required = false, nillable = true)
    private String tarjetaCredito;
    
    @XmlElement(name = "clabe", required = false, nillable = true)
    private String clabe;
    
    @XmlElement(name = "cuenta", required = false, nillable = true)
    private String cuenta;
    
    @XmlElement(name = "scoreHocelot", required = false, nillable = true)
    private String scoreHocelot;

    @XmlElement(name = "fechaVencimientoTarjeta", required = false, nillable = true)
    private Date fechaVencimientoTarjeta;
    
    @XmlElement(name = "nue", required = false, nillable = true)
    private String nue;
    
    @XmlElement(name = "tipoResidencia", required = false, nillable = true)
    private String tipoResidencia;
    
    public String getCodigoAgenteNowo() {
        return codigoAgenteNowo;
    }

    public void setCodigoAgenteNowo(String codigoAgenteNowo) {
        this.codigoAgenteNowo = codigoAgenteNowo;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCalleDomicilioCliente() {
        return calleDomicilioCliente;
    }

    public void setCalleDomicilioCliente(String calleDomicilioCliente) {
        this.calleDomicilioCliente = calleDomicilioCliente;
    }

    public String getNumExteriorDomicilioCliente() {
        return numExteriorDomicilioCliente;
    }

    public void setNumExteriorDomicilioCliente(String numExteriorDomicilioCliente) {
        this.numExteriorDomicilioCliente = numExteriorDomicilioCliente;
    }

    public String getNumInteriorDomicilioCliente() {
        return numInteriorDomicilioCliente;
    }

    public void setNumInteriorDomicilioCliente(String numInteriorDomicilioCliente) {
        this.numInteriorDomicilioCliente = numInteriorDomicilioCliente;
    }

    public String getCpDomicilioCliente() {
        return cpDomicilioCliente;
    }

    public void setCpDomicilioCliente(String cpDomicilioCliente) {
        this.cpDomicilioCliente = cpDomicilioCliente;
    }

    public String getColoniaDomicilioCliente() {
        return coloniaDomicilioCliente;
    }

    public void setColoniaDomicilioCliente(String coloniaDomicilioCliente) {
        this.coloniaDomicilioCliente = coloniaDomicilioCliente;
    }

    public String getDelMunDomicilioCliente() {
        return delMunDomicilioCliente;
    }

    public void setDelMunDomicilioCliente(String delMunDomicilioCliente) {
        this.delMunDomicilioCliente = delMunDomicilioCliente;
    }

    public String getCiudadDomicilioCliente() {
        return ciudadDomicilioCliente;
    }

    public void setCiudadDomicilioCliente(String ciudadDomicilioCliente) {
        this.ciudadDomicilioCliente = ciudadDomicilioCliente;
    }

    public String getEstadoDomicilioCliente() {
        return estadoDomicilioCliente;
    }

    public void setEstadoDomicilioCliente(String estadoDomicilioCliente) {
        this.estadoDomicilioCliente = estadoDomicilioCliente;
    }

    public String getPaisDomicilioCliente() {
        return paisDomicilioCliente;
    }

    public void setPaisDomicilioCliente(String paisDomicilioCliente) {
        this.paisDomicilioCliente = paisDomicilioCliente;
    }

    public String getNumeroIdentificacionCliente() {
        return numeroIdentificacionCliente;
    }

    public void setNumeroIdentificacionCliente(String numeroIdentificacionCliente) {
        this.numeroIdentificacionCliente = numeroIdentificacionCliente;
    }

    public String getTipoIdentificacionCliente() {
        return tipoIdentificacionCliente;
    }

    public void setTipoIdentificacionCliente(String tipoIdentificacionCliente) {
        this.tipoIdentificacionCliente = tipoIdentificacionCliente;
    }

    public String getCalleDomicilioRentar() {
        return calleDomicilioRentar;
    }

    public void setCalleDomicilioRentar(String calleDomicilioRentar) {
        this.calleDomicilioRentar = calleDomicilioRentar;
    }

    public String getNumExteriorDomicilioRentar() {
        return numExteriorDomicilioRentar;
    }

    public void setNumExteriorDomicilioRentar(String numExteriorDomicilioRentar) {
        this.numExteriorDomicilioRentar = numExteriorDomicilioRentar;
    }

    public String getNumInteriorDomicilioRentar() {
        return numInteriorDomicilioRentar;
    }

    public void setNumInteriorDomicilioRentar(String numInteriorDomicilioRentar) {
        this.numInteriorDomicilioRentar = numInteriorDomicilioRentar;
    }

    public String getCpDomicilioRentar() {
        return cpDomicilioRentar;
    }

    public void setCpDomicilioRentar(String cpDomicilioRentar) {
        this.cpDomicilioRentar = cpDomicilioRentar;
    }

    public String getColoniaDomicilioRentar() {
        return coloniaDomicilioRentar;
    }

    public void setColoniaDomicilioRentar(String coloniaDomicilioRentar) {
        this.coloniaDomicilioRentar = coloniaDomicilioRentar;
    }

    public String getDelMunDomicilioRentar() {
        return delMunDomicilioRentar;
    }

    public void setDelMunDomicilioRentar(String delMunDomicilioRentar) {
        this.delMunDomicilioRentar = delMunDomicilioRentar;
    }

    public String getCiudadDomicilioRentar() {
        return ciudadDomicilioRentar;
    }

    public void setCiudadDomicilioRentar(String ciudadDomicilioRentar) {
        this.ciudadDomicilioRentar = ciudadDomicilioRentar;
    }

    public String getEstadoDomicilioRentar() {
        return estadoDomicilioRentar;
    }

    public void setEstadoDomicilioRentar(String estadoDomicilioRentar) {
        this.estadoDomicilioRentar = estadoDomicilioRentar;
    }

    public String getPaisDomicilioRentar() {
        return paisDomicilioRentar;
    }

    public void setPaisDomicilioRentar(String paisDomicilioRentar) {
        this.paisDomicilioRentar = paisDomicilioRentar;
    }

    public BigDecimal getM2DomicilioRentar() {
        return m2DomicilioRentar;
    }

    public void setM2DomicilioRentar(BigDecimal m2DomicilioRentar) {
        this.m2DomicilioRentar = m2DomicilioRentar;
    }

    public BigDecimal getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(BigDecimal montoRenta) {
        this.montoRenta = montoRenta;
    }

    public String getDiaLimitePagoRenta() {
        return diaLimitePagoRenta;
    }

    public void setDiaLimitePagoRenta(String diaLimitePagoRenta) {
        this.diaLimitePagoRenta = diaLimitePagoRenta;
    }

    public String getCodigoPropiedad() {
        return codigoPropiedad;
    }

    public void setCodigoPropiedad(String codigoPropiedad) {
        this.codigoPropiedad = codigoPropiedad;
    }

    public Date getInicioVigenciaGarantia() {
        return inicioVigenciaGarantia;
    }

    public void setInicioVigenciaGarantia(Date inicioVigenciaGarantia) {
        this.inicioVigenciaGarantia = inicioVigenciaGarantia;
    }

    public Date getFinVigenciaGarantia() {
        return finVigenciaGarantia;
    }

    public void setFinVigenciaGarantia(Date finVigenciaGarantia) {
        this.finVigenciaGarantia = finVigenciaGarantia;
    }

    public Date getInicioVigenciaContrato() {
        return inicioVigenciaContrato;
    }

    public void setInicioVigenciaContrato(Date inicioVigenciaContrato) {
        this.inicioVigenciaContrato = inicioVigenciaContrato;
    }

    public Date getFinVigenciaContrato() {
        return finVigenciaContrato;
    }

    public void setFinVigenciaContrato(Date finVigenciaContrato) {
        this.finVigenciaContrato = finVigenciaContrato;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getTarjetaCredito() {
        return tarjetaCredito;
    }

    public void setTarjetaCredito(String tarjetaCredito) {
        this.tarjetaCredito = tarjetaCredito;
    }

    public String getClabe() {
        return clabe;
    }

    public void setClabe(String clabe) {
        this.clabe = clabe;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getScoreHocelot() {
        return scoreHocelot;
    }

    public void setScoreHocelot(String scoreHocelot) {
        this.scoreHocelot = scoreHocelot;
    }

    public Date getFechaVencimientoTarjeta() {
        return fechaVencimientoTarjeta;
    }

    public void setFechaVencimientoTarjeta(Date fechaVencimientoTarjeta) {
        this.fechaVencimientoTarjeta = fechaVencimientoTarjeta;
    }

    public String getNue() {
        return nue;
    }

    public void setNue(String nue) {
        this.nue = nue;
    }

    public String getTipoResidencia() {
        return tipoResidencia;
    }

    public void setTipoResidencia(String tipoResidencia) {
        this.tipoResidencia = tipoResidencia;
    }
}
