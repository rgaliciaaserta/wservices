/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoRentDecisionRequestVO {
    
    private Double montoRenta;
    
    private NowoRentDecisionInquilinoVO inquilino;

    public Double getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(Double montoRenta) {
        this.montoRenta = montoRenta;
    }

    public NowoRentDecisionInquilinoVO getInquilino() {
        return inquilino;
    }

    public void setInquilino(NowoRentDecisionInquilinoVO inquilino) {
        this.inquilino = inquilino;
    }
}
