/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.Map;

/**
 *
 * @author rgalicia
 */
public class ScoringResponseVO {
    
    /*private String operationId;
    
    private double scoreRate;
    
    private String error;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public double getScoreRate() {
        return scoreRate;
    }

    public void setScoreRate(double scoreRate) {
        this.scoreRate = scoreRate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }*/
    
    private String operationId;
    
    private double minAmount;
    
    private double maxAmount;
    
    private String additionalInformation;
    
    private String error;
    
    private Map<String, String> reputationalRiskScore;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Map<String, String> getReputationalRiskScore() {
        return reputationalRiskScore;
    }

    public void setReputationalRiskScore(Map<String, String> reputationalRiskScore) {
        this.reputationalRiskScore = reputationalRiskScore;
    }
}
