/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class VerificaCodigoAfiliadoResponseVO {
    
    private String exito;
    
    private String mensaje;
    
    private String promotorInmobiliario;

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getPromotorInmobiliario() {
        return promotorInmobiliario;
    }

    public void setPromotorInmobiliario(String promotorInmobiliario) {
        this.promotorInmobiliario = promotorInmobiliario;
    }
}
