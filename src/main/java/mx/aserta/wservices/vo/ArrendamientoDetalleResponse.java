/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.math.BigDecimal;

/**
 *
 * @author rgalicia
 */
public class ArrendamientoDetalleResponse {
    
    private Integer meses;
    
    private BigDecimal pctPrima;
    
    private BigDecimal primaPesos;
    
    private BigDecimal pctComisionInmobiliario;
    
    private BigDecimal comisionInmobiliariaPesos;
    
    private BigDecimal pctComisionAgente;
    
    private BigDecimal comisionAgentePesos;
    
    private BigDecimal pctComisionHocelot;
    
    private BigDecimal comisionHocelotPesos;
    
    private BigDecimal pctComisionSucursal;
    
    private BigDecimal comisionSucursalPesos;
    
    private BigDecimal totalCertificadoPesos;
    
    private BigDecimal pctGa;
    
    private BigDecimal primaTarifa;
    
    private BigDecimal derechosPoliza;
    
    private BigDecimal gastosInvestigacion;
    
    private BigDecimal subtotalRecibo;
    
    private BigDecimal ivaRecibo;
    
    private BigDecimal totalRecibo;
    
    private BigDecimal pi;
    
    private BigDecimal sp;
    
    private BigDecimal cc;
    
    private BigDecimal go;
    
    private BigDecimal ga;
    
    private BigDecimal ut;
    
    private BigDecimal recargos;
    
    private BigDecimal difRecargos;
    
    public BigDecimal getPctPrima() {
        return pctPrima;
    }

    public void setPctPrima(BigDecimal pctPrima) {
        this.pctPrima = pctPrima;
    }

    public BigDecimal getPrimaPesos() {
        return primaPesos;
    }

    public void setPrimaPesos(BigDecimal primaPesos) {
        this.primaPesos = primaPesos;
    }

    public BigDecimal getPctComisionInmobiliario() {
        return pctComisionInmobiliario;
    }

    public void setPctComisionInmobiliario(BigDecimal pctComisionInmobiliario) {
        this.pctComisionInmobiliario = pctComisionInmobiliario;
    }

    public BigDecimal getComisionInmobiliariaPesos() {
        return comisionInmobiliariaPesos;
    }

    public void setComisionInmobiliariaPesos(BigDecimal comisionInmobiliariaPesos) {
        this.comisionInmobiliariaPesos = comisionInmobiliariaPesos;
    }

    public BigDecimal getPctComisionAgente() {
        return pctComisionAgente;
    }

    public void setPctComisionAgente(BigDecimal pctComisionAgente) {
        this.pctComisionAgente = pctComisionAgente;
    }

    public BigDecimal getComisionAgentePesos() {
        return comisionAgentePesos;
    }

    public void setComisionAgentePesos(BigDecimal comisionAgentePesos) {
        this.comisionAgentePesos = comisionAgentePesos;
    }

    public BigDecimal getPctComisionHocelot() {
        return pctComisionHocelot;
    }

    public void setPctComisionHocelot(BigDecimal pctComisionHocelot) {
        this.pctComisionHocelot = pctComisionHocelot;
    }

    public BigDecimal getComisionHocelotPesos() {
        return comisionHocelotPesos;
    }

    public void setComisionHocelotPesos(BigDecimal comisionHocelotPesos) {
        this.comisionHocelotPesos = comisionHocelotPesos;
    }

    public BigDecimal getPctComisionSucursal() {
        return pctComisionSucursal;
    }

    public void setPctComisionSucursal(BigDecimal pctComisionSucursal) {
        this.pctComisionSucursal = pctComisionSucursal;
    }

    public BigDecimal getComisionSucursalPesos() {
        return comisionSucursalPesos;
    }

    public void setComisionSucursalPesos(BigDecimal comisionSucursalPesos) {
        this.comisionSucursalPesos = comisionSucursalPesos;
    }

    public BigDecimal getTotalCertificadoPesos() {
        return totalCertificadoPesos;
    }

    public void setTotalCertificadoPesos(BigDecimal totalCertificadoPesos) {
        this.totalCertificadoPesos = totalCertificadoPesos;
    }

    public BigDecimal getPctGa() {
        return pctGa;
    }

    public void setPctGa(BigDecimal pctGa) {
        this.pctGa = pctGa;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public BigDecimal getPrimaTarifa() {
        return primaTarifa;
    }

    public void setPrimaTarifa(BigDecimal primaTarifa) {
        this.primaTarifa = primaTarifa;
    }

    public BigDecimal getDerechosPoliza() {
        return derechosPoliza;
    }

    public void setDerechosPoliza(BigDecimal derechosPoliza) {
        this.derechosPoliza = derechosPoliza;
    }

    public BigDecimal getGastosInvestigacion() {
        return gastosInvestigacion;
    }

    public void setGastosInvestigacion(BigDecimal gastosInvestigacion) {
        this.gastosInvestigacion = gastosInvestigacion;
    }

    public BigDecimal getSubtotalRecibo() {
        return subtotalRecibo;
    }

    public void setSubtotalRecibo(BigDecimal subtotalRecibo) {
        this.subtotalRecibo = subtotalRecibo;
    }

    public BigDecimal getIvaRecibo() {
        return ivaRecibo;
    }

    public void setIvaRecibo(BigDecimal ivaRecibo) {
        this.ivaRecibo = ivaRecibo;
    }

    public BigDecimal getTotalRecibo() {
        return totalRecibo;
    }

    public void setTotalRecibo(BigDecimal totalRecibo) {
        this.totalRecibo = totalRecibo;
    }

    public BigDecimal getPi() {
        return pi;
    }

    public void setPi(BigDecimal pi) {
        this.pi = pi;
    }

    public BigDecimal getSp() {
        return sp;
    }

    public void setSp(BigDecimal sp) {
        this.sp = sp;
    }

    public BigDecimal getCc() {
        return cc;
    }

    public void setCc(BigDecimal cc) {
        this.cc = cc;
    }

    public BigDecimal getGo() {
        return go;
    }

    public void setGo(BigDecimal go) {
        this.go = go;
    }

    public BigDecimal getGa() {
        return ga;
    }

    public void setGa(BigDecimal ga) {
        this.ga = ga;
    }

    public BigDecimal getUt() {
        return ut;
    }

    public void setUt(BigDecimal ut) {
        this.ut = ut;
    }

    public BigDecimal getRecargos() {
        return recargos;
    }

    public void setRecargos(BigDecimal recargos) {
        this.recargos = recargos;
    }

    public BigDecimal getDifRecargos() {
        return difRecargos;
    }

    public void setDifRecargos(BigDecimal difRecargos) {
        this.difRecargos = difRecargos;
    }
}
