/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoPagoRequestVO {
    
    private String idTransaccion;
    
    private NowoDatosPagoVO datosPago;
    
    private NowoPropietarioVO propietario;

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public NowoDatosPagoVO getDatosPago() {
        return datosPago;
    }

    public void setDatosPago(NowoDatosPagoVO datosPago) {
        this.datosPago = datosPago;
    }

    public NowoPropietarioVO getPropietario() {
        return propietario;
    }

    public void setPropietario(NowoPropietarioVO propietario) {
        this.propietario = propietario;
    }
}
