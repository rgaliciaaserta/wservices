/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoCheckIdentityDataResponseVO {
    
    private String curpNumber;
    
    private String name;
    
    private String firstSurname;
    
    private String secondSurname;
    
    private String fullname;
    
    private String gender;
    
    private String birthDate;
    
    private String nationality;
    
    private String birthEntity;
    
    private String probationaryDocumentType;
    
    private String issuingEntity;
    
    private String registryMunicipality;
    
    private String yearReg;
    
    private String actNumber;
    
    private String book;
    
    private String volume;
    
    private String page;
    
    private String cripNumber;
    
    private List<String> historicalCurpNumbers;
    
    private String registryForeignersNumber;
    
    private String uniqueForeignerNumber;
    
    private String nssNumber;
    
    private String rfcNumber;
    
    private String curpStatus;

    public String getCurpNumber() {
        return curpNumber;
    }

    public void setCurpNumber(String curpNumber) {
        this.curpNumber = curpNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthEntity() {
        return birthEntity;
    }

    public void setBirthEntity(String birthEntity) {
        this.birthEntity = birthEntity;
    }

    public String getProbationaryDocumentType() {
        return probationaryDocumentType;
    }

    public void setProbationaryDocumentType(String probationaryDocumentType) {
        this.probationaryDocumentType = probationaryDocumentType;
    }

    public String getIssuingEntity() {
        return issuingEntity;
    }

    public void setIssuingEntity(String issuingEntity) {
        this.issuingEntity = issuingEntity;
    }

    public String getRegistryMunicipality() {
        return registryMunicipality;
    }

    public void setRegistryMunicipality(String registryMunicipality) {
        this.registryMunicipality = registryMunicipality;
    }

    public String getYearReg() {
        return yearReg;
    }

    public void setYearReg(String yearReg) {
        this.yearReg = yearReg;
    }

    public String getActNumber() {
        return actNumber;
    }

    public void setActNumber(String actNumber) {
        this.actNumber = actNumber;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getCripNumber() {
        return cripNumber;
    }

    public void setCripNumber(String cripNumber) {
        this.cripNumber = cripNumber;
    }

    public List<String> getHistoricalCurpNumbers() {
        return historicalCurpNumbers;
    }

    public void setHistoricalCurpNumbers(List<String> historicalCurpNumbers) {
        this.historicalCurpNumbers = historicalCurpNumbers;
    }

    public String getRegistryForeignersNumber() {
        return registryForeignersNumber;
    }

    public void setRegistryForeignersNumber(String registryForeignersNumber) {
        this.registryForeignersNumber = registryForeignersNumber;
    }

    public String getUniqueForeignerNumber() {
        return uniqueForeignerNumber;
    }

    public void setUniqueForeignerNumber(String uniqueForeignerNumber) {
        this.uniqueForeignerNumber = uniqueForeignerNumber;
    }

    public String getNssNumber() {
        return nssNumber;
    }

    public void setNssNumber(String nssNumber) {
        this.nssNumber = nssNumber;
    }

    public String getRfcNumber() {
        return rfcNumber;
    }

    public void setRfcNumber(String rfcNumber) {
        this.rfcNumber = rfcNumber;
    }

    public String getCurpStatus() {
        return curpStatus;
    }

    public void setCurpStatus(String curpStatus) {
        this.curpStatus = curpStatus;
    }
}
