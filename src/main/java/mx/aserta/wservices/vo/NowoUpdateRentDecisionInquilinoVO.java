/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoUpdateRentDecisionInquilinoVO {
    
    private String curp;
    
    private String nombreCompleto;
    
    private Double metrosCuadrados;
    
    private String telefono;
    
    private String email;
    
    private NowoUpateRentDecisionDireccionVO direccion;

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Double getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Double metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public NowoUpateRentDecisionDireccionVO getDireccion() {
        return direccion;
    }

    public void setDireccion(NowoUpateRentDecisionDireccionVO direccion) {
        this.direccion = direccion;
    }
}
