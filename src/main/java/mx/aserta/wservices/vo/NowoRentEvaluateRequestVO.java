/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoRentEvaluateRequestVO {
    
    private NowoEconomicRequestVO economicRequest;
    
    private List<NowoInterveningPartieVO> interveningParties; 

    public NowoEconomicRequestVO getEconomicRequest() {
        return economicRequest;
    }

    public void setEconomicRequest(NowoEconomicRequestVO economicRequest) {
        this.economicRequest = economicRequest;
    }

    public List<NowoInterveningPartieVO> getInterveningParties() {
        return interveningParties;
    }

    public void setInterveningParties(List<NowoInterveningPartieVO> interveningParties) {
        this.interveningParties = interveningParties;
    }
}
