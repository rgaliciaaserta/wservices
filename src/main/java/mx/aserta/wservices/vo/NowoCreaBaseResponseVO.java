/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.math.BigDecimal;

/**
 *
 * @author rgalicia
 */
public class NowoCreaBaseResponseVO {
    
    private String exito;
    
    private String mensaje;
    
    private String idBase;
    
    /*private BigDecimal comisionAgente;
    
    private BigDecimal comisionHocelot;
    
    private BigDecimal comisionInmobiliaria;
    
    private BigDecimal comisionSucursal;*/
    
    private BigDecimal derechosPoliza;
    
    private BigDecimal ivaRecibo;
    
    private BigDecimal subtotalRecibo;
    
    private BigDecimal totalCertificado;
    
    private BigDecimal totalRecibo;
    
    private BigDecimal gastosInvestigacion;
    
    private BigDecimal prima;
    
    private BigDecimal primaTarifa;

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getIdBase() {
        return idBase;
    }

    public void setIdBase(String idBase) {
        this.idBase = idBase;
    }

    /*public BigDecimal getComisionAgente() {
        return comisionAgente;
    }

    public void setComisionAgente(BigDecimal comisionAgente) {
        this.comisionAgente = comisionAgente;
    }

    public BigDecimal getComisionHocelot() {
        return comisionHocelot;
    }

    public void setComisionHocelot(BigDecimal comisionHocelot) {
        this.comisionHocelot = comisionHocelot;
    }

    public BigDecimal getComisionInmobiliaria() {
        return comisionInmobiliaria;
    }

    public void setComisionInmobiliaria(BigDecimal comisionInmobiliaria) {
        this.comisionInmobiliaria = comisionInmobiliaria;
    }

    public BigDecimal getComisionSucursal() {
        return comisionSucursal;
    }

    public void setComisionSucursal(BigDecimal comisionSucursal) {
        this.comisionSucursal = comisionSucursal;
    }*/

    public BigDecimal getDerechosPoliza() {
        return derechosPoliza;
    }

    public void setDerechosPoliza(BigDecimal derechosPoliza) {
        this.derechosPoliza = derechosPoliza;
    }

    public BigDecimal getIvaRecibo() {
        return ivaRecibo;
    }

    public void setIvaRecibo(BigDecimal ivaRecibo) {
        this.ivaRecibo = ivaRecibo;
    }

    public BigDecimal getSubtotalRecibo() {
        return subtotalRecibo;
    }

    public void setSubtotalRecibo(BigDecimal subtotalRecibo) {
        this.subtotalRecibo = subtotalRecibo;
    }

    public BigDecimal getTotalCertificado() {
        return totalCertificado;
    }

    public void setTotalCertificado(BigDecimal totalCertificado) {
        this.totalCertificado = totalCertificado;
    }

    public BigDecimal getTotalRecibo() {
        return totalRecibo;
    }

    public void setTotalRecibo(BigDecimal totalRecibo) {
        this.totalRecibo = totalRecibo;
    }

    public BigDecimal getGastosInvestigacion() {
        return gastosInvestigacion;
    }

    public void setGastosInvestigacion(BigDecimal gastosInvestigacion) {
        this.gastosInvestigacion = gastosInvestigacion;
    }

    public BigDecimal getPrima() {
        return prima;
    }

    public void setPrima(BigDecimal prima) {
        this.prima = prima;
    }

    public BigDecimal getPrimaTarifa() {
        return primaTarifa;
    }

    public void setPrimaTarifa(BigDecimal primaTarifa) {
        this.primaTarifa = primaTarifa;
    }
}
