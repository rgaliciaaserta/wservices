/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class ConsultaArchivoBase64VO {
    
    private String id;
    
    private String searchSpec;
    
    private String activityId;
    
    private String activityComments;
    
    private String activityFileExt;
    
    private String activityFileName;
    
    private String attachmentCategory;
    
    private String attachmentType;
    
    private String status;
    
    private byte[] activityFileBuffer;
    
    private String operation;
    
    /*private String llave;
    
    private String password;
    
    private String vector;*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearchSpec() {
        return searchSpec;
    }

    public void setSearchSpec(String searchSpec) {
        this.searchSpec = searchSpec;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityComments() {
        return activityComments;
    }

    public void setActivityComments(String activityComments) {
        this.activityComments = activityComments;
    }

    public String getActivityFileExt() {
        return activityFileExt;
    }

    public void setActivityFileExt(String activityFileExt) {
        this.activityFileExt = activityFileExt;
    }

    public String getActivityFileName() {
        return activityFileName;
    }

    public void setActivityFileName(String activityFileName) {
        this.activityFileName = activityFileName;
    }

    public String getAttachmentCategory() {
        return attachmentCategory;
    }

    public void setAttachmentCategory(String attachmentCategory) {
        this.attachmentCategory = attachmentCategory;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public byte[] getActivityFileBuffer() {
        return activityFileBuffer;
    }

    public void setActivityFileBuffer(byte[] activityFileBuffer) {
        this.activityFileBuffer = activityFileBuffer;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    /*public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVector() {
        return vector;
    }

    public void setVector(String vector) {
        this.vector = vector;
    }*/
}
