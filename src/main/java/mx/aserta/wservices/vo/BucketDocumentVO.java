/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class BucketDocumentVO {
    
    private String croppedBackID;

    public String getCroppedBackID() {
        return croppedBackID;
    }

    public void setCroppedBackID(String croppedBackID) {
        this.croppedBackID = croppedBackID;
    }
}
