/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoSinistroBiometriaVO {
    
    private long identificador;
    
    private String urlBiometria;

    public long getIdentificador() {
        return identificador;
    }

    public void setIdentificador(long identificador) {
        this.identificador = identificador;
    }

    public String getUrlBiometria() {
        return urlBiometria;
    }

    public void setUrlBiometria(String urlBiometria) {
        this.urlBiometria = urlBiometria;
    }
}
