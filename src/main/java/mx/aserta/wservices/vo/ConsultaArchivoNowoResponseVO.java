/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class ConsultaArchivoNowoResponseVO {
    
    private String errorCode;
    
    private String errorMessage;
    
    private String exito;
    
    private String mensaje;
    
    private List<ConsultaArchivoBase64VO> archivos;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ConsultaArchivoBase64VO> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<ConsultaArchivoBase64VO> archivos) {
        this.archivos = archivos;
    }
}
