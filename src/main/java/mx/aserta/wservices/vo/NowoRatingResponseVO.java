/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoRatingResponseVO {
    
    private String codigo;
    
    private String mensaje;
    
    private Double defaultScore;
    
    private Integer block;
    
    private Integer category;
    
    private String decision;
    
    private Double ingresos;
    
    private Double capacidadAhorro;
    
    private Double capacidadAlquiler;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Double getDefaultScore() {
        return defaultScore;
    }

    public void setDefaultScore(Double defaultScore) {
        this.defaultScore = defaultScore;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public Double getIngresos() {
        return ingresos;
    }

    public void setIngresos(Double ingresos) {
        this.ingresos = ingresos;
    }

    public Double getCapacidadAhorro() {
        return capacidadAhorro;
    }

    public void setCapacidadAhorro(Double capacidadAhorro) {
        this.capacidadAhorro = capacidadAhorro;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Double getCapacidadAlquiler() {
        return capacidadAlquiler;
    }

    public void setCapacidadAlquiler(Double capacidadAlquiler) {
        this.capacidadAlquiler = capacidadAlquiler;
    }
}
