/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rgalicia
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ArrendamientoVO {
    
    @XmlElement(name = "montoRenta", required = true, nillable = false)
    private BigDecimal montoRenta;
    
    @XmlElement(name = "esquema", required = true, nillable = false)
    private String esquema;
    
    @XmlElement(name = "bucket", required = true, nillable = false)
    private String bucket;
    
    @XmlElement(name = "meses", required = false, nillable = true)
    private Integer meses;

    public BigDecimal getMontoRenta() {
        return montoRenta;
    }

    public void setMontoRenta(BigDecimal montoRenta) {
        this.montoRenta = montoRenta;
    }

    public String getEsquema() {
        return esquema;
    }

    public void setEsquema(String esquema) {
        this.esquema = esquema;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }
}
