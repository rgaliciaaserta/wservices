/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

/**
 *
 * @author rgalicia
 */
public class NowoEmisionResponseVO {
    
    private String exito;
    
    private String mensaje;
    
    private String folioFianza;
    
    private String folioRecibo;
    
    private String idMovimiento;
    
    private String idPoliza;
    
    private String idRecibo;
    
    private String numPoliza;
    
    private String referenciaCaja;
    
    private String referenciaCliente;
    
    private String serieFianza;
    
    private String serieRecibo;
    
    private String tipoMovimiento;

    public String getExito() {
        return exito;
    }

    public void setExito(String exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFolioFianza() {
        return folioFianza;
    }

    public void setFolioFianza(String folioFianza) {
        this.folioFianza = folioFianza;
    }

    public String getFolioRecibo() {
        return folioRecibo;
    }

    public void setFolioRecibo(String folioRecibo) {
        this.folioRecibo = folioRecibo;
    }

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(String idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getIdPoliza() {
        return idPoliza;
    }

    public void setIdPoliza(String idPoliza) {
        this.idPoliza = idPoliza;
    }

    public String getIdRecibo() {
        return idRecibo;
    }

    public void setIdRecibo(String idRecibo) {
        this.idRecibo = idRecibo;
    }

    public String getNumPoliza() {
        return numPoliza;
    }

    public void setNumPoliza(String numPoliza) {
        this.numPoliza = numPoliza;
    }

    public String getReferenciaCaja() {
        return referenciaCaja;
    }

    public void setReferenciaCaja(String referenciaCaja) {
        this.referenciaCaja = referenciaCaja;
    }

    public String getReferenciaCliente() {
        return referenciaCliente;
    }

    public void setReferenciaCliente(String referenciaCliente) {
        this.referenciaCliente = referenciaCliente;
    }

    public String getSerieFianza() {
        return serieFianza;
    }

    public void setSerieFianza(String serieFianza) {
        this.serieFianza = serieFianza;
    }

    public String getSerieRecibo() {
        return serieRecibo;
    }

    public void setSerieRecibo(String serieRecibo) {
        this.serieRecibo = serieRecibo;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }
}
