/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.aserta.wservices.vo;

import java.util.List;

/**
 *
 * @author rgalicia
 */
public class NowoInterveningPartieVO {
    
    private String documentNumber;
    
    private String fullName;
    
    private String birthday;
    
    private String fullAddress;
    
    private String town;
    
    private String zipCode;
    
    private String email;
    
    private String intervenerType;
    
    private List<String> phones;
    
    private List<String> relations;
    
    private List<NowoVariablesVO> variables;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIntervenerType() {
        return intervenerType;
    }

    public void setIntervenerType(String intervenerType) {
        this.intervenerType = intervenerType;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public List<String> getRelations() {
        return relations;
    }

    public void setRelations(List<String> relations) {
        this.relations = relations;
    }

    public List<NowoVariablesVO> getVariables() {
        return variables;
    }

    public void setVariables(List<NowoVariablesVO> variables) {
        this.variables = variables;
    }
}
